-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2019 at 09:56 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`nama` VARCHAR(100), `email` TEXT, `nip` VARCHAR(25), `jabatan` VARCHAR(50), `id_bidang` VARCHAR(12), `lv` INT(2), `pass` VARCHAR(64), `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(14) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, '0', lv, email, pass, '0', nama, nip, jabatan, id_bidang, '0', '0000-00-00 00:00:00', admin_del, time_update);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_antrian` (`nik` VARCHAR(16), `id_layanan_menu` INT, `id_jenis_kesehatan` VARCHAR(8), `id_rs` VARCHAR(15), `id_poli` VARCHAR(11), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from kesehatan_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from kesehatan_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AC",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("AC",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into kesehatan_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis_kesehatan, id_rs, id_poli, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_dinas` (`nama_dinas` TEXT, `alamat` TEXT, `page_kelola` TEXT, `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(8) CHARSET latin1 BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from dinas;
        
  select id_dinas into last_key_item from dinas order by id_dinas desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("DN","200001");
  else
    set fix_key_item = concat("DN",RIGHT(last_key_item,6)+1);
      
  END IF;
  
  insert into dinas values(fix_key_item, nama_dinas, alamat, page_kelola, "0", "0000-00-00 00:00:00", admin_del, time_update);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_faskes_jenis` (`ket_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from faskes_jenis;
        
  select id_jenis into last_key_user from faskes_jenis order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("FSKS","100001");
  else
    set fix_key_user = concat("FSKS",RIGHT(last_key_user,6)+1);  
  END IF;
  
  INSERT INTO faskes_jenis VALUES(fix_key_user, ket_jenis, "", "", waktu, "0", id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_faskes_main` (`id_jenis` VARCHAR(10), `nama_faskes` TEXT, `foto_faskes` VARCHAR(70), `lokasi` VARCHAR(64), `detail_faskes` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(18) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(18);
  
  select count(*) into count_row_user from faskes_main where substr(id_faskes,5,8) = left(NOW()+0, 8);
        
  select id_faskes into last_key_user from faskes_main where substr(id_faskes,5,8) = left(NOW()+0, 8) order by id_faskes desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("FSKS", left(NOW()+0, 8)+0, "100001");
  else
    set fix_key_user = concat("FSKS", left(NOW()+0, 8)+0, RIGHT(last_key_user,6)+1);  
  END IF;
  
  INSERT INTO faskes_main VALUES(fix_key_user, id_jenis, nama_faskes, foto_faskes, lokasi, detail_faskes, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_antrian` (`nik` VARCHAR(16), `id_layanan_menu` VARCHAR(9), `id_jenis` VARCHAR(8), `id_kategori` VARCHAR(10), `id_sub` VARCHAR(10), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from ijin_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from ijin_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("IJ",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("IJ",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into ijin_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis, id_kategori, id_sub, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_jenis` (`ket_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_jenis;
        
  select id_jenis into last_key_user from ijin_jenis order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJJ","10001");
  else
    set fix_key_user = concat("IJJ",RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_jenis VALUES(fix_key_user, ket_jenis, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_kategori` (`id_jenis` VARCHAR(8), `ket_kategori` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_kategori WHERE substr(id_kategori, 4, 2) = RIGHT(id_jenis, 2);
        
  select id_kategori into last_key_user from ijin_kategori WHERE substr(id_kategori, 4, 2) = RIGHT(id_jenis, 2) order by id_kategori desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJK",RIGHT(id_jenis,2),"10001");
  else
    set fix_key_user = concat("IJK",RIGHT(id_jenis,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_kategori VALUES(fix_key_user, id_jenis, ket_kategori, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_sub` (`id_kategori` VARCHAR(12), `ket_sub` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(12) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_sub_kategori where substr(id_sub, 4, 2) = RIGHT(id_kategori, 2);
        
  select id_sub into last_key_user from ijin_sub_kategori where substr(id_sub, 4, 2) = RIGHT(id_kategori, 2) order by id_sub desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJS",RIGHT(id_kategori,2),"10001");
  else
    set fix_key_user = concat("IJS",RIGHT(id_kategori,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_sub_kategori VALUES(fix_key_user, id_kategori, ket_sub, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_jenis_rs` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_jenis;
        
  select id_layanan into last_key_item from kesehatan_jenis order by id_layanan desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KSJ","10001");
  else
    set fix_key_item = concat("KSJ",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_jenis values(fix_key_item, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kecamatan` (`nama` VARCHAR(50), `area` TEXT, `luas` DOUBLE, `id_admin` VARCHAR(12), `waktu` DATETIME) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from master_kecamatan;
        
  select id_kecamatan into last_key_user from master_kecamatan order by id_kecamatan desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEC1","001");
  else
    set fix_key_user = concat("KEC",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO master_kecamatan VALUES(fix_key_user, nama, area, luas, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kelurahan` (`nama` VARCHAR(64), `area` TEXT, `luas` DOUBLE, `id_kec` VARCHAR(7), `id_admin` VARCHAR(12), `waktu` INT) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from master_kelurahan;
        
  select id_kelurahan into last_key_user from master_kelurahan order by id_kelurahan desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEL1","001");
  else
    set fix_key_user = concat("KEL",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO master_kelurahan VALUES(fix_key_user, id_kec, nama, area, luas, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_antrian` (`nik` VARCHAR(16), `id_layanan_menu` INT, `id_jenis` VARCHAR(8), `id_kategori` VARCHAR(8), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from kependudukan_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from kependudukan_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("PD",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("PD",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into kependudukan_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis, id_kategori, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_jenis` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(8) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kependudukan_jenis;
        
  select id_jenis into last_key_item from kependudukan_jenis order by id_jenis desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KPJ","10001");
  else
    set fix_key_item = concat("KPJ",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kependudukan_jenis values(fix_key_item, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_kategori` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12), `id_jenis` VARCHAR(8)) RETURNS VARCHAR(8) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kependudukan_kategori;
        
  select id_kategori into last_key_item from kependudukan_kategori order by id_kategori desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KPC","10001");
  else
    set fix_key_item = concat("KPC",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kependudukan_kategori values(fix_key_item, id_jenis, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_page_main` (`id_kategori` INT(11), `nama_page` VARCHAR(100), `next_page` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from home_page_main;
        
  select id_page into last_key_item from home_page_main order by id_page desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("MENU","10001");
  else
    set fix_key_item = concat("MENU",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into home_page_main values(fix_key_item, id_kategori, nama_page, "-", next_page, "0", waktu, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_jenis` (`id_strata` VARCHAR(8), `nama_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from pendidikan_jenis WHERE substr(id_jenis, 4, 2) = RIGHT(id_strata,2);
        
  select id_jenis into last_key_user from pendidikan_jenis WHERE substr(id_jenis, 4, 2) = RIGHT(id_strata,2) order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("PDJ",RIGHT(id_strata,2),"10001");
  else
    set fix_key_user = concat("PDJ",RIGHT(id_strata,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO pendidikan_jenis VALUES(fix_key_user, id_strata, nama_jenis, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_sekolah` (`id_jenis` VARCHAR(10), `id_kecamatan` VARCHAR(8), `id_kelurahan` VARCHAR(8), `nama_sekolah` TEXT, `lokasi` TEXT, `detail_sekolah` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(17) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(17);
  
  select count(*) into count_row_user from pendidikan_sekolah where substr(id_sekolah,4,8) = left(NOW()+0, 8);
        
  select id_sekolah into last_key_user from pendidikan_sekolah where substr(id_sekolah,4,8) = left(NOW()+0, 8) order by id_sekolah desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("SKL", left(NOW()+0, 8)+0, "100001");
  else
    set fix_key_user = concat("SKL", left(NOW()+0, 8)+0, RIGHT(last_key_user,6)+1);  
  END IF;
  
  INSERT INTO pendidikan_sekolah VALUES(fix_key_user, id_jenis, id_kecamatan, id_kelurahan, nama_sekolah, "", lokasi, detail_sekolah, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_strata` (`nama` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(8) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from pendidikan_strata;
        
  select id_strata into last_key_user from pendidikan_strata order by id_strata desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("PDST1","001");
  else
    set fix_key_user = concat("PDST",SUBSTR(last_key_user,5,4)+1);  
  END IF;
  
  INSERT INTO pendidikan_strata VALUES(fix_key_user, nama, "", "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_poli` (`nama` VARCHAR(30), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_poli;
        
  select id_poli into last_key_item from kesehatan_poli order by id_poli desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("P","10001");
  else
    set fix_key_item = concat("P",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_poli values(fix_key_item, nama, sha2(fix_key_item, '256'), "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rs` (`nama_rs` TEXT, `alamat` TEXT, `foto` VARCHAR(70), `telephone` VARCHAR(13), `id_layanan` VARCHAR(8), `id_poli` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(15) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(15);
  
  select count(*) into count_row_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8);
        
  select id_rs into last_key_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8) order by id_rs desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("RS", left(NOW()+0, 8), "10001");
  else
    set fix_key_item = concat("RS",substr(last_key_item,3,13)+1);
      
  END IF;
  
  insert into kesehatan_rs values(fix_key_item, nama_rs, alamat, foto, telephone, id_layanan, id_poli, "0", waktu, id_admin);
  
  return fix_key_item;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `del` enum('0','1') NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_bidang` varchar(8) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `del`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_bidang`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('AD2019020001', '0', 1, 'kominfo_super@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Suwardi Surya Ningrat', '19091999012130', 'staff bidang penelitian', 'DN200002', '0', '2019-03-13 08:22:29', '0', '2019-02-21 00:00:00'),
('AD2019030001', '0', 1, 'dinkes@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Sukiman Sumajono', '123456789011', 'Pengamat lapangan', 'DN200002', '0', '2019-03-13 08:22:20', 'AD2019020001', '2019-03-13 08:17:16'),
('AD2019040001', '0', 1, 'meilinda_ika@yahoo.com', 'cd6fdc4a0ba635db34d809ce66b01fe8f53129d8db083c2550c1a562402fda2f', '0', 'Mei', '5555555555555555', 'tpok', 'DN200002', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-04-15 10:49:01');

-- --------------------------------------------------------

--
-- Table structure for table `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'admin super', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `id_device_kios` varchar(7) NOT NULL,
  `ip_lan` varchar(16) NOT NULL,
  `ip_public` varchar(16) NOT NULL,
  `key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id_device_kios`, `ip_lan`, `ip_public`, `key`) VALUES
('1', '192.168.21.26', '36.74.196.6', 'myname_surya_hanggara'),
('2', '192.168.86.250', '222.124.213.68', 'aku_ini_sehat');

-- --------------------------------------------------------

--
-- Table structure for table `dinas`
--

CREATE TABLE `dinas` (
  `id_dinas` varchar(8) NOT NULL,
  `nama_dinas` text NOT NULL,
  `alamat` text NOT NULL,
  `page_kelola` text NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dinas`
--

INSERT INTO `dinas` (`id_dinas`, `nama_dinas`, `alamat`, `page_kelola`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('DN200001', 'Dinas Perdagangan', 'Jl. Simp. Terusan Danau Sentani 3 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200002', 'Dinas Komunikasi dan Informatika (DISKOMINFO)', 'Perkantoran Terpadu Gedung A Lt. 4 Malang, Jl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200003', 'Dinas Kebudayaan dan Pariwisata (DISBUDPAR)', 'Museum Mpu Purwa, Jl. Sukarno Hatta B. 210 Malang 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200004', 'Dinas Perindustrian (DISPERIN)', 'Perkantoran Terpadu Gedung A Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200005', 'Dinas Pendidikan (DISDIK)', 'Jl. Veteran 19 Malang 65145', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200006', 'Dinas Kesehatan (DINKES)', 'Jl. Simp. Laksda Adisucipto 45 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200007', 'Dinas Perhubungan (DISHUB)', 'Jl. Raden Intan 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200008', 'Dinas Pertanian dan Ketahanan Pangan', 'Jl. A. Yani Utara 202 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200009', 'Dinas Perumahan dan Kawasan Pemukiman (DISPERKIM)', 'Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200010', 'Dinas Koperasi dan Usaha Mikro', 'Jl. Panji Suroso 18 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200011', 'Dinas Kepemudaan dan Olahraga (DISPORA)', 'Jl. Tenes Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200012', 'Dinas Kepedudukan dan Pencatatan Sipil (DISPENDUKCAPIL)', 'Perkantoran Terpadu Gedung A Lt. 2, Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200013', 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)', 'Jl. Ki Ageng Gribig Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200014', 'Dinas Lingkungan Hidup (DLH)', 'Jl. Mojopahit Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200015', 'Dinas Perpustakaan Umum dan Arsip Daerah', 'Jl. Besar Ijen No.30a Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200016', 'Badan Kesatuan Bangsa dan Politik (BAKESBANGPOL)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200017', 'Badan Kepegawaian Daerah', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200018', 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)', 'Perkantoran Terpadu Gedung A Lt. 2 Malang,\r\nJl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200019', 'Dinas Tenaga Kerja (DISNAKER)', 'Perkantoran Terpadu Gedung B Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200020', 'Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)', ' Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200021', 'Dinas Sosial (DINSOS)', 'Jl. Sulfat No. 12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200022', 'Inspektorat', 'Jl. Gajah Mada No. 2A Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200023', 'Badan Perencanaan, Penelitian dan Pengembangan (Barenlitbang)', 'Jl. Tugu No. 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200024', 'Badan Pelayanan Pajak Daerah (BP2D)', 'Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200025', 'Badan Pengelola Keuangan dan Aset Daerah (BPKAD)', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200026', 'Badan Penanggulangan Bencana Daerah (BPBD)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200027', 'Satuan Polisi Pamong Praja', 'Jl. Simpang Mojopahit No 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200028', 'Kecamatan Klojen', 'Jl. Surabaya 6 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200029', 'Kecamatan Blimbing', 'Jl. Raden Intan Kav. 14 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200030', 'Kecamatan Lowokwaru', 'Jl. Cengger Ayam I/12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200031', 'Kecamatan Sukun', 'Jl. Keben I Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200032', 'Kecamatan Kedungkandang', 'Jl. Mayjen Sungkono 59 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200033', 'Badan Pengawas Pemilihan Umum (BAWASLU)', 'Jl. Teluk Cendrawasih No.206, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200034', 'Dewan Perwakilan Rakyat Daerah Kota Malang (DPRD)', 'Jl. Tugu No.1A, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200035', 'Rumah Potong Hewan Kota Malang', 'Jl. Kol. Sugiono No. 176, Malang, Jawa Timur 65148', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200036', 'PDAM Kota Malang ', 'Jl. Danau Sentani Raya No.100, Madyopuro, Kedungkandang, Kota Malang, Jawa Timur 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200037', 'Komisi Pemilihan Umum Kota Malang (KPU) ', 'Jl. Bantaran No.6, Purwantoro, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200038', 'Bank Perkreditan Rakyat', 'JL. Borobudur no. 18', '', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-28 09:48:41'),
('50', 'dinas perbuatan tidak menyenangkan', 'Malang', '', '1', '2019-02-27 03:35:41', 'AD2019020001', '2019-02-27 03:35:24'),
('52', 'surya', 'surya', '', '1', '2019-03-04 02:58:07', 'AD2019020001', '2019-03-01 03:57:29'),
('', 'admin', 'malang', '{}', '0', '0000-00-00 00:00:00', '0', '2019-03-13 10:03:22'),
('DN200039', 'test surya', 'malang', 'no', '0', '0000-00-00 00:00:00', '123', '0000-00-00 00:00:00'),
('DN200040', 'test_surya1', 'malang', '@p2', '0', '0000-00-00 00:00:00', '@p3', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faskes_jenis`
--

CREATE TABLE `faskes_jenis` (
  `id_jenis` varchar(10) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL,
  `icon_32` text NOT NULL,
  `icon_64` text NOT NULL,
  `waktu` datetime NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faskes_jenis`
--

INSERT INTO `faskes_jenis` (`id_jenis`, `nama_jenis`, `icon_32`, `icon_64`, `waktu`, `is_delete`, `id_admin`) VALUES
('FSKS100001', 'Apotik', '6be374069041ea3fb6b9f2509321d5bdf4a123f120991fbc3fc3ff9f1b32208e_32.jpg', '6be374069041ea3fb6b9f2509321d5bdf4a123f120991fbc3fc3ff9f1b32208e_64.jpg', '2019-05-14 06:08:24', '0', 'AD2019020001'),
('FSKS100002', 'Laboratorium', 'c24579fc05ffe0e7318adce9b86bb986d89a398f36772cb4f1916eb727ffec90_32.jpg', 'c24579fc05ffe0e7318adce9b86bb986d89a398f36772cb4f1916eb727ffec90_64.jpg', '2019-05-14 06:08:05', '0', '0'),
('FSKS100003', 'Optik', '2899dcb1416a59041a5a337b84901a2a613e08cd71f5cb47939dd773e80a2c90_32.jpg', '2899dcb1416a59041a5a337b84901a2a613e08cd71f5cb47939dd773e80a2c90_64.jpg', '2019-05-14 06:06:39', '0', '0'),
('FSKS100004', 'Dokter Gigi', 'f5a9cee1802979ad9494ebd2555dc5021d1ee9d29de1615505ede4635d4aa379_32.jpg', 'f5a9cee1802979ad9494ebd2555dc5021d1ee9d29de1615505ede4635d4aa379_64.jpg', '2019-05-14 06:10:42', '0', '0'),
('FSKS100005', 'Dokter Gigi', '', '', '2019-05-14 06:10:26', '1', '0'),
('FSKS100006', 'Rumah Sakit', '28b37a4c9364981692a47b7d7c8e9a3e76f130334a1872d6c8bbeccae43c7147_32.jpg', '28b37a4c9364981692a47b7d7c8e9a3e76f130334a1872d6c8bbeccae43c7147_64.jpg', '2019-05-14 06:11:14', '0', '0'),
('FSKS100007', 'Puskesmas', '68c98b2321e10f1727c229a67315edc01456836c0342a002cadbc046ee730845_32.jpg', '68c98b2321e10f1727c229a67315edc01456836c0342a002cadbc046ee730845_64.jpg', '2019-05-14 06:11:31', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `faskes_main`
--

CREATE TABLE `faskes_main` (
  `id_faskes` varchar(18) NOT NULL,
  `id_jenis` varchar(10) NOT NULL,
  `nama_faskes` text NOT NULL,
  `foto_faskes` varchar(70) NOT NULL,
  `lokasi` varchar(64) NOT NULL,
  `detail_faskes` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faskes_main`
--

INSERT INTO `faskes_main` (`id_faskes`, `id_jenis`, `nama_faskes`, `foto_faskes`, `lokasi`, `detail_faskes`, `is_delete`, `waktu`, `id_admin`) VALUES
('FSKS20190514100001', 'FSKS100001', 'apotik pandanwangi malang', '5a91865c206a45b6dbec0d47e4bf87ebf3efc584185dcf80819527deb91b4f7c.jpg', '[\'-7.970732\', \'112.632759\']', '{\'alamat\': \'malang\',\'url\': \'https://www.youtube.com/watch?v=cL78cHRPptc\',\'tlp\': \'081230695447\'}', '0', '2019-05-14 08:56:30', '');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_kategori`
--

CREATE TABLE `home_page_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_page_kategori`
--

INSERT INTO `home_page_kategori` (`id_kategori`, `nama_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
(1, 'Daftar Antrian', '0', '2019-04-02 09:07:12', 'AD2019020001'),
(2, 'Pelayanan Kesehatan', '0', '2019-04-02 09:08:29', 'AD2019020001'),
(3, 'Pelayanan Pendidikan', '0', '2019-04-02 09:08:44', 'AD2019020001'),
(4, 'Pariwisata dan Lingkungan Terbuka Hijau', '0', '2019-04-02 09:09:22', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_main`
--

CREATE TABLE `home_page_main` (
  `id_page` varchar(9) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_page` text NOT NULL,
  `foto_page` varchar(70) NOT NULL,
  `next_page` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_page_main`
--

INSERT INTO `home_page_main` (`id_page`, `id_kategori`, `nama_page`, `foto_page`, `next_page`, `is_delete`, `waktu`, `id_admin`) VALUES
('MENU10001', 1, 'Kesehatan', 'ed88bc4c866227e94d8eda9d75e84e1a710dbad4313b38a21e2f98da393f8c45.jpg', 'beranda/kesehatan/jenis_rumah_sakit', '0', '2019-04-23 05:50:07', 'AD2019020001'),
('MENU10002', 1, 'Kependudukan', 'cc2cd863e992fa37b539a9b9f0d694e1372913711bf38eaedcf6944d80853f6b.jpg', 'beranda/kependudukan/jenis', '0', '2019-05-03 04:34:18', 'AD2019020001'),
('MENU10003', 1, 'Perijinan', 'e429082bcf2cf1a17177355ac172db5ddeb8f12a2c11bcd91a71d4d988dde9b7.jpg', 'beranda/perijinan/jenis', '0', '2019-05-03 09:31:49', 'AD2019020001'),
('MENU10004', 3, 'Daftar Sekolah Menengah Pertama', '72899716adeffb9cf601d31228768070cd754dced9d930c7725a539ad04821df.jpg', 'beranda/pendidikan/a723ff721d91d9f34309debc39b7082f69c6b9990eb1c4aff93361aeec412bcb', '0', '2019-05-06 09:03:22', 'AD2019020001'),
('MENU10005', 3, 'Daftar Sekolah Dasar', 'b2c92547857a6d646e1353592b56ddfa12c8bd41e573b7d31fa0977030b8eac0.jpg', 'beranda/pendidikan/7fd7db09359f9723b6ba6e98be7a685ceae45115d77f5d7026c167a3f6e557b9', '0', '2019-05-06 09:03:39', 'AD2019020001'),
('MENU10006', 3, 'Daftar Sekolah Menegah Atas dan Kejuruan', 'bb0e73ca3117728c47a3f4857a6d193da4a70ce1e80da5cd0ebd2aa29931b0a9.jpg', 'beranda/pendidikan/7110c3557c18ae66396a476e0213dca86d950a532e6a1623b0b698da8b9e0fbf', '0', '2019-05-13 05:14:21', 'AD2019020001'),
('MENU10007', 3, 'Pondok Pesantren', '8c0de8bb3e203c5611262e9e33814f8632a864a1889c43c75df0e184355e8b30.jpg', 'localhost', '0', '2019-04-23 06:00:16', 'AD2019020001'),
('MENU10008', 3, 'Universitas, Politehnik, Sekolah Tinggi dan Akademi', '44e81762f202cc44c6cf991597904802907e8e01a11ebef94d460f062575e06d.jpg', 'localhost', '0', '2019-04-23 08:22:16', 'AD2019020001'),
('MENU10009', 3, 'Perpustakaan', '6ba4c8b0fcea36da1c18274363b1be1bcf76dbaf90b81bb1f84a47a6ef81311a.jpg', 'local', '0', '2019-04-23 06:01:42', 'AD2019020001'),
('MENU10010', 2, 'Daftar Puskesmas', '03cab6c4801747cd8e34811cd74ebaf32e85422122f23ebf5cb41047cdeb224e.jpg', 'local', '0', '2019-04-23 06:04:07', 'AD2019020001'),
('MENU10011', 2, 'Daftar Klinik', '9f4c5c0c6117a92e225b76e9c5b315e3dd21429eaa187c0652596e7108fed168.jpg', 'local', '0', '2019-04-23 06:04:30', 'AD2019020001'),
('MENU10012', 2, 'Daftar Rumah Sakit', '973f1680e3b39cea60ce18e29f3e93bee61fcbd6ae193ffa3e4586f0520dd17c.jpg', 'local', '0', '2019-04-23 06:05:04', 'AD2019020001'),
('MENU10013', 2, 'Apotik', 'fb3c2af21f1b5021f459269e892f5edcc716873cc46ef8ade17db2e30d66520a.jpg', 'local', '0', '2019-04-23 06:09:32', 'AD2019020001'),
('MENU10014', 2, 'Dokter Praktek', '84d0e058a241a28ee081e938d912a4ca859cb6553a151fd67820b5b933042748.jpg', 'local', '0', '2019-04-23 06:08:10', 'AD2019020001'),
('MENU10015', 2, 'Dokter Gigi', '1f95613edfef290f100164ed291c24be3b96e697f120fc53b6a39d6b03ec8b3e.jpg', 'local', '0', '2019-04-23 06:08:39', 'AD2019020001'),
('MENU10016', 2, 'Optik', '489b6471fed740d5057001bfea73e4038ea9b27278f440b19774ff9c6352ef9a.jpg', 'local', '0', '2019-04-23 06:09:18', 'AD2019020001'),
('MENU10017', 2, 'Laboratorium', '786b8c352d1a0fc2c9cbd301c13d707ef5f4cdb690cd90827e1355d63d65afd3.jpg', 'local', '0', '2019-04-23 06:10:09', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_antrian`
--

CREATE TABLE `ijin_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(9) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `id_sub` varchar(10) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_antrian`
--

INSERT INTO `ijin_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis`, `id_kategori`, `id_sub`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('IJ20190506100000001', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '', '', '0', '0000-00-00 00:00:00', '0'),
('IJ20190506100000002', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '', '', '0', '0000-00-00 00:00:00', '0'),
('IJ20190506100000003', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0', '0', '', '', '0', '0000-00-00 00:00:00', '0'),
('IJ20190506100000004', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', '', '2019-05-06 05:15:15', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:15:15', '0'),
('IJ20190506100000005', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:16:03', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:16:03', '0'),
('IJ20190506100000006', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:17:21', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:17:21', '0'),
('IJ20190506100000007', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:17:46', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:17:46', '0'),
('IJ20190506100000008', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:18:54', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:18:54', '0'),
('IJ20190506100000009', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-06 05:22:09', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:22:09', '0'),
('IJ20190506100000010', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110002', 'IJS0210002', '2019-05-06 05:26:08', '2019-05-08 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:26:08', '0'),
('IJ20190520100000001', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-20 05:26:11', '2019-05-21 00:00:00', 'A-002', '0', '', '', '0', '2019-05-20 05:26:11', '0'),
('IJ20190520100000002', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-05-20 05:29:38', '2019-05-21 00:00:00', 'A-002', '0', '', '', '0', '2019-05-20 05:29:38', '0');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_jenis`
--

CREATE TABLE `ijin_jenis` (
  `id_jenis` varchar(8) NOT NULL,
  `ket_jenis` text NOT NULL,
  `foto_jenis` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_jenis`
--

INSERT INTO `ijin_jenis` (`id_jenis`, `ket_jenis`, `foto_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('IJJ10001', 'usaha pariwisata', 'b189b2f570a3504e71d1b204a1f88f93d2f823f9c4e24373dec77c0bbfd36e6c.jpg', '0', '2019-04-30 07:47:10', 'AD2019020001'),
('IJJ10002', 'usaha makanan', '4371f391ef11c3f8ade3b53b6720a5ca7b2e8ff81985c2cb83fc51f1e8d0c79a.jpg', '0', '2019-04-30 07:48:42', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_kategori`
--

CREATE TABLE `ijin_kategori` (
  `id_kategori` varchar(10) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `ket_kategori` text NOT NULL,
  `foto_kategori` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_kategori`
--

INSERT INTO `ijin_kategori` (`id_kategori`, `id_jenis`, `ket_kategori`, `foto_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
('IJK0110001', 'IJJ10001', 'ijin keramainan umum', 'cc8069250df5515236f666f0070a5de9cfb5b6ad7f5223a5b631cb52b22585ea.jpg', '0', '2019-04-30 08:29:27', 'AD2019020001'),
('IJK0110002', 'IJJ10001', 'ijin penggunaan tanah atau makam', '02fa195b7e83d3edd5ee8adfdb24327c29b70d4c1b77246c6abc5a89e0eb4d36.jpg', '0', '2019-04-30 08:29:35', 'AD2019020001'),
('IJK0110003', 'IJJ10001', 'ijin sewa', 'c10b1999e0e3d079789db43b05e027b5742eaaa0e2822584d1612e6e93b3adbd.jpg', '0', '2019-04-30 08:29:42', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_sub_kategori`
--

CREATE TABLE `ijin_sub_kategori` (
  `id_sub` varchar(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `ket_sub` text NOT NULL,
  `foto_sub` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_sub_kategori`
--

INSERT INTO `ijin_sub_kategori` (`id_sub`, `id_kategori`, `ket_sub`, `foto_sub`, `is_delete`, `time_update`, `id_admin`) VALUES
('IJS0110001', 'IJK0110001', 'Memperpanjang', '39430fb6f0f37571c6674ed5947604f20e197c88f0a424a17a76ee8bbdd8b8d6.jpg', '0', '2019-05-02 10:33:21', 'AD2019020001'),
('IJS0110002', 'IJK0110001', 'ok', 'ec166c7bd6f59683052ac9ee83d9fac34d464dada02350accb679ef7a14975fd.jpg', '0', '2019-05-02 10:36:58', 'AD2019020001'),
('IJS0210001', 'IJK0110002', 'Membuat Baru', '49ca361f5fa014e9a6888f93bb629b7b75e47ee8e8817362c493af03ece1c95f.jpg', '0', '2019-05-02 10:34:55', 'AD2019020001'),
('IJS0210002', 'IJK0110002', 'Memperpanjang', 'd1368b1f694c97c2e2fbe560c8423a29969a1be45fb02482db28452cb646cb40.jpg', '0', '2019-05-02 10:35:15', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_antrian`
--

CREATE TABLE `kependudukan_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(1) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `id_kategori` varchar(8) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_antrian`
--

INSERT INTO `kependudukan_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis`, `id_kategori`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('PD20190503100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:02:03', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:02:03', '0'),
('PD20190503100000002', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:02:53', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:02:53', '0'),
('PD20190503100000003', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:04:09', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:04:09', '0'),
('PD20190503100000004', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:04:49', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:04:49', '0'),
('PD20190503100000005', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:05:02', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:05:02', '0'),
('PD20190503100000006', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:05:35', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:05:35', '0'),
('PD20190503100000007', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:06:24', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:06:24', '0'),
('PD20190503100000008', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:07:13', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:07:13', '0'),
('PD20190503100000009', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:09:14', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:09:14', '0'),
('PD20190503100000010', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:10:46', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:10:46', '0'),
('PD20190503100000011', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:11:35', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:11:35', '0'),
('PD20190503100000012', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:12:05', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:12:05', '0'),
('PD20190503100000013', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:12:54', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:12:54', '0'),
('PD20190503100000014', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:13:21', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:13:21', '0'),
('PD20190503100000015', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:22:26', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:22:26', '0'),
('PD20190503100000016', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:22:55', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:22:55', '0'),
('PD20190503100000017', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:23:18', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:23:18', '0'),
('PD20190503100000018', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:25:29', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:25:29', '0'),
('PD20190503100000019', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:25:54', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:25:54', '0'),
('PD20190503100000020', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:31:44', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:31:44', '0'),
('PD20190506100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-06 05:25:22', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:25:22', '0');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_jenis`
--

CREATE TABLE `kependudukan_jenis` (
  `id_jenis` varchar(8) NOT NULL,
  `ket_jenis` text NOT NULL,
  `foto_jenis` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_jenis`
--

INSERT INTO `kependudukan_jenis` (`id_jenis`, `ket_jenis`, `foto_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('KPJ10001', 'Kematian', 'f5a947d14485004b9af99c6ae699cd36aa01d9429cd97ca9235f49c16a2e9bb5.jpg', '0', '2019-04-29 07:14:46', 'AD2019020001'),
('KPJ10002', 'Kematian', 'c1887f6856a874b80daf339597e748df1ddf7df9b35f32648b794257333990b0.jpg', '0', '2019-04-29 07:14:54', 'AD2019020001'),
('KPJ10003', 'Kematian', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.jpg', '0', '2019-04-29 07:12:18', 'AD2019020001'),
('KPJ10004', 'perceraian', 'a853833515f259419d39493197156c46634599ce8d65d88c2f65b634f5e890fb.jpg', '0', '2019-04-29 07:15:40', 'AD2019020001'),
('KPJ10005', 'pindah datang', '5adf937d088955a45e377cca1eeb3f22f6bf6a98ef0d6441914aeda712d97856.jpg', '0', '2019-04-29 07:15:54', 'AD2019020001'),
('KPJ10006', 'perpindahan', '6611484c096c23acfcb2a441887171f5b62668a4cd4510ebce01366bb23bfcd4.jpg', '0', '2019-04-29 07:16:20', 'AD2019020001'),
('KPJ10007', 'donal', 'bd0d411b5ed5d23e326df94f71723c27d1a2785a248aa30f54725f2115986454.jpg', '1', '2019-04-29 07:16:36', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_kategori`
--

CREATE TABLE `kependudukan_kategori` (
  `id_kategori` varchar(8) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `ket_kategori` text NOT NULL,
  `foto_kategori` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_kategori`
--

INSERT INTO `kependudukan_kategori` (`id_kategori`, `id_jenis`, `ket_kategori`, `foto_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
('KPC10001', 'KPJ10001', 'baru', '3079daa67b41e5fe307aece27914a28a01e1af6106679dd660f95cc32a59e2c3.jpg', '0', '2019-04-29 07:00:52', 'AD2019020001'),
('KPC10002', 'KPJ10001', 'perbaikan', '15fb79b3b82d6dc7783b356b91a6374b9e6a3dbed4e268e745c76b1c370916d2.jpg', '0', '2019-04-29 07:02:08', 'AD2019020001'),
('KPC10003', 'KPJ10002', 'baru', '37453faee4b7ccdbe84bb49b4faf19a2536f978ee4834583953b9360f529de6a.jpg', '0', '2019-04-29 07:02:21', 'AD2019020001'),
('KPC10004', 'KPJ10002', 'perbaikan', '416eb5cffdcc5b1d134be61c8928621ffbc28cc32d468b57f7beb390f7787bdf.jpg', '0', '2019-04-29 07:02:40', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_antrian`
--

CREATE TABLE `kesehatan_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(1) NOT NULL,
  `id_jenis_kesehatan` varchar(8) NOT NULL,
  `id_rs` varchar(15) NOT NULL,
  `id_poli` varchar(11) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_antrian`
--

INSERT INTO `kesehatan_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis_kesehatan`, `id_rs`, `id_poli`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('AC20190412100000001', '3573011903940006', '0', '1', 'RS2019032710003', 'P10001', '2019-04-12 05:02:17', '2019-04-12 00:00:00', 'A-002', '0', '', '', '0', '2019-04-12 05:02:17', ''),
('AC20190412100000002', '3573011903940006', '0', '1', 'RS2019032710003', 'P10001', '2019-04-12 05:02:57', '2019-04-15 00:00:00', 'A-002', '0', '', '', '0', '2019-04-12 05:02:57', ''),
('AC20190418100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:09:11', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:09:11', ''),
('AC20190418100000002', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:13:18', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:13:18', ''),
('AC20190418100000003', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:39:34', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:39:34', ''),
('AC20190418100000004', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:51:36', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:51:36', ''),
('AC20190418100000005', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:51:39', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:51:39', ''),
('AC20190418100000006', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:54:52', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:54:52', ''),
('AC20190418100000007', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:07', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:07', ''),
('AC20190418100000008', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:16', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:16', ''),
('AC20190418100000009', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:23', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:23', ''),
('AC20190418100000010', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:33', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:33', ''),
('AC20190418100000011', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:55:50', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:55:50', ''),
('AC20190418100000012', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:56:11', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:56:11', ''),
('AC20190418100000013', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:56:33', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:56:33', ''),
('AC20190418100000014', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:57:57', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:57:57', ''),
('AC20190418100000015', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:58:16', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:58:16', ''),
('AC20190418100000016', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:58:30', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:58:30', ''),
('AC20190418100000017', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 09:58:43', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 09:58:43', ''),
('AC20190418100000018', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:00:58', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:00:58', ''),
('AC20190418100000019', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:01:08', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:01:08', ''),
('AC20190418100000020', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:06:15', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:06:15', ''),
('AC20190418100000021', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:06:40', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:06:40', ''),
('AC20190418100000022', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:09:28', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:09:28', ''),
('AC20190418100000023', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:41:06', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:41:06', ''),
('AC20190418100000024', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:41:29', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:41:29', ''),
('AC20190418100000025', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-18 10:49:49', '2019-04-18 00:00:00', 'A-002', '0', '', '', '0', '2019-04-18 10:49:49', ''),
('AC20190422100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:17:13', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:17:13', ''),
('AC20190422100000002', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:19:37', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:19:37', ''),
('AC20190422100000003', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:21:38', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:21:38', ''),
('AC20190422100000004', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:22:34', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:22:34', ''),
('AC20190422100000005', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:23:41', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:23:41', ''),
('AC20190422100000006', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 08:38:53', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:38:53', ''),
('AC20190422100000007', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 08:39:46', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 08:39:46', ''),
('AC20190422100000008', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 09:24:47', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 09:24:47', ''),
('AC20190422100000009', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:18:32', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:18:32', ''),
('AC20190422100000010', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:18:32', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:18:32', ''),
('AC20190422100000011', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:24:16', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:24:16', ''),
('AC20190422100000012', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:28:50', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:28:50', ''),
('AC20190422100000013', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-04-22 10:32:44', '2019-04-22 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:32:44', ''),
('AC20190422100000014', '3573011903940006', '0', '1', 'RS2019032710001', 'P10001', '2019-04-22 10:33:23', '0000-00-00 00:00:00', 'A-002', '0', '', '', '0', '2019-04-22 10:33:23', ''),
('AC20190503100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-05-03 08:00:50', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:00:50', ''),
('AC20190503100000002', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-05-03 08:01:12', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:01:12', ''),
('AC20190506100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-05-06 05:24:27', '2019-05-07 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:24:27', ''),
('AC20190520100000001', '3573011903940006', '0', '1', 'RS2019032710002', 'P10001', '2019-05-20 04:51:56', '2019-05-20 00:00:00', 'A-002', '0', '', '', '0', '2019-05-20 04:51:56', '');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_jenis`
--

CREATE TABLE `kesehatan_jenis` (
  `id_layanan` varchar(8) NOT NULL,
  `nama_layanan` text NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_jenis`
--

INSERT INTO `kesehatan_jenis` (`id_layanan`, `nama_layanan`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('KSJ10001', 'rumah sakit umum', '41e216aa43a641cac7d611b84323e37dfd67043b28a6d0d50fc16fbd46b189b6.jpg', '0', '2019-03-25 08:53:55', 'AD2019020001'),
('KSJ10002', 'rumah sakit ibu dan anak', '1483bcd0544c8299806b216c188f730ef9e187c076d69c75248f2dce221f8f28.jpg', '0', '2019-03-25 08:51:17', 'AD2019020001'),
('KSJ10003', 'rumah sakit islam', 'b990492b26b993f77bab8fae398eb0909b2ac41e1c942c9e2d7786871ef3c4ce.jpg', '0', '2019-03-25 08:51:42', 'AD2019020001'),
('KSJ10004', 'rumah sakit bersalin', '4a607a6fb0c8c71ca148b9377b0912a4d5f5e6ed9ed6995439e236f7f4cc8d1d.jpg', '0', '2019-03-25 08:52:54', 'AD2019020001'),
('KSJ10005', 'klinik', '113cbc2d33df0f661807223a4533f00de21d8b7e6b2034bc787b26a6ac3dcc68.jpg', '0', '2019-03-25 08:53:06', 'AD2019020001'),
('KSJ10006', 'puskesmas', 'f52d513956d3b4bbbf1b198a74dd7b9fa8e1ef9e52dc0166e86455ecadad3bf1.jpg', '0', '2019-03-25 08:53:20', 'AD2019020001'),
('KSJ10007', 'test 1', 'c25f2a7fafaa546cf1da84ac792f033b9fb1c0deea1ac3d6340055b0405e629c.jpg', '1', '2019-03-25 08:57:04', 'AD2019020001'),
('KSJ10008', 'test 2', '048ef36684ed06d8c259d24a37ac70aefb0eae1dea84482c686cfea246b482c5.jpg', '1', '2019-03-25 08:57:00', 'AD2019020001'),
('KSJ10009', 'sadsad', '', '1', '2019-05-13 05:57:38', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_poli`
--

CREATE TABLE `kesehatan_poli` (
  `id_poli` varchar(11) NOT NULL,
  `nama_poli` varchar(30) NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_poli`
--

INSERT INTO `kesehatan_poli` (`id_poli`, `nama_poli`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('P10001', 'mata', '931085c2eaa6b8e4d8f49226d3177ec1522cec30262d0b99bb5d28b7fff0edb5.jpg', '0', '2019-03-25 05:33:43', 'AD2019020001'),
('P10002', 'gigi', '50059ad385a3bd7b793c72d11735b8d0d63ff93e88d284b1c55104006fef17bc.jpg', '0', '2019-03-25 05:33:36', 'AD2019020001'),
('P10003', 'penyakit dalam', '5f36c0d7291d99d01226f8fe60574df9ebb62b97fbe3f6e05848d6ac28f72bab.jpg', '0', '2019-03-25 03:07:55', 'AD2019020001'),
('P10004', 'paru-paru', '6a118570fc84e88aebb381ec8b27ae7d7348efc32220c3c4783e82c8fc89a5f4.jpg', '0', '2019-03-25 05:32:28', 'AD2019020001'),
('P10005', 'jantung', '2ab596154ea6d2f891ccd586291f50849447152e97071737baed6b815136015a.jpg', '0', '2019-03-25 05:32:36', 'AD2019020001'),
('P10006', 'bedah khusus', '2fbc38462a94c056d78ff07e23916c04615c60e90b07153b46c14d527ba92606.jpg', '0', '2019-03-25 05:32:55', 'AD2019020001'),
('P10007', 'bedah ringan', 'bc0a539247035b47948776452eebf9cfbd26b93719677204b6c7f571093181a1.jpg', '0', '2019-03-25 05:33:06', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_rs`
--

CREATE TABLE `kesehatan_rs` (
  `id_rs` varchar(15) NOT NULL,
  `nama_rumah_sakit` text NOT NULL,
  `alamat` text NOT NULL,
  `foto_rs` varchar(70) DEFAULT NULL,
  `telepon` varchar(13) NOT NULL,
  `id_layanan` varchar(8) NOT NULL,
  `id_poli` text NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_rs`
--

INSERT INTO `kesehatan_rs` (`id_rs`, `nama_rumah_sakit`, `alamat`, `foto_rs`, `telepon`, `id_layanan`, `id_poli`, `is_delete`, `waktu`, `id_admin`) VALUES
('RS2019032710001', 'RSJ', 'malang', '82361e1c39f6b3fd75aa97b9dbe470af7ad1a0d81cb1e5afc28df7091ad67076.jpg', '085841920243', 'KSJ10002', '[\'P10001\',\'P10003\',\'P10006\']', '0', '2019-04-15 09:56:09', 'AD2019020001'),
('RS2019032710002', 'panti nirmala', 'malang', '0788dcb56780f8d06164aa6635a3e500b9f4b37ea790b6de9454fea8a9f62ca0.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10005\',\'P10006\',\'P10007\']', '0', '2019-04-15 09:54:58', 'AD2019020001'),
('RS2019032710003', 'Rumah Sakit Saiful Anwar', 'malang', '576ef93d7b0f65f037da189f569e938045c26f753d1b42c3a1fbc61bb0ecea64.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10005\',\'P10006\',\'P10007\']', '1', '2019-04-15 10:59:13', 'AD2019020001'),
('RS2019032710004', 'Rumah Sakit Lavalete malang', 'malang', 'b12e37dc1ad28043eea7cbaab0d24ef82210017b58baa0ff81e5721994694879.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '0', '2019-04-15 10:50:10', 'AD2019020001'),
('RS2019032710005', 'Rumah Sakit Saiful Anwar ', 'malang', '2522405e2d190fa08cb58b6f989941cbad42e0ce4143eba42d9fe392d720fee1.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '0', '2019-04-15 10:50:42', 'AD2019020001'),
('RS2019032710006', 'Rumah Sakti Lavalete malangsa', 'malang', 'e44c711b36f1fa21b433a5494829a7dab4fd4e1032b19f6348eaf586f76adb00.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10003\',\'P10005\',\'P10007\']', '1', '2019-03-27 09:30:15', 'AD2019020001'),
('RS2019032710007', 'Rumah Sakit Daerah Aisiyah', 'Malang', 'cd933da191a29d2df0f2f26afcac8c3445b81bd38191f81b2689ab367e40b5b1.jpg', '085841920243', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\']', '1', '2019-03-27 09:30:02', 'AD2019020001'),
('RS2019041510001', 'Rumah Sakit Saiful Anwar Malang', 'malang', 'b81184675053bd523fec65d76759f16979b0b1735b0b761b9068e929bd0ab5f4.jpg', '034712532', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\']', '0', '2019-04-15 11:00:13', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `master_kecamatan`
--

CREATE TABLE `master_kecamatan` (
  `id_kecamatan` varchar(7) NOT NULL,
  `nama_kecamatan` varchar(64) NOT NULL,
  `area` text NOT NULL,
  `luas` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kecamatan`
--

INSERT INTO `master_kecamatan` (`id_kecamatan`, `nama_kecamatan`, `area`, `luas`, `is_delete`, `waktu`, `id_admin`) VALUES
('KEC1001', 'Kedungkandang', 'null', 39.87, '0', '0000-00-00 00:00:00', ''),
('KEC1002', 'Sukun', 'null', 20.97, '0', '0000-00-00 00:00:00', ''),
('KEC1003', 'Klojen', 'null', 8.83, '0', '0000-00-00 00:00:00', ''),
('KEC1004', 'Blimbing', 'null', 17.77, '0', '0000-00-00 00:00:00', ''),
('KEC1005', 'Lowokwaru', 'null', 22.6, '0', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `master_kelurahan`
--

CREATE TABLE `master_kelurahan` (
  `id_kelurahan` varchar(7) NOT NULL,
  `id_kecamatan` varchar(7) NOT NULL,
  `nama_kelurahan` varchar(64) NOT NULL,
  `area` text NOT NULL,
  `luas_kel` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kelurahan`
--

INSERT INTO `master_kelurahan` (`id_kelurahan`, `id_kecamatan`, `nama_kelurahan`, `area`, `luas_kel`, `is_delete`, `waktu`, `id_admin`) VALUES
('KEL1001', 'KEC1004', 'Arjosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1002', 'KEC1004', 'Balearjosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1003', 'KEC1004', 'Blimbing', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1004', 'KEC1001', 'Bunulrejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1005', 'KEC1004', 'Jodipan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1006', 'KEC1004', 'Kesatrian', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1007', 'KEC1004', 'Pandanwangi', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1008', 'KEC1004', 'Polehan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1009', 'KEC1004', 'Polowijen', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1010', 'KEC1004', 'Purwantoro', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1011', 'KEC1004', 'Purwodadi', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1012', 'KEC1001', 'Arjowinangung', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1013', 'KEC1001', 'Bumiayu', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1014', 'KEC1001', 'Buring', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1015', 'KEC1001', 'Cemorokandang', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1016', 'KEC1001', 'Kedungkandang', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1017', 'KEC1001', 'Kotalama', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1018', 'KEC1001', 'Lesanpuro', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1019', 'KEC1001', 'Madyopuro', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1020', 'KEC1001', 'Mergosono', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1021', 'KEC1001', 'Sawojajar', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1022', 'KEC1001', 'Tlogowaru', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1023', 'KEC1001', 'Wonokoyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1024', 'KEC1003', 'Bareng', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1025', 'KEC1003', 'Gadingsari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1026', 'KEC1003', 'Kasin', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1027', 'KEC1003', 'Kauman', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1028', 'KEC1003', 'Kiduldalem', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1029', 'KEC1003', 'Klojen', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1030', 'KEC1003', 'Penanggungan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1031', 'KEC1003', 'Rampal Celaket', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1032', 'KEC1003', 'Samaan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1033', 'KEC1003', 'Sukoharjo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1034', 'KEC1005', 'Dinoyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1035', 'KEC1005', 'Jatimulyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1036', 'KEC1005', 'Ketawanggede', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1037', 'KEC1005', 'Lowokwaru', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1038', 'KEC1005', 'Merjosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1039', 'KEC1005', 'Mojolangu', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1040', 'KEC1005', 'Sumbersari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1041', 'KEC1005', 'Tasikmadu', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1042', 'KEC1005', 'Tlogomas', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1043', 'KEC1005', 'Tulusrejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1044', 'KEC1005', 'Tunggulwulung', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1045', 'KEC1005', 'Tunjungsekar', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1046', 'KEC1002', 'Bukalankrajan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1047', 'KEC1002', 'Bandulan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1048', 'KEC1002', 'Bandungrejosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1049', 'KEC1002', 'Ciptomulyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1050', 'KEC1002', 'Gadang', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1051', 'KEC1002', 'Karangbesuki', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1052', 'KEC1002', 'Kebonsari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1053', 'KEC1002', 'Mulyorejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1054', 'KEC1002', 'Pisangcandi', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1055', 'KEC1002', 'Sukun', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1056', 'KEC1002', 'Tanjungrejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1057', 'KEC1003', 'Oro Oro Dowo', '0', 0, '0', '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jenis`
--

CREATE TABLE `pendidikan_jenis` (
  `id_jenis` varchar(10) NOT NULL,
  `id_strata` varchar(8) NOT NULL,
  `nama_jenis` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jenis`
--

INSERT INTO `pendidikan_jenis` (`id_jenis`, `id_strata`, `nama_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('PDJ0110001', 'PDST1002', 'SD Negeri', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDJ0110002', 'PDST1001', 'SD Swasta', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDJ0310001', 'PDST1003', 'SMP Negeri', '0', '2019-05-09 08:27:00', 'AD2019020001'),
('PDJ0810001', 'PDST1008', 'SMA Negeri', '0', '2019-05-10 09:34:46', '');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_sekolah`
--

CREATE TABLE `pendidikan_sekolah` (
  `id_sekolah` varchar(17) NOT NULL,
  `id_jenis` varchar(10) NOT NULL,
  `id_kecamatan` varchar(7) NOT NULL,
  `id_kelurahan` varchar(7) NOT NULL,
  `nama_sekolah` text NOT NULL,
  `foto_sklh` text NOT NULL,
  `lokasi` varchar(32) NOT NULL,
  `detail_sekolah` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_sekolah`
--

INSERT INTO `pendidikan_sekolah` (`id_sekolah`, `id_jenis`, `id_kecamatan`, `id_kelurahan`, `nama_sekolah`, `foto_sklh`, `lokasi`, `detail_sekolah`, `is_delete`, `waktu`, `id_admin`) VALUES
('SKL20190429100001', 'PDJ0110001', 'KEC1003', 'KEL1029', 'SDN KLOJEN', '717a788ff7d293fca75109012e772e77dadf6cda5da9da18a7db13a33a1f78bb.jpg', '[\'-7.970732\', \'112.632759\']', '{\'alamat\': \'Jl. Patimura No.1, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'http://sdnklojen.blogspot.com/\',\'tlp\': \'0341350806\'}', '0', '2019-05-13 06:27:04', 'AD2019020001'),
('SKL20190429100002', 'PDJ0110001', 'KEC1003', 'KEL1028', 'SDN Kiduldalem 1', '1fb0246f490a904ff647619ba9fa8daf05379189fc672926bb36c17e116bb90c.jpg', '[\'-7.978529\', \'112.633037\']', '{\'alamat\': \'malang\',\'url\': \'https://www.php.net/manual/en/language.exceptions.php\',\'tlp\': \'0341123123\'}', '0', '2019-05-09 07:15:10', 'AD2019020001'),
('SKL20190429100003', 'PDJ0110001', 'KEC1003', '0', 'SDN Kiduldalem 2', '0', '[\'-7.979886\', \'112.635053\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100004', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 1', '0', '[\'-7.984757\', \'112.630161\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100005', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 2', '0', '[\'-7.978359\', \'112.624812\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100006', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 3', '0', '[\'-7.983679\', \'112.628547\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100007', 'PDJ0110001', 'KEC1003', '0', 'SDN Kasin', '0', '[\'-7.985550\', \'112.625451\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100008', 'PDJ0110001', 'KEC1003', '0', 'SDN Sukoharjo 1', '0', '[\'-7.990147\', \'112.633196\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100009', 'PDJ0110001', 'KEC1003', '0', 'SDN Sukoharjo 2', '0', '[\'-7.989915\', \'112.632845\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100010', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 1', '0', '[\'-7.980727\', \'112.625345\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100011', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 2', '0', '[\'-7.979924\', \'112.621778\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100012', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 3', '0', '[\'-7.975719\', \'112.618941\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100001', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 4', '0', '[\'-7.976788\', \'112.617087\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100002', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 5', '0', '[\'-7.977995\', \'112.622715\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100003', 'PDJ0110001', 'KEC1004', '0', 'SDN Balearjosari 1', '0', '[\'-7.923529\', \'112.651103\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100004', 'PDJ0110001', 'KEC1004', '0', 'SDN Balearjosari 2', '0', '[\'-7.926450\', \'112.657830\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100005', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 1', '0', '[\'-7.929054\', \'112.648380\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100006', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 2', '0', '[\'-7.929044\', \'112.642837\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100007', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 3', '0', '[\'-7.928972\', \'112.645164\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100008', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 1', '0', '[\'-7.922440\', \'112.651512\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100009', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 2', '0', '[\'-7.930654\', \'112.658624\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100010', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 3', '0', '[\'-7.927625\', \'112.654032\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100011', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 1', '0', '[\'-7.932690\', \'112.646803\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100012', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 2', '0', '[\'-7.9382737\', \' 112.6464189\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100013', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 3', '0', '[\'-7.9384437 \', \'112.6469653\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100014', 'PDJ0110001', 'KEC1001', '0', 'SDN Kedungkandang 1', '0', '[\'-7.989953 \', \'112.650530\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100015', 'PDJ0110001', 'KEC1001', '0', 'SDN Kedungkandang 2', '0', '[\'-7.99115 \', \'112.647413\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100016', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 1', '0', '[\'-7.984983 \', \'112.657145\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100017', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 2', '0', '[\'-7.991327 \', \'112.664996\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100018', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 3', '0', '[\'-7.981281 \', \'112.660270\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100019', 'PDJ0110001', 'KEC1001', '0', 'SDN Buring', '0', '[\'-8.005362 \', \'112.644048\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100020', 'PDJ0110001', 'KEC1001', '0', 'SDN Wonokoyo 1', '0', '[\'-8.021992 \', \'112.648034\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100021', 'PDJ0110001', 'KEC1001', '0', 'SDN Wonokoyo 2', '0', '[\'-8.023499 \', \'112.669706\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100022', 'PDJ0110001', 'KEC1001', '0', 'SDN Tlogowaru 1', '0', '[\'-8.039433 \', \'112.650648\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100023', 'PDJ0110001', 'KEC1001', '0', 'SDN Tlogowaru 2', '0', '[\'-8.037279 \', \'112.666295\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100024', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 1', '0', '[\'-7.956957 \', \'112.635960\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100025', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 2', '0', '[\'-7.962162 \', \'112.632506\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100026', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 3', '0', '[\'-7.961433 \', \'112.634560\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100027', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 4', '0', '[\'-7.952290 \', \'112.632080\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100028', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 5', '0', '[\'-7.956750 \', \'112.625608\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100029', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 1', '0', '[\'-7.951383 \', \'112.633635\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100030', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 2', '0', '[\'-7.944885 \', \' 112.626397\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100031', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 3', '0', '[\'-7.946951 \', \' 112.635431\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100032', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 4', '0', '[\'-7.951106 \', \' 112.634715\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100033', 'PDJ0110001', 'KEC1005', '0', 'SDN Jatimulyo', '0', '[\'-7.942121 \', \' 112.616067\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100034', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 1', '0', '[\'-7.994275 \', \' 112.620484\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100035', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 2', '0', '[\'-7.991602 \', \' 112.616819\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100036', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 3', '0', '[\'-7.991235 \', \' 112.620182\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100037', 'PDJ0110001', 'KEC1002', '0', 'SDN Bandungrejosari 1', '0', '[\'-8.008017 \', \' 112.618806\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100038', 'PDJ0110001', 'KEC1002', '0', 'SDN Bandungrejosari 2', '0', '[\'-8.001061 \', \' 112.614058\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190509100001', 'PDJ0110001', 'KEC1002', 'KEL1013', 'sd n pakis', '', '0', '{\'alamat\': \'JL Teluk Grajakan XVII 49AB\',\'url\': \'https://www.google.com/\',\'tlp\': \'12\'}', '0', '2019-05-09 04:31:25', 'AD2019020001'),
('SKL20190509100002', 'PDJ0110001', 'KEC1002', 'KEL1013', 'sd n pakis', '682baad983953bbe6bc3c32eae11017694d224750107386fe646095d912795aa.jpg', '0', '{\'alamat\': \'JL Teluk Grajakan XVII 49AB\',\'url\': \'https://www.google.com/\',\'tlp\': \'12\'}', '0', '2019-05-09 04:33:05', 'AD2019020001'),
('SKL20190509100003', 'PDJ0110001', 'KEC1002', 'KEL1013', 'sd n pakis', '5f4a48fa7203f2c3a203738f6bb44b688e6c7a7c70dbc959afdb472d58c66637.jpg', '0', '{\'alamat\': \'JL Teluk Grajakan XVII 49AB\',\'url\': \'https://www.google.com/\',\'tlp\': \'12\'}', '0', '2019-05-09 04:39:13', 'AD2019020001'),
('SKL20190509100005', 'PDJ0110001', 'KEC1002', 'KEL1013', 'sdk sukijan', '5e58f8ced8ff288b9a7ee7cb6a7c108dad77e6929e67b29cf1533a0acd7b8a9f.jpg', '0', '{\'alamat\': \'JL Teluk Grajakan XVII 49AB\',\'url\': \'https://www.google.com/\',\'tlp\': \'081230695774\'}', '1', '2019-05-09 07:22:55', 'AD2019020001'),
('SKL20190509100006', 'PDJ0110001', 'KEC1003', 'KEL1021', 'sdi alam sutra', '16be9097b5248499c0443aa6abec153a5bf9db1c27a0eefe8249dd7f94d778d7.jpg', '0', '{\'alamat\': \'JL Teluk Grajakan XVII 49AB\',\'url\': \'https://www.google.com/\',\'tlp\': \'081230695774\'}', '1', '2019-05-09 07:22:47', 'AD2019020001'),
('SKL20190509100007', 'PDJ0110001', 'KEC1002', 'KEL1013', 'sd n pakis', '', '0', '0', '0', '0000-00-00 00:00:00', '0'),
('SKL20190509100008', 'PDJ0110002', 'KEC1002', 'KEL1046', 'sdasdas', '0cbf8c8d1e43207f15e2c02bbad10faaa4f57b64fd5457989250b3b0687cede2.jpg', '0', '{\'alamat\': \'malang\',\'url\': \'https://opendata.malangkota.go.id/data/geografi_wilayah\',\'tlp\': \'021232133\'}', '1', '2019-05-10 04:33:23', 'AD2019020001'),
('SKL20190509100009', 'PDJ0310001', 'KEC1003', 'KEL1057', 'SMP Negeri 1 Malang', '1aed74b869912c26d20d212568544efb36c2f539b8cb24d03b11bb4c415978fe.jpg', '[\'-7.971186 \', \'112.623810\']', '{\'alamat\': \'Jl. Lawu No.12, Oro-oro Dowo, Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'http://smpn1-mlg.sch.id\',\'tlp\': \'0341325206\'}', '0', '2019-05-10 04:32:58', 'AD2019020001'),
('SKL20190509100010', 'PDJ0310001', 'KEC1003', 'KEL1033', 'SMP Negeri 2 Malang', 'cbe7b86d2a50b37ab2703512f783931bf706c551ee401e3453c503bfdb3eb277.jpg', '[\'-7.990190 \', \'112.630876\']', '{\'alamat\': \'Jl. Prof. Moch Yamin No.60, Sukoharjo, Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'smpn2mlg.sch.id\',\'tlp\': \'0341325508\'}', '0', '2019-05-10 04:32:46', 'AD2019020001'),
('SKL20190509100013', 'PDJ0110001', 'KEC1003', 'KEL1029', 'SMP Negeri 3 Malang', 'a613e2a2cfd9044665bdafb114b66eb594f3b92c239af14c848f02a5c5063e91.jpg', '[\'-7.969050 \', \'112.635547\']', '{\'alamat\': \'Jl. Dr. Cipto No.20, 3, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'smpn3-mlg.sch.id\',\'tlp\': \'0341362612\'}', '0', '2019-05-10 04:32:30', 'AD2019020001'),
('SKL20190509100014', 'PDJ0310001', 'KEC1005', 'KEL1040', 'SMP Negeri 4 Malang', '1f34e7106a0cb27c4fb661f02ca43180ba6c31c1e90eb88fb016bf26e395e43b.jpg', '[\'-7.957819 \', \'112.616046\']', '{\'alamat\': \'Jl. Veteran No.37, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'smpn4malang.wordpress.com\',\'tlp\': \'0341551289\'}', '0', '2019-05-10 04:32:15', 'AD2019020001'),
('SKL20190509100015', 'PDJ0310001', 'KEC1003', 'KEL1031', 'SMP Negeri 5 Malang', '7fba9bfc984026233c4d728aac7923e08306c5ebb5cef0a70eba0cac000bceec.jpg', '[\'-7.966499 \', \'112.638411\']', '{\'alamat\': \'Jalan Wage Rudolf Supratman No.12, RT./ RW:/RW.3, Rampal Celaket, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'smpn5-mlg.sch.id\',\'tlp\': \'0341482713\'}', '0', '2019-05-10 04:31:26', 'AD2019020001'),
('SKL20190510100001', 'PDJ0310001', 'KEC1003', 'KEL1024', 'SMP Negeri 6 Malang', '8ce2ba3bf92bb11baf9f355d2a0252a67b92e36d2c241682100ce2d61b3a4fe3.jpg', '[\'-7.979525 \', \'112.624688\']', '{\'alamat\': \'Jl. Kawi No.15A, Bareng, Klojen, Kota Malang, Jawa Timur 65116\',\'url\': \'smpn6-mlg.sch.id\',\'tlp\': \'0341364710\'}', '0', '2019-05-10 04:32:00', 'AD2019020001'),
('SKL20190510100002', 'PDJ0310001', 'KEC1001', 'KEL1013', 'SMP Negeri 7 Malang', '0251a2d1ac1d1573b6e2dae1895b77d74ff0ae884f95f1c43af2c500863557e6.jpg', '[\'-8.003875 \', \'112.636590\']', '{\'alamat\': \'Jl. Lembayung, Bumiayu, Kedungkandang, Kota Malang, Jawa Timur 65143\',\'url\': \'www.google.com\',\'tlp\': \'0341752032\'}', '0', '2019-05-10 04:31:49', 'AD2019020001'),
('SKL20190510100003', 'PDJ0310001', 'KEC1003', 'KEL1027', 'SMP Negeri 8 Malang', '260f2e9b61f2fed4cca693e17fbe756d25416dd334c69df7d4010603d6197c91.jpg', '[\'-7.977110 \', \'112.627288\']', '{\'alamat\': \'Jl. Arjuno No.19, Kauman, Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'www.google.com\',\'tlp\': \'0341325506\'}', '0', '2019-05-10 04:31:37', 'AD2019020001'),
('SKL20190510100004', 'PDJ0110001', 'KEC1003', 'KEL1033', 'SMP Negeri 9 Malang', '7750bd66c77c9be9b89891e0b448db9cc54a1b38e9269509af59077580c0e8f5.jpg', '[\'-7.989593 \', \'112.630854\']', '{\'alamat\': \'Jl. Prof. Moch Yamin Gg. 6 No.26, Sukoharjo, Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'smpn9mlg.sch.id\',\'tlp\': \'0341364842\'}', '0', '2019-05-10 04:31:09', 'AD2019020001'),
('SKL20190510100005', 'PDJ0310001', 'KEC1001', 'KEL1014', 'SMP Negeri 10 Malang', '2912531587133db9ea11d3437bf92d9f133c9aa29227d2b27e91261905883ead.jpg', '[\'-7.9384437 \', \'112.6469653\']', '{\'alamat\': \'Jl. Mayjen Sungkono No.57, Buring, Kedungkandang, Kota Malang, Jawa Timur 65136\',\'url\': \'www.google.com\',\'tlp\': \'0341752035\'}', '0', '2019-05-10 04:35:11', 'AD2019020001'),
('SKL20190510100006', 'PDJ0310001', 'KEC1005', 'KEL1045', 'SMP Negeri 11 Malang', 'df4df32633bb692dfb0d4825a4b576d24da6c65c5a51a6efc91351d9c645f746.jpg', '[\'-7.932676\', \'112.637659\']', '{\'alamat\': \'Jl. Ikan Piranha Atas No.185, Tunjungsekar, Kec. Lowokwaru, Kota Malang, Jawa Timur 65122\',\'url\': \'smpn11-mlg.sch.id\',\'tlp\': \'0341494086\'}', '0', '2019-05-10 04:50:04', 'AD2019020001'),
('SKL20190510100007', 'PDJ0310001', 'KEC1002', 'KEL1048', 'SMP Negeri 12 Malang', '8f1526b78f32f50af83971a5fb3dcf0cb40f03c3636402514ecec013ed747c17.jpg', '[\'-8.006130 \', \'112.620903\']', '{\'alamat\': \'Jl. Slamet Supriadi No.49, Bandungrejosari, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341801186\'}', '0', '2019-05-10 04:49:32', 'AD2019020001'),
('SKL20190510100008', 'PDJ0310001', 'KEC1005', 'KEL1034', 'SMP Negeri 13 Malang', 'ff84b5e660abde24c5c8615be08b6ff53deba6cf252a3373b0ce925502fb5cdc.jpg', '[\'-7.948816 \', \'1112.607352\']', '{\'alamat\': \'Jl. Sunan Ampel 2, RT.9/RW.2, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65149\',\'url\': \'smpngalasmalang.sch.id\',\'tlp\': \'0341552864\'}', '0', '2019-05-10 04:51:23', 'AD2019020001'),
('SKL20190510100009', 'PDJ0310001', 'KEC1004', 'KEL1007', 'SMP Negeri 14 Malang', '52c7755e78735f0392b0d9839d426854c6fc2e0568f8b8de45f85163d2fcbecc.jpg', '[\'-7.943489 \', \'112.662495\']', '{\'alamat\': \'Jl. Tlk. Bayur No.2, Pandanwangi, Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'smpn14-mlg.sch.id\',\'tlp\': \'0341474458\'}', '0', '2019-05-10 04:52:54', 'AD2019020001'),
('SKL20190510100010', 'PDJ0310001', 'KEC1002', 'KEL1054', 'SMP Negeri 15 Malang', '121ebdc6db0a98182dfa451f8cdc32bc807ad9f7e404436d34d00acb2a7dfb46.jpg', '[\'-7.976119 \', \'112.604433\']', '{\'alamat\': \'Jl. Bukit Dieng Permai No.8, Pisang Candi, Sukun, Kota Malang, Jawa Timur 65146\',\'url\': \'smp15mlg.blogspot.co.id\',\'tlp\': \'0341571715\'}', '0', '2019-05-10 05:07:27', 'AD2019020001'),
('SKL20190510100011', 'PDJ0310001', 'KEC1004', 'KEL1001', 'SMP Negeri 16 Malang', '277b952bd47721f411cdb094e1aeb5401829f44143c74af3253e9aa52df1ff7e.jpg', '[\'-7.934457 \', \'112.662148\']', '{\'alamat\': \'Jl. Teluk Pacitan No.46, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'0341490441\'}', '0', '2019-05-10 05:32:24', 'AD2019020001'),
('SKL20190510100012', 'PDJ0110001', 'KEC1002', 'KEL1046', 'SMP Negeri 17 Malang', 'f1382755d59f33d6a6ffb2c0d6b077dfe7a4b7db5195db13abfade2d964dcbae.jpg', '[\'-8.006499 \', \'112.603642\']', '{\'alamat\': \'Jl. Pelabuhan Tanjung Priok No.170, Bakalankrajan, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-05-10 05:33:54', 'AD2019020001'),
('SKL20190510100013', 'PDJ0310001', 'KEC1005', 'KEL1039', 'SMP Negeri 18 Malang', '8609fdcb5a14379e035b0838189dfda1d594563c8b4de1718d9e413e192ed9cf.jpg', '[\'-7.939397 \', \'112.619543\']', '{\'alamat\': \'Jalan Soekarno Hatta No. A-394, Lowokwaru, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141\',\'url\': \'smpn18malang.sch.id\',\'tlp\': \'0341472418\'}', '0', '2019-05-10 05:39:53', 'AD2019020001'),
('SKL20190510100014', 'PDJ0310001', 'KEC1003', 'KEL1026', 'SMP Negeri 19 Malang', '', '[\'-7.993887\', \'112.625095\']', '{\'alamat\': \'Jl. Belitung No.1, Kasin, Klojen, Kota Malang, Jawa Timur 65117\',\'url\': \'beta.smpn20-malang.sch.id\',\'tlp\': \'0341324960\'}', '0', '2019-05-10 05:41:26', 'AD2019020001'),
('SKL20190510100015', 'PDJ0310001', 'KEC1004', 'KEL1003', 'SMP Negeri 20 Malang', '7a04681095008a825f81a8cd569b8cefd6bbe52b787a6407703b2d5460043f69.jpg', '[\'-7.963623, \'112.640839\']', '{\'alamat\': \'Jl. R. Tumenggung Suryo No.38, Bunulrejo, Blimbing, Kota Malang, Jawa Timur 65123\',\'url\': \'beta.smpn20-malang.sch.id\',\'tlp\': \'0341491806\'}', '0', '2019-05-10 05:44:32', 'AD2019020001'),
('SKL20190510100016', 'PDJ0110001', 'KEC1001', 'KEL1018', 'SMP Negeri 21 Malang', 'c251515fd2e99e83af9c022beb3bb382b59a6765592f306e8e4959332b9e4b06.jpg', '[\'-7.974113 \', \' 112.664685\']', '{\'alamat\': \'Jl. Danau Tigi, Lesanpuro, Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'smpn21-mlg.sch.id\',\'tlp\': \'0341718066\'}', '0', '2019-05-10 05:50:52', 'AD2019020001'),
('SKL20190510100017', 'PDJ0310001', 'KEC1001', 'KEL1014', 'SMP Negeri 22 Malang', '6c7566f571d3bc09076222f925690a5200c46a929db9851b0fc02a879ad50985.jpg', '[\'-7.999339 \', \'112.680257\']', '{\'alamat\': \'Jl. Raya Tlogowaru No.23, Tlogowaru, Kedungkandang, Kota Malang, Jawa Timur 65133\',\'url\': \'smpn23-malang.sch.id\',\'tlp\': \'0341754085\'}', '0', '2019-05-10 05:53:52', 'AD2019020001'),
('SKL20190510100018', 'PDJ0310001', 'KEC1001', 'KEL1022', 'SMP Negeri 23 Malang', '2ec03fd83aa042a516f058f17e67f14c5e0e0634a15bdbab49c32b8da2b624ac.jpg', '[\'-8.034877\', \'112.647305\']', '{\'alamat\': \'Jl. Raya Tlogowaru No.23, Tlogowaru, Kedungkandang, Kota Malang, Jawa Timur 65133\',\'url\': \'smpn23-malang.sch.id\',\'tlp\': \'0341754085\'}', '0', '2019-05-10 05:59:53', 'AD2019020001'),
('SKL20190510100019', 'PDJ0110001', 'KEC1004', 'KEL1007', 'SMP Negeri 24 Malang', '2785fa8544a0f9b9685d99e07dbaf88a1f5cadaa8f6e5522a46b2cb3fba55af2.jpg', '[\'-7.953842 \', \'112.661222\']', '{\'alamat\': \'Gg. Makam, Pandanwangi, Blimbing, Kota Malang, Jawa Timur 65124\',\'url\': \'smpn24mlg.wordpress.com\',\'tlp\': \'0341415415\'}', '0', '2019-05-10 06:01:00', 'AD2019020001'),
('SKL20190510100020', 'PDJ0810001', 'KEC1003', 'KEL1028', 'SMA Negeri 1 Malang', '182b84337b93ff48a0c9aea1e99c6d87dfa58651484d06e29fb3be619524b238.jpg', '[\'-7.976751 \', \'112.634870\']', '{\'alamat\': \'Jl. Tugu Utara No.1, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'sman1-mlg.sch.id\',\'tlp\': \'0341366454\'}', '0', '2019-05-10 09:38:18', ''),
('SKL20190510100021', 'PDJ0810001', 'KEC1003', 'KEL1033', 'SMA Negeri 2 Malang', '891c13b50f1c664913b0627b36f7a4cb846b2533606707b881c470a0962c5c9d.jpg', '[\'-7.991443 \', \'112.633712\']', '{\'alamat\': \'Jl. Laksamana Martadinata No.84, Sukoharjo, Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'sman2-malang.sch.id\',\'tlp\': \'0341366311\'}', '0', '2019-05-10 09:46:26', ''),
('SKL20190510100022', 'PDJ0110001', 'KEC1003', 'KEL1029', 'SMA Negeri 3 Malang', 'd3066568b6db13d83c3ed2bceb741e58f6668f2df8c331b9efa8babd41d0140b.jpg', '[\'-7.975866 \', \'112.635134\']', '{\'alamat\': \'Jl. Sultan Agung No.7, Klojen, Kota Malang, Jawa Timur 65144\',\'url\': \'sman3-malang.sch.id\',\'tlp\': \'0341324768\'}', '0', '2019-05-10 09:57:58', ''),
('SKL20190510100023', 'PDJ0810001', 'KEC1003', 'KEL1029', 'SMA Negeri 4 Malang', 'cf0e648fee985e72f7bed86b4f5906c0f67b17468bda37dfa1ff3d8f2ebaef8d.jpg', '[\'-7.976350 \', \'112.634529\']', '{\'alamat\': \'Jl. Tugu No.1, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'sman4malang.sch.id\',\'tlp\': \'0341325267\'}', '0', '2019-05-10 09:59:27', ''),
('SKL20190510100024', 'PDJ0810001', 'KEC1001', 'KEL1016', 'SMA Negeri 6 Malang', 'd524231b6e12141cbd7384113769e20bffae6649c4982acb2778913a79ae2867.jpg', '[\'-8.010166 \', \'112.643391\']', '{\'alamat\': \'Jl. Mayjen Sungkono No.58, Buring, Kedungkandang, Kota Malang, Jawa Timur 65136\',\'url\': \'Sman6malang.sch.id\',\'tlp\': \'0341752036\'}', '0', '2019-05-13 06:14:25', 'AD2019020001'),
('SKL20190513100001', 'PDJ0810001', 'KEC1003', 'KEL1026', 'SMA Negeri 5 Malang', '7e046a5de048a761e9c9742b199b3559407082e3511c97dab17594e2b87aaea6.jpg', '[\'-7.989170 \', \'112.626277\']', '{\'alamat\': \'Jl. Tanimbar No.24, Kasin, Klojen, Kota Malang, Jawa Timur 65117\',\'url\': \'sman5malang.sch.id\',\'tlp\': \'0341364580\'}', '0', '2019-05-13 05:57:01', 'AD2019020001'),
('SKL20190513100002', 'PDJ0810001', 'KEC1005', 'KEL1043', 'SMA Negeri 7 Malang', 'b95b6a527e25fe4bb4fcdd137cb20e342156816e6da9b5a863b40a0a3a06a05c.jpg', '[\'-7.945189 \', \'112.628170\']', '{\'alamat\': \'Jl. Cengger Ayam I No.14, Tulusrejo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141\',\'url\': \'sman7malang.sch.id\',\'tlp\': \'0341495256\'}', '0', '2019-05-13 06:02:26', 'AD2019020001'),
('SKL20190513100003', 'PDJ0810001', 'KEC1005', 'KEL1040', 'SMA Negeri 8 Malang', 'a99b5bb449a62cce9881df07a80bb5aaa7fd1dba3d1b9b88a351ea662584336f.jpg', '[\'-7.956855 \', \'112.616811\']', '{\'alamat\': \'Jl. Veteran No.37, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'sman8malang.sch.id\',\'tlp\': \'0341551096\'}', '0', '2019-05-13 06:15:52', 'AD2019020001'),
('SKL20190513100004', 'PDJ0810001', 'KEC1005', 'KEL1039', 'SMA Negeri 9 Malang', '2f1d6103bbd5805cb67a09adb14694b461192d65ba92bfa70c9ced036287f81b.jpg', '[\'-7.936086 \', \'112.625215\']', '{\'alamat\': \'Jl. Puncak Borobudur No.1, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'sman9-mlg.sch.id\',\'tlp\': \'0341471855\'}', '0', '2019-05-13 06:17:32', 'AD2019020001'),
('SKL20190513100005', 'PDJ0810001', 'KEC1001', 'KEL1021', 'SMA Negeri 10 Malang', '3ccaa7e6a2f9db5c213152466ebbc65da824889a5b5f741d6e0a800a3834f456.jpg', '[\'-7.974468 \', \'112.660143\']', '{\'alamat\': \'Jl. Danau Grati No.1, Sawojajar, Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'sman10malang.sch.id\',\'tlp\': \'0341719300\'}', '0', '2019-05-13 06:18:56', 'AD2019020001'),
('SKL20190513100006', 'PDJ0810001', 'KEC1002', 'KEL1046', 'SMA Negeri 11 Malang', '4b9903a1f9b67453a9dc1981f7c2e685774217d2285e801ca44a10affddff53c.jpg', '[\'-8.006901 \', \'112.610152\']', '{\'alamat\': \'Jl. Pelabuhan Bakahuni No.1, Bakalankrajan, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-05-13 08:35:53', '0'),
('SKL20190513100007', 'PDJ0810001', 'KEC1002', 'KEL1048', 'SMA Negeri 12 Malang', 'f5ac506296913f5f26d6a536fc8b46e2a071c927c3b512225218b2549c38b09c.jpg', '[\'-8.006147 \', \'1112.620906\']', '{\'alamat\': \'Jl. Slamet Supriadi No.49, Bandungrejosari, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341801186\'}', '0', '2019-05-13 08:36:34', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_strata`
--

CREATE TABLE `pendidikan_strata` (
  `id_strata` varchar(8) NOT NULL,
  `nama_strata` text NOT NULL,
  `icon_32` text NOT NULL,
  `icon_64` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_strata`
--

INSERT INTO `pendidikan_strata` (`id_strata`, `nama_strata`, `icon_32`, `icon_64`, `is_delete`, `waktu`, `id_admin`) VALUES
('PDST1001', 'Taman Kanak-Kanak (TK)', '', '', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1002', 'Sekolah Dasar (SD)', 'sd_32.png', 'sd_64.png', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1003', 'Sekolah Menengah Pertama (SMP)', 'smp_32.png', 'smp_64.png', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1004', 'Universitas', '', '', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1005', 'bagus', '', '', '1', '2019-05-07 09:16:45', 'AD2019020001'),
('PDST1006', 'mas', '', '', '1', '2019-05-07 09:16:39', 'AD2019020001'),
('PDST1007', 'jhonathan', '', '', '1', '2019-05-10 09:33:54', 'AD2019020001'),
('PDST1008', 'Sekolah Menengah Atas (SMA)', 'sma_32.png', 'sma_64.png', '0', '2019-05-10 09:34:15', '');

-- --------------------------------------------------------

--
-- Table structure for table `table1`
--

CREATE TABLE `table1` (
  `id` varchar(7) NOT NULL DEFAULT '0',
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table1`
--

INSERT INTO `table1` (`id`, `name`) VALUES
('LHPL001', 'joni'),
('LHPL002', 'joni'),
('LHPL003', 'joni'),
('LHPL004', 'joni'),
('LHPL005', 'joni'),
('LHPL006', 'joni'),
('LHPL007', 'joni'),
('LHPL008', 'joni'),
('LHPL009', 'joni'),
('LHPL010', 'joni'),
('LHPL011', 'joni'),
('LHPL012', 'joni'),
('LHPL013', 'joni'),
('LHPL014', 'joni'),
('LHPL015', 'joni'),
('LHPL016', 'joni'),
('LHPL017', 'joni'),
('LHPL018', 'joni'),
('LHPL019', 'joni'),
('LHPL020', 'joni'),
('LHPL021', 'joni'),
('LHPL022', 'joni'),
('LHPL023', 'joni'),
('LHPL024', 'joni'),
('LHPL025', 'joni'),
('LHPL026', 'joni'),
('LHPL027', 'joni'),
('LHPL028', 'joni'),
('LHPL029', 'joni'),
('LHPL030', 'joni'),
('LHPL031', 'joni'),
('LHPL032', 'joni'),
('LHPL033', 'joni'),
('LHPL034', 'joni'),
('LHPL035', 'joni'),
('LHPL036', 'joni'),
('LHPL037', 'joni'),
('LHPL038', 'joni'),
('LHPL039', 'joni'),
('LHPL040', 'joni'),
('LHPL041', 'joni'),
('LHPL042', 'joni'),
('LHPL043', 'joni'),
('LHPL044', 'joni'),
('LHPL045', 'joni'),
('LHPL046', 'joni'),
('LHPL047', 'joni'),
('LHPL048', 'joni'),
('LHPL049', 'joni'),
('LHPL050', 'joni'),
('LHPL051', 'joni'),
('LHPL052', 'joni'),
('LHPL053', 'joni'),
('LHPL054', 'joni'),
('LHPL055', 'joni'),
('LHPL056', 'joni'),
('LHPL057', 'joni'),
('LHPL058', 'joni'),
('LHPL059', 'joni'),
('LHPL060', 'joni'),
('LHPL061', 'joni'),
('LHPL062', 'joni'),
('LHPL063', 'joni'),
('LHPL064', 'joni'),
('LHPL065', 'joni'),
('LHPL066', 'joni'),
('LHPL067', 'joni'),
('LHPL068', 'joni'),
('LHPL069', 'joni'),
('LHPL070', 'joni'),
('LHPL071', 'joni'),
('LHPL072', 'joni'),
('LHPL073', 'joni'),
('LHPL074', 'joni'),
('LHPL075', 'joni'),
('LHPL076', 'joni'),
('LHPL077', 'joni'),
('LHPL078', 'joni'),
('LHPL079', 'joni'),
('LHPL080', 'joni'),
('LHPL081', 'joni'),
('LHPL082', 'joni'),
('LHPL083', 'joni'),
('LHPL084', 'joni'),
('LHPL085', 'joni'),
('LHPL086', 'joni'),
('LHPL087', 'joni'),
('LHPL088', 'joni'),
('LHPL089', 'joni'),
('LHPL090', 'joni'),
('LHPL091', 'joni'),
('LHPL092', 'joni'),
('LHPL093', 'joni'),
('LHPL094', 'joni'),
('LHPL095', 'joni'),
('LHPL096', 'joni'),
('LHPL097', 'joni'),
('LHPL098', 'joni'),
('LHPL099', 'joni'),
('LHPL100', 'joni'),
('LHPL101', 'joni'),
('LHPL102', 'joni'),
('LHPL103', 'joni'),
('LHPL104', 'joni'),
('LHPL105', 'joni'),
('LHPL106', 'joni'),
('LHPL107', 'joni'),
('LHPL108', 'joni'),
('LHPL109', 'joni'),
('LHPL110', 'joni'),
('LHPL111', 'joni'),
('LHPL112', 'joni'),
('LHPL113', 'joni'),
('LHPL114', 'joni'),
('LHPL115', 'joni'),
('LHPL116', 'joni'),
('LHPL117', 'joni'),
('LHPL118', 'joni'),
('LHPL119', 'joni'),
('LHPL120', 'joni'),
('LHPL121', 'joni'),
('LHPL122', 'joni'),
('LHPL123', 'joni'),
('LHPL124', 'joni'),
('LHPL125', 'joni'),
('LHPL126', 'joni'),
('LHPL127', 'joni'),
('LHPL128', 'joni'),
('LHPL129', 'joni'),
('LHPL130', 'joni'),
('LHPL131', 'joni'),
('LHPL132', 'joni'),
('LHPL133', 'joni'),
('LHPL134', 'joni'),
('LHPL135', 'joni'),
('LHPL136', 'joni'),
('LHPL137', 'joni'),
('LHPL138', 'joni'),
('LHPL139', 'joni'),
('LHPL140', 'joni'),
('LHPL141', 'joni'),
('LHPL142', 'joni'),
('LHPL143', 'joni'),
('LHPL144', 'joni'),
('LHPL145', 'joni'),
('LHPL146', 'joni'),
('LHPL147', 'joni'),
('LHPL148', 'joni'),
('LHPL149', 'joni'),
('LHPL150', 'joni'),
('LHPL151', 'joni'),
('LHPL152', 'joni'),
('LHPL153', 'joni'),
('LHPL154', 'joni'),
('LHPL155', 'joni'),
('LHPL156', 'joni'),
('LHPL157', 'joni'),
('LHPL158', 'joni'),
('LHPL159', 'joni'),
('LHPL160', 'joni'),
('LHPL161', 'joni'),
('LHPL162', 'joni'),
('LHPL163', 'joni'),
('LHPL164', 'joni'),
('LHPL165', 'joni'),
('LHPL166', 'joni'),
('LHPL167', 'joni'),
('LHPL168', 'joni'),
('LHPL169', 'joni'),
('LHPL170', 'joni'),
('LHPL171', 'joni'),
('LHPL172', 'joni'),
('LHPL173', 'joni'),
('LHPL174', 'joni'),
('LHPL175', 'joni'),
('LHPL176', 'joni'),
('LHPL177', 'joni'),
('LHPL178', 'joni'),
('LHPL179', 'joni'),
('LHPL180', 'joni'),
('LHPL181', 'joni'),
('LHPL182', 'joni'),
('LHPL183', 'joni'),
('LHPL184', 'joni'),
('LHPL185', 'joni'),
('LHPL186', 'joni'),
('LHPL187', 'joni'),
('LHPL188', 'joni'),
('LHPL189', 'joni'),
('LHPL190', 'joni'),
('LHPL191', 'joni'),
('LHPL192', 'joni'),
('LHPL193', 'joni'),
('LHPL194', 'joni'),
('LHPL195', 'joni'),
('LHPL196', 'joni'),
('LHPL197', 'joni'),
('LHPL198', 'joni'),
('LHPL199', 'joni'),
('LHPL200', 'joni'),
('LHPL201', 'joni'),
('LHPL202', 'joni'),
('LHPL203', 'joni'),
('LHPL204', 'joni'),
('LHPL205', 'joni'),
('LHPL206', 'joni'),
('LHPL207', 'joni'),
('LHPL208', 'joni'),
('LHPL209', 'joni'),
('LHPL210', 'joni'),
('LHPL211', 'joni'),
('LHPL212', 'joni'),
('LHPL213', 'joni'),
('LHPL214', 'joni'),
('LHPL215', 'joni'),
('LHPL216', 'joni'),
('LHPL217', 'joni'),
('LHPL218', 'joni'),
('LHPL219', 'joni'),
('LHPL220', 'joni'),
('LHPL221', 'joni'),
('LHPL222', 'joni'),
('LHPL223', 'joni'),
('LHPL224', 'joni'),
('LHPL225', 'joni'),
('LHPL226', 'joni'),
('LHPL227', 'joni'),
('LHPL228', 'joni'),
('LHPL229', 'joni'),
('LHPL230', 'joni'),
('LHPL231', 'joni'),
('LHPL232', 'joni'),
('LHPL233', 'joni'),
('LHPL234', 'joni'),
('LHPL235', 'joni'),
('LHPL236', 'joni'),
('LHPL237', 'joni'),
('LHPL238', 'joni'),
('LHPL239', 'joni'),
('LHPL240', 'joni'),
('LHPL241', 'joni'),
('LHPL242', 'joni'),
('LHPL243', 'joni'),
('LHPL244', 'joni'),
('LHPL245', 'joni'),
('LHPL246', 'joni'),
('LHPL247', 'joni'),
('LHPL248', 'joni'),
('LHPL249', 'joni'),
('LHPL250', 'joni'),
('LHPL251', 'joni'),
('LHPL252', 'joni'),
('LHPL253', 'joni'),
('LHPL254', 'joni'),
('LHPL255', 'joni'),
('LHPL256', 'joni'),
('LHPL257', 'joni'),
('LHPL258', 'joni'),
('LHPL259', 'joni'),
('LHPL260', 'joni'),
('LHPL261', 'joni'),
('LHPL262', 'joni'),
('LHPL263', 'joni'),
('LHPL264', 'joni'),
('LHPL265', 'joni'),
('LHPL266', 'joni'),
('LHPL267', 'joni'),
('LHPL268', 'joni'),
('LHPL269', 'joni'),
('LHPL270', 'joni'),
('LHPL271', 'joni'),
('LHPL272', 'joni'),
('LHPL273', 'joni'),
('LHPL274', 'joni'),
('LHPL275', 'joni'),
('LHPL276', 'joni'),
('LHPL277', 'joni'),
('LHPL278', 'joni'),
('LHPL279', 'joni'),
('LHPL280', 'joni'),
('LHPL281', 'joni'),
('LHPL282', 'joni'),
('LHPL283', 'joni'),
('LHPL284', 'joni'),
('LHPL285', 'joni'),
('LHPL286', 'joni'),
('LHPL287', 'joni'),
('LHPL288', 'joni'),
('LHPL289', 'joni'),
('LHPL290', 'joni'),
('LHPL291', 'joni'),
('LHPL292', 'joni'),
('LHPL293', 'joni'),
('LHPL294', 'joni'),
('LHPL295', 'joni'),
('LHPL296', 'joni'),
('LHPL297', 'joni'),
('LHPL298', 'joni'),
('LHPL299', 'joni'),
('LHPL300', 'joni'),
('LHPL301', 'joni'),
('LHPL302', 'joni'),
('LHPL303', 'joni'),
('LHPL304', 'joni'),
('LHPL305', 'joni'),
('LHPL306', 'joni'),
('LHPL307', 'joni'),
('LHPL308', 'joni'),
('LHPL309', 'joni'),
('LHPL310', 'joni'),
('LHPL311', 'joni'),
('LHPL312', 'joni'),
('LHPL313', 'joni'),
('LHPL314', 'joni'),
('LHPL315', 'joni'),
('LHPL316', 'joni'),
('LHPL317', 'joni'),
('LHPL318', 'joni'),
('LHPL319', 'joni'),
('LHPL320', 'joni'),
('LHPL321', 'joni'),
('LHPL322', 'joni'),
('LHPL323', 'joni'),
('LHPL324', 'joni'),
('LHPL325', 'joni'),
('LHPL326', 'joni'),
('LHPL327', 'joni'),
('LHPL328', 'joni'),
('LHPL329', 'joni'),
('LHPL330', 'joni'),
('LHPL331', 'joni'),
('LHPL332', 'joni'),
('LHPL333', 'joni'),
('LHPL334', 'joni'),
('LHPL335', 'joni'),
('LHPL336', 'joni'),
('LHPL337', 'joni'),
('LHPL338', 'joni'),
('LHPL339', 'joni'),
('LHPL340', 'joni'),
('LHPL341', 'joni'),
('LHPL342', 'joni'),
('LHPL343', 'joni'),
('LHPL344', 'joni'),
('LHPL345', 'joni'),
('LHPL346', 'joni'),
('LHPL347', 'joni'),
('LHPL348', 'joni'),
('LHPL349', 'joni'),
('LHPL350', 'joni'),
('LHPL351', 'joni'),
('LHPL352', 'joni'),
('LHPL353', 'joni'),
('LHPL354', 'joni'),
('LHPL355', 'joni'),
('LHPL356', 'joni'),
('LHPL357', 'joni'),
('LHPL358', 'joni'),
('LHPL359', 'joni'),
('LHPL360', 'joni'),
('LHPL361', 'joni'),
('LHPL362', 'joni'),
('LHPL363', 'joni'),
('LHPL364', 'joni'),
('LHPL365', 'joni'),
('LHPL366', 'joni'),
('LHPL367', 'joni'),
('LHPL368', 'joni'),
('LHPL369', 'joni'),
('LHPL370', 'joni'),
('LHPL371', 'joni'),
('LHPL372', 'joni'),
('LHPL373', 'joni'),
('LHPL374', 'joni'),
('LHPL375', 'joni'),
('LHPL376', 'joni'),
('LHPL377', 'joni'),
('LHPL378', 'joni'),
('LHPL379', 'joni'),
('LHPL380', 'joni'),
('LHPL381', 'joni'),
('LHPL382', 'joni'),
('LHPL383', 'joni'),
('LHPL384', 'joni'),
('LHPL385', 'joni'),
('LHPL386', 'joni'),
('LHPL387', 'joni'),
('LHPL388', 'joni'),
('LHPL389', 'joni'),
('LHPL390', 'joni'),
('LHPL391', 'joni'),
('LHPL392', 'joni'),
('LHPL393', 'joni'),
('LHPL394', 'joni'),
('LHPL395', 'joni'),
('LHPL396', 'joni'),
('LHPL397', 'joni'),
('LHPL398', 'joni'),
('LHPL399', 'joni'),
('LHPL400', 'joni'),
('LHPL401', 'joni'),
('LHPL402', 'joni'),
('LHPL403', 'joni'),
('LHPL404', 'joni'),
('LHPL405', 'joni'),
('LHPL406', 'joni');

--
-- Triggers `table1`
--
DELIMITER $$
CREATE TRIGGER `tg_table1_insert` BEFORE INSERT ON `table1` FOR EACH ROW BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from table1;
  
  select id into last_key_item from table1 order by id desc limit 1;
  
  
  if(count_row_item <1) then
  	set fix_key_item = concat("LHPL","001");
  else
    set fix_key_item = concat('LHPL', LPAD(RIGHT(last_key_item, 3)+1, 3, '0'));
      
  END IF;
  
  SET NEW.id = fix_key_item;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `table1_seq`
--

CREATE TABLE `table1_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table1_seq`
--

INSERT INTO `table1_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90),
(91),
(92),
(93),
(94),
(95),
(96),
(97),
(98),
(99),
(100),
(101),
(102),
(103),
(104),
(105),
(106),
(107),
(108),
(109),
(110),
(111),
(112),
(113),
(114),
(115),
(116),
(117),
(118),
(119),
(120),
(121),
(122),
(123),
(124),
(125),
(126),
(127),
(128),
(129),
(130),
(131),
(132),
(133),
(134),
(135),
(136),
(137),
(138),
(139),
(140),
(141),
(142),
(143),
(144),
(145),
(146),
(147),
(148),
(149),
(150),
(151),
(152);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id_device_kios`);

--
-- Indexes for table `dinas`
--
ALTER TABLE `dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `faskes_jenis`
--
ALTER TABLE `faskes_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `faskes_main`
--
ALTER TABLE `faskes_main`
  ADD PRIMARY KEY (`id_faskes`);

--
-- Indexes for table `home_page_kategori`
--
ALTER TABLE `home_page_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `home_page_main`
--
ALTER TABLE `home_page_main`
  ADD PRIMARY KEY (`id_page`);

--
-- Indexes for table `ijin_antrian`
--
ALTER TABLE `ijin_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `ijin_jenis`
--
ALTER TABLE `ijin_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `ijin_kategori`
--
ALTER TABLE `ijin_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `ijin_sub_kategori`
--
ALTER TABLE `ijin_sub_kategori`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `kependudukan_antrian`
--
ALTER TABLE `kependudukan_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `kependudukan_jenis`
--
ALTER TABLE `kependudukan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kependudukan_kategori`
--
ALTER TABLE `kependudukan_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kesehatan_antrian`
--
ALTER TABLE `kesehatan_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `kesehatan_jenis`
--
ALTER TABLE `kesehatan_jenis`
  ADD PRIMARY KEY (`id_layanan`);

--
-- Indexes for table `kesehatan_poli`
--
ALTER TABLE `kesehatan_poli`
  ADD PRIMARY KEY (`id_poli`);

--
-- Indexes for table `kesehatan_rs`
--
ALTER TABLE `kesehatan_rs`
  ADD PRIMARY KEY (`id_rs`);

--
-- Indexes for table `master_kecamatan`
--
ALTER TABLE `master_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `master_kelurahan`
--
ALTER TABLE `master_kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indexes for table `pendidikan_jenis`
--
ALTER TABLE `pendidikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_sekolah`
--
ALTER TABLE `pendidikan_sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indexes for table `pendidikan_strata`
--
ALTER TABLE `pendidikan_strata`
  ADD PRIMARY KEY (`id_strata`);

--
-- Indexes for table `table1`
--
ALTER TABLE `table1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table1_seq`
--
ALTER TABLE `table1_seq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_kategori`
--
ALTER TABLE `home_page_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table1_seq`
--
ALTER TABLE `table1_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
