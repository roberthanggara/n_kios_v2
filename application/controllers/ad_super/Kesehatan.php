<?php 
defined('BASEPATH') OR exit('No direct script access allowed');



class Kesehatan extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Kesehatan_main", "ks");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("file_upload");
		$this->load->library("response_message");

        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != "1" and $session["is_log"] != "1"){
                redirect(base_url("back-admin/login"));
            }
        }else{
            redirect(base_url("back-admin/login"));
        }
	}

    public function un_link(){
        $config['upload_path']          = './assets/core_img/icon_menu_jenis/';
        $config['file_name']            = "2518ffca8740277c52d7f4f7e4ee55bc203a09eeb79e72fde94270e20f688047.jpg";
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }

    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#
    private function main_upload_file($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#

    public function index_jenis_rs(){
        $data["list_jenis_rs"] = $this->mm->get_data_all_where("kesehatan_jenis", array("is_delete"=>"0"));
        
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/kesehatan/v_rumah_sakit_jenis", $data);
        $this->load->view("ad_super/footer");
    }

    private function validate_input_jenis_rs(){
        $config_val_input = array(
                array(
                    'field'=>'nama_jenis',
                    'label'=>'Keterangan Jenis Rumah Sakit',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_jenis_rs(){
        $detail_msg = array("nama_jenis"=>"", "foto_jenis"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_jenis_rs()){
            $nama_jenis = $this->input->post("nama_jenis");
            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->ks->insert_jenis_rs($nama_jenis, $time_update, $id_admin)->row_array();
            if($insert){
                $config['upload_path']          = './assets/core_img/icon_menu_jenis/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 200;
                $config['file_name']            = hash("sha256", $insert["id_jenis_rs"]).".jpg";
                   
                $upload_data = $this->main_upload_file($config, "foto_jenis");
                
                if($upload_data["status"]){
                    $where = array("id_layanan"=>$insert["id_jenis_rs"]);
                    $set = array("foto"=>$upload_data["main_data"]["upload_data"]["file_name"]);

                    if($this->mm->update_data("kesehatan_jenis", $set, $where)){
                        $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else{
                    $detail_msg["foto_jenis"] = $upload_data["main_msg"]["error"];
                    $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }            
        }else {
            $detail_msg["nama_jenis"] = strip_tags(form_error("nama_jenis"));
        }
        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        print_r(json_encode($msg_array));
    }


    public function get_jenis_rs(){
        $id = $this->encrypt->decode($this->input->post("id_layanan"));
        $data = $this->mm->get_data_each("kesehatan_jenis",array("id_layanan"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_layanan"=>$this->encrypt->encode($data["id_layanan"]),
                                                "nama_layanan"=>$data["nama_layanan"],
                                                "foto"=> base_url()."assets/core_img/icon_menu_jenis/".$data["foto"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_jenis_rs(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array("nama_jenis"=>"", "foto_jenis"=>"");

        // print_r($_POST);

        if($this->validate_input_jenis_rs()){
            $id_layanan = $this->encrypt->decode($this->input->post("id_jenis"));
            $nama_jenis = $this->input->post("nama_jenis");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto_jenis"])){
            #----cek empty or not----#
                if($_FILES["foto_jenis"]){
                    $config['upload_path']          = './assets/core_img/icon_menu_jenis/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 200;
                    $config['file_name']            = hash("sha256", $id_layanan).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto_jenis");
                    if($upload_data["status"]){
                        $set["foto"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto_jenis"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }

                }
            }
            
            $set["nama_layanan"]=$nama_jenis;
            $set["waktu"]=$time_update;
            $set["id_admin"]=$id_admin;

            $where = array(
                "id_layanan"=>$id_layanan
            );

            if($this->mm->update_data("kesehatan_jenis", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }       
        }else{
            $msg_detail["nama_jenis"] = strip_tags(form_error('nama_jenis'));               
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_jenis_rs(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_jenis_rs(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_jenis_rs()){
            $id_layanan = $this->encrypt->decode($this->input->post("id_jenis"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_layanan"=>$id_layanan);

            if($this->mm->update_data("kesehatan_jenis", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#


#=================================================================================================#
#-------------------------------------------Poli--------------------------------------------------#
#=================================================================================================#

    public function index_poli(){
    	$data["list_poli"] = $this->mm->get_data_all_where("kesehatan_poli", array("is_delete"=>"0"));
    	
    	$this->load->view("ad_super/header");
    	$this->load->view("ad_super/kesehatan/v_poli", $data);
    	$this->load->view("ad_super/footer");
    }

    private function validate_input_poli(){
        $config_val_input = array(
                array(
                    'field'=>'nama_poli',
                    'label'=>'Nama Poli',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_poli(){
    	$detail_msg = array("nama_poli"=>"", "foto_poli"=>"");
    	$main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

    	if($this->validate_input_poli()){
    		$nama_poli = $this->input->post("nama_poli");
    		$time_update = date("Y-m-d h:i:s");
    		$id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

    		$insert = $this->ks->insert_poli($nama_poli, $time_update, $id_admin);
    		$id_poli = $insert->row_array()["id_poli"];

    		if($insert){
    			$config['upload_path']          = './assets/core_img/icon_menu/';
		        $config['allowed_types']        = "jpg|png";
		        $config['max_size']             = 200;
		        $config['file_name']            = hash("sha256", $id_poli).".jpg";
		       
		        $upload_data = $this->main_upload_file($config, "foto_poli");
		        // print_r($upload_data);
		        if($upload_data["status"]){
		        	$where = array("id_poli"=>$id_poli);
		        	$set = array("foto"=>$upload_data["main_data"]["upload_data"]["file_name"]);
		        	if($this->mm->update_data("kesehatan_poli", $set, $where)){
		        		$main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
		        	}else {
		        		$main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
		        	}
		        }else {
		        	$detail_msg["foto_poli"] = $this->response_message->get_error_msg("UPLOAD_FAIL");
		        	$main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
		        }
    		}
    	}else {
    		$detail_msg["nama_poli"] = strip_tags(form_error("nama_poli"));
    	}

    	$msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);

    	print_r(json_encode($msg_array));
    }


    public function get_poli_update(){
        $id = $this->encrypt->decode($this->input->post("id_poli"));
        $data = $this->mm->get_data_each("kesehatan_poli",array("id_poli"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_poli"=>$this->encrypt->encode($data["id_poli"]),
                                                "nama_poli"=>$data["nama_poli"],
                                                "foto_poli"=> base_url()."assets/core_img/icon_menu/".$data["foto"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_poli(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array(
                    "nama_poli"=>"",
                    "foto_poli"=>""
                );

        if($this->validate_input_poli()){
            $id_poli = $this->encrypt->decode($this->input->post("id_poli"));
            $nama_poli = $this->input->post("nama_poli");
            $admin_del = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto_poli"])){
            #----cek empty or not----#
                if($_FILES["foto_poli"]){
                    $config['upload_path']          = './assets/core_img/icon_menu/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 200;
                    $config['file_name']            = hash("sha256", $this->input->post("id_poli")).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto_poli");
                    if($upload_data["status"]){
                        $set["foto"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto_poli"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }

                }
            }
            
            $set["nama_poli"]=$nama_poli;
            $set["waktu"]=$time_update;
            $set["id_admin"]=$admin_del;

            $where = array(
                "id_poli"=>$id_poli
            );

            if($this->mm->update_data("kesehatan_poli", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }       
        }else{
            $msg_detail["nama_poli"] = strip_tags(form_error('nama_poli'));               
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_poli(){
        $config_val_input = array(
                array(
                    'field'=>'id_poli',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_poli(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_poli()){
            $id_poli = $this->encrypt->decode($this->input->post("id_poli"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_poli"=>$id_poli);

            if($this->mm->update_data("kesehatan_poli", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------Poli--------------------------------------------------#
#=================================================================================================#


#=================================================================================================#
#-------------------------------------------Rumah_Sakit-------------------------------------------#
#=================================================================================================#
    public function index_rs(){
    	$data["list_data_rs"] =  $this->ks->get_rs_all(array("b.is_delete"=>"0"));
        $data["list_data_poli"] = $this->mm->get_data_all_where("kesehatan_poli", array("is_delete"=>"0"));
        $data["list_data_jenis"] = $this->mm->get_data_all_where("kesehatan_jenis", array("is_delete"=>"0"));

        $this->load->view("ad_super/header");
    	$this->load->view("ad_super/kesehatan/v_rumahsakit", $data);
    	$this->load->view("ad_super/footer");
    }

    public function index_rs_new(){
        $this->load->view("ad_super/kesehatan/v_rs_new");
    }

    public function val_form_rs(){
        $config_val_input = array(
                array(
                    'field'=>'nama_rumah_sakit',
                    'label'=>'Nama Rumah Sakit',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'alamat',
                    'label'=>'Alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'telepon',
                    'label'=>'Telepon',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                ),
                array(
                    'field'=>'id_layanan',
                    'label'=>'Layanan',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'alpha_numeric'=>"%s ".$this->response_message->get_error_msg("NUMBER")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_rs(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_rs"=>"",
                    "alamat"=>"",
                    "telepon"=>"",
                    "id_layanan"=>"",
                    "foto"=>""
                );

        if($this->val_form_rs()){
            $nama_rs = $this->input->post("nama_rumah_sakit");
            $alamat = $this->input->post("alamat");
            $telepon = $this->input->post("telepon");
            $id_layanan = $this->input->post("id_layanan");
            $id_poli = str_replace("\"", "'", $this->input->post("id_poli"));

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");
            
            $insert = $this->db->query("select insert_rs('".$nama_rs."','".$alamat."','-','".$telepon."','".$id_layanan."',\"".$id_poli."\",'".$time_update."','".$id_admin."') as id_rs");

            if($insert){
                $config['upload_path']          = './assets/core_img/icon_menu_rs/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 512;
                $config['file_name']            = hash("sha256", $insert->row_array()["id_rs"]).".jpg";
                   
                $upload_data = $this->main_upload_file($config, "foto");
                
                if($upload_data["status"]){
                    $where = array("id_rs" => $insert->row_array()["id_rs"]);
                    $set = array("foto_rs" => $upload_data["main_data"]["upload_data"]["file_name"]);

                    if($this->mm->update_data("kesehatan_rs", $set, $where)){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else{
                    $detail_msg["foto_jenis"] = $upload_data["main_msg"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_rs"]      = strip_tags(form_error('nama_rumah_sakit'));
            $msg_detail["alamat"]       = strip_tags(form_error('alamat'));
            $msg_detail["telepon"]      = strip_tags(form_error('telepon'));
            $msg_detail["id_layanan"]   = strip_tags(form_error('id_layanan'));            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function get_rs_update(){
        $id = $this->encrypt->decode($this->input->post("id_rs"));
        $data = $this->ks->get_rs_each(array("id_rs"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_rs"=>$this->encrypt->encode($data["id_rs"]),
                                                "nama_rs"=>$data["nama_rumah_sakit"],
                                                "alamat"=>$data["alamat"],
                                                "telepon"=>$data["telepon"],
                                                "id_layanan"=>$data["id_layanan"],
                                                "id_poli"=>str_replace("'", "\"", $data["id_poli"]),
                                                "foto"=>$data["foto_rs"]
                                            );
        }

        print_r(json_encode($data_json));
    }

    public function update_rs(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "nama_rs"=>"",
                    "alamat"=>"",
                    "telepon"=>"",
                    "id_layanan"=>"",
                    "foto"=>""
                );

        // print_r("<pre>");
        // print_r($_POST);
        // print_r($_FILES);

        if($this->val_form_rs()){
            $id_rs      = $this->encrypt->decode($this->input->post("id_rs"));

            $nama_rs    = $this->input->post("nama_rumah_sakit");
            $alamat     = $this->input->post("alamat");
            $telepon    = $this->input->post("telepon");
            $id_layanan = $this->input->post("id_layanan");
            $id_poli    = str_replace("\"", "'", $this->input->post("id_poli"));

            $id_admin   = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");
            
            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto"])){
            #----cek empty or not----#
                if($_FILES["foto"]){
                    $config['upload_path']          = './assets/core_img/icon_menu_rs/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 512;
                    $config['file_name']            = hash("sha256", $id_rs).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto");
                    if($upload_data["status"]){
                        $set["foto_rs"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }
            }
            
            $set["nama_rumah_sakit"]=$nama_rs;
            $set["alamat"]=$alamat;
            $set["telepon"]=$telepon;

            $set["id_layanan"]=$id_layanan;
            $set["id_poli"]=$id_poli;
            $set["id_admin"]=$id_admin;
            $set["waktu"]=$time_update;

            $where = array(
                "id_rs"=>$id_rs
            );

            if($this->mm->update_data("kesehatan_rs", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            } 
            
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["nama_rs"]      = strip_tags(form_error('nama_rumah_sakit'));
            $msg_detail["alamat"]       = strip_tags(form_error('alamat'));
            $msg_detail["telepon"]      = strip_tags(form_error('telepon'));
            $msg_detail["id_layanan"]   = strip_tags(form_error('id_layanan'));            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    
    public function val_form_delete_rs(){
        $config_val_input = array(
                array(
                    'field'=>'id_rs',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_rs(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = "null";
        if($this->val_form_delete_rs()){
            $id_rs = $this->encrypt->decode($this->input->post("id_rs"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_rs"=>$id_rs);

            if($this->mm->update_data("kesehatan_rs", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=================================================================================================#
#-------------------------------------------Rumah_Sakit-------------------------------------------#
#=================================================================================================#

}
?>