<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Faskes extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Faskes_main", "fm");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("file_upload");
		$this->load->library("response_message");
        $this->load->library("Jsoncheck");

	}

    
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#
    private function main_upload_file($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#

#=================================================================================================#
#------------------------------------------Jenis--------------------------------------------------#
#=================================================================================================#

    public function index_jenis(){
        $data["list_jenis"] = $this->mm->get_data_all_where("faskes_jenis", array("is_delete"=>"0"));
        
        // print_r($data);
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/faskes/faskes_jenis", $data);
        $this->load->view("ad_super/footer");
    }

    private function validate_input_jenis(){
        $config_val_input = array(
                array(
                    'field'=>'ket_jenis',
                    'label'=>'ket_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_jenis(){
        $msg_detail = array("ket_jenis"=>"", "icon_32"=>"", "icon_64"=>"");
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_jenis()){
            $ket_jenis = $this->input->post("ket_jenis");

            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->fm->insert_jenis($ket_jenis, $time_update, $id_admin);
            if($insert){
                $set = array();

                $config['upload_path']          = './assets/core_img/icon_fk_jenis/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 512;
                $config['file_name']            = hash("sha256", $insert["id_jenis"])."_32.jpg";
                   
                $upload_data = $this->main_upload_file($config, "icon_32");
                
                if($upload_data["status"]){
                    $set["icon_32"] = $upload_data["main_data"]["upload_data"]["file_name"];
                }else{
                    $msg_detail["icon_32"] = $upload_data["msg_main"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }

                $config['upload_path']          = './assets/core_img/icon_fk_jenis/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 512;
                $config['file_name']            = hash("sha256", $insert["id_jenis"])."_64.jpg";
                   
                $upload_data = $this->main_upload_file($config, "icon_64");
                
                if($upload_data["status"]){
                    $set["icon_64"] = $upload_data["main_data"]["upload_data"]["file_name"];
                }else{
                    $msg_detail["icon_64"] = $upload_data["msg_main"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }

                $where = array("id_jenis" => $insert["id_jenis"]);

                if($this->mm->update_data("faskes_jenis", $set, $where)){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }else {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }

                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
            }            
        }else {
            $msg_detail["ket_jenis"] = strip_tags(form_error("ket_jenis"));
        }
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }


    public function get_jenis(){
        $id = $this->encrypt->decode($this->input->post("id_jenis"));
        $data = $this->mm->get_data_each("faskes_jenis",array("id_jenis"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_jenis"=>$this->encrypt->encode($data["id_jenis"]),
                                                "nama_jenis"=>$data["nama_jenis"],
                                                "icon_32"=>base_url()."assets/core_img/icon_fk_jenis/".$data["icon_32"],
                                                "icon_64"=>base_url()."assets/core_img/icon_fk_jenis/".$data["icon_64"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_jenis(){
        // print_r($_POST);
        $msg_detail = array("ket_jenis"=>"", "icon_32"=>"", "icon_64"=>"");
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));

        if($this->validate_input_jenis()){
            $ket_jenis = $this->input->post("ket_jenis");

            $id_jenis = $this->encrypt->decode($this->input->post("id_jenis"));

            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $set = array("nama_jenis"=>$ket_jenis,
                        "waktu"=>$time_update,
                        "id_admin"=>$id_admin
                        );
            // $update = $this->mm->update_data("faskes_jenis", $set, array("id_jenis"=>$id_jenis));
            // if($update){

            if(isset($_FILES["icon_32"])){
                $config['upload_path']          = './assets/core_img/icon_fk_jenis/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 512;
                $config['file_name']            = hash("sha256", $id_jenis)."_32.jpg";
                   
                $upload_data = $this->main_upload_file($config, "icon_32");
                
                if($upload_data["status"]){
                    $set["icon_32"] = $upload_data["main_data"]["upload_data"]["file_name"];
                }else{
                    $msg_detail["icon_32"] = $upload_data["msg_main"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }
                
            if(isset($_FILES["icon_64"])){
                $config['upload_path']          = './assets/core_img/icon_fk_jenis/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 512;
                $config['file_name']            = hash("sha256", $id_jenis)."_64.jpg";
                   
                $upload_data = $this->main_upload_file($config, "icon_64");
                
                if($upload_data["status"]){
                    $set["icon_64"] = $upload_data["main_data"]["upload_data"]["file_name"];
                }else{
                    $msg_detail["icon_64"] = $upload_data["msg_main"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }

            $where = array("id_jenis" => $id_jenis);

            if($this->mm->update_data("faskes_jenis", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }else {
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
            }
            // }            
        }else {
            $msg_detail["ket_jenis"] = strip_tags(form_error("ket_jenis"));
        }
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
    

    public function val_form_delete_jenis(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_jenis(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_jenis()){
            $id_jenis = $this->encrypt->decode($this->input->post("id_jenis"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del,
                    "id_admin"=>$id_admin
                );

            $where = array("id_jenis"=>$id_jenis);

            if($this->mm->update_data("faskes_jenis", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------faskes------------------------------------------------#
#=================================================================================================#
    public function index_faskes(){
        $data["list_faskes"]    = $this->fm->get_faskes_all(array("pm.is_delete"=>"0"));
        $data["jenis_faskes"]   = $this->mm->get_data_all_where("faskes_jenis", array("is_delete"=>"0"));

        // print_r("<pre>");
        // print_r($data);

        $this->load->view("ad_super/header");
        $this->load->view("ad_super/faskes/faskes_main", $data);
        $this->load->view("ad_super/footer");
    }

    public function val_form_faskes(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'id_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nama_faskes',
                    'label'=>'nama_faskes',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'lokasi',
                    'label'=>'lokasi',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'alamat',
                    'label'=>'alamat',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'tlp',
                    'label'=>'tlp',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'website',
                    'label'=>'website',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_faskes(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_jenis"=>"",
                    "nama_faskes"=>"",
                    "lokasi"=>"",
                    "alamat"=>"",
                    "tlp"=>"",
                    "website"=>""
                );

        if($this->val_form_faskes()){
            $id_jenis       = $this->input->post("id_jenis");
            $nama_faskes    = $this->input->post("nama_faskes");
            $lokasi         = $this->input->post("lokasi");
            $alamat         = $this->input->post("alamat");
            $tlp            = $this->input->post("tlp");
            $website        = $this->input->post("website");

            $json_detail = "{'alamat': '".$alamat."','url': '".$website."','tlp': '".$tlp."'}";

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");
            
            $insert = $this->fm->insert_faskes($id_jenis, $nama_faskes, "", $lokasi, $json_detail, $time_update, $id_admin);

            if($insert){
                // print_r($_POST);
                $config['upload_path']          = './assets/core_img/gmbr_faskes/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 512;
                $config['file_name']            = hash("sha256", $insert["id_faskes"]).".jpg";
                   
                $upload_data = $this->main_upload_file($config, "foto_faskes");
                
                if($upload_data["status"]){
                    $where = array("id_faskes" => $insert["id_faskes"]);
                    $set = array("foto_faskes" => $upload_data["main_data"]["upload_data"]["file_name"]);

                    if($this->mm->update_data("faskes_main", $set, $where)){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else{
                    $detail_msg["foto_faskes"] = $upload_data["main_msg"]["error"];
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_jenis"]     = strip_tags(form_error('id_jenis'));
            $msg_detail["nama_faskes"]  = strip_tags(form_error('nama_faskes'));
            $msg_detail["lokasi"]       = strip_tags(form_error('lokasi'));
            $msg_detail["alamat"]       = strip_tags(form_error('alamat'));
            $msg_detail["tlp"]          = strip_tags(form_error('tlp'));
            $msg_detail["website"]      = strip_tags(form_error('website'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function get_faskes_update(){
        $id = $this->encrypt->decode($this->input->post("id_faskes"));
        $data = $this->fm->get_faskes_all(array("id_faskes"=>$id));
        // print_r($data);

        // print_r($id);
        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;

            $data_json["val_response"] = array("id_faskes"=>$this->encrypt->encode($data[0]->id_faskes),
                                                "id_jenis"=>$data[0]->id_jenis,
                                                "nama_faskes"=>$data[0]->nama_faskes,
                                                "foto_faskes"=>base_url()."assets/core_img/gmbr_faskes/".$data[0]->foto_faskes,
                                                "lokasi"=>$data[0]->lokasi,
                                                "detail_faskes"=>str_replace("'", "\"", $data[0]->detail_faskes)
                                            );
        }
        print_r(json_encode($data_json));
    }


    public function update_faskes(){

        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_jenis"=>"",
                    "nama_faskes"=>"",
                    "lokasi"=>"",
                    "alamat"=>"",
                    "tlp"=>"",
                    "website"=>""
                );

        if($this->val_form_faskes()){
            $id_faskes      = $this->encrypt->decode($this->input->post("id_faskes"));

            $id_jenis       = $this->input->post("id_jenis");
            $nama_faskes    = $this->input->post("nama_faskes");
            $lokasi         = $this->input->post("lokasi");
            $alamat         = $this->input->post("alamat");
            $tlp            = $this->input->post("tlp");
            $website        = $this->input->post("website");

            $json_detail = "{'alamat': '".$alamat."','url': '".$website."','tlp': '".$tlp."'}";

            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");
            
            $set = array();

            #----cek isset image avail or not----#
            if(isset($_FILES["foto_faskes"])){
            #----cek empty or not----#
                if($_FILES["foto_faskes"]){
                    $config['upload_path']          = './assets/core_img/gmbr_faskes/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 512;
                    $config['file_name']            = hash("sha256", $id_faskes).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto_faskes");
                    
                    if($upload_data["status"]){
                        $set["foto_faskes"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto_faskes"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }
            }


            $set["id_jenis"]        = $id_jenis;
            $set["nama_faskes"]     = $nama_faskes;
            $set["lokasi"]          = $lokasi;
            $set["detail_faskes"]   = $json_detail;

            $set["id_admin"]        = $id_admin;
            $set["waktu"]           = $time_update;


            $where = array(
                "id_faskes"=>$id_faskes
            );

            // print_r($set);
            // print_r($where);

            if($this->mm->update_data("faskes_main", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            } 
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_jenis"]     = strip_tags(form_error('id_jenis'));
            $msg_detail["nama_faskes"]  = strip_tags(form_error('nama_faskes'));
            $msg_detail["lokasi"]       = strip_tags(form_error('lokasi'));
            $msg_detail["alamat"]       = strip_tags(form_error('alamat'));
            $msg_detail["tlp"]          = strip_tags(form_error('tlp'));
            $msg_detail["website"]      = strip_tags(form_error('website'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }


    public function val_form_delete_faskes(){
        $config_val_input = array(
                array(
                    'field'=>'id_faskes',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_faskes(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        $msg_detail = "null";
        if($this->val_form_delete_faskes()){
            $id_faskes = $this->encrypt->decode($this->input->post("id_faskes"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_faskes"=>$id_faskes);

            if($this->mm->update_data("faskes_main", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#=================================================================================================#
#-------------------------------------------faskes------------------------------------------------#
#=================================================================================================#

}
?>