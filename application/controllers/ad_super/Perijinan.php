<?php 
defined('BASEPATH') OR exit('No direct script access allowed');



class Perijinan extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Perijinan_main", "pm");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("file_upload");
		$this->load->library("response_message");

        $session = $this->session->userdata("admin_lv_1");
        if(isset($session)){
            if($session["status_active"] != "1" and $session["is_log"] != "1"){
                redirect(base_url("back-admin/login"));
            }
        }else{
            redirect(base_url("back-admin/login"));
        }
	}

    public function un_link(){
        $config['upload_path']          = './assets/core_img/icon_menu_jenis/';
        $config['file_name']            = "2518ffca8740277c52d7f4f7e4ee55bc203a09eeb79e72fde94270e20f688047.jpg";
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }

    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#
    private function main_upload_file($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }
#=================================================================================================#
#-------------------------------------------main_upload_file--------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#

    public function index_kategori_perijinan(){
        $data["list_kategori_perijinan"] = $this->pm->get_kategori(array("k.is_delete"=>"0"));
        $data["list_jenis_ijin"]    = $this->mm->get_data_all_where("ijin_jenis", array("is_delete"=>"0"));
        
        
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/perijinan/ijin_kategori", $data);
        $this->load->view("ad_super/footer");
    }

    private function validate_input_kategori_perijinan(){
        $config_val_input = array(
                array(
                    'field'=>'ket_kategori',
                    'label'=>'Keterangan Kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_kategori_perijinan(){
        $detail_msg = array("ket_kategori"=>"", "foto_kategori"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_kategori_perijinan()){
            $id_jenis = $this->input->post("jenis");
            $ket_kategori = $this->input->post("ket_kategori");
            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->pm->insert_kategori($ket_kategori, $time_update, $id_admin, $id_jenis)->row_array();
            if($insert){
                $config['upload_path']          = './assets/core_img/icon_ij_kategori/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 200;
                $config['file_name']            = hash("sha256", $insert["id_kategori"]).".jpg";
                   
                $upload_data = $this->main_upload_file($config, "foto_kategori");
                
                if($upload_data["status"]){
                    $where = array("id_kategori"=>$insert["id_kategori"]);
                    $set = array("foto_kategori"=>$upload_data["main_data"]["upload_data"]["file_name"]);

                    if($this->mm->update_data("ijin_kategori", $set, $where)){
                        $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else{
                    $detail_msg["foto_kategori"] = $upload_data["main_msg"]["error"];
                    $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }            
        }else {
            $detail_msg["ket_kategori"] = strip_tags(form_error("ket_kategori"));
        }
        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        print_r(json_encode($msg_array));
    }


    public function get_kategori_perijinan(){
        $id = $this->encrypt->decode($this->input->post("id_kategori"));
        $data = $this->mm->get_data_each("ijin_kategori",array("id_kategori"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_kategori"=>$this->encrypt->encode($data["id_kategori"]),
                                                "ket_kategori"=>$data["ket_kategori"],
                                                "id_jenis"=>$data["id_jenis"],
                                                "foto_kategori"=> base_url()."assets/core_img/icon_ij_kategori/".$data["foto_kategori"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_kategori_perijinan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array("ket_kategori"=>"", "foto_kategori"=>"");

        // print_r($_POST);

        if($this->validate_input_kategori_perijinan()){
            $id_kategori = $this->encrypt->decode($this->input->post("id_kategori"));
            $id_jenis = $this->encrypt->decode($this->input->post("id_jenis"));
            $ket_kategori = $this->input->post("ket_kategori");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto_kategori"])){
            #----cek empty or not----#
                if($_FILES["foto_kategori"]){
                    $config['upload_path']          = './assets/core_img/icon_ij_kategori/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 200;
                    $config['file_name']            = hash("sha256", $id_kategori).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto_kategori");
                    if($upload_data["status"]){
                        $set["foto_kategori"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto_kategori"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }

                }
            }
            
            $set["ket_kategori"]=$ket_kategori;
            $set["waktu"]=$time_update;
            $set["id_admin"]=$id_admin;

            $where = array(
                "id_kategori"=>$id_kategori
            );

            if($this->mm->update_data("ijin_kategori", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }       
        }else{
            $msg_detail["ket_kategori"] = strip_tags(form_error('ket_kategori'));               
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_kategori_perijinan(){
        $config_val_input = array(
                array(
                    'field'=>'id_kategori',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_kategori_perijinan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_kategori_perijinan()){
            $id_kategori = $this->encrypt->decode($this->input->post("id_kategori"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_kategori"=>$id_kategori);

            if($this->mm->update_data("ijin_kategori", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------sub_Kategori------------------------------------------#
#=================================================================================================#

    public function index_sub_kategori_perijinan(){
        $data["list_sub_kategori_perijinan"] = $this->pm->get_sub_kategori(array("sk.is_delete"=>"0"));
        $data["list_kategori_perijinan"] = $this->pm->get_kategori(array("k.is_delete"=>"0"));
        
        
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/perijinan/ijin_sub_kategori", $data);
        $this->load->view("ad_super/footer");
    }

    private function validate_input_sub_kategori_perijinan(){
        $config_val_input = array(
                array(
                    'field'=>'ket_sub_kategori',
                    'label'=>'Keterangan sub_Kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


    public function insert_sub_kategori_perijinan(){
        $detail_msg = array("ket_sub_kategori"=>"", "foto_sub"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_sub_kategori_perijinan()){
            $id_kategori = $this->input->post("jenis");
            $ket_sub_kategori = $this->input->post("ket_sub_kategori");
            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->pm->insert_sub_kategori($id_kategori, $ket_sub_kategori, $time_update, $id_admin)->row_array();
            if($insert){
                $config['upload_path']          = './assets/core_img/icon_ij_sub_kategori/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 200;
                $config['file_name']            = hash("sha256", $insert["id_sub"]).".jpg";
                   
                $upload_data = $this->main_upload_file($config, "foto_sub");
                
                if($upload_data["status"]){
                    $where = array("id_sub"=>$insert["id_sub"]);
                    $set = array("foto_sub"=>$upload_data["main_data"]["upload_data"]["file_name"]);

                    if($this->mm->update_data("ijin_sub_kategori", $set, $where)){
                        $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else{
                    $detail_msg["foto_sub"] = $upload_data["main_msg"]["error"];
                    $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }            
        }else {
            $detail_msg["ket_sub_kategori"] = strip_tags(form_error("ket_sub_kategori"));
        }
        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        print_r(json_encode($msg_array));
    }


    public function get_sub_kategori_perijinan(){
        $id = $this->encrypt->decode($this->input->post("id_sub_kategori"));
        $data = $this->mm->get_data_each("ijin_sub_kategori",array("id_sub"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_sub_kategori"=>$this->encrypt->encode($data["id_sub"]),
                                                "ket_sub"=>$data["ket_sub"],
                                                "id_kategori"=>$data["id_kategori"],
                                                "foto_sub"=> base_url()."assets/core_img/icon_ij_sub_kategori/".$data["foto_sub"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_sub_kategori_perijinan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array("ket_sub_kategori"=>"", "foto_sub"=>"");

        // print_r($_POST);

        if($this->validate_input_sub_kategori_perijinan()){
            $id_sub_kategori = $this->encrypt->decode($this->input->post("id_sub_kategori"));
            $id_kategori = $this->input->post("jenis");
            $ket_sub_kategori = $this->input->post("ket_sub_kategori");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto_sub"])){
            #----cek empty or not----#
                if($_FILES["foto_sub"]){
                    $config['upload_path']          = './assets/core_img/icon_ij_sub_kategori/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 200;
                    $config['file_name']            = hash("sha256", $id_sub_kategori).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto_sub");
                    if($upload_data["status"]){
                        $set["foto_sub"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto_sub"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }

                }
            }
            
            $set["id_kategori"]=$id_kategori;
            $set["ket_sub"]=$ket_sub_kategori;
            $set["time_update"]=$time_update;
            $set["id_admin"]=$id_admin;

            $where = array(
                "id_sub"=>$id_sub_kategori
            );

            if($this->mm->update_data("ijin_sub_kategori", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }       
        }else{
            $msg_detail["ket_sub_kategori"] = strip_tags(form_error('ket_sub_kategori'));               
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_sub_kategori_perijinan(){
        $config_val_input = array(
                array(
                    'field'=>'id_sub_kategori',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_sub_kategori_perijinan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_sub_kategori_perijinan()){
            $id_sub_kategori = $this->encrypt->decode($this->input->post("id_sub_kategori"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "time_update"=>$time_del
                );

            $where = array("id_sub"=>$id_sub_kategori);

            if($this->mm->update_data("ijin_sub_kategori", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------sub_Kategori------------------------------------------#
#=================================================================================================#


#=================================================================================================#
#-------------------------------------------jenis-------------------------------------------------#
#=================================================================================================#

    public function index_jenis_perijinan(){
        $data["list_jenis_ijin"]    = $this->mm->get_data_all_where("ijin_jenis", array("is_delete"=>"0"));
        
        
        $this->load->view("ad_super/header");
        $this->load->view("ad_super/perijinan/ijin_jenis", $data);
        $this->load->view("ad_super/footer");
    }

    private function validate_input_jenis_ijin(){
        $config_val_input = array(
                array(
                    'field'=>'ket_jenis',
                    'label'=>'Keterangan jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_jenis_perijinan(){
        $detail_msg = array("ket_jenis"=>"", "foto_jenis"=>"");
        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));

        if($this->validate_input_jenis_ijin()){
            $id_jenis = $this->input->post("jenis");
            $ket_jenis = $this->input->post("ket_jenis");
            $time_update = date("Y-m-d h:i:s");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);

            $insert = $this->pm->insert_jenis($ket_jenis, $time_update, $id_admin)->row_array();
            if($insert){
                $config['upload_path']          = './assets/core_img/icon_ij_jenis/';
                $config['allowed_types']        = "jpg|png";
                $config['max_size']             = 200;
                $config['file_name']            = hash("sha256", $insert["id_jenis"]).".jpg";
                   
                $upload_data = $this->main_upload_file($config, "foto_jenis");
                
                if($upload_data["status"]){
                    $where = array("id_jenis"=>$insert["id_jenis"]);
                    $set = array("foto_jenis"=>$upload_data["main_data"]["upload_data"]["file_name"]);

                    if($this->mm->update_data("ijin_jenis", $set, $where)){
                        $main_msg = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }else {
                        $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }
                }else{
                    $detail_msg["foto_jenis"] = $upload_data["main_msg"]["error"];
                    $main_msg = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                }
            }            
        }else {
            $detail_msg["ket_jenis"] = strip_tags(form_error("ket_jenis"));
        }
        $msg_array = $this->response_message->default_mgs($main_msg,$detail_msg);
        print_r(json_encode($msg_array));
    }


    public function get_jenis_perijinan(){
        $id = $this->encrypt->decode($this->input->post("id_jenis"));
        $data = $this->mm->get_data_each("ijin_jenis",array("id_jenis"=>$id));

        $data_json["status"] = false;
        $data_json["val_response"] = null;
        if(!empty($data)){
            $data_json["status"] = true;
            $data_json["val_response"] = array("id_jenis"=>$this->encrypt->encode($data["id_jenis"]),
                                                "ket_jenis"=>$data["ket_jenis"],
                                                "foto_jenis"=> base_url()."assets/core_img/icon_ij_jenis/".$data["foto_jenis"]);
        }

        // print_r($data_json);
        print_r(json_encode($data_json));
    }

    public function update_jenis_perijinan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPDATE_FAIL"));
        $msg_detail = array("ket_jenis"=>"", "foto_jenis"=>"");

        // print_r($_POST);

        if($this->validate_input_jenis_ijin()){
            $id_jenis = $this->encrypt->decode($this->input->post("id_jenis"));
            $ket_jenis = $this->input->post("ket_jenis");
            $id_admin = $this->encrypt->decode($this->session->userdata("admin_lv_1")["id_admin"]);
            $time_update = date("Y-m-d h:i:s");

            $set = array();
            
            $status_foto_upload = false;
            #----cek isset image avail or not----#
            if(isset($_FILES["foto_jenis"])){
            #----cek empty or not----#
                if($_FILES["foto_jenis"]){
                    $config['upload_path']          = './assets/core_img/icon_ij_jenis/';
                    $config['allowed_types']        = "jpg|png";
                    $config['max_size']             = 200;
                    $config['file_name']            = hash("sha256", $id_jenis).".jpg";
                       
                    $upload_data = $this->main_upload_file($config, "foto_jenis");
                    if($upload_data["status"]){
                        $set["foto_jenis"] = $config["file_name"];
                        $status_foto_upload = true;
                    }else{
                        $detail_msg["foto_jenis"] = $upload_data["main_msg"]["error"];
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("UPLOAD_FAIL"));
                    }

                }
            }
            
            $set["ket_jenis"]=$ket_jenis;
            $set["waktu"]=$time_update;
            $set["id_admin"]=$id_admin;

            $where = array(
                "id_jenis"=>$id_jenis
            );

            if($this->mm->update_data("ijin_jenis", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
            }       
        }else{
            $msg_detail["ket_jenis"] = strip_tags(form_error('ket_jenis'));               
        }
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    

    public function val_form_delete_jenis_perijinan(){
        $config_val_input = array(
                array(
                    'field'=>'id_jenis',
                    'label'=>'id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function delete_jenis_perijinan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("DELETE_FAIL"));
        if($this->val_form_delete_jenis_perijinan()){
            $id_jenis = $this->encrypt->decode($this->input->post("id_jenis"));

            $is_del = "1";
            $time_del = date("Y-m-d h:i:s");

            $set = array(
                    "is_delete"=>$is_del,
                    "waktu"=>$time_del
                );

            $where = array("id_jenis"=>$id_jenis);

            if($this->mm->update_data("ijin_jenis", $set, $where)){
                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, "null");
        print_r(json_encode($res_msg));
    }  
#=================================================================================================#
#-------------------------------------------jenis-------------------------------------------------#
#=================================================================================================#

}
?>