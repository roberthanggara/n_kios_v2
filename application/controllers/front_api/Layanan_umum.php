<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_umum extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Kesehatan_main", "ks");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("response_message");
	}
    

#=================================================================================================#
#-------------------------------------------Halaman_Menu------------------------------------------#
#=================================================================================================#
    public function get_data_layanan(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = null;

        $data_layanan =  $this->mm->get_data_all_where("home_page_kategori",array("is_delete"=>"0"));

        if($data_layanan){
            $array_data_menu= array();
            foreach ($data_layanan as $r_data_layanan => $v_data_layanan) {
                $data_menu = $this->mm->get_data_all_where("home_page_main",array("is_delete"=>"0", "id_kategori"=>$v_data_layanan->id_kategori));

                $array_data_menu[$v_data_layanan->id_kategori]["kategori"] 
                    = array("id_kategori"=>$this->encrypt->encode($v_data_layanan->id_kategori),
                            "nama_kategori"=>$v_data_layanan->nama_kategori);

                $r=0;
                foreach ($data_menu as $r_data_menu => $v_data_menu) {
                    $array_data_menu[$v_data_layanan->id_kategori]["list_menu"][$r] = array(
                                                                    "id_page"=>hash("sha512", $v_data_menu->id_page),
                                                                    "nama_page"=>$v_data_menu->nama_page,
                                                                    "foto_page"=>$v_data_menu->foto_page,
                                                                    "next_page"=>$v_data_menu->next_page
                                                                );
                    $r++;    
                }
            }

            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            $msg_detail["item"] = $array_data_menu;
            $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu_layanan/";   
        }
        
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }

    private function get_page(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_page(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = null;

        $msg_detail["list_menu"] = array();

        if($this->get_page()){
            $id_layanan = $this->input->post("id_layanan");
            $data_layanan =  $this->mm->get_data_each("home_page_main", array("sha2(id_page, '512')="=>$id_layanan,"is_delete"=>"0"));

            $temp = array();
            if($data_layanan){
                $temp = array(
                    "id_page"=>hash("sha512", $data_layanan["id_page"]),
                    "nama_page"=>$data_layanan["nama_page"],
                    "next_page"=>$data_layanan["next_page"]
                );
            }

            $msg_detail["list_menu"] = $temp;
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            // print_r($data_layanan);
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------Halaman_Menu------------------------------------------#
#=================================================================================================#
}
?>