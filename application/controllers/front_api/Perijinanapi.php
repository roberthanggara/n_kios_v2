<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Perijinanapi extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model("main/mainmodel", "mm");
        $this->load->model("Perijinan_main", "im");
        
        $this->load->library("encrypt");

        $this->load->library("get_identity");
        $this->load->library("response_message");
    }
    

#=================================================================================================#
#-------------------------------------------ijin_jenis--------------------------------------------#
#=================================================================================================#
    private function validate_post_get_ijin_jenis(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id Layanan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_ijin_jenis(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("id_layanan"=>"");

        if($this->validate_post_get_ijin_jenis()){
            $id_layanan = $this->input->post("id_layanan");
            $data = $this->im->get_jenis_api();

            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
            $msg_detail["id_layanan"] = $id_layanan;
            $msg_detail["item"] = $data;
            $msg_detail["url_core"] = base_url()."assets/core_img/icon_ij_jenis/";
        }
      
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------ijin_jenis--------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------ijin_kategori-----------------------------------------#
#=================================================================================================#
    private function validate_post_get_ijin_kategori(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'id_jenis',
                    'label'=>'Id Jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_ijin_kategori(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("id_jenis"=>"", "id_layanan"=>""); 

        if($this->validate_post_get_ijin_kategori()){
            $id_jenis = $this->input->post("id_jenis");
            $id_layanan = $this->input->post("id_layanan");

            $data = $this->im->get_kategori_api(array("sha2(k.id_jenis, '512')="=>$id_jenis));

            if($data){
                $list_jenis = $this->mm->get_data_each("ijin_jenis", array("sha2(id_jenis, '512')="=>$id_jenis));

                if($list_jenis){
                    $data_jenis = array("id_jenis"=>hash("sha512", $list_jenis["id_jenis"]),
                                    "ket_jenis"=> $list_jenis["ket_jenis"]);

                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["jenis"] = $data_jenis;
                    $msg_detail["layanan"] = $id_layanan;
                    $msg_detail["item"] = $data;
                    $msg_detail["url_core"] = base_url()."assets/core_img/icon_ij_kategori/";
                }
            }
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------ijin_kategori-----------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------ijin_sub_kategori-------------------------------------#
#=================================================================================================#
    private function validate_post_get_ijin_sub_kategori(){
        $config_val_input = array(
                array(
                    'field'=>'id_layanan',
                    'label'=>'Id',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'id_jenis',
                    'label'=>'Id Jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                ),array(
                    'field'=>'id_kategori',
                    'label'=>'Id kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function get_data_ijin_sub_kategori(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("id_jenis"=>"", "id_layanan"=>"", "id_kategori"=>""); 

        if($this->validate_post_get_ijin_sub_kategori()){
            $id_jenis = $this->input->post("id_jenis");
            $id_kategori = $this->input->post("id_kategori");
            $id_layanan = $this->input->post("id_layanan");

            $data = $this->im->get_sub_kategori_api(array("sha2(s.id_kategori, '512')="=>$id_kategori));

            if($data){
                $list_jenis     = $this->mm->get_data_each("ijin_jenis", array("sha2(id_jenis, '512')="=>$id_jenis));
                $list_kategori  = $this->mm->get_data_each("ijin_kategori", array("sha2(id_kategori, '512')="=>$id_kategori));

                if($list_jenis){
                    $data_jenis = array("id_jenis"=>hash("sha512", $list_jenis["id_jenis"]),
                                    "ket_jenis"=> $list_jenis["ket_jenis"]);

                    $data_kategori = array("id_kategori"=>hash("sha512", $list_kategori["id_kategori"]),
                                    "ket_kategori"=> $list_kategori["ket_kategori"]);

                    $msg_main               = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                    $msg_detail["jenis"]    = $data_jenis;
                    $msg_detail["kategori"] = $data_kategori;
                    $msg_detail["layanan"]  = $id_layanan;
                    $msg_detail["item"]     = $data;
                    $msg_detail["url_core"] = base_url()."assets/core_img/icon_ij_sub_kategori/";
                }
            }
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------ijin_sub_kategori-------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------insert_antrian----------------------------------------#
#=================================================================================================#
    private function validate_insert_antrian(){
        $config_val_input = array(
                array(
                    'field'=>'nik',
                    'label'=>'Nomor Induk ijin',
                    'rules'=>'required|exact_length[16]|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'exact_length'=>"%s 16 ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s n ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'nama',
                    'label'=>'Nama Anda',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'waktu',
                    'label'=>'Tanggal Pendaftaran',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_layanan',
                    'label'=>'id_layanan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_jenis',
                    'label'=>'id_jenis',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_kategori',
                    'label'=>'id_kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'id_sub_kategori',
                    'label'=>'id_sub_kategori',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'ip_lan',
                    'label'=>'ip_lan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),array(
                    'field'=>'ip_public',
                    'label'=>'ip_public',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_antrian(){
        // print_r("<pre>");
        // print_r($_POST);
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array("nik"=>"","nama"=>"","waktu"=>"","id_layanan"=>"","id_jenis"=>"","id_kategori"=>""); 

        if($this->validate_insert_antrian()){
            $nik    = $this->input->post("nik");
            $nama   = $this->input->post("nama");
            $waktu  = $this->input->post("waktu");
            
            $id_jenis           = $this->input->post("id_jenis");
            $id_layanan         = $this->input->post("id_layanan");
            $id_kategori        = $this->input->post("id_kategori");
            $id_sub_kategori    = $this->input->post("id_sub_kategori");

            $ip_lan     = $this->input->post("ip_lan");
            $ip_public  = $this->input->post("ip_public");

            $time_add = date("Y-m-d H:i:s");

            #----------get_identity--------------
            $msg_detail["data_response"]["data_identity"] = array(
                                                                "nik"=>$nik,
                                                                "nama"=>$nama,
                                                                "time_add"=>$time_add,
                                                                "time_book"=>$waktu
                                                            );

            #----------get_layanan--------------
            $data_layanan = $this->mm->get_data_each("home_page_main", array("sha2(id_page, '512')="=>$id_layanan));
            $msg_detail["data_response"]["data_layanan"] = array("id_page"=>hash("sha512", $data_layanan["id_page"]), 
                                                            "nama_page"=>$data_layanan["nama_page"],
                                                            "alamat"=>"Jl. Mayjen Sungkono, Arjowinangun, Kedungkandang, Kota Malang");

            #----------get_jenis---------
            $data_jenis = $this->mm->get_data_each("ijin_jenis", array("sha2(id_jenis, '512')="=>$id_jenis));
            $msg_detail["data_response"]["data_jenis"] = array("id_jenis"=>hash("sha512", $data_jenis["id_jenis"]), 
                                                            "nama_jenis"=>$data_jenis["ket_jenis"]);

            #----------get_kategori------------
            $data_kategori = $this->mm->get_data_each("ijin_kategori", array("sha2(id_kategori, '512')="=>$id_kategori));
            $msg_detail["data_response"]["data_kategori"] = array("id_kategori"=>hash("sha512", $data_kategori["id_kategori"]), 
                                                            "nama_kategori"=>$data_kategori["ket_kategori"]);

            #----------get_sub_kategori------------
            $data_sub_kategori = $this->mm->get_data_each("ijin_sub_kategori", array("sha2(id_sub, '512')="=>$id_sub_kategori));
            $msg_detail["data_response"]["data_sub_kategori"] = array("id_sub_kategori"=>hash("sha512", $data_sub_kategori["id_sub"]), 
                                                            "nama_sub_kategori"=>$data_sub_kategori["ket_sub"]);

            #----------set_ip--------------
            $msg_detail["data_response"]["set_ip"] = array("ip_public"=>$ip_public, 
                                                            "ip_lan"=>$ip_lan);

            
            #----------date_daftar---------
                        if($waktu > date("Y-m-d") || $waktu == date("Y-m-d")){
            #----------cek_device_antrian_register------
                            $where_device = array("ip_lan"=>$ip_lan, "ip_public"=>$ip_public);
                            $check_device = $this->mm->get_data_each("device", $where_device);
                            if($check_device){
                                $count_antrian = $this->mm->get_data_each("ijin_antrian", array());
            #----------cek_kuota_antrian_hari_ini-------
                                $insert = $this->db->query("SELECT insert_ij_antrian('".$nik."',
                                                                            '".$data_layanan["id_page"]."',
                                                                            '".$data_jenis["id_jenis"]."',
                                                                            '".$data_kategori["id_kategori"]."',
                                                                            '".$data_sub_kategori["id_sub"]."',
                                                                            '".$time_add."',
                                                                            '".$waktu."',
                                                                            '".$time_add."',
                                                                            '0',
                                                                            'A-002') AS insert_antrian")->row_array();

                                $msg_detail["data_response"]["data_identity"]["no_antrean"] = 'A-002';
            #----------jika_tersedia_maka_insert-------- 
            #----------jika_tidak_maka_send_response_fail--------
            
                                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
                            }
                        }
                    
            
            #----------send_response-------
            
        }else {
            $msg_detail["nik"]      = strip_tags(form_error("nik"));
            $msg_detail["nama"]     = strip_tags(form_error("nama"));
            $msg_detail["waktu"]    = strip_tags(form_error("waktu"));
            $msg_detail["id_layanan"] = strip_tags(form_error("id_layanan"));
            $msg_detail["id_jenis"]    = strip_tags(form_error("id_jenis"));
            $msg_detail["id_kategori"]  = strip_tags(form_error("id_kategori"));
            $msg_detail["id_sub_kategori"]  = strip_tags(form_error("id_sub_kategori"));

            $msg_detail["ip_lan"]    = strip_tags(form_error("ip_lan"));
            $msg_detail["ip_public"]  = strip_tags(form_error("ip_public"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------insert_antrian----------------------------------------#
#=================================================================================================#


}
?>