<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Faskesapi extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model("main/mainmodel", "mm");
		$this->load->model("Faskes_main", "pm");
		
        $this->load->library("encrypt");

		$this->load->library("get_identity");
		$this->load->library("response_message");
	}

#=================================================================================================#
#-------------------------------------------index_penddidikan_faskes-----------------------------#
#=================================================================================================#
    public function index_faskes_home($id_jenis){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $data_send = array();
        $faskes_main = $this->pm->get_faskes_all(array("sha2(pm.id_jenis, \"256\")="=>$id_jenis,"pm.is_delete"=>"0"));

        $msg_detail["item"] = $faskes_main;
        $msg_detail["url_core"] = base_url()."assets/core_img/icon_fk_jenis/";
        if($msg_detail["item"]){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }

    public function get_data_all(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $array_send = array();
        $data_jenis = $this->pm->get_data_jenis_all();
        foreach ($data_jenis as $key => $value) {
            $data_main = $this->pm->get_data_main_all_encrypt(array("sha2(fj.id_jenis, '512')="=>$value->id_jenis));
            $array_send[$value->id_jenis]["info"] = $value;
            $array_send[$value->id_jenis]["item"] = $data_main;

            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        // print_r();

        $msg_detail["item"] = $array_send;
        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------index_penddidikan_faskes-----------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------index_penddidikan_detail------------------------------#
#=================================================================================================#
    public function get_faskes_detail($id_faskes){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $faskes_main = $this->pm->get_faskes_all(array("sha2(pm.id_faskes, \"256\")="=>$id_faskes,"pm.is_delete"=>"0"));

        $msg_detail["item"] = $faskes_main;
        $msg_detail["url_core"] = base_url()."assets/core_img/icon_fk_jenis/";
                
        if($msg_detail["item"]){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------index_penddidikan_detail------------------------------#
#=================================================================================================#



#=================================================================================================#
#-------------------------------------------index_penddidikan_univ--------------------------------#
#=================================================================================================#
    public function index_Faskes_univ(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        $data_send = array();
        $kec = $this->mm->get_data_all_where("master_kecamatan", array("is_delete"=>"0"));

        $no = 0;
        foreach ($kec as $key => $value) {
            $data_send[$no]["kec"]["id_kec"]    = $this->encrypt->encode($value->id_kecamatan);
            $data_send[$no]["kec"]["nama_kec"]  = $value->nama_kecamatan;

            $data_send[$no]["item"] = $this->pm->get_sch_api(array("ps.id_kecamatan"=>$value->id_kecamatan, "pj.id_strata"=> "PDST1002", "ps.is_delete"=>"0"));
            // $data_send[$no]["item"] = $this->pm->get_sch_api(array("ps.id_kecamatan"=>$value->id_kecamatan, "ps.is_delete"=>"0"));

            $msg_detail["item"] = $data_send;
            $msg_detail["url_core"] = base_url()."assets/core_img/icon_menu_jenis/";
            $no++;
        }

        
        if($msg_detail["item"]){
            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
        }

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
    }
#=================================================================================================#
#-------------------------------------------index_penddidikan_univ--------------------------------#
#=================================================================================================#

    public function cek(){
        echo hash("sha256", "PDST1002");
        print_r("<br>");
        echo hash("sha256", "PDST1003");
        print_r("<br>");
        echo hash("sha256", "PDST1008");
    }

}
?>