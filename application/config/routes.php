<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['back-admin/logout'] = "ad_super/Adminlogout/logout";

$route['back-admin/login'] = "ad_super/adminlogin/index";
$route['back-admin/auth'] = "ad_super/adminlogin/auth";

#---------------------------------------------Admin--------------------------------

	#=============================================================================#
	#-------------------------------------------page_admin_super------------------#
	#=============================================================================#
		$route['admin/super/home'] 		= "ad_super/Adminmain/index_home";
		$route['admin/super/add_admin'] = "ad_super/Adminmain/index_admin";
		$route['admin/super/dinas'] 	= "ad_super/Adminmain/index_dinas";
		$route['admin/super/halaman_menu'] 	= "ad_super/Adminmain/index_page_menu";
	#=============================================================================#
	#-------------------------------------------page_admin_super------------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_admin_lv-------------------#
	#=============================================================================#
		$route['super/act/add/lv_admin']["post"] = "ad_super/Adminmain/insert_admin_lv";
		$route['super/act/del/lv_admin']["post"] = "ad_super/Adminmain/delete_admin_lv";
	#=============================================================================#
	#-------------------------------------------action_admin_lv-------------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_admin_main-----------------#
	#=============================================================================#
		$route['super/act/get/admin_main']["post"] = "ad_super/Adminmain/get_admin_update";
		$route['super/act/add/admin_main']["post"] = "ad_super/Adminmain/insert_admin";
		$route['super/act/up/admin_main']["post"] = "ad_super/Adminmain/update_admin";
		$route['super/act/del/admin_main']["post"] = "ad_super/Adminmain/delete_admin";

		$route['super/act/chpass/admin_main']["post"] = "ad_super/Adminmain/get_password";
		$route['super/act/repass/admin_main']["post"] = "ad_super/Adminmain/change_pass";	
	#=============================================================================#
	#-------------------------------------------action_admin_main-----------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_admin_dinas----------------#
	#=============================================================================#
		$route['super/act/get/dinas']["post"] = "ad_super/Adminmain/get_dinas_update";
		$route['super/act/add/dinas']["post"] = "ad_super/Adminmain/insert_dinas";
		$route['super/act/up/dinas']["post"] = "ad_super/Adminmain/update_dinas";
		$route['super/act/del/dinas']["post"] = "ad_super/Adminmain/delete_dinas";
	#=============================================================================#
	#-------------------------------------------action_admin_dinas----------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_menu_kategori--------------#
	#=============================================================================#
		$route['super/act/get/menu_kategori']["post"] = "ad_super/Adminmain/get_page_kategori_update";
		$route['super/act/add/menu_kategori']["post"] = "ad_super/Adminmain/insert_page_kategori";
		$route['super/act/up/menu_kategori']["post"] = "ad_super/Adminmain/update_page_kategori";
		$route['super/act/del/menu_kategori']["post"] = "ad_super/Adminmain/delete_page_kategori";
	#=============================================================================#
	#-------------------------------------------action_menu_kategori--------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_menu-----------------------#
	#=============================================================================#
		$route['super/act/get/menu_main']["post"] = "ad_super/Adminmain/get_page_menu";
		$route['super/act/add/menu_main']["post"] = "ad_super/Adminmain/insert_page_menu";
		$route['super/act/up/menu_main']["post"] = "ad_super/Adminmain/update_page_menu";
		$route['super/act/del/menu_main']["post"] = "ad_super/Adminmain/delete_page_menu";
	#=============================================================================#
	#-------------------------------------------action_menu-----------------------#
	#=============================================================================#

#---------------------------------------------Admin--------------------------------


#---------------------------------------------Kesehatan----------------------------

	#=============================================================================#
	#-------------------------------------------page_kesehatan_super--------------#
	#=============================================================================#
		$route['admin/super/jenis_rumah_sakit'] 	= "ad_super/kesehatan/index_jenis_rs";
		$route['admin/super/poli'] 					= "ad_super/kesehatan/index_poli";
		$route['admin/super/rumah_sakit'] 			= "ad_super/kesehatan/index_rs";
	#=============================================================================#
	#-------------------------------------------page_kesehatan_super--------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_kesehatan_poli-------------#
	#=============================================================================#
		$route['super/act/get/poli']["post"] = "ad_super/kesehatan/get_poli_update";
		$route['super/act/add/poli']["post"] = "ad_super/kesehatan/insert_poli";
		$route['super/act/up/poli']["post"] = "ad_super/kesehatan/update_poli";
		$route['super/act/del/poli']["post"] = "ad_super/kesehatan/delete_poli";
	#=============================================================================#
	#-------------------------------------------action_kesehatan_poli-------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_kesehatan_rs---------------#
	#=============================================================================#
		$route['super/act/get/rs']["post"] 	= "ad_super/kesehatan/get_rs_update";
		$route['super/act/add/rs']["post"] 	= "ad_super/kesehatan/insert_rs";
		$route['super/act/up/rs']["post"] 	= "ad_super/kesehatan/update_rs";
		$route['super/act/del/rs']["post"] 	= "ad_super/kesehatan/delete_rs";
	#=============================================================================#
	#-------------------------------------------action_kesehatan_rs---------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_jenis_rs-------------------#
	#=============================================================================#
		$route['super/act/get/jenis']["post"] 	= "ad_super/kesehatan/get_jenis_rs";
		$route['super/act/add/jenis']["post"] 	= "ad_super/kesehatan/insert_jenis_rs";
		$route['super/act/up/jenis']["post"] 	= "ad_super/kesehatan/update_jenis_rs";
		$route['super/act/del/jenis']["post"] 	= "ad_super/kesehatan/delete_jenis_rs";
	#=============================================================================#
	#-------------------------------------------action_jenis_rs-------------------#
	#=============================================================================#

#---------------------------------------------Kesehatan----------------------------


#---------------------------------------------Kependudukan-------------------------

	#=============================================================================#
	#-------------------------------------------page_kependudukan_super-----------#
	#=============================================================================#
		$route['admin/super/kependudukan_kategori'] 	= "ad_super/kependudukan/index_kategori_kependudukan";
		$route['admin/super/kependudukan_jenis'] 		= "ad_super/kependudukan/index_jenis_kependudukan";
	#=============================================================================#
	#-------------------------------------------page_kependudukan_super-----------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_kependudukan_kategori------#
	#=============================================================================#
		$route['super/act/get/kate_pend']["post"] 	= "ad_super/kependudukan/get_kategori_kependudukan";
		$route['super/act/add/kate_pend']["post"] 	= "ad_super/kependudukan/insert_kategori_kependudukan";
		$route['super/act/up/kate_pend']["post"] 	= "ad_super/kependudukan/update_kategori_kependudukan";
		$route['super/act/del/kate_pend']["post"] 	= "ad_super/kependudukan/delete_kategori_kependudukan";
	#=============================================================================#
	#-------------------------------------------action_kependudukan_kategori------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_kependudukan_kategori------#
	#=============================================================================#
		$route['super/act/get/jenis_pend']["post"] 	= "ad_super/kependudukan/get_jenis_kependudukan";
		$route['super/act/add/jenis_pend']["post"] 	= "ad_super/kependudukan/insert_jenis_kependudukan";
		$route['super/act/up/jenis_pend']["post"] 	= "ad_super/kependudukan/update_jenis_kependudukan";
		$route['super/act/del/jenis_pend']["post"] 	= "ad_super/kependudukan/delete_jenis_kependudukan";
	#=============================================================================#
	#-------------------------------------------action_kependudukan_kategori------#
	#=============================================================================#

#---------------------------------------------Kependudukan-------------------------


#---------------------------------------------Perijinan----------------------------

	#=============================================================================#
	#-------------------------------------------page_perijinan_super--------------#
	#=============================================================================#
		$route['admin/super/perijinan_kategori'] 		= "ad_super/perijinan/index_kategori_perijinan";
		$route['admin/super/perijinan_sub_kategori'] 	= "ad_super/perijinan/index_sub_kategori_perijinan";
		$route['admin/super/perijinan_jenis'] 			= "ad_super/perijinan/index_jenis_perijinan";
	#=============================================================================#
	#-------------------------------------------page_perijinan_super--------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_perijinan_kategori---------#
	#=============================================================================#
		$route['super/act/get/kate_ijin']["post"] 	= "ad_super/perijinan/get_kategori_perijinan";
		$route['super/act/add/kate_ijin']["post"] 	= "ad_super/perijinan/insert_kategori_perijinan";
		$route['super/act/up/kate_ijin']["post"] 	= "ad_super/perijinan/update_kategori_perijinan";
		$route['super/act/del/kate_ijin']["post"] 	= "ad_super/perijinan/delete_kategori_perijinan";
	#=============================================================================#
	#-------------------------------------------action_perijinan_kategori---------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_perijinan_kategori---------#
	#=============================================================================#
		$route['super/act/get/jenis_ijin']["post"] 	= "ad_super/perijinan/get_jenis_perijinan";
		$route['super/act/add/jenis_ijin']["post"] 	= "ad_super/perijinan/insert_jenis_perijinan";
		$route['super/act/up/jenis_ijin']["post"] 	= "ad_super/perijinan/update_jenis_perijinan";
		$route['super/act/del/jenis_ijin']["post"] 	= "ad_super/perijinan/delete_jenis_perijinan";
	#=============================================================================#
	#-------------------------------------------action_perijinan_kategori---------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_perijinan_sub--------------#
	#=============================================================================#
	    $route['super/act/get/sub_ijin']["post"]  = "ad_super/perijinan/get_sub_kategori_perijinan";
	    $route['super/act/add/sub_ijin']["post"]  = "ad_super/perijinan/insert_sub_kategori_perijinan";
	    $route['super/act/up/sub_ijin']["post"]   = "ad_super/perijinan/update_sub_kategori_perijinan";
	    $route['super/act/del/sub_ijin']["post"]  = "ad_super/perijinan/delete_sub_kategori_perijinan";
	#=============================================================================#
	#-------------------------------------------action_perijinan_sub--------------#
	#=============================================================================#

#---------------------------------------------Perijinan----------------------------


#---------------------------------------------Pendidikan---------------------------

	#=============================================================================#
	#-------------------------------------------page_pendidikan_super-------------#
	#=============================================================================#
		$route['admin/super/pd_strata'] 		= "ad_super/pendidikan/index_strata";
		$route['admin/super/pd_jenis'] 			= "ad_super/pendidikan/index_jenis";
		$route['admin/super/pd_sekolah'] 		= "ad_super/pendidikan/index_sekolah";
	#=============================================================================#
	#-------------------------------------------page_pendidikan_super-------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_jenis_pendidikan-----------#
	#=============================================================================#
		$route['super/act/get/pd_jenis']["post"] 	= "ad_super/pendidikan/get_jenis";
		$route['super/act/add/pd_jenis']["post"] 	= "ad_super/pendidikan/insert_jenis";
		$route['super/act/up/pd_jenis']["post"] 	= "ad_super/pendidikan/update_jenis";
		$route['super/act/del/pd_jenis']["post"] 	= "ad_super/pendidikan/delete_jenis";
	#=============================================================================#
	#-------------------------------------------action_jenis_pendidikan-----------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_strata_pendidikan-----------#
	#=============================================================================#
		$route['super/act/get/pd_strata']["post"] 	= "ad_super/pendidikan/get_strata";
		$route['super/act/add/pd_strata']["post"] 	= "ad_super/pendidikan/insert_strata";
		$route['super/act/up/pd_strata']["post"] 	= "ad_super/pendidikan/update_strata";
		$route['super/act/del/pd_strata']["post"] 	= "ad_super/pendidikan/delete_strata";
	#=============================================================================#
	#-------------------------------------------action_strata_pendidikan-----------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------action_sch_pendidikan-------------#
	#=============================================================================#
		$route['super/act/get/pd_sch']["post"] 	= "ad_super/pendidikan/get_sekolah_update";
		$route['super/act/add/pd_sch']["post"] 	= "ad_super/pendidikan/insert_sekolah";
		$route['super/act/up/pd_sch']["post"] 	= "ad_super/pendidikan/update_sekolah";
		$route['super/act/del/pd_sch']["post"] 	= "ad_super/pendidikan/delete_sekolah";
	#=============================================================================#
	#-------------------------------------------action_sch_pendidikan-------------#
	#=============================================================================#

#---------------------------------------------Pendidikan---------------------------


#---------------------------------------------kesehatan_fasilitas------------------
	#=============================================================================#
	#-------------------------------------------kesehatan_fasilitas_page----------#
	#=============================================================================#
		$route['admin/super/fasilitas_jenis'] 		= "ad_super/faskes/index_jenis";
		$route['admin/super/fasilitas_kesehatan'] 	= "ad_super/faskes/index_faskes";
	#=============================================================================#
	#-------------------------------------------kesehatan_fasilitas_page----------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------kesehatan_fasilitas_jenis_act-----#
	#=============================================================================#
		$route['super/act/get/faskes']["post"] 	= "ad_super/faskes/get_jenis";
		$route['super/act/add/faskes']["post"] 	= "ad_super/faskes/insert_jenis";
		$route['super/act/up/faskes']["post"] 	= "ad_super/faskes/update_jenis";
		$route['super/act/del/faskes']["post"] 	= "ad_super/faskes/delete_jenis";
	#=============================================================================#
	#-------------------------------------------kesehatan_fasilitas_jenis_act-----#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------kesehatan_fasilitas---------------#
	#=============================================================================#
		$route['super/act/get/faskes_main']["post"] 	= "ad_super/faskes/get_faskes_update";
		$route['super/act/add/faskes_main']["post"] 	= "ad_super/faskes/insert_faskes";
		$route['super/act/up/faskes_main']["post"] 		= "ad_super/faskes/update_faskes";
		$route['super/act/del/faskes_main']["post"] 	= "ad_super/faskes/delete_faskes";
	#=============================================================================#
	#-------------------------------------------kesehatan_fasilitas---------------#
	#=============================================================================#

#---------------------------------------------kesehatan_fasilitas------------------


#---------------------------------------------kesehatan_umum------------------
	#=============================================================================#
	#-------------------------------------------kesehatan_umum_page---------------#
	#=============================================================================#
		$route['admin/super/fasilitas_umum_jenis'] 		= "ad_super/fasum/index_jenis";
		$route['admin/super/fasilitas_umum_sub_jenis'] 	= "ad_super/fasum/index_sub";
		$route['admin/super/fasilitas_umum_kesehatan'] 	= "ad_super/fasum/index_fasum";
	#=============================================================================#
	#-------------------------------------------kesehatan_umum_page---------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------kesehatan_umum_sub_act------------#
	#=============================================================================#
		$route['super/act/get/fasum_sub']["post"] 	= "ad_super/fasum/get_sub";
		$route['super/act/add/fasum_sub']["post"] 	= "ad_super/fasum/insert_sub";
		$route['super/act/up/fasum_sub']["post"] 	= "ad_super/fasum/update_sub";
		$route['super/act/del/fasum_sub']["post"] 	= "ad_super/fasum/delete_sub";
	#=============================================================================#
	#-------------------------------------------kesehatan_umum_sub_act------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------kesehatan_umum_jenis_act----------#
	#=============================================================================#
		$route['super/act/get/fasum_jenis']["post"] 	= "ad_super/fasum/get_jenis";
		$route['super/act/add/fasum_jenis']["post"] 	= "ad_super/fasum/insert_jenis";
		$route['super/act/up/fasum_jenis']["post"] 		= "ad_super/fasum/update_jenis";
		$route['super/act/del/fasum_jenis']["post"] 	= "ad_super/fasum/delete_jenis";
	#=============================================================================#
	#-------------------------------------------kesehatan_umum_jenis_act----------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------kesehatan_umum_main_act-----------#
	#=============================================================================#
		$route['super/act/get/fasum_main']["post"] 	= "ad_super/fasum/get_fasum";
		$route['super/act/add/fasum_main']["post"] 	= "ad_super/fasum/insert_fasum";
		$route['super/act/up/fasum_main']["post"] 	= "ad_super/fasum/update_fasum";
		$route['super/act/del/fasum_main']["post"] 	= "ad_super/fasum/delete_fasum";
	#=============================================================================#
	#-------------------------------------------kesehatan_umum_main_act-----------#
	#=============================================================================#

#---------------------------------------------kesehatan_umum------------------


#---------------------------------------------wisata------------------
	#=============================================================================#
	#-------------------------------------------wisata_page-----------------------#
	#=============================================================================#
		$route['admin/super/wisata_jenis'] 		= "ad_super/wisata/index_jenis";
		$route['admin/super/wisata_sub_jenis'] 	= "ad_super/wisata/index_sub";
		$route['admin/super/wisata_kesehatan'] 	= "ad_super/wisata/index_wisata";
	#=============================================================================#
	#-------------------------------------------wisata_page-----------------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------wisata_sub_act--------------------#
	#=============================================================================#
		$route['super/act/get/wisata_sub']["post"] 	= "ad_super/wisata/get_sub";
		$route['super/act/add/wisata_sub']["post"] 	= "ad_super/wisata/insert_sub";
		$route['super/act/up/wisata_sub']["post"] 	= "ad_super/wisata/update_sub";
		$route['super/act/del/wisata_sub']["post"] 	= "ad_super/wisata/delete_sub";
	#=============================================================================#
	#-------------------------------------------wisata_sub_act--------------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------wisata_jenis_act------------------#
	#=============================================================================#
		$route['super/act/get/wisata_jenis']["post"] 	= "ad_super/wisata/get_jenis";
		$route['super/act/add/wisata_jenis']["post"] 	= "ad_super/wisata/insert_jenis";
		$route['super/act/up/wisata_jenis']["post"] 		= "ad_super/wisata/update_jenis";
		$route['super/act/del/wisata_jenis']["post"] 	= "ad_super/wisata/delete_jenis";
	#=============================================================================#
	#-------------------------------------------wisata_jenis_act------------------#
	#=============================================================================#

	#=============================================================================#
	#-------------------------------------------wisata_main_act-------------------#
	#=============================================================================#
		$route['super/act/get/wisata_main']["post"] 	= "ad_super/wisata/get_wisata";
		$route['super/act/add/wisata_main']["post"] 	= "ad_super/wisata/insert_wisata";
		$route['super/act/up/wisata_main']["post"] 		= "ad_super/wisata/update_wisata";
		$route['super/act/del/wisata_main']["post"] 	= "ad_super/wisata/delete_wisata";
	#=============================================================================#
	#-------------------------------------------wisata_main_act-------------------#
	#=============================================================================#

#---------------------------------------------wisata------------------




#=============================================================================#
#-------------------------------------------page_api_kios---------------------#
#=============================================================================#

	#------------------------------------kesehatan_antrian----------------------------------------------------
		$route['get/api/halaman_utama/json']				= "front_api/layanan_umum/get_data_layanan";
		$route['get/api/detail_page/json']					= "front_api/layanan_umum/get_data_page";

		$route['get/api/kesehatan/jenis/json']["post"] 		= "front_api/kesehatanapi/get_data_kesehatan_jenis";
		$route['get/api/kesehatan/rs/json']["post"] 		= "front_api/kesehatanapi/get_data_kesehatan_rs";
		$route['get/api/kesehatan/poli/json']["post"] 		= "front_api/kesehatanapi/get_data_kesehatan_poli";

		$route['get/api/kesehatan/insert_antrian']["post"] 	= "front_api/kesehatanapi/insert_antrian";

	#------------------------------------kesehatan_antrian----------------------------------------------------

	#------------------------------------kependudukan_antrian-------------------------------------------------
		$route['get/api/kependudukan/jenis/json']["post"] 		= "front_api/Kependudukanapi/get_data_kependudukan_jenis";
		$route['get/api/kependudukan/kategori/json']["post"] 	= "front_api/Kependudukanapi/get_data_kependudukan_kategori";


		//-----kategori for kependudukan free---//
		$route['get/api/kependudukan/kategori_free/json']["post"] 	= "front_api/Kependudukanapi/get_data_kependudukan_kategori_free";

		$route['get/api/kependudukan/insert_antrian']["post"] 	= "front_api/Kependudukanapi/insert_antrian";

	#------------------------------------kependudukan_antrian-------------------------------------------------

	#------------------------------------perijinan_antrian----------------------------------------------------
		$route['get/api/perijinan/jenis/json']["post"] 			= "front_api/Perijinanapi/get_data_ijin_jenis";
		$route['get/api/perijinan/kategori/json']["post"] 		= "front_api/Perijinanapi/get_data_ijin_kategori";
		$route['get/api/perijinan/sub_kategori/json']["post"] 	= "front_api/Perijinanapi/get_data_ijin_sub_kategori";

		$route['get/api/perijinan/insert_antrian']["post"] 		= "front_api/Perijinanapi/insert_antrian";

	#------------------------------------perijinan_antrian----------------------------------------------------

	#------------------------------------pendidikan_info------------------------------------------------------
		$route['get/api/pendidikan/sekolah/json/(:any)']	= "front_api/Pendidikanapi/index_pendidikan_home/$1";
		$route['get/api/pendidikan/sekolah/detail/(:any)']	= "front_api/pendidikanapi/get_sekolah_detail/$1";

		$route['get/api/pendidikan/cek/json']	= "front_api/Pendidikanapi/cek";
		// $route['get/api/pendidikan/sekolah_dasar/json']	= "front_api/Pendidikanapi/index_pendidikan_home";

	#------------------------------------pendidikan_info------------------------------------------------------

	#------------------------------------faskes_info------------------------------------------------------
		$route['get/api/faskes/kesehatan/json/(:any)']		= "front_api/Faskesapi/index_faskes_home/$1";
		$route['get/api/faskes/kesehatan/detail/(:any)']	= "front_api/Faskesapi/get_faskes_detail/$1";

		$route['get/api/pendidikan/cek/json']	= "front_api/Pendidikanapi/cek";
		// $route['get/api/pendidikan/sekolah_dasar/json']	= "front_api/Pendidikanapi/index_pendidikan_home";

	#------------------------------------faskes_info------------------------------------------------------

#=============================================================================#
#-------------------------------------------page_api_kios---------------------#
#=============================================================================#
