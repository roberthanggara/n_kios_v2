<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kependudukan_main extends CI_Model{

#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#    
    public function insert_jenis($nama_jenis, $time_update, $id_admin){
    	$data = $this->db->query("select insert_kp_jenis (\"".$nama_jenis."\", \"".$time_update."\", \"".$id_admin."\") AS id_jenis");
    	return $data;
    }

    public function get_jenis_api(){
        $this->db->select("foto_jenis, ket_jenis, sha2(id_jenis,\"512\") as id_jenis");
        $data = $this->db->get("kependudukan_jenis")->result();
        return $data;
    }
#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#    
    public function insert_kategori($nama_kategori, $time_update, $id_admin, $id_jenis, $syarat_kategori){
        $data = $this->db->query("select insert_kp_kategori (\"".$nama_kategori."\", \"".$time_update."\", \"".$id_admin."\", \"".$id_jenis."\", \"".$syarat_kategori."\") AS id_kategori");
        return $data;
    }

    public function get_kategori($where){
        $this->db->select("foto_jenis, ket_jenis, k.id_kategori, ket_kategori, foto_kategori");
        $this->db->join("kependudukan_jenis j", "j.id_jenis=k.id_jenis");
        $data = $this->db->get_where("kependudukan_kategori k", $where)->result();
        return $data;
    }

    public function get_kategori_api($where){
        $this->db->select("foto_jenis, ket_jenis, sha2(k.id_kategori, '512') as id_kategori, ket_kategori, foto_kategori, txt_syarat_kategori");
        $this->db->join("kependudukan_jenis j", "j.id_jenis=k.id_jenis");
        $data = $this->db->get_where("kependudukan_kategori k", $where)->result();
        return $data;
    }
#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------antrian_kependudukan----------------------------------#
#=================================================================================================#    
    public function insert_kp_antrian($nik, $id_layanan, $id_jenis, $id_kategori, $wkt_pendaftaran, $wkt_booking, $time_update, $id_admin, $no_booking){
        $data = $this->db->query("select insert_kp_antrian (\"".$nik."\", \"".$id_layanan."\", \"".$id_jenis."\", \"".$id_kategori."\", \"".$wkt_pendaftaran."\", \"".$wkt_booking."\", \"".$time_update."\", \"".$id_admin."\", \"".$no_booking."\") AS id_kategori");
        return $data;
    }
#=================================================================================================#
#-------------------------------------------antrian_kependudukan----------------------------------#
#=================================================================================================#
}
?>