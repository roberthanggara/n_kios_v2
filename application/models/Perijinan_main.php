<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Perijinan_main extends CI_Model{

#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#    
    public function insert_jenis($nama_jenis, $time_update, $id_admin){
    	$data = $this->db->query("select insert_ij_jenis (\"".$nama_jenis."\", \"".$time_update."\", \"".$id_admin."\") AS id_jenis");
    	return $data;
    }

    public function get_jenis_api(){
       $this->db->select("sha2(id_jenis, '512') as id_jenis, ket_jenis, foto_jenis");
       $data = $this->db->get("ijin_jenis")->result();
       return $data; 
    }
#=================================================================================================#
#-------------------------------------------Jenis-------------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#    
    public function insert_kategori($nama_kategori, $time_update, $id_admin, $id_jenis){
        $data = $this->db->query("select insert_ij_kategori(\"".$id_jenis."\", \"".$nama_kategori."\", \"".$time_update."\", \"".$id_admin."\") AS id_kategori");
        return $data;
    }

    public function get_kategori($where){
        $this->db->join("ijin_jenis j", "j.id_jenis=k.id_jenis");
        $data = $this->db->get_where("ijin_kategori k", $where)->result();
        return $data;
    }

    public function get_kategori_api($where){
        $this->db->select("sha2(id_kategori, '512') as id_kategori, ket_kategori, foto_kategori");
        $this->db->join("ijin_jenis j", "j.id_jenis=k.id_jenis");
        $data = $this->db->get_where("ijin_kategori k", $where)->result();
        return $data; 
    }
#=================================================================================================#
#-------------------------------------------Kategori----------------------------------------------#
#=================================================================================================#

#=================================================================================================#
#-------------------------------------------sub_Kategori------------------------------------------#
#=================================================================================================#    
    public function insert_sub_kategori($id_kategori, $nama_kategori, $time_update, $id_admin){
        $data = $this->db->query("SELECT insert_ij_sub('".$id_kategori."', '".$nama_kategori."', '".$time_update."', '".$id_admin."') AS id_sub");
        return $data;
    }

    public function get_sub_kategori($where){
        $this->db->join("ijin_kategori k", "sk.id_kategori=k.id_kategori");
        $this->db->join("ijin_jenis j", "j.id_jenis=k.id_jenis");
        
        $data = $this->db->get_where("ijin_sub_kategori sk", $where)->result();
        return $data;
    }

    public function get_sub_kategori_api($where){
        $this->db->select("sha2(id_sub, '512') as id_sub, ket_sub, foto_sub");
        $this->db->join("ijin_kategori k", "k.id_kategori=s.id_kategori");
        $data = $this->db->get_where("ijin_sub_kategori s", $where)->result();
        return $data; 
    }
#=================================================================================================#
#-------------------------------------------sub_Kategori------------------------------------------#
#=================================================================================================#


#=================================================================================================#
#-------------------------------------------antrian_ijin------------------------------------------#
#=================================================================================================#    
    public function insert_ij_antrian($nik, $id_layanan, $id_jenis, $id_kategori, $wkt_pendaftaran, $wkt_booking, $time_update, $id_admin, $no_booking){
        $data = $this->db->query("select insert_ij_antrian (\"".$nik."\", \"".$id_layanan."\", \"".$id_jenis."\", \"".$id_kategori."\", \"".$wkt_pendaftaran."\", \"".$wkt_booking."\", \"".$time_update."\", \"".$id_admin."\", \"".$no_booking."\") AS id_kategori");
        return $data;
    }
#=================================================================================================#
#-------------------------------------------antrian_ijin------------------------------------------#
#=================================================================================================#
}
?>