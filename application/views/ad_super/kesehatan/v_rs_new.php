<!DOCTYPE html>
<html>
<head>
	<title></title>

	<!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>admin_template/assets/images/favicon.png">
    <title>Apel_Mas_Admin</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>admin_template/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url()?>admin_template/main/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?=base_url()?>admin_template/main/css/colors/blue.css" id="theme" rel="stylesheet">

	<link href="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/skins/all.css" rel="stylesheet">
</head>
<body>
	<input type="checkbox" class="check" data-checkbox="icheckbox_flat-blue" name="ok" id="ok_ok_ok" value="1">


	<script src="<?=base_url()?>admin_template/assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?=base_url()?>admin_template/assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?=base_url()?>admin_template/main/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=base_url()?>admin_template/main/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?=base_url()?>admin_template/main/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?=base_url()?>admin_template/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?=base_url()?>admin_template/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?=base_url()?>admin_template/main/js/custom.min.js"></script>

	<!-- icheck -->
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.min.js"></script>
    <script src="<?php print_r(base_url());?>admin_template/assets/plugins/icheck/icheck.init.js"></script>

	<script type="text/javascript" src="<?=base_url()?>assets/js/jquery-3.2.1.js"></script>
	<script type="text/javascript">
		// $("#ok_ok_ok").prop('checked',true);
	</script>
</body>
</html>

