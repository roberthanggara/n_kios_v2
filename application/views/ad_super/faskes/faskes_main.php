<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Halaman Fasilitas Kesehatan - Fasilitas Kesehatan</h3>
        </div>
    </div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <h4 class="card-title">Daftar Fasilitas Kesehatan Kota Malang</h4>
                            </div>
                            <div class="col-lg-6 text-right">
                                <button type="button" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#insert_data">
                                    <i class="fa fa-plus-circle"></i> Tambah
                                </button>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Fasilitas Kesehatan</th>
                                        <th>Foto Fasilitas Kesehatan</th>
                                        <th>Alamat</th>
                                        <th>URL(Website)</th>
                                        <th>Telephon</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(isset($list_faskes)){
                                            if(!empty($list_faskes)){
                                                // print_r($list_faskes);
                                                $no = 1;
                                                foreach ($list_faskes as $r_list_faskes => $v_list_faskes) {

                                                    $alamat = "-";
                                                    $website = "-";
                                                    $tlp = "-";

                                                    $data_detail = str_replace("'", "\"", $v_list_faskes->detail_faskes);
                                                    // print_r("<pre>");
                                                    // print_r($data_detail);

                                                    if($this->jsoncheck->isJSON($data_detail)){
                                                        
                                                        $data_detail = json_decode($data_detail);

                                                        $alamat     = $data_detail->alamat;
                                                        $website    = $data_detail->url;
                                                        $tlp        = $data_detail->tlp;
                                                    }
                                                    
                                                    print_r("<tr>
                                                                <td>".$no++."</td>
                                                                <td>".$v_list_faskes->nama_faskes." (".$v_list_faskes->nama_jenis.") </td>
                                                                <td>".$v_list_faskes->nama_faskes."</td>
                                                                <td>".$alamat."</td>
                                                                <td>".$website."</td>
                                                                <td>".$tlp."</td>
                                                                <td>
                                                                <center>
                                                                    <button type=\"button\" class=\"btn btn-info\" id=\"up_faskes\" onclick=\"get_update_faskes('".$this->encrypt->encode($v_list_faskes->id_faskes)."')\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                                                    <button class=\"btn btn-danger\" id=\"del_faskes\" onclick=\"delete_faskes('".$this->encrypt->encode($v_list_faskes->id_faskes)."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                                </td>
                                                            </tr>");
                                                }
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="insert_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Tambah Fasilitas Kesehatan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Kategori Fasilitas Kesehatan</label>
                                
                                <select name="id_jenis" id="id_jenis" class="form-control form-control-line">
                                    <?php
                                        if($jenis_faskes){
                                            foreach ($jenis_faskes as $key => $value) {
                                                print_r("<option value=\"".$value->id_jenis."\">".$value->nama_jenis."</option>");            
                                            }
                                        }
                                    ?>
                                </select>
                                <a id="msg_id_jenis" style="color: red;"></a>
                            </div>   
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nama Fasilitas Kesehatan</label>
                                <input name="nama_faskes" id="nama_faskes" class="form-control form-control-line">
                                <a id="msg_nama_faskes" style="color: red;"></a>
                            </div>    
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Lokasi (Latitude & longitude)</label>
                                <input type="text" name="loc_faskes" id="loc_faskes" class="form-control form-control-line">
                                <a id="msg_loc_faskes" style="color: red;"></a>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="alamat_faskes" id="alamat_faskes" class="form-control form-control-line"></textarea>
                                <a id="msg_alamat_faskes" style="color: red;"></a>
                            </div>
                        </div>



                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="text" name="tlp_faskes" id="tlp_faskes" class="form-control form-control-line">
                                <a id="msg_tlp_faskes" style="color: red;"></a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" name="web_faskes" id="web_faskes" class="form-control form-control-line">
                                <a id="msg_web_faskes" style="color: red;"></a>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Gambar Keterangan Fasilitas Kesehatan</label>
                                <input type="file" name="foto_faskes" id="foto_faskes" class="form-control form-control-line">
                                <a id="msg_foto_faskes" style="color: red;"></a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <center><img id="img_foto_faskes" src="" style="width: 350px; height: 225px;"></center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_insert_data" class="btn btn-info waves-effect">Simpan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div id="update_data" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Form Tambah Fasilitas Kesehatan</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Kategori Fasilitas Kesehatan</label>
                                
                                <select name="id_jenis" id="_id_jenis" class="form-control form-control-line">
                                    <?php
                                        if($jenis_faskes){
                                            foreach ($jenis_faskes as $key => $value) {
                                                print_r("<option value=\"".$value->id_jenis."\">".$value->nama_jenis."</option>");            
                                            }
                                        }
                                    ?>
                                </select>
                                <a id="_msg_id_jenis" style="color: red;"></a>
                            </div>   
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Nama Fasilitas Kesehatan</label>
                                <input name="nama_faskes" id="_nama_faskes" class="form-control form-control-line">
                                <a id="_msg_nama_faskes" style="color: red;"></a>
                            </div>    
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Lokasi (Latitude & longitude)</label>
                                <input type="text" name="loc_faskes" id="_loc_faskes" class="form-control form-control-line">
                                <a id="_msg_loc_faskes" style="color: red;"></a>
                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea name="alamat_faskes" id="_alamat_faskes" class="form-control form-control-line"></textarea>
                                <a id="_msg_alamat_faskes" style="color: red;"></a>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Telepon</label>
                                <input type="text" name="tlp_faskes" id="_tlp_faskes" class="form-control form-control-line">
                                <a id="_msg_tlp_faskes" style="color: red;"></a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Website</label>
                                <input type="text" name="web_faskes" id="_web_faskes" class="form-control form-control-line">
                                <a id="_msg_web_faskes" style="color: red;"></a>
                            </div>
                        </div>


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Gambar Keterangan Fasilitas Kesehatan</label>
                                <input type="file" name="foto_faskes" id="_foto_faskes" class="form-control form-control-line">
                                <a id="_msg_foto_faskes" style="color: red;"></a>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <center><img id="_img_foto_faskes" src="" style="width: 350px; height: 225px;"></center>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                    <button type="submit" id="btn_update_data" class="btn btn-info waves-effect">Simpan</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.mod8al-dialog -->
    </div>

    

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================

    $(document).ready(function(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showLocation);
        }else{ 
            console.log('Geolocation is not supported by this browser.');
        }
    });

    function showLocation(position){
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        console.log(position);
    }
//========================================================================
//--------------------------------Get_Lokasi------------------------------
//========================================================================

//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================
    var file = [];
    $("#foto_faskes").change(function(e){
        file = e.target.files[0];

        $("#img_foto_faskes").attr("src",URL.createObjectURL(file));
        console.log(file);
    });  

        $("#btn_insert_data").click(function(){
            var data_main =  new FormData();
            data_main.append('id_jenis' , $("#id_jenis").val());
            data_main.append('nama_faskes' , $("#nama_faskes").val());
            data_main.append('lokasi'   , $("#loc_faskes").val());
            data_main.append('alamat'   , $("#alamat_faskes").val());
            data_main.append('tlp'      , $("#tlp_faskes").val());
            data_main.append('website'  , $("#web_faskes").val());

            data_main.append('foto_faskes' , file);
                                        
            $.ajax({
                url: "<?php echo base_url()."super/act/add/faskes_main";?>",
                dataType: 'html',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,                         
                type: 'post',
                success: function(res){
                    console.log(res);
                    response_insert(res);
                }
            });
        });

        function response_insert(res){
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
            if(main_msg.status){
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({   
                            title: "Proses Berhasil.!!",   
                            text: "Data admin berhasil disimpan ..!",   
                            type: "success",   
                            showCancelButton: false,   
                            confirmButtonColor: "#28a745",   
                            confirmButtonText: "Lanjutkan",   
                            closeOnConfirm: false 
                        }, function(){
                            window.location.href = "<?php echo base_url()."admin/super/fasilitas_kesehatan";?>";
                        });                              
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),
                
                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            }else{
                !function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#msg_id_jenis").html(detail_msg.id_jenis);
                        $("#msg_nama_faskes").html(detail_msg.nama_faskes);
                        $("#msg_loc_faskes").html(detail_msg.lokasi);
                        $("#msg_alamat_faskes").html(detail_msg.alamat);
                        $("#msg_tlp_faskes").html(detail_msg.tlp);
                        $("#msg_web_faskes").html(detail_msg.website);

                        $("#msg_foto_faskes").html(detail_msg.foto_faskes);

                        swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");                   
                    },
                                              
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
                
            }
        }
//========================================================================
//--------------------------------Insert_data-----------------------------
//========================================================================

var id_faskes_glob = "";

//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================
    function clear_from_update(){
        $("#_id_jenis").val("");
        $("#_nama_faskes").val("");
        $("#_loc_faskes").val("");
        $("#_alamat_faskes").val("");
        $("#_tlp_faskes").val("");
        $("#_web_faskes").val("");
        $("#_img_foto_faskes").attr("src", "");
        id_faskes_glob = "";
    }

    function get_update_faskes(param){
        clear_from_update();

        var data_main =  new FormData();
        data_main.append('id_faskes', param);
                                        
        $.ajax({
            url: "<?php echo base_url()."super/act/get/faskes_main";?>",
            dataType: 'html',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,                         
            type: 'post',
            success: function(res){
                console.log(res);
                set_val_update(res, param);
                $("#update_data").modal('show');
            }
        });
    }

    function set_val_update(res, param){
        var res_pemohon = JSON.parse(res.toString());

        if(res_pemohon.status == true){
            var id_faskes_chahce = res_pemohon.val_response.id_faskes;
            var detail = res_pemohon.val_response.detail_faskes;

            var alamat_faskes = "";
            var tlp_faskes = "";
            var web_faskes = "";
            
            try{
                detail = JSON.parse(detail);
                alamat_faskes = detail.alamat;
                tlp_faskes = detail.tlp;
                web_faskes = detail.url;
            }catch(e){
                console.log(e);   
            }

            var val_kec = res_pemohon.val_response.id_kecamatan;

            $("#_id_jenis").val(res_pemohon.val_response.id_jenis);
            $("#_nama_faskes").val(res_pemohon.val_response.nama_faskes);
            $("#_loc_faskes").val(res_pemohon.val_response.lokasi);
            $("#_alamat_faskes").val(alamat_faskes);
            $("#_tlp_faskes").val(tlp_faskes);
            $("#_web_faskes").val(web_faskes);
            $("#_img_foto_faskes").attr("src", res_pemohon.val_response.foto_faskes);

            id_faskes_glob = id_faskes_chahce;
        }else {
            clear_from_update();
        }
    }
//========================================================================
//--------------------------------Get_Update_data-------------------------
//========================================================================

//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================
    var file = [];
    $("#_foto_faskes").change(function(e){
        file = e.target.files[0];

        $("#_img_foto_faskes").attr("src",URL.createObjectURL(file));
        console.log(file);
    });

    $("#btn_update_data").click(function() {
        var data_main =  new FormData();

            data_main.append('id_jenis'     , $("#_id_jenis").val());
            data_main.append('nama_faskes'  , $("#_nama_faskes").val());
            data_main.append('lokasi'       , $("#_loc_faskes").val());
            data_main.append('alamat'       , $("#_alamat_faskes").val());
            data_main.append('tlp'          , $("#_tlp_faskes").val());
            data_main.append('website'      , $("#_web_faskes").val());

            data_main.append('foto_faskes'  , file);
            data_main.append('id_faskes'    , id_faskes_glob);

        $.ajax({
            url: "<?php echo base_url()."super/act/up/faskes_main";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {
                console.log(res);
                response_update(res);
            }
        });
    });

    function response_update(res) {
        var data_json = JSON.parse(res);

        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Proses Berhasil.!!",
                            text: "Data admin berhasil disimpan ..!",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#28a745",
                            confirmButtonText: "Lanjutkan",
                            closeOnConfirm: false
                        }, function() {
                            window.location.href = "<?php echo base_url()."admin/super/fasilitas_kesehatan";?>";
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        } else {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        $("#_msg_id_jenis").html(detail_msg.id_jenis);
                        $("#_msg_nama_faskes").html(detail_msg.nama_faskes);
                        $("#_msg_loc_faskes").html(detail_msg.lokasi);
                        $("#_msg_alamat_faskes").html(detail_msg.alamat);
                        $("#_msg_web_faskes").html(detail_msg.website);
                        $("#_msg_tlp_faskes").html(detail_msg.tlp);
                        $("#_msg_foto_faskes").html(detail_msg.foto_faskes);

                        swal("Proses Gagal.!!", "Data admin gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                    },

                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);

        }
    }
//========================================================================
//--------------------------------Update_data-----------------------------
//========================================================================

//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================
        function delete_faskes(id_faskes){
            !function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                    //Warning Message
                    swal({   
                        title: "Pesan Konfirmasi",   
                        text: "Silahkan Cermati data sebelem di hapus permanen, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",   
                        type: "warning",   
                        showCancelButton: true,   
                        confirmButtonColor: "#ffb22b",   
                        confirmButtonText: "Hapus",   
                        closeOnConfirm: true 
                    }, function(){
                        
                        var data_main =  new FormData();
                        data_main.append('id_faskes', id_faskes);
                                                    
                        $.ajax({
                            url: "<?php echo base_url()."super/act/del/faskes_main";?>",
                            dataType: 'html',  // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: data_main,                         
                            type: 'post',
                            success: function(res){
                                console.log(res);
                                swal("Proses Berhasil.!!", "Penghapusan Data Berhasil", "success");
                                location.href="<?php print_r(base_url()."admin/super/fasilitas_kesehatan");?>";
                            }
                        });   
                    });                                     
                },
                                          
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }
//========================================================================
//--------------------------------delete_data-----------------------------
//========================================================================

</script>
