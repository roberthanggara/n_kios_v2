<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-5 align-self-center">
        <h3 class="text-themecolor">Halaman Dinas</h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">

        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="card-title">Daftar Data Dinas</h4>
                        </div>
                        <div class="col-lg-6 text-right">
                            <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_dinas"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;&nbsp;Tambah Data Dinas</button>
                        </div>
                    </div>

                    
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%">No. </th>
                                            <th width="*">Nama Dinas</th>
                                            <th width="40%">Alamat</th>
                                            <th width="15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                        if(!empty($dinas)){
                                                            foreach ($dinas as $r_dinas => $v_dinas) {
                                                                echo "<tr>
                                                                        <td>".($r_dinas+1)."</td>
                                                                        <td>".$v_dinas->nama_dinas."</td>
                                                                        <td>".$v_dinas->alamat."</td>
                                                                        <td>
                                                                        <center>
                                                                            <button type=\"button\" class=\"btn btn-info\" id=\"up_dinas\" onclick=\"up_dinas_fc('".$this->encrypt->encode($v_dinas->id_dinas)."')\" style=\"width: 40px;\">
                                                                                <i class=\"fa fa-pencil-square-o\"></i>
                                                                            </button>

                                                                            <button type=\"button\" class=\"btn btn-danger\" id=\"del_dinas\" onclick=\"delete_dinas('".$this->encrypt->encode($v_dinas->id_dinas)."')\" style=\"width: 40px;\">
                                                                                <i class=\"fa fa-trash-o\"></i>
                                                                            </button>
                                                                        </center>
                                                                        </td>
                                                                    </tr>";
                                                            }
                                                        }
                                                    ?>

                                    </tbody>
                                </table>
                            </div>
                        
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_dinas------------------------ -->
<!-- ============================================================== -->

    <div class="modal fade" id="insert_dinas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Tambah Dinas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nama Dinas :</label>
                        <input type="text" class="form-control" id="nm_dinas" name="nm_dinas" required="">
                        <p id="msg_nm_dinas" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Alamat :</label>
                        <textarea class="form-control" id="alamat" name="alamat" required=""></textarea>
                        <p id="msg_alamat" style="color: red;"></p>
                    </div>

                </div>
                <div class="modal-footer">
                    <button id="add_dinas" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
<!-- ============================================================== -->
<!-- --------------------------insert_dinas------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_dinas------------------------ -->
<!-- ============================================================== -->

    <div class="modal fade" id="update_dinas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Dinas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Nama Dinas :</label>
                        <input type="text" class="form-control" id="nm_dinas_up" name="nm_dinas" required="">
                        <p id="_msg_nm_dinas" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Alamat :</label>
                        <textarea class="form-control" id="alamat_up" name="alamat" required=""></textarea>
                        <p id="_msg_alamat" style="color: red;"></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="up_dinas_btn" type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal -->
<!-- ============================================================== -->
<!-- --------------------------update_dinas------------------------ -->
<!-- ============================================================== -->

<script type="text/javascript">
    var id_dinas_gl;

    //==========================================================================================//
    //----------------------------------------------------Insert_Dinas--------------------------//
    //==========================================================================================//
        $("#add_dinas").click(function() {
            var data_main = new FormData();
            data_main.append('nm_dinas', $("#nm_dinas").val());
            data_main.append('alamat', $("#alamat").val());

            $.ajax({
                url: "<?php echo base_url()."super/act/add/dinas";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_add_dinas(res);
                }
            });
        });

        function response_add_dinas(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Dinas berhasil disimpan ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/super/dinas";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#msg_nm_dinas").html(detail_msg.nm_dinas);
                            $("#msg_alamat").html(detail_msg.alamat);

                            swal("Proses Gagal.!!", "Data Dinas gagal disimpan, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //==========================================================================================//
    //----------------------------------------------------Insert_Dinas--------------------------//
    //==========================================================================================//

    //==========================================================================================//
    //----------------------------------------------------Get_Dinas-----------------------------//
    //==========================================================================================//
        function up_dinas_fc(id_dinas) {
            clear_from_update();

            var data_main = new FormData();
            data_main.append('id_dinas', id_dinas);

            $.ajax({
                url: "<?php echo base_url()."super/act/get/dinas";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    set_val_update(res, id_dinas);
                    $("#update_dinas").modal('show');
                }
            });
        }

        function set_val_update(res, id_dinas) {
            var res_pemohon = JSON.parse(res);
            id_dinas_gl = id_dinas;

            if (res_pemohon.status == true) {
                $("#nm_dinas_up").val(res_pemohon.val_response.nama_dinas);
                $("#alamat_up").val(res_pemohon.val_response.alamat);
            } else {
                clear_from_update();
            }
        }

        function clear_from_update() {
            $("#nm_dinas_up").val("");
            $("#alamat_up").val("");
        }
    //==========================================================================================//
    //----------------------------------------------------Get_Dinas-----------------------------//
    //==========================================================================================//

    //==========================================================================================//
    //----------------------------------------------------Update_Dinas--------------------------//
    //==========================================================================================//
        $("#up_dinas_btn").click(function() {
            var data_main = new FormData();
            data_main.append('nm_dinas', $("#nm_dinas_up").val());
            data_main.append('alamat', $("#alamat_up").val());

            data_main.append('id_dinas', id_dinas_gl);

            $.ajax({
                url: "<?php echo base_url()."super/act/up/dinas";?>", // point to server-side PHP script 
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_update_dinas(res);
                }
            });
        });

        function response_update_dinas(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            swal({
                                title: "Proses Berhasil.!!",
                                text: "Data Dinas berhasil diubah ..!",
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#28a745",
                                confirmButtonText: "Lanjutkan",
                                closeOnConfirm: false
                            }, function() {
                                window.location.href = "<?php echo base_url()."admin/super/dinas";?>";
                            });
                        },
                        //init
                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);
            } else {
                ! function($) {
                    "use strict";
                    var SweetAlert = function() {};
                    //examples 
                    SweetAlert.prototype.init = function() {
                            //Warning Message
                            $("#_msg_nm_dinas").html(detail_msg.nm_dinas);
                            $("#_msg_alamat").html(detail_msg.alamat);

                            swal("Proses Gagal.!!", "Data Dinas gagal diubah, coba periksa jaringan dan koneksi anda", "warning");
                        },

                        $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
                }(window.jQuery),

                function($) {
                    "use strict";
                    $.SweetAlert.init()
                }(window.jQuery);

            }
        }
    //==========================================================================================//
    //----------------------------------------------------Update_Dinas--------------------------//
    //==========================================================================================//

    //==========================================================================================//
    //----------------------------------------------------delete_dinas--------------------------//
    //==========================================================================================//
        function delete_dinas(id_dinas) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                //examples 
                SweetAlert.prototype.init = function() {
                        //Warning Message
                        swal({
                            title: "Pesan Konfirmasi",
                            text: "Silahkan Cermati data sebelem di hapus, jika anda sudah yakin maka data ini dan seluruh data yang berkaitan akan di hapus",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Hapus",
                            closeOnConfirm: false
                        }, function() {
                            var data_main = new FormData();
                            data_main.append('id_dinas', id_dinas);

                            $.ajax({
                                url: "<?php echo base_url()."super/act/del/dinas";?>", // point to server-side PHP script 
                                dataType: 'html', // what to expect back from the PHP script, if anything
                                cache: false,
                                contentType: false,
                                processData: false,
                                data: data_main,
                                type: 'post',
                                success: function(res) {
                                    response_delete_dinas(res);
                                }
                            });
                        });
                    },
                    //init
                    $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete_dinas(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                swal({
                    title: "Proses Berhasil.!!",
                    text: "Data Dinas berhasil dihapus ..!",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#28a745",
                    confirmButtonText: "Lanjutkan",
                    closeOnConfirm: false
                }, function() {
                    window.location.href = "<?php echo base_url()."admin/super/dinas";?>";
                });
            } else {

                swal("Proses Gagal.!!", "Data Dinas gagal dihapus, coba periksa jaringan dan koneksi anda", "warning");
            }
        }
    //==========================================================================================//
    //----------------------------------------------------delete_dinas--------------------------//
    //==========================================================================================//
</script>