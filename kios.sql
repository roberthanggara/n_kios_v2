-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2019 at 06:22 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kios`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `insert_admin` (`nama` VARCHAR(100), `email` TEXT, `nip` VARCHAR(25), `jabatan` VARCHAR(50), `id_bidang` VARCHAR(12), `lv` INT(2), `pass` VARCHAR(64), `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(14) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from admin 
  	where substr(id_admin,3,6) = left(NOW()+0, 6);
        
  select id_admin into last_key_user from admin
  	where substr(id_admin,3,6) = left(NOW()+0, 6)
  	order by id_admin desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AD",left(NOW()+0, 6),"0001");
  else
      	set fix_key_user = concat("AD",substr(last_key_user,3,10)+1);
      
  END IF;
  
  
  insert into admin values(fix_key_user, '0', lv, email, pass, '0', nama, nip, jabatan, id_bidang, '0', '0000-00-00 00:00:00', admin_del, time_update);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_antrian` (`nik` VARCHAR(16), `id_layanan_menu` INT, `id_jenis_kesehatan` VARCHAR(8), `id_rs` VARCHAR(15), `id_poli` VARCHAR(11), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from kesehatan_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from kesehatan_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("AC",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("AC",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into kesehatan_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis_kesehatan, id_rs, id_poli, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_dinas` (`nama_dinas` TEXT, `alamat` TEXT, `page_kelola` TEXT, `admin_del` VARCHAR(12), `time_update` DATETIME) RETURNS VARCHAR(8) CHARSET latin1 BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from dinas;
        
  select id_dinas into last_key_item from dinas order by id_dinas desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("DN","200001");
  else
    set fix_key_item = concat("DN",RIGHT(last_key_item,6)+1);
      
  END IF;
  
  insert into dinas values(fix_key_item, nama_dinas, alamat, page_kelola, "0", "0000-00-00 00:00:00", admin_del, time_update);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_faskes_jenis` (`ket_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from faskes_jenis;
        
  select id_jenis into last_key_user from faskes_jenis order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("FSKS","100001");
  else
    set fix_key_user = concat("FSKS",RIGHT(last_key_user,6)+1);  
  END IF;
  
  INSERT INTO faskes_jenis VALUES(fix_key_user, ket_jenis, "", "", waktu, "0", id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_faskes_main` (`id_jenis` VARCHAR(10), `nama_faskes` TEXT, `foto_faskes` VARCHAR(70), `lokasi` VARCHAR(64), `detail_faskes` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(19) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(19);
  
  select count(*) into count_row_user from faskes_main where substr(id_faskes,6,8) = left(NOW()+0, 8);
        
  select id_faskes into last_key_user from faskes_main where substr(id_faskes,6,8) = left(NOW()+0, 8) order by id_faskes desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("FSKSM", left(NOW()+0, 8)+0, "100001");
  else
    set fix_key_user = concat("FSKSM", left(NOW()+0, 8)+0, RIGHT(last_key_user,6)+1);  
  END IF;
  
  INSERT INTO faskes_main VALUES(fix_key_user, id_jenis, nama_faskes, foto_faskes, lokasi, detail_faskes, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_antrian` (`nik` VARCHAR(16), `id_layanan_menu` VARCHAR(9), `id_jenis` VARCHAR(8), `id_kategori` VARCHAR(10), `id_sub` VARCHAR(10), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from ijin_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from ijin_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("IJ",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("IJ",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into ijin_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis, id_kategori, id_sub, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_jenis` (`ket_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_jenis;
        
  select id_jenis into last_key_user from ijin_jenis order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJJ","10001");
  else
    set fix_key_user = concat("IJJ",RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_jenis VALUES(fix_key_user, ket_jenis, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_kategori` (`id_jenis` VARCHAR(8), `ket_kategori` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_kategori WHERE substr(id_kategori, 4, 2) = RIGHT(id_jenis, 2);
        
  select id_kategori into last_key_user from ijin_kategori WHERE substr(id_kategori, 4, 2) = RIGHT(id_jenis, 2) order by id_kategori desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJK",RIGHT(id_jenis,2),"10001");
  else
    set fix_key_user = concat("IJK",RIGHT(id_jenis,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_kategori VALUES(fix_key_user, id_jenis, ket_kategori, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_ij_sub` (`id_kategori` VARCHAR(12), `ket_sub` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(12) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from ijin_sub_kategori where substr(id_sub, 4, 2) = RIGHT(id_kategori, 2);
        
  select id_sub into last_key_user from ijin_sub_kategori where substr(id_sub, 4, 2) = RIGHT(id_kategori, 2) order by id_sub desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("IJS",RIGHT(id_kategori,2),"10001");
  else
    set fix_key_user = concat("IJS",RIGHT(id_kategori,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO ijin_sub_kategori VALUES(fix_key_user, id_kategori, ket_sub, "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_jenis_rs` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_jenis;
        
  select id_layanan into last_key_item from kesehatan_jenis order by id_layanan desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KSJ","10001");
  else
    set fix_key_item = concat("KSJ",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_jenis values(fix_key_item, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kecamatan` (`nama` VARCHAR(50), `area` TEXT, `luas` DOUBLE, `id_admin` VARCHAR(12), `waktu` DATETIME) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from master_kecamatan;
        
  select id_kecamatan into last_key_user from master_kecamatan order by id_kecamatan desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEC1","001");
  else
    set fix_key_user = concat("KEC",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO master_kecamatan VALUES(fix_key_user, nama, area, luas, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kelurahan` (`nama` VARCHAR(64), `area` TEXT, `luas` DOUBLE, `id_kec` VARCHAR(7), `id_admin` VARCHAR(12), `waktu` INT) RETURNS VARCHAR(7) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from master_kelurahan;
        
  select id_kelurahan into last_key_user from master_kelurahan order by id_kelurahan desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("KEL1","001");
  else
    set fix_key_user = concat("KEL",SUBSTR(last_key_user,4,4)+1);  
  END IF;
  
  INSERT INTO master_kelurahan VALUES(fix_key_user, id_kec, nama, area, luas, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_antrian` (`nik` VARCHAR(16), `id_layanan_menu` INT, `id_jenis` VARCHAR(8), `id_kategori` VARCHAR(8), `wkt_pendaftaran` DATETIME, `wkt_booking` DATETIME, `time_update` DATETIME, `id_admin` VARCHAR(12), `no_booking` VARCHAR(10)) RETURNS VARCHAR(21) CHARSET latin1 NO SQL
BEGIN
  declare last_key_user varchar(30);
  declare count_row_user int;
  declare fix_key_user varchar(30);
  
  select count(*) into count_row_user from kependudukan_antrian 
  	where substr(id_antrian,3,8) = left(NOW()+0, 8);
        
  select id_antrian into last_key_user from kependudukan_antrian
  	where substr(id_antrian,3,8) = left(NOW()+0, 8)
  	order by id_antrian desc limit 1;
    
  if(count_row_user <1) then
  	set fix_key_user = concat("PD",left(NOW()+0, 8),"100000001");
  else
    set fix_key_user = concat("PD",left(NOW()+0, 8), RIGHT(last_key_user,9)+1);
      
  END IF;
  
  insert into kependudukan_antrian values(fix_key_user, nik, id_layanan_menu, id_jenis, id_kategori, wkt_pendaftaran, wkt_booking, no_booking, '0', '', '', '0', time_update, id_admin);
 
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_jenis` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(8) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kependudukan_jenis;
        
  select id_jenis into last_key_item from kependudukan_jenis order by id_jenis desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KPJ","10001");
  else
    set fix_key_item = concat("KPJ",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kependudukan_jenis values(fix_key_item, nama, "", "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_kp_kategori` (`nama` VARCHAR(75), `time_update` DATETIME, `id_admin` VARCHAR(12), `id_jenis` VARCHAR(8), `syarat_kategori` TEXT) RETURNS VARCHAR(8) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kependudukan_kategori;
        
  select id_kategori into last_key_item from kependudukan_kategori order by id_kategori desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("KPC","10001");
  else
    set fix_key_item = concat("KPC",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kependudukan_kategori values(fix_key_item, id_jenis, nama, "", syarat_kategori, "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_page_main` (`id_kategori` INT(11), `nama_page` VARCHAR(100), `next_page` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from home_page_main;
        
  select id_page into last_key_item from home_page_main order by id_page desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("MENU","10001");
  else
    set fix_key_item = concat("MENU",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into home_page_main values(fix_key_item, id_kategori, nama_page, "-", next_page, "0", waktu, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_jenis` (`id_strata` VARCHAR(8), `nama_jenis` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(10) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from pendidikan_jenis WHERE substr(id_jenis, 4, 2) = RIGHT(id_strata,2);
        
  select id_jenis into last_key_user from pendidikan_jenis WHERE substr(id_jenis, 4, 2) = RIGHT(id_strata,2) order by id_jenis desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("PDJ",RIGHT(id_strata,2),"10001");
  else
    set fix_key_user = concat("PDJ",RIGHT(id_strata,2),RIGHT(last_key_user,5)+1);  
  END IF;
  
  INSERT INTO pendidikan_jenis VALUES(fix_key_user, id_strata, nama_jenis, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_sekolah` (`id_jenis` VARCHAR(10), `id_kecamatan` VARCHAR(8), `id_kelurahan` VARCHAR(8), `nama_sekolah` TEXT, `lokasi` TEXT, `detail_sekolah` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(17) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(17);
  
  select count(*) into count_row_user from pendidikan_sekolah where substr(id_sekolah,4,8) = left(NOW()+0, 8);
        
  select id_sekolah into last_key_user from pendidikan_sekolah where substr(id_sekolah,4,8) = left(NOW()+0, 8) order by id_sekolah desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("SKL", left(NOW()+0, 8)+0, "100001");
  else
    set fix_key_user = concat("SKL", left(NOW()+0, 8)+0, RIGHT(last_key_user,6)+1);  
  END IF;
  
  INSERT INTO pendidikan_sekolah VALUES(fix_key_user, id_jenis, id_kecamatan, id_kelurahan, nama_sekolah, "", lokasi, detail_sekolah, "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_pd_strata` (`nama` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(8) CHARSET latin1 BEGIN
  declare last_key_user varchar(20);
  declare count_row_user int;
  declare fix_key_user varchar(12);
  
  select count(*) into count_row_user from pendidikan_strata;
        
  select id_strata into last_key_user from pendidikan_strata order by id_strata desc limit 1;
        
  if(count_row_user <1) then
  	set fix_key_user = concat("PDST1","001");
  else
    set fix_key_user = concat("PDST",SUBSTR(last_key_user,5,4)+1);  
  END IF;
  
  INSERT INTO pendidikan_strata VALUES(fix_key_user, nama, "", "", "0", waktu, id_admin);
  
  return fix_key_user;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_poli` (`nama` VARCHAR(30), `time_update` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(70) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from kesehatan_poli;
        
  select id_poli into last_key_item from kesehatan_poli order by id_poli desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("P","10001");
  else
    set fix_key_item = concat("P",RIGHT(last_key_item,5)+1);
      
  END IF;
  
  insert into kesehatan_poli values(fix_key_item, nama, sha2(fix_key_item, '256'), "0", time_update, id_admin);
  
  return fix_key_item;
  
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `insert_rs` (`nama_rs` TEXT, `alamat` TEXT, `foto` VARCHAR(70), `telephone` VARCHAR(13), `id_layanan` VARCHAR(8), `id_poli` TEXT, `waktu` DATETIME, `id_admin` VARCHAR(12)) RETURNS VARCHAR(15) CHARSET latin1 NO SQL
BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(15);
  
  select count(*) into count_row_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8);
        
  select id_rs into last_key_item from kesehatan_rs where substr(id_rs,3,8) = left(NOW()+0, 8) order by id_rs desc limit 1;
        
 if(count_row_item <1) then
  	set fix_key_item = concat("RS", left(NOW()+0, 8), "10001");
  else
    set fix_key_item = concat("RS",substr(last_key_item,3,13)+1);
      
  END IF;
  
  insert into kesehatan_rs values(fix_key_item, nama_rs, alamat, foto, telephone, id_layanan, id_poli, "0", waktu, id_admin);
  
  return fix_key_item;
  
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `del` enum('0','1') NOT NULL,
  `id_lv` int(2) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status_active` enum('0','1') NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `id_bidang` varchar(8) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `del`, `id_lv`, `email`, `password`, `status_active`, `nama`, `nip`, `jabatan`, `id_bidang`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('AD2019020001', '0', 1, 'kominfo_super@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Suwardi Surya Ningrat', '19091999012130', 'staff bidang penelitian', 'DN200002', '0', '2019-03-13 08:22:29', '0', '2019-02-21 00:00:00'),
('AD2019030001', '0', 1, 'dinkes@gmail.com', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'Sukiman Sumajono', '123456789011', 'Pengamat lapangan', 'DN200002', '0', '2019-03-13 08:22:20', 'AD2019020001', '2019-03-13 08:17:16'),
('AD2019040001', '0', 1, 'meilinda_ika@yahoo.com', '7717a57ec3d7fb939eff71741ceca0a01808b7b6ea01edb40b9b7facf5a061cf', '1', 'Mei', '5555555555555555', 'tpok', 'DN200002', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-04-15 10:49:01'),
('AD2019080001', '0', 2, 'surya@gmail.com', 'surya', '0', 'surya hanggara', '123', '123', '123', '0', '0000-00-00 00:00:00', 'sip', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_lv`
--

CREATE TABLE `admin_lv` (
  `id_lv` int(2) NOT NULL,
  `ket` varchar(32) NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_lv`
--

INSERT INTO `admin_lv` (`id_lv`, `ket`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
(1, 'admin super', '0', '0000-00-00 00:00:00', '0', '2019-02-21 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `device`
--

CREATE TABLE `device` (
  `id_device_kios` varchar(7) NOT NULL,
  `ip_lan` varchar(16) NOT NULL,
  `ip_public` varchar(16) NOT NULL,
  `key` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `device`
--

INSERT INTO `device` (`id_device_kios`, `ip_lan`, `ip_public`, `key`) VALUES
('1', '192.168.21.26', '36.74.196.6', 'myname_surya_hanggara'),
('2', '192.168.86.246', '222.124.213.68', 'aku_ini_sehat'),
('3', '192.168.86.243', '103.135.14.17', 'lan new'),
('4', '192.168.86.197', '103.135.14.17', 'test'),
('5', '192.168.86.22', '103.135.14.17', 'ip_test');

-- --------------------------------------------------------

--
-- Table structure for table `dinas`
--

CREATE TABLE `dinas` (
  `id_dinas` varchar(8) NOT NULL,
  `nama_dinas` text NOT NULL,
  `alamat` text NOT NULL,
  `page_kelola` text NOT NULL,
  `is_del` enum('0','1') NOT NULL,
  `time_del` datetime NOT NULL,
  `admin_del` varchar(12) NOT NULL,
  `time_update` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dinas`
--

INSERT INTO `dinas` (`id_dinas`, `nama_dinas`, `alamat`, `page_kelola`, `is_del`, `time_del`, `admin_del`, `time_update`) VALUES
('DN200001', 'Dinas Perdagangan', 'Jl. Simp. Terusan Danau Sentani 3 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200002', 'Dinas Komunikasi dan Informatika (DISKOMINFO)', 'Perkantoran Terpadu Gedung A Lt. 4 Malang, Jl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200003', 'Dinas Kebudayaan dan Pariwisata (DISBUDPAR)', 'Museum Mpu Purwa, Jl. Sukarno Hatta B. 210 Malang 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200004', 'Dinas Perindustrian (DISPERIN)', 'Perkantoran Terpadu Gedung A Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200005', 'Dinas Pendidikan (DISDIK)', 'Jl. Veteran 19 Malang 65145', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200006', 'Dinas Kesehatan (DINKES)', 'Jl. Simp. Laksda Adisucipto 45 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200007', 'Dinas Perhubungan (DISHUB)', 'Jl. Raden Intan 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200008', 'Dinas Pertanian dan Ketahanan Pangan', 'Jl. A. Yani Utara 202 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200009', 'Dinas Perumahan dan Kawasan Pemukiman (DISPERKIM)', 'Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200010', 'Dinas Koperasi dan Usaha Mikro', 'Jl. Panji Suroso 18 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200011', 'Dinas Kepemudaan dan Olahraga (DISPORA)', 'Jl. Tenes Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200012', 'Dinas Kepedudukan dan Pencatatan Sipil (DISPENDUKCAPIL)', 'Perkantoran Terpadu Gedung A Lt. 2, Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200013', 'Dinas Pemberdayaan Perempuan, Perlindungan Anak, Pengendalian Penduduk dan Keluarga Berencana (DP3AP2KB)', 'Jl. Ki Ageng Gribig Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200014', 'Dinas Lingkungan Hidup (DLH)', 'Jl. Mojopahit Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200015', 'Dinas Perpustakaan Umum dan Arsip Daerah', 'Jl. Besar Ijen No.30a Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200016', 'Badan Kesatuan Bangsa dan Politik (BAKESBANGPOL)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200017', 'Badan Kepegawaian Daerah', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200018', 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (DPMPTSP)', 'Perkantoran Terpadu Gedung A Lt. 2 Malang,\r\nJl. Mayjend. Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200019', 'Dinas Tenaga Kerja (DISNAKER)', 'Perkantoran Terpadu Gedung B Lt.3, Jl. Mayjen Sungkono Malang 65132', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200020', 'Dinas Pekerjaan Umum dan Penataan Ruang (DPUPR)', ' Jl. Bingkil 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200021', 'Dinas Sosial (DINSOS)', 'Jl. Sulfat No. 12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200022', 'Inspektorat', 'Jl. Gajah Mada No. 2A Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200023', 'Badan Perencanaan, Penelitian dan Pengembangan (Barenlitbang)', 'Jl. Tugu No. 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200024', 'Badan Pelayanan Pajak Daerah (BP2D)', 'Perkantoran Terpadu Gedung B Lt.1 Jl. Mayjen Sungkono Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200025', 'Badan Pengelola Keuangan dan Aset Daerah (BPKAD)', 'Jl. Tugu 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200026', 'Badan Penanggulangan Bencana Daerah (BPBD)', 'Jl. Jend. A. Yani 98 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200027', 'Satuan Polisi Pamong Praja', 'Jl. Simpang Mojopahit No 1 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200028', 'Kecamatan Klojen', 'Jl. Surabaya 6 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200029', 'Kecamatan Blimbing', 'Jl. Raden Intan Kav. 14 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200030', 'Kecamatan Lowokwaru', 'Jl. Cengger Ayam I/12 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200031', 'Kecamatan Sukun', 'Jl. Keben I Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200032', 'Kecamatan Kedungkandang', 'Jl. Mayjen Sungkono 59 Malang', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200033', 'Badan Pengawas Pemilihan Umum (BAWASLU)', 'Jl. Teluk Cendrawasih No.206, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200034', 'Dewan Perwakilan Rakyat Daerah Kota Malang (DPRD)', 'Jl. Tugu No.1A, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65119', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200035', 'Rumah Potong Hewan Kota Malang', 'Jl. Kol. Sugiono No. 176, Malang, Jawa Timur 65148', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200036', 'PDAM Kota Malang ', 'Jl. Danau Sentani Raya No.100, Madyopuro, Kedungkandang, Kota Malang, Jawa Timur 65142', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200037', 'Komisi Pemilihan Umum Kota Malang (KPU) ', 'Jl. Bantaran No.6, Purwantoro, Blimbing, Kota Malang, Jawa Timur 65126', '', '0', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00'),
('DN200038', 'Bank Perkreditan Rakyat', 'JL. Borobudur no. 18', '', '0', '0000-00-00 00:00:00', 'AD2019020001', '2019-02-28 09:48:41'),
('50', 'dinas perbuatan tidak menyenangkan', 'Malang', '', '1', '2019-02-27 03:35:41', 'AD2019020001', '2019-02-27 03:35:24'),
('52', 'surya', 'surya', '', '1', '2019-03-04 02:58:07', 'AD2019020001', '2019-03-01 03:57:29'),
('', 'admin', 'malang', '{}', '0', '0000-00-00 00:00:00', '0', '2019-03-13 10:03:22'),
('DN200039', 'test surya', 'malang', 'no', '0', '0000-00-00 00:00:00', '123', '0000-00-00 00:00:00'),
('DN200040', 'test_surya1', 'malang', '@p2', '0', '0000-00-00 00:00:00', '@p3', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `faskes_jenis`
--

CREATE TABLE `faskes_jenis` (
  `id_jenis` varchar(10) NOT NULL,
  `nama_jenis` varchar(64) NOT NULL,
  `icon_32` text NOT NULL,
  `icon_64` text NOT NULL,
  `waktu` datetime NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faskes_jenis`
--

INSERT INTO `faskes_jenis` (`id_jenis`, `nama_jenis`, `icon_32`, `icon_64`, `waktu`, `is_delete`, `id_admin`) VALUES
('FSKS100001', 'Apotek', '6be374069041ea3fb6b9f2509321d5bdf4a123f120991fbc3fc3ff9f1b32208e_32.jpg', '6be374069041ea3fb6b9f2509321d5bdf4a123f120991fbc3fc3ff9f1b32208e_64.jpg', '2019-06-27 10:24:20', '0', '0'),
('FSKS100002', 'Laboratorium', 'c24579fc05ffe0e7318adce9b86bb986d89a398f36772cb4f1916eb727ffec90_32.jpg', 'c24579fc05ffe0e7318adce9b86bb986d89a398f36772cb4f1916eb727ffec90_64.jpg', '2019-07-03 05:34:09', '0', 'AD2019020001'),
('FSKS100003', 'Optik', '2899dcb1416a59041a5a337b84901a2a613e08cd71f5cb47939dd773e80a2c90_32.jpg', '2899dcb1416a59041a5a337b84901a2a613e08cd71f5cb47939dd773e80a2c90_64.jpg', '2019-07-02 05:43:51', '0', 'AD2019020001'),
('FSKS100004', 'Dokter Gigi', 'f5a9cee1802979ad9494ebd2555dc5021d1ee9d29de1615505ede4635d4aa379_32.jpg', 'f5a9cee1802979ad9494ebd2555dc5021d1ee9d29de1615505ede4635d4aa379_64.jpg', '2019-05-14 06:10:42', '0', '0'),
('FSKS100006', 'Rumah Sakit', '28b37a4c9364981692a47b7d7c8e9a3e76f130334a1872d6c8bbeccae43c7147_32.jpg', '28b37a4c9364981692a47b7d7c8e9a3e76f130334a1872d6c8bbeccae43c7147_64.jpg', '2019-05-14 06:11:14', '0', '0'),
('FSKS100007', 'Puskesmas', '68c98b2321e10f1727c229a67315edc01456836c0342a002cadbc046ee730845_32.jpg', '68c98b2321e10f1727c229a67315edc01456836c0342a002cadbc046ee730845_64.jpg', '2019-05-14 06:11:31', '0', '0'),
('FSKS100008', 'Dokter Praktek', '209c422369445884ec9c2e3e664f152241f008f0905beb3152af46901b014ad0_32.jpg', '209c422369445884ec9c2e3e664f152241f008f0905beb3152af46901b014ad0_64.jpg', '2019-06-27 10:16:54', '0', 'AD2019020001'),
('FSKS100009', 'Klinik', '28cad0dec0f28514f3b45f8e2ec4781c665d377eb2196388a82866575217c7f3_32.jpg', '28cad0dec0f28514f3b45f8e2ec4781c665d377eb2196388a82866575217c7f3_64.jpg', '2019-07-03 05:34:40', '0', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `faskes_main`
--

CREATE TABLE `faskes_main` (
  `id_faskes` varchar(19) NOT NULL,
  `id_jenis` varchar(10) NOT NULL,
  `nama_faskes` text NOT NULL,
  `foto_faskes` varchar(70) NOT NULL,
  `lokasi` varchar(64) NOT NULL,
  `detail_faskes` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faskes_main`
--

INSERT INTO `faskes_main` (`id_faskes`, `id_jenis`, `nama_faskes`, `foto_faskes`, `lokasi`, `detail_faskes`, `is_delete`, `waktu`, `id_admin`) VALUES
('FSKSM20190627100003', 'FSKS100003', 'Vista Optik', '81114f36cb1c21575d80d992dde5b68aaef61c0b41aa5c55da7d28ac9cbb3d82.jpg', '[\'-7.984975\',\'112.631139\']', '{\'alamat\': \'Jl. S.W Pranoto No.31 A, Sukoharjo, Kec. Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'vista-optic.business.site\',\'tlp\': \'0341351466\'}', '0', '2019-06-27 10:44:41', 'AD2019020001'),
('FSKSM20190701100001', 'FSKS100003', 'Intercontinental Optik', 'bcbbd6f98c7cc3a5bb0afe79315fa992be2197e967c7c0b249781d70e0f953ac.jpg', '[\'-7.984713\',\'112.630831\']', '{\'alamat\': \'Jl. S.W Pranoto No.2F Kauman, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'intercontinental.business.site\',\'tlp\': \'0341366306\'}', '0', '2019-07-01 03:01:51', 'AD2019020001'),
('FSKSM20190701100002', 'FSKS100003', 'Optik Lensa', 'e518802e6040a56d577ebaff16a38f8f29522ceb1f0cbf7a21a324f898161ff5.jpg', '[\'-7.951862\',\'112.609068\']', '{\'alamat\': \'Jl. Raya Sumbersari No.Kav.5, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'www.google.com\',\'tlp\': \'081333525624\'}', '0', '2019-07-02 04:55:21', 'AD2019020001'),
('FSKSM20190701100003', 'FSKS100003', 'Optik Melawai (Blimbing)', '842fdeb910f8e100e11ee2317c66094bb336c7dfdc8c58b21d52a80bfec62e5b.jpg', '[\'-7.936493\',\'112.650152\']', '{\'alamat\': \'Araya Plaza Lt. Dasar No.I14-I15 Megah, Jl. Raya Blimbing Indah, Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65111\',\'url\': \'optikmelawai.com\',\'tlp\': \'0341409325\'}', '0', '2019-07-01 03:03:08', 'AD2019020001'),
('FSKSM20190701100004', 'FSKS100003', 'Optik Melawai (MATOS)', '2f8e149f27adc5daa5f2f05860daf41a52cec9ab66f2c84b579a1517acbfb3d2.jpg', '[\'-7.956783\',\'112.618697\']', '{\'alamat\': \'Malang Town Square GF Blok Gs 39 No. 1-3, 7, Jalan Veteran Malang No.5, Ketawanggede, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113\',\'url\': \'optikmelawai.com\',\'tlp\': \'0341559211\'}', '0', '2019-07-01 03:04:48', 'AD2019020001'),
('FSKSM20190702100001', 'FSKS100003', 'Optik Melawai (S.W Pranoto)', 'b011d71933ffd9d41b781a58c8da02e07d3bf792047dbc0843c78b06df401e3f.jpg', '[\'-7.985029\',\'112.630778\']', '{\'alamat\': \'Wiryopranoto No.2-2A, Jl. S.W Pranoto, Sukoharjo, Malang, Kota Malang, Jawa Timur 65118\',\'url\': \'optikmelawai.com\',\'tlp\': \'0341368591\'}', '0', '2019-07-02 04:57:29', 'AD2019020001'),
('FSKSM20190702100002', 'FSKS100003', 'Internasional Optik (S.W Pranoto)', 'b59f7ecf2cddbf4320c83813735b3ab0d2332cc1195cd145c216607486b66c60.jpg', '[\'-7.984637\',\'112.631222\']', '{\'alamat\': \'Jl. S.W Pranoto No.25, Sukoharjo, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'internasionaloptik.com\',\'tlp\': \'0341361651\'}', '0', '2019-07-02 04:58:34', 'AD2019020001'),
('FSKSM20190702100003', 'FSKS100003', 'Jaya Optik', '68c575e59c3ecfb1d666186859b2e5d5ca1e3e1aa06bd78b94281795bd16114b.jpg', '[\'-7.984782\',\'112.631154\']', '{\'alamat\': \'Jl. S.W Pranoto No.25-A, Sukoharjo, Kec. Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'jaya-optic.business.site\',\'tlp\': \'0341366593\'}', '0', '2019-07-02 05:07:31', 'AD2019020001'),
('FSKSM20190702100004', 'FSKS100003', 'Focus Optik', '4e740cd1d400358c7b4cec199b5c5d028a70aad310a80fb96e067a1965edd3ef.jpg', '[\'-7.976379\',\'112.630330\']', '{\'alamat\': \'Jl. Kahuripan No.16 B, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'www.google.com\',\'tlp\': \'0341347109\'}', '0', '2019-07-02 05:08:07', 'AD2019020001'),
('FSKSM20190702100005', 'FSKS100003', 'Optik Sebintang', 'eb2da250d280e2cfe9f9690c564f69ac762d85e166d174fcfadf3e0e0990c3f7.jpg', '[\'-7.954499\',\'112.644521\']', '{\'alamat\': \'No. 50 Ruko Kav. H, Jl. Ciliwung, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65125\',\'url\': \'optik-sebintang.business.site\',\'tlp\': \'0341480607\'}', '0', '2019-07-02 05:06:49', 'AD2019020001'),
('FSKSM20190702100006', 'FSKS100003', 'Optik Star', '650475a31153e17f788dfe26457ad43870fdb523e39005aefdd57c5d8e03b1fc.jpg', '[\'-7.960934\',\'112.636844\']', '{\'alamat\': \'Jl. Letjen Sutoyo No.56, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65141\',\'url\': \'optikstar.business.site\',\'tlp\': \'0341491873\'}', '0', '2019-07-02 05:11:51', 'AD2019020001'),
('FSKSM20190702100007', 'FSKS100003', 'Optik Malang Eye Center', '072a1e1d8e21cc80f8dd2875de7ec84f7223fcd8ae2373f82b7eb6a317b09bea.jpg', '[\'-7.967599\',\'112.634793\']', '{\'alamat\': \'Jalan Doktor Cipto No. 3, Rampal Celaket, Klojen, 7-12 siang 3 -8 4-7, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'www.google.com\',\'tlp\': \'0341341666\'}', '0', '2019-07-02 05:12:25', 'AD2019020001'),
('FSKSM20190702100008', 'FSKS100003', 'Djoyo Abadi Optik', '5fc2663e403cc3a7344c5c1d29792e952a6ace4d6355061427d367dcbe731623.jpg', '[\'-7.984447\',\'112.632960\']', '{\'alamat\': \'Plaza Malang, Jl. Agus Salim No.26-28, Sukoharjo, Kec. Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'djoyoabadioptik.business.site\',\'tlp\': \'08123316559\'}', '0', '2019-07-02 05:15:29', 'AD2019020001'),
('FSKSM20190702100009', 'FSKS100003', 'Optik Tunggal', 'd4515f211d01dd853ed46593c53f832c2d89030c66cb735bf59aec8b14d22174.jpg', '[\'-7.977205\',\'112.622882\']', '{\'alamat\': \'Mal Olympic Garden, Jl. Kawi No.24, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'optiktunggal.com\',\'tlp\': \'0341363299\'}', '0', '2019-07-02 05:17:26', 'AD2019020001'),
('FSKSM20190702100010', 'FSKS100003', 'Indo Optik', '7ec9ebfc1d13e6c2685485d165ccbe557670b4fe7d20dd3108ee4de3e74c4527.jpg', '[\'-7.980745\',\'112.627686\']', '{\'alamat\': \'Jl. Arif Rahman Hakim No.26F, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'altrams.id\',\'tlp\': \'0341329466\'}', '0', '2019-07-02 05:18:07', 'AD2019020001'),
('FSKSM20190702100011', 'FSKS100003', 'Optik Seis', '5b0aaf4ed1ccc7a104b1935e9c8eed2d90128fea0b2059a93543dd37b9257551.jpg', '[\'-7.976978\',\'112.623576\']', '{\'alamat\': \'Ground Floor, Kawi Street No.24, Kauman, Klojen, Malang City, East Java 65116\',\'url\': \'optikseis.com\',\'tlp\': \'0341363288\'}', '0', '2019-07-02 05:18:48', 'AD2019020001'),
('FSKSM20190702100012', 'FSKS100003', 'Optik Talun', 'e00ecd3d69720220d7bbc6e40aa4303b1b422266067e4bca3eaec5db61d37dc6.jpg', '[\'-7.980970\',\'112.628254\']', '{\'alamat\': \'Jl. Arif Rahman Hakim No.18, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'optik-talun.business.site\',\'tlp\': \'0341327719\'}', '0', '2019-07-02 05:19:59', 'AD2019020001'),
('FSKSM20190702100013', 'FSKS100003', 'Optik Melawai (MOG)', 'd84ca77d1683669657d9ea81bda8946fea562b477de09afd711eb35564ff3242.jpg', '[\'-7.977170\',\'112.623665\']', '{\'alamat\': \'Olympic Mall Ground Floor No. 8-10, 18-20, Jalan Kawi No.18, Kauman, Klojen, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65146\',\'url\': \'optikmelawai.com\',\'tlp\': \'0341363110\'}', '0', '2019-07-02 05:20:50', 'AD2019020001'),
('FSKSM20190702100014', 'FSKS100003', 'Optik Internasional (Klojen)', 'e03c06f5a8414591b61b9afacb0b78c59b2e56319e5392544f2c342d0d2c2570.jpg', '[\'-7.984639\',\'112.631198\']', '{\'alamat\': \'Jl. S.W Pranoto No.25, Sukoharjo, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'internasionaloptik.com\',\'tlp\': \'0341361651\'}', '0', '2019-07-02 05:21:41', 'AD2019020001'),
('FSKSM20190702100015', 'FSKS100003', 'Optik Tunggal (Terusan Dieng)', '3b39331b2a7f0e386828add7642b7d76927c5b1ccf477f190f703a54c12033bc.jpg', '[\'-7.973583\',\'112.611872\']', '{\'alamat\': \'Jln. Terusan Dieng No.32 Lantai 1 No.7A, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65115\',\'url\': \'optiktunggal.com\',\'tlp\': \'03415024709\'}', '0', '2019-07-02 05:22:38', 'AD2019020001'),
('FSKSM20190702100016', 'FSKS100003', 'JM TOP Optik', 'e7fd5cf8c93135f83a1754fe23f33157695b39888b0210972478a78d68102dcd.jpg', '[\'-7.984716\',\'112.630833\']', '{\'alamat\': \'Jl. S.W Pranoto No.2E, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'jmtopoptical.com\',\'tlp\': \'0341353357\'}', '0', '2019-07-02 05:24:45', 'AD2019020001'),
('FSKSM20190702100017', 'FSKS100003', 'Optik Internasional A Yani', '50371f919431b4ec00bc1a20e24176bcf426a6e48b0aa3d3e2a060f8987d281b.jpg', '[\'-7.943350\', \'112.641644\']', '{\'alamat\': \'Jl. A. Yani No.14b, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'internasionaloptik.com\',\'tlp\': \'0341353357\'}', '0', '2019-07-02 05:26:12', 'AD2019020001'),
('FSKSM20190702100018', 'FSKS100003', 'Optik Internasional MT Haryomo', 'd511c5d1922111cfb2122bf6a5ad11f8d96f4c2bff034580eeeae36516839717.jpg', '[\'-7.939946\',\'112.608042\']', '{\'alamat\': \'Jl. MT. Haryono No.154, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'internasionaloptik.com\',\'tlp\': \'+62341551830\'}', '0', '2019-07-03 05:05:01', 'AD2019020001'),
('FSKSM20190702100019', 'FSKS100003', 'Optik Visual', 'b0516fb3b2cfa4402f2d15e1b9f6ac71dafe38bb095ff970704a7ce0039d2284.jpg', '[\'-7.983168\',\'112.616149\']', '{\'alamat\': \'Ruko Centra Niaga Ijen H2-6, Jl. IR. Rais, Tanjung Rejo, Bareng, Kec. Klojen, Kota Malang, Jawa Timur 65147\',\'url\': \'www.google.com\',\'tlp\': \'0341350915\'}', '0', '2019-07-02 05:31:45', 'AD2019020001'),
('FSKSM20190702100020', 'FSKS100003', 'Optik Melawai', 'c7737ea6c0b5d9f6c95fa38a9cadb17f36bcffc53ebd45af7695b4564c987046.jpg', '[\'-7.981444\',\'112.654074\']', '{\'alamat\': \'Giant Sawojajar Malang GF-06, Jl. Raya Sawojajar, Sawojajar, Kedungkandang, Lesanpuro, Kedungkandang, Malang City, East Java 65139\',\'url\': \'optikmelawai.com\',\'tlp\': \'optikmelawai.com\'}', '0', '2019-07-02 05:32:19', 'AD2019020001'),
('FSKSM20190702100021', 'FSKS100003', 'Optik Internasional MOG', '0b035609efc50690b55f7bff2d304dddf279b2b99ab6e3693bca465a074676f8.jpg', '[\'-7.977185\',\'112.622904\']', '{\'alamat\': \'Mall Olympic Garden Lt. LG No.2, Jl. Kawi No.24, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65116\',\'url\': \'internasionaloptik.com\',\'tlp\': \'0341363396\'}', '0', '2019-07-02 05:32:51', 'AD2019020001'),
('FSKSM20190702100022', 'FSKS100003', 'Grosir Optical', '7afbf3c8afb0e2999b6d8c80ced62215a46023bec1220afd2730dcd00f48a35c.jpg', '[\'-7.948470\',\'112.626583\']', '{\'alamat\': \'Jl. Kalpataru No.106, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141\',\'url\': \'grosiroptical.business.site\',\'tlp\': \'082230577567\'}', '0', '2019-07-02 05:33:19', 'AD2019020001'),
('FSKSM20190702100023', 'FSKS100003', 'Nowo Optical ', '54eb91618e32e3239b31655ae3938b7e85d573349705c64c8398ecee8e4d6d6e.jpg', '[\'-7.937872\',\'112.621193\']\r\n', '{\'alamat\': \'Jl. Candi Panggung No.23 A, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'nwsoftlens.com\',\'tlp\': \'085785199969\'}', '0', '2019-07-02 05:40:54', 'AD2019020001'),
('FSKSM20190702100024', 'FSKS100003', 'Optik Dharma Lensa', 'b9eb79ff4fd95fc3b1270acab9c4a6d10231d6ce3b98c80f39dc3cd5000187d5.jpg', '[\'-7.979770\',\'112.660604\']', '{\'alamat\': \'Jalan Danau Toba Blok E6 No.19, Lesanpuro, Kedungkandang, Lesanpuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65138\',\'url\': \'www.google.com\',\'tlp\': \'0341725964\'}', '0', '2019-07-02 05:41:47', 'AD2019020001'),
('FSKSM20190703100001', 'FSKS100002', 'Laboratorium Klinik Umum (Bromo)', '8d509a0a80b9ec9507d9e373a1ed68e6e912bb1a0353b7c65925e65dd403e44c.jpg', '[\'-7.972917\',\'112.627601\']', '{\'alamat\': \'Jl. Bromo No.49, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia\',\'url\': \'labbromo.com\',\'tlp\': \'+62341327787\'}', '0', '2019-07-03 05:03:31', 'AD2019020001'),
('FSKSM20190703100002', 'FSKS100002', 'Laboratorium Klinik Umum (Kawi 31)', '8cbe58da58af5ec6834996841b53d3fdd0a503a495e032f568cebe984683d93e.jpg', '[\'-7.977840\',\'112.623144\']', '{\'alamat\': \'Jl. Kawi No.31, Bareng, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'www.google.com\',\'tlp\': \'+62341325197\'}', '0', '2019-07-03 05:04:08', 'AD2019020001'),
('FSKSM20190703100003', 'FSKS100002', 'Laboratorium Klinik Umum (Pattimura)', '7137f2a06031ca234666dceaed7c6bd6ef077def03eb12636ba14acdd8a62d5f.jpg', '[\'-7.972368\',\'112.633585\']', '{\'alamat\': \'Jl. Patimura No.17, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62341324747\'}', '0', '2019-07-03 05:05:52', 'AD2019020001'),
('FSKSM20190703100004', 'FSKS100002', 'Laboratorium Klinik Umum (Sima Takuban Perahu)', '0c6fc4580cb437d23bc56d6a31fee1d0e7105edafb8b9358e5ba6526f2055378.jpg', '[\'-7.976623\',\'112.625538\']', '{\'alamat\': \'Jl. Tangkuban Perahu No.14, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia\',\'url\': \'simalab.co.id\',\'tlp\': \'+62 341 326060\'}', '0', '2019-07-03 05:07:16', 'AD2019020001'),
('FSKSM20190703100005', 'FSKS100002', 'Laboratorium Klinik Umum (Ciliwung)', 'b3383ac5d818e7d7ce410efe2b2331dd8583fed831d84fe451623ff7d9e8b16f.jpg', '[\'-7.952794\',\'112.640990\']', '{\'alamat\': \'Jl. Ciliwung No.10, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia\',\'url\': \'labciliwung.co.id\',\'tlp\': \'+62341491681\'}', '0', '2019-07-03 05:08:14', 'AD2019020001'),
('FSKSM20190703100006', 'FSKS100002', 'Laboratorium Klinik Umum  (Griya Melati)', 'f258d76402e0f09e42d166a82ec0f78c547b28a3e7997cbb0ee714ebb3ad927d.jpg', '[\'-7.971357\',\'112.631336\']', '{\'alamat\': \'Jl. Jaksa Agung Suprapto No.23, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65112, Indonesia\',\'url\': \'klinikbungamelati.com\',\'tlp\': \'+62341360049\'}', '0', '2019-07-03 05:09:11', 'AD2019020001'),
('FSKSM20190703100007', 'FSKS100002', 'Laboratorium Klinik Umum (Sima Ciliwung)', '0e8b8353770f78b0846b99882b7a85a7094b92c9f723c80b13da18c548345ecd.jpg', '[\'-7.953748\',\'112.643661\']', '{\'alamat\': \'Jl. Ciliwung No.51, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65122, Indonesia\',\'url\': \'simalab.co.id\',\'tlp\': \'+62341486629\'}', '0', '2019-07-03 05:09:48', 'AD2019020001'),
('FSKSM20190703100008', 'FSKS100002', 'Laboratorium Klinik Umum (Parahita Diagnostic Centre)', '0fa445b202cf817b784a266a44f5a619ccfbfc61aeab97dedacee0626624974b.jpg', '[\'-7.963430\',\'112.633156\']', '{\'alamat\': \'Jl. Kaliurang No.30, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65111, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 4376877\'}', '0', '2019-07-03 05:10:41', 'AD2019020001'),
('FSKSM20190703100009', 'FSKS100002', 'Laboratorium Klinik Umum (Prodia)', '694a4f429c9c52e9d37d9ac7b871d4f57427a13bb66b525816d7e1756fc620bd.jpg', '[\'-7.965243\',\'112.619972\']', '{\'alamat\': \'Jl. Jakarta No.60, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145, Indonesia\',\'url\': \'prodia.co.id\',\'tlp\': \'+62 341 566444\'}', '0', '2019-07-03 05:11:16', 'AD2019020001'),
('FSKSM20190703100010', 'FSKS100002', 'Laboratorium Klinik Umum (Kimia Farma)', '22f4cd0af3c51b03ee09375ab67342d0d1a7d761f5376541cd79e7836c325d2f.jpg', '[\'-7.269568\',\'112.756204\']', '{\'alamat\': \'Jl. Dharmawangsa No.24, Airlangga, Kec. Gubeng, Kota SBY, Jawa Timur 60286, Indonesia\',\'url\': \'kimiafarma.co.id\',\'tlp\': \'+62 31 5043356\'}', '0', '2019-07-03 05:13:49', 'AD2019020001'),
('FSKSM20190704100001', 'FSKS100007', 'Puskesmas Gribig', '6e79a7d6ce08e6bfe1d5ac6cc114dc3f8d38ac1317e9ebe8875a948922d8cb1c.jpg', '[\'-7.980657\',\'112.665262\']', '{\'alamat\': \'Jl. Raya Ki Ageng Gribig No.97, Madyopuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 718165\'}', '0', '2019-07-04 04:59:54', 'AD2019020001'),
('FSKSM20190704100002', 'FSKS100007', 'Puskesmas Arjowinangun', 'be2e940e31d8d4de83807ce89a158960b2e246763e7368c4535fe5eab88652d2.jpg', '[\'-8.038610\',\'112.641868\']', '{\'alamat\': \'Jl. Raya Arjowinangun No.2, Arjowinangun, Kec. Kedungkandang, Kota Malang, Jawa Timur 65132, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 754909\'}', '0', '2019-07-04 05:00:14', 'AD2019020001'),
('FSKSM20190704100003', 'FSKS100007', 'Puskesmas Janti', '5178b83ffda3f9b5477aafdcd2bfac333a49dfe20e4c38a2eaa292d1d3cc48d6.jpg', '[\'-8.001003\',\'112.620544\']', '{\'alamat\': \'Jalan Janti, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65148, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 352203\'}', '0', '2019-07-04 05:00:35', 'AD2019020001'),
('FSKSM20190704100004', 'FSKS100007', 'Puskesmas Ciptomulyo', 'a4352168145c16d6bb34d48fccdd37dddefe6bd78643845b1c84444e1859fa18.jpg', '[\'-8.002040\',\'112.629969\']', '{\'alamat\': \'Jalan Kolonel Sugiono 8 No.54, Ciptomulyo, Kec. Sukun, Kota Malang, Jawa Timur 65148, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 329918\'}', '0', '2019-07-04 04:16:18', 'AD2019020001'),
('FSKSM20190704100005', 'FSKS100007', 'Puskesmas Mojolangu ', '18b71ddca902a117c092f791b8e6105d1bd2309a9c6ad291f6877078d010e08b.jpg', '[\'-7.938533\',\'112.632035\']', '{\'alamat\': \'Jl. Sudimoro No.17 A, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142, Indonesia\',\'url\': \'puskmojolangu.malangkota.go.id\',\'tlp\': \'+62 341 482905\'}', '0', '2019-07-04 04:16:59', 'AD2019020001'),
('FSKSM20190704100006', 'FSKS100007', 'Puskesmas Arjuno ', '88ee7ee336abbfe4977b313293b3f7c89c5dd746c24b09029f86494181e742da.jpg', '[\'-7.978134\',\'112.626635\']', '{\'alamat\': \'Jl. Simpang Arjuno No.17, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 356339\'}', '0', '2019-07-04 04:17:52', 'AD2019020001'),
('FSKSM20190704100007', 'FSKS100007', 'Puskesmas Bareng ', 'd47d0da58bb922329e18a26f7943cdbe422194516135613a7bebc7a7de3a20ad.jpg', '[\'-7.978525\',\'112.622758\']', '{\'alamat\': \'Jl. Bareng Tenes 4A No.639, Bareng, Kec. Klojen, Kota Malang, Jawa Timur 65116, Indonesia\',\'url\': \'puskbareng.malangkota.go.id\',\'tlp\': \'+62 341 322280\'}', '0', '2019-07-04 04:18:39', 'AD2019020001'),
('FSKSM20190704100008', 'FSKS100007', 'Puskesmas Rampal Celaket ', 'ee30917f2668598cb0e28a7e8258eec40340dfacfe4d7cccb17bbeafbbba0296.jpg', '[\'-7.964381\',\'112.631729\']', '{\'alamat\': \'Jalan Simpang Kesembon No. 5, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 356380\'}', '0', '2019-07-04 04:19:13', 'AD2019020001'),
('FSKSM20190704100009', 'FSKS100007', 'Puskesmas Kendalkerep', '5a2668a31408fd39ef6ce5a523652b6a7a14c8653832f1441986bcc65b2e2bb7.jpg', '+62 341 484477', '{\'alamat\': \'Jalan Raya Sulfat No. 100 Purwantoro, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 484477\'}', '0', '2019-07-04 04:19:57', 'AD2019020001'),
('FSKSM20190704100010', 'FSKS100007', 'Puskesmas Cisadea ', 'c1294d5d8950ea9308304e4c00f83678fe155eeba736ec6f66fae6b582c53cdf.jpg', '[\'-7.955448\',\'112.643581\']', '{\'alamat\': \'Jl. Cisadea No.19, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65122, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 489540\'}', '0', '2019-07-04 04:20:33', 'AD2019020001'),
('FSKSM20190704100011', 'FSKS100007', 'Puskesmas Pandanwangi', 'bef95f2a346adc640c52475188081fcfd8fb73cdab607107ad7f46d724403c57.jpg', '[\'-7.947333\',\'112.655490\']', '{\'alamat\': \'Jl. Laksda Adi Sucipto No.315, Pandanwangi, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 484472\'}', '0', '2019-07-04 04:21:06', 'AD2019020001'),
('FSKSM20190704100012', 'FSKS100007', 'Puskesmas Kedungkandang ', 'fca81ce3a5a69c2d04a24eb6554938923b65075da376c3782e8e63f015aba83b.jpg', '[\'-7.993112\',\'112.648132\']', '{\'alamat\': \'Jl. Raya Ki Ageng Gribig No.142, Kedungkandang, Kec. Kedungkandang, Kota Malang, Jawa Timur 65138, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 710112\'}', '0', '2019-07-04 04:22:01', 'AD2019020001'),
('FSKSM20190704100013', 'FSKS100007', 'Puskesmas Dinoyo ', '9fdf40a932bd6a07a14b8cfe2557e9c39ba7847142a2b91ad74b0db8267a43b0.jpg', '[\'-7.943372\',\'112.611279\']', '{\'alamat\': \'Jalan Mayjend M.T. Haryono, Dinoyo, Kecamatan Lowokwaru, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 572640\'}', '0', '2019-07-04 04:55:44', 'AD2019020001'),
('FSKSM20190704100014', 'FSKS100007', 'Puskesmas Kendalsari ', 'b9cc43368d037396d45510eb741b80eaab89fd698e9afd5a63de02ef3ab9b878.jpg', '[\'-7.946198\',\'112.630853\']', '{\'alamat\': \'Jl. Cengger Ayam I No.8, RW.02, Tulusrejo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 478215\'}', '0', '2019-07-04 04:58:09', 'AD2019020001'),
('FSKSM20190704100015', 'FSKS100007', 'Puskesmas Mulyorejo ', '5cc3f9439898f0c6b9e3f88b4c04a4b2bd4030c56db8045ae9c9c5c9d9c3165d.jpg', '[\'-7.987897\',\'112.597677\']', '{\'alamat\': \'Jl. Raya Mulyorejo No.11A, Mulyorejo, Kec. Sukun, Kota Malang, Jawa Timur 65147, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 580955\'}', '0', '2019-07-04 04:58:38', 'AD2019020001'),
('FSKSM20190704100016', 'FSKS100007', 'Puskesmas Polowijen', '1887b817852bee894e6b636d426d7c77f55e5901048e5a3ee89a1834262016ca.jpg', '[\'-7.932217\',\'112.650060\']', '{\'alamat\': \'Polowijen, Blimbing, Malang City, East Java 65126, Indonesia\',\'url\': \'dinkes.malangkota.go.id\',\'tlp\': \'+62 341 491320\'}', '0', '2019-07-04 04:59:15', 'AD2019020001'),
('FSKSM20190704100017', 'FSKS100009', 'Klinik PT. HM. Sampoerna, Tbk', 'ed42fc6032312c051852bd95813462f67a96e8c84f4fcb2229e7775a025619b9.jpg', '[\'-7.945451\',\'112.643253\']', '{\'alamat\': \'JL. Industri Barat, 2, Malang, 65122, Kauman, Klojen, Malang City, East Java 65119\',\'url\': \'sampoerna.com\',\'tlp\': \'+62 341 291003\'}', '0', '2019-07-05 05:51:09', 'AD2019020001'),
('FSKSM20190704100018', 'FSKS100009', 'Klinik Panti Rahayu', '584c4ca37bbad7d710aa2dd8e8951e09b2c674c71e387f5467ad685a468b1eb0.jpg', '[\'-7.957863\',\'112.614115\']', '{\'alamat\': \'Jl. Bendungan Sutami, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'umc.umm.ac.id\',\'tlp\': \'+62 341 587760\'}', '0', '2019-07-04 09:31:47', ''),
('FSKSM20190704100019', 'FSKS100009', 'Klinik Panti Rahayu', '418bd10d94c710dfa2046aca51eafe387aabd978678dbb017128164ddd640bb3.jpg', '[\'-7.939785\',\'112.638655\']', '{\'alamat\': \'Jl. Simpang Borobudur No.1, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 491776\'}', '0', '2019-07-04 09:32:38', ''),
('FSKSM20190704100020', 'FSKS100009', 'Malang Eye Centre', 'a4de70701fa87d35142c85b5b11715764a2d511b03a58004579a9e1d7432a8ca.jpg', '[\'-7.967590\',\'112.634812\']', '{\'alamat\': \'Jalan Doktor Cipto No. 3, Rampal Celaket, Klojen, 7-12 siang 3 -8 4-7, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 341666\'}', '0', '2019-07-04 09:33:29', ''),
('FSKSM20190704100021', 'FSKS100009', 'Klinik Bunga Melati', '6fb3898bcf56568c90f5030d4ac8a745f0a63dafa1b1c895b6f68b78ccdfbf2f.jpg', '[\'-7.971141\',\'112.631556\']', '{\'alamat\': \'Jl. Jaksa Agung Suprapto No.23, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65112\',\'url\': \'klinikbungamelati.com\',\'tlp\': \'+62 341 360049\'}', '0', '2019-07-04 09:34:30', ''),
('FSKSM20190704100022', 'FSKS100009', 'Klinik Patimura', '1e0bc2b06de3bd600dce0bf6cd6374a91797f25c501b3cb6eb8d3df241aaee60.jpg', '[\'-7.973028\',\'112.634042\']', '{\'alamat\': \'Jl. Pattimura No. 14A, Klojen, Kec. Klojen, Malang, Jawa Timur 65111\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 324747\'}', '0', '2019-07-04 09:35:18', ''),
('FSKSM20190704100023', 'FSKS100009', 'Kilink Adi Bungsu', 'ac58f67acbc43af8e05f39dd6ec57931c52a74fd071a9e82264f589f43ae86ca.jpg', '[\'-7.985243\',\'112.657411\']', '{\'alamat\': \'Jl. Raya Ki Ageng Gribig, Lesanpuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65138\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 7222229\'}', '0', '2019-07-04 09:36:05', ''),
('FSKSM20190704100024', 'FSKS100009', 'Klinik Widya Husada', 'a351e0328f9002e1c03ee08d51ef01d677ac57861c6261da5a5117b919fd99dc.jpg', '[\'-7.938385\',\'112.632007\']', '{\'alamat\': \'Jl. Sudimoro No.16, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'www.google.com\',\'tlp\': \' +62 341 493080\'}', '0', '2019-07-04 09:52:05', ''),
('FSKSM20190704100025', 'FSKS100009', 'Klinik Higina Medical Centre', '999ee3e5643db06924f9213c9e6b46cacff356df1097229431aeb317aada2a31.jpg', '[\'-7.976767\',\'112.636425\']', '{\'alamat\': \'Jl. Ronggo Warsito No.23, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 368341\'}', '0', '2019-07-04 09:52:42', ''),
('FSKSM20190704100026', 'FSKS100009', 'Klinik Panglima Sudirman', '3356259e3a1d8da32e3501fcc3f084ba0c5dd9c5201dc9aedb32027a50c0e0f7.jpg', '[\'-7.965832\',\'112.639492\']', '{\'alamat\': \'Jl. R. Tumenggung Suryo, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 494795\'}', '0', '2019-07-04 09:53:21', ''),
('FSKSM20190704100027', 'FSKS100009', 'Klinik Karya Nusantara Medica', '963c2ba46fc813781db8472575408b38e46fe83924010c88690df6610bd0a615.jpg', '[\'-7.976892\',\'112.656425\']', '{\'alamat\': \'Jl. Maninjau Raya No. 33-35, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'www.google.com\',\'tlp\': \'+62 812-4581-3623\'}', '0', '2019-07-04 09:53:52', ''),
('FSKSM20190704100028', 'FSKS100009', 'Klinik Sabilillah Medical Service', 'd4a2d41ccac47edcc6bdca99cb8251efd823c18bc740570e1b88e47b46fa8c03.jpg', '[\'-7.941610\',\'112.640970\']', '{\'alamat\': \'Jl. Candi Kidal No.6, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65142\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 493080\'}', '0', '2019-07-04 09:54:34', ''),
('FSKSM20190704100029', 'FSKS100009', 'Klinik Telemedika Health Centre', '79e962a394b1fba266afa191375144d3a971d6f4c39fce1a5fc05c63bcb93c0d.jpg', '[\'-7.979525\',\'112.662929\']', '{\'alamat\': \'STO TELKOM,, Jl. Danau Sentani Utara IV, Madyopuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'m.inhealth.co.id\',\'tlp\': \'+62 341 720655\'}', '0', '2019-07-04 10:02:08', ''),
('FSKSM20190704100030', 'FSKS100009', 'Klinik RN. Klayatan ', 'dca55a82d7b5f6fd08565e1d881b935da291b4e8a5a3d55878c44bd49b6514c1.jpg', '[\'-8.004076\',\'112.613288\']', '{\'alamat\': \'Jl. Klayatan 3, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 837251\'}', '0', '2019-07-04 10:02:41', ''),
('FSKSM20190704100031', 'FSKS100009', 'Klinik Kimia Farma Bromo', '981f7f11f0f7a8e7d4f835e393b5783eafbb00113453aae5d9f4344e773658be.jpg', '[\'-7.978855\',\'112.625134\']', '{\'alamat\': \'Jl. Bromo No.1, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 358021\'}', '0', '2019-07-04 10:03:19', ''),
('FSKSM20190704100032', 'FSKS100009', 'Klinik Bhayangkara Polres Kota Malang', 'f299c9f78085ac75c6194ebc7de22f378992a0b9dec37dbf96e343cc955b4823.jpg', '[\'-7.968682\',\'112.622692\']', '{\'alamat\': \'Jl. Pahlawan Trip No.1, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65115\',\'url\': \'urkesmakota.blogspot.com\',\'tlp\': \'+62 341 568974\'}', '0', '2019-07-04 10:07:04', ''),
('FSKSM20190704100033', 'FSKS100009', 'Klinik Wira Husada', 'eb37415533201f27dcc9f93da21c96fe1ad3311f0132fdfbbd26248a95d8b91f.jpg', '[\'-7.990549\',\'112.621434\']', '{\'alamat\': \'Jl. S. Supriadi 23, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65147\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-07-04 10:07:44', ''),
('FSKSM20190704100034', 'FSKS100009', 'Klinik Bahrul Maghfiroh Cinta Indonesia', 'ed8a8bab383c32eb9bef322dbbdd8613008ed3b3d23325840727a5d121c62105.jpg', '[\'-7.933676\',\'112.603317\']', '{\'alamat\': \'Jl. Batu Permata 1, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 363165\'}', '0', '2019-07-04 10:08:23', ''),
('FSKSM20190704100035', 'FSKS100009', 'Klinik Rampal Denkesyah', 'bb688a7d604390a32a61ffc6719a41d83d15718c1682108faa22f0ffbaea6d65.jpg', '[\'-7.975666\',\'112.638642\']', '{\'alamat\': \'Jl. Panglima Sudirman No.D-9A, Kesatrian, Kec. Blimbing, Kota Malang, Jawa Timur 65111\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 363165\'}', '0', '2019-07-04 10:14:48', ''),
('FSKSM20190704100036', 'FSKS100009', 'Klinik Hamid Rusdi', 'd155688591eafdae86feabd0f526b27a504782a6b43b07955bc63807e8f6312d.jpg', '[\'-7.967963\',\'112.641300\']', '{\'alamat\': \'Jl. Hamid Rusdi No.45, 3, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 3012961\'}', '0', '2019-07-04 10:15:20', ''),
('FSKSM20190704100037', 'FSKS100009', 'Klinik Delta', 'f84dac01e99940a4bef05c2eb8b7b2184c6db4c5ea691bb0e4680a46461c5c2f.jpg', '[\'-8.012526\',\'112.620254\']', '{\'alamat\': \'Jl. Kepuh Gg. X No.47, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 802584\'}', '0', '2019-07-04 10:15:58', ''),
('FSKSM20190704100038', 'FSKS100009', 'Klinik A. Care Clinic', '4a28a854f04d9706315e1e296d3c98baa295a452eeefe8bf360a03daf9bf1ffe.jpg', '[\'-7.992013\',\'112.621192\']', '{\'alamat\': \'Jl. S. Supriadi No.59B, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65147\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 368655\'}', '0', '2019-07-04 10:17:14', ''),
('FSKSM20190704100039', 'FSKS100009', 'Klinik Intan Mandiri', 'cc1819c310c9e110c55390c67b9217b88e1a5bb98d7843ff5beac5c665981c4a.jpg', '[\'-7.976072\',\'112.591898\']', '{\'alamat\': \'Perum Regency One Kav. R6 Bandulan, Kec. Sukun, Kota Malang, Jawa Timur 65146\n\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-07-04 10:22:13', 'AD2019020001'),
('FSKSM20190704100040', 'FSKS100009', 'G and G Health Clinic', 'c8410ab8368d741972c5b46e15a5bcd73a733bc438b153feab7099597deee7a0.jpg', '[\'-7.974101\',\'112.615728\']', '{\'alamat\': \'Jl. Terusan Kawi No.9, Bareng, Kec. Klojen, Kota Malang, Jawa Timur 65116\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 568527\'}', '0', '2019-07-04 10:25:18', 'AD2019020001'),
('FSKSM20190704100041', 'FSKS100009', 'Dinamika Sehat Sakwarase', '6f7e3f542a67f25addf43ac9d345fa0c11103ad8cc087cf1782b536bda9c6e41.jpg', '[\'-7.974786\',\'112.655777\']', '{\'alamat\': \'Jl. Danau Maninjau Barat BI A47 Rt 03 Rw 08, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 3021071\'}', '0', '2019-07-04 10:25:49', 'AD2019020001'),
('FSKSM20190704100042', 'FSKS100009', 'Klinik Bunga Melati Panjaitan', 'a9ad8cc916e79fc20c56874b275c753f009524a086a92b88132c822433d1fb80.jpg', '[\'-7.951527\',\'112.617113\']', '{\'alamat\': \'Jl. Mayjend. Panjaitan No.247, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113\',\'url\': \'klinikbungamelati.com\',\'tlp\': \'+62 341 550121\'}', '0', '2019-07-04 10:26:23', 'AD2019020001'),
('FSKSM20190704100043', 'FSKS100009', 'Almira Medical Clinic', '1de9b742e5e75d262ed38482b8d4e20edbf073d451869b4a5e6eab75a1438410.jpg', '[\'-7.945672\',\'112.651977\']', '{\'alamat\': \'Jl. Laksda Adi Sucipto No.208, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65125\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 493549\'}', '0', '2019-07-04 10:27:11', 'AD2019020001'),
('FSKSM20190704100044', 'FSKS100009', 'Tidar Medika Clinic', '5622c331855890b13980955ccaacd0b307a94ba0061e8572684f45e6c5b46aca.jpg', '[\'-7.962064\',\'112.600120\']', '{\'alamat\': \'Jl. Esberg V-1 No. 7, Tidar, Karangbesuki, Kec. Sukun, Kota Malang, Jawa Timur 65146\',\'url\': \'tidarmedika.com\',\'tlp\': \'+62 341 561373\'}', '0', '2019-07-04 10:27:37', 'AD2019020001'),
('FSKSM20190704100045', 'FSKS100009', 'Esthetic Dental Care', '28a26247a00f32adf6e5d636eb1a8b53643a5ac77e9265e0ae7dfa42321ec073.jpg', '[\'-7.980222\',\'112.661334\']', '{\'alamat\': \'Jalan Raya Danau Toba Blok. E9 No. 01, Lesanpuro, Kedungkandang, Lesanpuro, Malang, Jawa Timur, 65138\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 727822\'}', '0', '2019-07-04 10:28:09', 'AD2019020001'),
('FSKSM20190704100046', 'FSKS100009', 'Klinik Muhammadiyah', '25fdb54798b16e235cd02591e50e65f3f393b098ed8d58ca63fa71511018529c.jpg', '[\'-7.932781\',\'112.646707\']', '{\'alamat\': \'Jl. A. Yani No.165, Purwodadi, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 493059\'}', '0', '2019-07-04 10:32:53', 'AD2019020001'),
('FSKSM20190704100047', 'FSKS100009', 'Klinik Daqu Sehat', '3ab7cdbacdbc98e9d0e99afce76fa419378652d283583f109240b7ed6ff2b733.jpg', '[\'-7.957051\',\'112.605859\']', '{\'alamat\': \'Jl. Sigura - Gura No.15A, Karangbesuki, Kec. Sukun, Kota Malang, Jawa Timur 65145\',\'url\': \'+62 341 2991199\',\'tlp\': \'+62 341 2991199\'}', '0', '2019-07-04 10:33:30', 'AD2019020001'),
('FSKSM20190704100048', 'FSKS100009', 'Klinik Husada Prima', '8275178e0622ecf2cd5d82959a1209ddf67f2e18021881e6e3657fcb0f62c5dc.jpg', '[\'-7.959300\',\'112.643040\']', '{\'alamat\': \'Jl. R. Tumenggung Suryo No.45, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65123\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 411389\'}', '0', '2019-07-04 10:34:07', 'AD2019020001'),
('FSKSM20190704100049', 'FSKS100009', 'Argaraya Medika Clinic', '501675dba028ee794cf2df14b293d9b2ca2ff604d8483993b751a1243f2b5cb2.jpg', '[\'-7.950536\',\'112.629886\']', '{\'alamat\': \'Jl. Kalpataru No.39, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 4345510\'}', '0', '2019-07-04 10:34:36', 'AD2019020001'),
('FSKSM20190704100050', 'FSKS100009', 'Griya Bromo Clinic', 'a8d9a0664eb4c02f8cf3217bf390ccaea875352dcb43590b2937d3feb33231e2.jpg', '[\'-7.978161\',\'112.625503\']', '{\'alamat\': \'Jl. Bromo No.7A, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 335957\'}', '0', '2019-07-04 10:35:05', 'AD2019020001'),
('FSKSM20190705100001', 'FSKS100009', 'Klinik Elisa', '9ed570b945f67afa558d8c917b3f8ccc24c2f5759115fa4653c7f1e114fd4657.jpg', '[\'-7.979880\',\'112.658349\']', '{\'alamat\': \'Jl. Danau Toba E5 No.22, Lesanpuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 718377\'}', '0', '2019-07-05 03:35:49', 'AD2019020001'),
('FSKSM20190705100002', 'FSKS100009', 'Klinik Sanan Medika', '8b6e95b1d53981af4911eb4f226d71ac224510bb127ac041d659a35ec56b5b0f.jpg', '[\'-7.959871\',\'112.642958\']', '{\'alamat\': \'Jl. R. Tumenggung Suryo No.98, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 486874\'}', '0', '2019-07-05 03:41:43', 'AD2019020001'),
('FSKSM20190705100003', 'FSKS100009', 'Klinik Medis Ontoseno', '85da06a1e805cb601fe6ad547e7326dfc12689fa2a3a60bf830b001459e7e84a.jpg', '[\'-7.983800\',\'112.644421\']', '{\'alamat\': \'Jl. Ontoseno I No.2, Polehan, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 335154\'}', '0', '2019-07-05 03:42:17', 'AD2019020001'),
('FSKSM20190705100004', 'FSKS100009', 'Klinik Rahma Husada Celaket', 'd06a9940a3c26d8b851210b620c6feed148968b4c95a4a649f37855366796a73.jpg', '[\'-7.964569\',\'112.631255\']', '{\'alamat\': \'Jl. J.Agung Suprapto IIB No. 69, Samaan, Kec. Klojen, Kota Malang, Jawa Timur 65112\',\'url\': \'business.site\',\'tlp\': \'+62 341 352003\'}', '0', '2019-07-05 03:42:48', 'AD2019020001'),
('FSKSM20190705100005', 'FSKS100009', 'Prodia Medical  Clinic', '6cb326fad40741c5a530fde45bbc858418807115b6b7ca38c184bf661520976a.jpg', '[\'-7.965246\',\'112.620000\']', '{\'alamat\': \'Jl. Jakarta No.60, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'prodia.co.id\',\'tlp\': \'+62 341 566444\'}', '0', '2019-07-05 03:43:27', 'AD2019020001'),
('FSKSM20190705100006', 'FSKS100009', 'SMEC Ophthalmologist', '855027a58d0966506eabdd0234c8702df819a6c9d46328ac6e4732e2ef744e5a.jpg', '[\'-7.951953\',\'112.646188\']', '{\'alamat\': \'Jl. Sunandar Priyo Sudarmo No.55, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'rsmatasmec.com\',\'tlp\': \'+62 341 4373333\'}', '0', '2019-07-05 03:43:57', 'AD2019020001'),
('FSKSM20190705100007', 'FSKS100009', 'Klinik Bunga Melati', '114f0c37d1db39c026fb60642d756f0b5416f16e0732f678eedb8a91331da6f6.jpg', '[\'-7.973182\',\'112.625295\']', '{\'alamat\': \'Jl. Welirang No.27, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65112\',\'url\': \'klinikbungamelati.com\',\'tlp\': \'+62 341 360049\'}', '0', '2019-07-05 03:44:37', 'AD2019020001'),
('FSKSM20190705100008', 'FSKS100009', 'Klinik Blimbing - Lockermed', 'eb4eddb880dc4f5307a24ba0ca8545371d64a519a2bc0176c79f632950729510.jpg', '[\'-7.943689\',\'112.648393\']', '{\'alamat\': \'Jl. Laksda Adi Sucipto No.179, Blimbing, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'klinikblimbing.wordpress.com\',\'tlp\': \'+62 341 491895\'}', '0', '2019-07-05 03:45:15', 'AD2019020001'),
('FSKSM20190705100009', 'FSKS100009', 'Klinik Universitas Brawijaya', '6e986ea2ebf294e8b8c9641accf17cf75c1b591bdaf8f2be97875d91b18ef90c.jpg', '[\'-7.950710\',\'112.615755\']', '{\'alamat\': \'Jl. MT. Haryono, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65113\',\'url\': \'poliklinik.ub.ac.id\',\'tlp\': \'+62 341 571197\'}', '0', '2019-07-05 03:48:44', 'AD2019020001'),
('FSKSM20190705100010', 'FSKS100009', 'Panti Gowindo', '25ac694abfb4b4a6cc559adf527a86461756b17eb054bcc881e81423dc21738e.jpg', '[\'-7.993725\',\'112.616165\']', '{\'alamat\': \'Jl. Manyar No.42, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65147\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 363127\'}', '0', '2019-07-05 03:58:26', 'AD2019020001'),
('FSKSM20190705100011', 'FSKS100009', 'Arthetics Clinic', '0ba4d3083bd387cf1df413af90a0ad6a183ac16a425fcd81c20d79f21acdcf55.jpg', '[\'-7.939030\',\'112.630847\']', '{\'alamat\': \'Jln. Soekarno hatta, Ruko Grand Soekarno Hatta kav.22, Mojolangu, kec, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'www.google.com\',\'tlp\': \'+62 818-0500-6368\'}', '0', '2019-07-05 05:08:46', 'AD2019020001'),
('FSKSM20190705100012', 'FSKS100009', 'Esther House of Beauty', '731b296cf6039052d624c10b100ce1a6b380cf8210e6a8529bb95201c08dd5cf.jpg', '[\'-7.985180\',\'112.625248\']', '{\'alamat\': \'Jl. Yulius Usman No.64, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117\',\'url\': \'ehob.co.id\',\'tlp\': \'+62 341 322576\'}', '0', '2019-07-05 05:09:19', 'AD2019020001'),
('FSKSM20190705100013', 'FSKS100009', 'Miracle Aesthetic Clinic', 'e1d3740497758dc59974b2cf8f2684ada2a250303957e95da3f819e3feced48b.jpg', '[\'-7.972051\',\'112.619573\']', '{\'alamat\': \'Jl. Wilis No.6, Gading Kasri, Kec. Klojen, Kota Malang, Jawa Timur 65115\',\'url\': \'miracle-clinic.com\',\'tlp\': \'+62 341 362608\'}', '0', '2019-07-05 05:09:53', 'AD2019020001'),
('FSKSM20190705100014', 'FSKS100009', 'Belle Crown Lembah Dieng', 'a1a1929246a43d012b3afb7b9899c4c32345b19e10cabce5af77394c1fdb5d4a.jpg', '[\'-7.970277\',\'112.603377\']', '{\'alamat\': \'Jl. Lembah Dieng no. 6, Karangbesuki, Kec. Sukun, Kota Malang, Jawa Timur 65146\',\'url\': \'bellecrown.com\',\'tlp\': \'+62 341 566313\'}', '0', '2019-07-05 05:10:29', 'AD2019020001'),
('FSKSM20190705100015', 'FSKS100009', 'London Beauty Centre', 'c4e31c0222c447d8cf79c3931d95aa912cea021c380e7021a9254d999c1827a7.jpg', '+62 341 321346', '{\'alamat\': \'Jl. Kawi No.39, Bareng, Kec. Klojen, Kota Malang, Jawa Timur 65116\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 321346\'}', '0', '2019-07-05 05:11:11', 'AD2019020001'),
('FSKSM20190705100016', 'FSKS100009', 'Natasha Skin Care', 'eec8477bef86a255d3eee36e760306eb4636f07e7af950773843c67ab775327e.jpg', '[\'-7.937323\',\'112.625794\']', '{\'alamat\': \'Jl. soekarno Hatta PTP II No.1, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'natasha-skin.com\',\'tlp\': \'+62 341 488290\'}', '0', '2019-07-05 05:26:27', 'AD2019020001'),
('FSKSM20190705100017', 'FSKS100009', 'Natasha Skin Care ( Gading Kasri)', '48977a5755b55aa75d01649724bb65d05586a92292c8050383ccb4e0725bb009.jpg', '[\'-7.967953\',\'112.616408\']', '{\'alamat\': \'Jl. Bondowoso No.19, Gading Kasri, Malang, Kota Malang, Jawa Timur 65115\',\'url\': \'natasha-skin.com\',\'tlp\': \'+62 341 551460\'}', '0', '2019-07-05 05:27:27', 'AD2019020001'),
('FSKSM20190705100018', 'FSKS100009', 'Flonura Skin Centre', 'c8401ba107cacee5f99616b6289341357a29aed7bf29e6d939e9a199c0fa5202.jpg', '[\'-7.960509\',\'112.633898\']', '{\'alamat\': \'Jl. Sarangan No.7, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 491107\'}', '0', '2019-07-05 05:28:10', 'AD2019020001'),
('FSKSM20190705100019', 'FSKS100009', 'Spesialis Kulit Erha', 'afaabadf97540782f8bde14fb904bbb43f938c0bec800951b1bd9d07faf6b48d.jpg', '[\'-7.972769\',\'112.627825\']', '{\'alamat\': \'Jl. Bromo No.2, Oro-oro Dowo, Klojen, Malang City, East Java 65119\',\'url\': \'business.site\',\'tlp\': \'+62 341 3012543\'}', '0', '2019-07-05 05:28:45', 'AD2019020001'),
('FSKSM20190705100020', 'FSKS100009', 'Navagreen Natural Skin Care', 'fdd938e0adfcaade44feba82bcbfab8c056ef9a0d26e1bf92cbb4c5f7e314d2c.jpg', '[\'-7.964043\',\'112.627372\']', '{\'alamat\': \'Jl. Brigjend Slamet Riadi No.165a, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'naavagreen.com\',\'tlp\': \'+62 341 364978\'}', '0', '2019-07-05 05:29:28', 'AD2019020001'),
('FSKSM20190705100021', 'FSKS100009', 'Larissa Aesthetic Center Malang', '567bb992d123c4f788d0169fcb5c1f0c8116b05d40c85f8d71ae423bfcf08cdc.jpg', '[\'-7.978385\',\'112.626831\']', '{\'alamat\': \'Jl. Arjuno No.17, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'larissa.co.id\',\'tlp\': \'+62 341 363330\'}', '0', '2019-07-05 05:30:00', 'AD2019020001'),
('FSKSM20190705100022', 'FSKS100009', 'Profira Aesthetic dan Antiaging', 'a28ae014001a4378bb0dbbe657255fa788c495e08783d17633d5fedcea304804.jpg', '[\'-7.970777\',\'112.620382\']', '{\'alamat\': \'Jl. Retawu No.8, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65112\',\'url\': \'profira-clinic.com\',\'tlp\': \'+62 341 586456\'}', '0', '2019-07-05 05:30:50', 'AD2019020001'),
('FSKSM20190705100023', 'FSKS100009', 'Aurell Skin care clinic', '7dbf99b314280f9e99e5f5bc445519edd8d87fbd04892f260bc38cd787eaa771.jpg', '[\'-7.977370\',\'112.614673\']', '{\'alamat\': \'Jl. Kepundung No.50, Bareng, Kec. Klojen, Kota Malang, Jawa Timur 65116\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 2366088\'}', '0', '2019-07-05 05:31:23', 'AD2019020001'),
('FSKSM20190705100024', 'FSKS100009', 'Beauty Medika Klinik', 'd6ddd5063425fcabbdcdd5ac87c7c90a6d8d503c5be2779b357349ca83ef891e.jpg', '[\'-7.938707\',\'112.630445\']', '{\'alamat\': \'Grand Ruko, Jl. Soekarno Hatta No.9, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'beautymedika.com\',\'tlp\': \'0341 481666\'}', '0', '2019-07-05 05:32:05', 'AD2019020001'),
('FSKSM20190705100025', 'FSKS100009', 'Glow Clinic', '78cf05d50d86110dcc5e33da15b561f46dee47964104e911028995a7b0986e82.jpg', '[\'-7.980514\',\'112.656809\']', '{\'alamat\': \'Jl. Borobudur No.1A, Blimbing, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142, Indonesia\',\'url\': \'msglowid.com\',\'tlp\': \'+62 341 499990\'}', '0', '2019-07-05 05:32:50', 'AD2019020001'),
('FSKSM20190705100026', 'FSKS100009', 'Beauty Rossa Skin Care', '0d32a64a293955c76685ba4c9f630cc6cfdb7854b123a64a7cd3bef80ed11d05.jpg', '[\'-7.940664\',\'112.627638\']', '{\'alamat\': \'Jl. Trs.Candi Mendut No.M2, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'beautyrossamalang.net\',\'tlp\': \'+62 341 4359016\'}', '0', '2019-07-05 05:33:23', 'AD2019020001'),
('FSKSM20190705100027', 'FSKS100009', 'Aurell Skin Care', 'b7a91e21a0e7eefacd8f2d9e33f7078d3585177e7d677927bcd8c77812145cff.jpg', '[\'-7.941024\',\'112.608895\']', '{\'alamat\': \'Jl. MT. Haryono No.131, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'business.site\',\'tlp\': \'+62 341 5071433\'}', '0', '2019-07-05 05:33:53', 'AD2019020001'),
('FSKSM20190705100028', 'FSKS100009', 'eLBe Clinic', 'aa77aa44aaa05ecb61cc187b92efb0730a8fe4036af87b90d89d6fc0beaa9dea.jpg', '[\'-7.953467\',\'112.641107\']', '{\'alamat\': \'Jl. Ciujung No.28, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 4373735\'}', '0', '2019-07-05 05:34:26', 'AD2019020001'),
('FSKSM20190705100029', 'FSKS100009', 'NSM  Mahakam  Medical Clinic', 'f56be1cbbb11164a73a2e37bba57c240c492d0635210599a4f81e8d0fac67455.jpg', '[\'-7.965124\',\'112.636800\']', '{\'alamat\': \'Jl. Mahakam No. 4, Rampal Claket, Kec. Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'www.google.com\',\'tlp\': \'+62 852-4436-6441\'}', '0', '2019-07-05 05:35:04', 'AD2019020001'),
('FSKSM20190705100030', 'FSKS100009', 'Klinik Trio Husada', '3c6114840129dd9713d01814f4217c2448e5fa5214675d6effbf123e871e0433.jpg', '[\'-7.922773\',\'112.634166\']', '{\'alamat\': \'Jl. Ikan Tombro Barat No.47, Tunjungsekar, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'triohusadamalang.business.site\',\'tlp\': \'+62 341 417528\'}', '0', '2019-07-05 05:35:45', 'AD2019020001'),
('FSKSM20190705100031', 'FSKS100009', 'Klinik Kendedes', 'a2663655f50f0b6702834cdf4d3494aaf09b424920583d56809dc6f8a61e49cf.jpg', '[\'-7.930097\',\'112.648591\']', '{\'alamat\': \'Jl. A. Yani No.16, Polowijen, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'+62 813-3326-7782\'}', '0', '2019-07-05 05:36:21', 'AD2019020001'),
('FSKSM20190705100032', 'FSKS100009', 'Klinik UIN Maulana Malik Ibrahim Malang', '0fa9bcc434dae29c3ddf28b51423fe8a1f91686a07d30a0d3d10a2ef122e5821.jpg', '[\'-7.951768\',\'112.607497\']', '{\'alamat\': \'Jl. Gajayana No.50, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'etheses.uin-malang.ac.id\',\'tlp\': \'0341\'}', '0', '2019-07-05 05:36:50', 'AD2019020001'),
('FSKSM20190705100033', 'FSKS100009', 'Klinik Sumba Husada', 'd3d60d33ea1cd88cfd3d80dbba88bef1dc672123567f072eb7e4dad9bf5a8b8a.jpg', '[\'-7.988054\',\'112.624787\']', '{\'alamat\': \'Jl. Sumba No.5, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 353919\'}', '0', '2019-07-05 05:37:23', 'AD2019020001'),
('FSKSM20190705100034', 'FSKS100009', 'Ar-Razy Health Care', 'b329fcac9f14b7d730dd10b3eb24082a4a195e99aed01a53456c3ff87b6aeaa0.jpg', '[\'-7.987977\',\'112.615924\']', '{\'alamat\': \'Jl. Mergan Lori No.3, Tanjungrejo, Kec. Sukun, Kota Malang, Jawa Timur 65147\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-07-05 05:37:57', 'AD2019020001'),
('FSKSM20190705100035', 'FSKS100009', 'Klinik Rolas Medika', '95c06d76fa733b7c84db64c7ded0b9cdd50065b3cd10a1c80fa253b79c113d96.jpg', '[\'-7.967207\',\'112.636197\']', '{\'alamat\': \'Jl. Dokter Wahidin No.54, Kel, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'rolasmedika.com\',\'tlp\': \'+62 341 3022856\'}', '0', '2019-07-05 05:38:26', 'AD2019020001'),
('FSKSM20190705100036', 'FSKS100009', 'Klinik Pratama Tandya', 'fb4ede2c7c0cf75f323254a00921a3e48952af4fe17afaf3aded27922cdbb894.jpg', '[\'-7.973501\',\'112.624002\']', '{\'alamat\': \'Jalan Semeru No. 43, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'klinikpratamatandya.blogspot.co.id\',\'tlp\': \'+62 341 369142\'}', '0', '2019-07-05 05:46:48', 'AD2019020001'),
('FSKSM20190705100037', 'FSKS100009', 'Klinik Husada Asih', '1788e29c7fce1f9c01a35da57640ef10f5db5883ad615a263b1d2d46514970db.jpg', '[\'-7.962713\',\'112.640844\']', '{\'alamat\': \'Jl. R. Tumenggung Suryo No.39, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'ypacmalang.org\',\'tlp\': \'+62 813-9345-0827\'}', '0', '2019-07-05 05:47:28', 'AD2019020001'),
('FSKSM20190705100038', 'FSKS100009', 'MS. Glow  Aesthic Clinic', 'd9842774fdde6e06d693ad6a72b0248e10c7405f4ad96c08a8732bbc2f275dc8.jpg', '[\'-7.939004\',\'112.634253\']', '{\'alamat\': \'Jl. Borobudur No.1A, Blimbing, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'msglowid.com\',\'tlp\': \'+62 341 499990\'}', '0', '2019-07-05 05:48:01', 'AD2019020001'),
('FSKSM20190705100039', 'FSKS100009', 'Niaga Medical clinic', '19140f003e544fa0940102d82336a90ec08d3e010ad0a13b8ef5efb7acd835c6.jpg', '[\'-7.995783\',\'112.625565\']', '{\'alamat\': \'Jl. Siberut No.2, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117\',\'url\': \'www.goole.com\',\'tlp\': \'+62 881-3315-805\'}', '0', '2019-07-05 05:48:35', 'AD2019020001'),
('FSKSM20190705100040', 'FSKS100009', 'Alya Medical Center', '787eaee0a009ba52b608431bd207e63e777dc87bc4d794fd7b8dbe5e575fc208.jpg', '[\'-7.952440\',\'112.646042\']', '{\'alamat\': \'Jl. Sunandar Priyo Sudarmo No.17 A, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-07-05 05:49:08', 'AD2019020001'),
('FSKSM20190705100041', 'FSKS100009', 'Dnl Skin care clinic', 'f84dc4d3f60cda8954de505ac92dbc058a9106f167ba1698b438ed35009c92fc.jpg', '[\'-7.944256\',\'112.620059\']', '{\'alamat\': \'Jl. Soekarno Hatta No.4, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'skincentrebali.com\',\'tlp\': \'+62 341 4374036\'}', '0', '2019-07-05 05:50:01', 'AD2019020001'),
('FSKSM20190708100001', 'FSKS100006', 'RSUD. Dr. Saiful Anwar', '6b5b1e64ca16617826eafef21f29d41f4d171803a894e5f040ca3afa26a4d72d.jpg', '[\'-7.971628\',\'112.632105\']', '{\'alamat\': \'Jl. Jaksa Agung Suprapto, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65112, Indonesia\',\'url\': \'https://rsusaifulanwar.jatimprov.go.id/\',\'tlp\': \'+62 341 366242\'}', '0', '2019-07-08 04:17:28', 'AD2019020001'),
('FSKSM20190708100002', 'FSKS100006', 'RS. Tk.II dr. Soepraoen', '4dc04379293ff78e562aeb33f7ea18a5d223eff6c98458a421a089a80c83a996.jpg', '[\'-7.989862\',\'112.620477\']', '{\'alamat\': \'Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65112, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 325112\'}', '0', '2019-07-08 10:34:40', '');
INSERT INTO `faskes_main` (`id_faskes`, `id_jenis`, `nama_faskes`, `foto_faskes`, `lokasi`, `detail_faskes`, `is_delete`, `waktu`, `id_admin`) VALUES
('FSKSM20190708100003', 'FSKS100006', 'RS. Tk.II dr. Soepraoen', '84ef2b06d384566005978b98029ff2c724c6486a96585bd7b364b588dcb51a37.jpg', '[\'-7.989862\',\'112.620477\']', '{\'alamat\': \'Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65112, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 325112\'}', '1', '2019-07-08 10:36:42', ''),
('FSKSM20190708100004', 'FSKS100006', 'RS. Panti Nirmala', 'fa484900e14a09b4f8221f4503bc4f6da5cf65380ba4951d924722fa5a62c8d4.jpg', '[\'-7.994696\',\'112.634043\']', '{\'alamat\': \'Jl. Kebalen Wetan No.2-8, Kotalama, Kec. Kedungkandang, Kota Malang, Jawa Timur 65134, Indonesia\',\'url\': \'rspantinirmala.com\',\'tlp\': \'+62 341 362459\'}', '0', '2019-07-08 10:37:30', ''),
('FSKSM20190708100005', 'FSKS100006', 'RS. Panti Waluya Sawahan', 'b1590d92a2ea5f05b7442c065102e41bfba1c60bae9d9f176157289298bb3c46.jpg', '[\'-7.986550\',\'112.624945\']', '{\'alamat\': \'Jl. Nusakambangan No.56, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117, Indonesia\',\'url\': \'pantiwaluya.org\',\'tlp\': \'+62 341 366033\'}', '0', '2019-07-08 10:38:06', ''),
('FSKSM20190708100006', 'FSKS100006', 'RS. Lavalette', 'e623a379bd5614f2d2b296f0b1f391eb4684e0adb909c7f4a5acec1d688eb28d.jpg', '[\'-7.965756\',\'112.637926\']', '{\'alamat\': \'Jalan Wage Rudolf Supratman No.10, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65121, Indonesia\',\'url\': \'lavalettehospital.com\',\'tlp\': \'+62 341 470805\'}', '0', '2019-07-08 10:38:37', ''),
('FSKSM20190708100007', 'FSKS100006', 'RSI. Aisyiyah', 'b309f134a0e947c50a6722908e5481e57e9a51853f531e472a8fd7224d150671.jpg', '[\'-7.988678\',\'112.625463\']', '{\'alamat\': \'Jl. Sulawesi No.16, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117, Indonesia\',\'url\': \'rsiaisyiyah-malang.or.id\',\'tlp\': \'+62 341 326773\'}', '0', '2019-07-08 10:39:11', ''),
('FSKSM20190708100008', 'FSKS100006', 'RSI. UNISMA MALANG', 'd6d67af1b83b9286a3eb87d07e3ab965c9ba288441cef908efa7b70d34a40743.jpg', '[\'-7.940186\',\'112.608977\']', '{\'alamat\': \'Jalan, Jl. Mayjen Haryono Gg. 10 No.139, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144, Indonesia\',\'url\': \'rsiunisma.com\',\'tlp\': \'+62 341 551356\'}', '0', '2019-07-08 10:39:41', ''),
('FSKSM20190708100009', 'FSKS100006', 'RS. Permata Bunda', 'e8fde0d6d12703bb788ebca32454e08acab7d4f5995f51d8c806a0b00f2336aa.jpg', '[\'-7.938879\',\'112.624835\']', '{\'alamat\': \'Jl. Soekarno Hatta No.75, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 487487\'}', '0', '2019-07-08 10:40:16', ''),
('FSKSM20190708100010', 'FSKS100006', 'RS. Hermina Tangkubanprahu', '1c9a87874c0c3ada21ff269f07fe47a1e9637c920cd15df9b9aef9f11bf23c61.jpg', '[\'-7.978079\',\'112.624517\']', '{\'alamat\': \'Jl. Tangkuban Perahu No.31-33, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia\',\'url\': \'herminahospitalgroup.com\',\'tlp\': \'+62 341 325082\'}', '0', '2019-07-08 10:41:15', ''),
('FSKSM20190708100011', 'FSKS100006', 'RS Persada Hospital', '43726e365df7feb059e6d12b1506a24310ab7066688ee924aef4ac59b104cf5f.jpg', '[\'-7.934970\',\'112.650195\']', '{\'alamat\': \'Jl. Panji Suroso Araya Bussiness Centre Kav. II-IV\',\'url\': \'www.persadahospital.co.id/\',\'tlp\': \'+62 341 2996333\'}', '0', '2019-07-09 04:36:45', 'AD2019020001'),
('FSKSM20190708100012', 'FSKS100006', 'RSUD Kota Malang', '0adc80f37248f7ae8d053a5e532fc692479bef725c837e21eb871a47077244d4.jpg', '[\'-8.026355\',\'112.639395\']', '{\'alamat\': \'Jl. Rajasa No.27 Kel. Bumiayu Kec. Kedungkandang\',\'url\': \'https://rsud.malangkota.go.id/\',\'tlp\': \'+62 341 754338\'}', '0', '2019-07-08 10:47:23', ''),
('FSKSM20190708100013', 'FSKS100006', 'RS Universitas Brawijaya', '98bb925e3982d18d7e47575d6a97fd6cca74ea59005fbbdf257bd24508e78b59.jpg', '[\'-7.941220\',\'112.621538\']', '{\'alamat\': \'Jl. Soekarno - Hatta, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141, Indonesia\',\'url\': \'rumahsakit.ub.ac.id\',\'tlp\': \'+62 341 403000\'}', '0', '2019-07-08 10:50:37', ''),
('FSKSM20190708100014', 'FSKS100006', 'RS. Bhakti Bunda', 'c64373989719089c24e254e41f576a2b3178ef10a0af51b154929ff2f22c0ab8.jpg', '[\'-7.953036\',\'112.619467\']', '{\'alamat\': \'Jl. Mayjend. Panjaitan No.176, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 552955\'}', '0', '2019-07-08 10:51:10', ''),
('FSKSM20190708100015', 'FSKS100006', 'RSIA Husada Bunda', '27171cfda2f2d94a75962997ca462fbd2550457f9e7f70307223a132dbc116ea.jpg', '[\'-7.968300\',\'112.622882\']', '{\'alamat\': \'Malang No. 2, Jl. Pahlawan Trip, Oro-oro Dowo, Klojen, Malang City, East Java 65112, Indonesia\',\'url\': \'rsiahusadabunda.com\',\'tlp\': \'+62 341 566972\'}', '0', '2019-07-08 10:51:42', ''),
('FSKSM20190708100016', 'FSKS100006', 'RSIA Muhammadiyah', '99147b88b15dbeaa501b02051ae0279492893f3317eb0e1a6b8fd6da0e4a52a4.jpg', '[\'-7.984554\',\'112.628788\']', '{\'alamat\': \'Jl. Kyai H. Wahid Hasyim No.30, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia\',\'url\': \'rsmuhammadiyahjatim.com\',\'tlp\': \'+62 341 351549\'}', '0', '2019-07-08 10:52:23', ''),
('FSKSM20190708100017', 'FSKS100006', 'RSIA Mardi Waloeja Kauman', '30e80d8e5c3cef366bad248497656e5047fad41f5d0f9135806d53821318b2bb.jpg', '[\'-7.982989\',\'112.627844\']', '{\'alamat\': \'Jl. Kauman No.23, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia\',\'url\': \'rsiamardiwaloejarampal.com\',\'tlp\': \'+62 341 358508\'}', '0', '2019-07-08 10:53:36', ''),
('FSKSM20190708100018', 'FSKS100006', 'RSIA Puri', '0858218d1cc64e8b2e85327e1ba6d10172f21aaff78796fa97f98cfdfe1117b3.jpg', '[\'-7.974202\',\'112.621852\']', '{\'alamat\': \'Taman Slamet Street No.20, Gading Kasri, Klojen, Malang City, East Java 65115, Indonesia\',\'url\': \'rsiapuri.com\',\'tlp\': \'+62 341 325329\'}', '0', '2019-07-08 10:54:13', ''),
('FSKSM20190708100019', 'FSKS100006', 'RSIA Permata Hati', 'db5f1855740c488c2d633010822def059952e2037d22ea431f50333f5f20ddb6.jpg', '[\'-7.980062\',\'112.660718\']', '{\'alamat\': \'Jl. Danau Toba Blok E 6 No.16 - 18, Lesanpuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65138, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 718068\'}', '0', '2019-07-08 10:55:08', ''),
('FSKSM20190708100020', 'FSKS100006', 'RSIA Puri Bunda', 'd3a1f711dc3f34777c74d4060ba1b0f9c1c66901306ea8856a05d9cda4d3d34e.jpg', '[\'-7.95878\',\'112.655553\']', '{\'alamat\': \'Jl. Simpang Sulfat Utara No.60A, Pandanwangi, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia\',\'url\': \'puribundamalang.com\',\'tlp\': \'+62 341 480047\'}', '0', '2019-07-08 10:55:40', ''),
('FSKSM20190708100021', 'FSKS100006', 'RSIA Mardi Waloeja Rampal', 'e9fec9f351afe936c531a26eef6e7cddc8d4d6821d0806113c7ed6caf8932964.jpg', '[\'-7.964888\',\'112.635214\']', '{\'alamat\': \'Jl. W.R. Supratman No.1, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65111, Indonesia\',\'url\': \'rsiamardiwaloejarampal.com\',\'tlp\': \'+62 341 364756\'}', '0', '2019-07-08 10:56:16', ''),
('FSKSM20190708100022', 'FSKS100006', 'RSIA Mutiara Bunda', 'a54ad704cda9d44039ae2d88d13c3ad9aabe38c74bffad8826ea5421c3df2abb.jpg', '[\'-7.953355\',\'112.641049\']', '{\'alamat\': \'Jl. Ciujung No.19, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia\',\'url\': \'rsiamutiarabundamalang.blogspot.com\',\'tlp\': \'+62 341 400403\'}', '0', '2019-07-08 10:56:43', ''),
('FSKSM20190708100023', 'FSKS100006', 'RSIA Melati Husada', '75ae2683a616ed49ae061fa2d216042ca974dc0ce3f0dbefeecb66e487ea94c8.jpg', '[\'-7.975840\',\'112.621132\']', '{\'alamat\': \'Jalan Kawi No 32 - 34, Gading Kasri, Klojen, Gading Kasri, Kec. Klojen, Kota Malang, Jawa Timur 65115, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 341357\'}', '0', '2019-07-08 10:57:16', ''),
('FSKSM20190708100024', 'FSKS100006', 'RSIA Galeri Candra', '19b67b13d9a2cd40d3c47b396d5e4903f2827dcf905ff1739bde0a221fabdb19.jpg', '[\'-7.947945\',\'112.619575\']', '{\'alamat\': \'Jl. Andong No.3, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 478571\'}', '0', '2019-07-08 10:57:49', ''),
('FSKSM20190708100025', 'FSKS100006', 'RSIA Rumkitban Rampal', '9e7e91543405d72319372276d605782836e50ce7c9433e486c552c1ee8f4eec4.jpg', '[\'-7.974089\',\'112.638704\']', '{\'alamat\': \'Jl. Panglima Sudirman No.E 20, Kesatrian, Kec. Blimbing, Kota Malang, Jawa Timur 65111, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 354396\'}', '0', '2019-07-09 04:48:32', 'AD2019020001'),
('FSKSM20190709100001', 'FSKS100006', 'RSIA Refa Husada', '0ef2eff79710be4faa829e60723875a1e51f6130713cebeea903c70c33383afa.jpg', '[\'-8.035716\',\'112.642680\']', '{\'alamat\': \'Jl. Mayjend Sungkono No. 9 RT. 005 RW. 004, Tlogowaru, Kedungkandang, Tlogowaru, Kec. Kedungkandang, Kota Malang, Jawa Timur 65132, Indonesia\',\'url\': \'www.google.com\',\'tlp\': \'+62 341 754075\'}', '0', '2019-07-09 04:33:58', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_kategori`
--

CREATE TABLE `home_page_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_page_kategori`
--

INSERT INTO `home_page_kategori` (`id_kategori`, `nama_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
(1, 'Daftar Antrian', '0', '2019-04-02 09:07:12', 'AD2019020001'),
(2, 'Pelayanan Kesehatan', '0', '2019-04-02 09:08:29', 'AD2019020001'),
(3, 'Pelayanan Pendidikan', '0', '2019-04-02 09:08:44', 'AD2019020001'),
(4, 'Pariwisata dan Lingkungan Terbuka Hijau', '0', '2019-04-02 09:09:22', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `home_page_main`
--

CREATE TABLE `home_page_main` (
  `id_page` varchar(9) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_page` text NOT NULL,
  `foto_page` varchar(70) NOT NULL,
  `next_page` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_page_main`
--

INSERT INTO `home_page_main` (`id_page`, `id_kategori`, `nama_page`, `foto_page`, `next_page`, `is_delete`, `waktu`, `id_admin`) VALUES
('MENU10001', 1, 'Kesehatan', 'ed88bc4c866227e94d8eda9d75e84e1a710dbad4313b38a21e2f98da393f8c45.jpg', 'beranda/kesehatan/jenis_rumah_sakit', '0', '2019-04-23 05:50:07', 'AD2019020001'),
('MENU10002', 1, 'Kependudukan', 'cc2cd863e992fa37b539a9b9f0d694e1372913711bf38eaedcf6944d80853f6b.jpg', 'beranda/kependudukan/jenis', '0', '2019-05-03 04:34:18', 'AD2019020001'),
('MENU10003', 1, 'Perijinan', 'e429082bcf2cf1a17177355ac172db5ddeb8f12a2c11bcd91a71d4d988dde9b7.jpg', 'beranda/perijinan/jenis', '0', '2019-05-03 09:31:49', 'AD2019020001'),
('MENU10004', 3, 'Daftar Sekolah Menengah Pertama', '72899716adeffb9cf601d31228768070cd754dced9d930c7725a539ad04821df.jpg', 'beranda/pendidikan/a723ff721d91d9f34309debc39b7082f69c6b9990eb1c4aff93361aeec412bcb', '0', '2019-05-06 09:03:22', 'AD2019020001'),
('MENU10005', 3, 'Daftar Sekolah Dasar', 'b2c92547857a6d646e1353592b56ddfa12c8bd41e573b7d31fa0977030b8eac0.jpg', 'beranda/pendidikan/7fd7db09359f9723b6ba6e98be7a685ceae45115d77f5d7026c167a3f6e557b9', '0', '2019-05-06 09:03:39', 'AD2019020001'),
('MENU10006', 3, 'Daftar Sekolah Menegah Atas dan Kejuruan', 'bb0e73ca3117728c47a3f4857a6d193da4a70ce1e80da5cd0ebd2aa29931b0a9.jpg', 'beranda/pendidikan/7110c3557c18ae66396a476e0213dca86d950a532e6a1623b0b698da8b9e0fbf', '0', '2019-05-13 05:14:21', 'AD2019020001'),
('MENU10007', 3, 'Pondok Pesantren', '8c0de8bb3e203c5611262e9e33814f8632a864a1889c43c75df0e184355e8b30.jpg', 'beranda/pendidikan/fc0bbb11535d92ec0dcf43b83967c88b24c4bdc28dd7343c3478d8c99cb8a3bd', '0', '2019-06-19 07:18:03', 'AD2019040001'),
('MENU10008', 3, 'Perguruan Tinggi', '44e81762f202cc44c6cf991597904802907e8e01a11ebef94d460f062575e06d.jpg', 'beranda/pendidikan/53f85325db3e4f62a90336bf07ff30a26db554224d126e93f3e74990eec67810', '0', '2019-06-19 07:18:40', 'AD2019040001'),
('MENU10009', 3, 'Perpustakaan', '6ba4c8b0fcea36da1c18274363b1be1bcf76dbaf90b81bb1f84a47a6ef81311a.jpg', 'beranda/pendidikan/41f4bc8c99de770a5695285fd42e42e6e6f0664396eafddb2524f29febcd8b8d', '0', '2019-06-19 08:10:01', 'AD2019040001'),
('MENU10010', 2, 'Daftar Puskesmas', '03cab6c4801747cd8e34811cd74ebaf32e85422122f23ebf5cb41047cdeb224e.jpg', 'beranda/faskes/68c98b2321e10f1727c229a67315edc01456836c0342a002cadbc046ee730845', '0', '2019-07-01 10:44:13', 'AD2019020001'),
('MENU10011', 2, 'Daftar Klinik', '9f4c5c0c6117a92e225b76e9c5b315e3dd21429eaa187c0652596e7108fed168.jpg', 'beranda/faskes/28cad0dec0f28514f3b45f8e2ec4781c665d377eb2196388a82866575217c7f3', '0', '2019-07-04 05:52:01', 'AD2019020001'),
('MENU10012', 2, 'Daftar Rumah Sakit', '973f1680e3b39cea60ce18e29f3e93bee61fcbd6ae193ffa3e4586f0520dd17c.jpg', 'beranda/faskes/28b37a4c9364981692a47b7d7c8e9a3e76f130334a1872d6c8bbeccae43c7147', '0', '2019-07-01 10:42:25', 'AD2019020001'),
('MENU10013', 2, 'Apotek', 'fb3c2af21f1b5021f459269e892f5edcc716873cc46ef8ade17db2e30d66520a.jpg', 'beranda/faskes/6be374069041ea3fb6b9f2509321d5bdf4a123f120991fbc3fc3ff9f1b32208e', '0', '2019-07-01 10:40:28', 'AD2019020001'),
('MENU10014', 2, 'Dokter Praktek', '84d0e058a241a28ee081e938d912a4ca859cb6553a151fd67820b5b933042748.jpg', 'beranda/faskes/209c422369445884ec9c2e3e664f152241f008f0905beb3152af46901b014ad0', '0', '2019-07-01 10:40:59', 'AD2019020001'),
('MENU10015', 2, 'Dokter Gigi', '1f95613edfef290f100164ed291c24be3b96e697f120fc53b6a39d6b03ec8b3e.jpg', 'beranda/faskes/f5a9cee1802979ad9494ebd2555dc5021d1ee9d29de1615505ede4635d4aa379', '0', '2019-07-01 10:41:36', 'AD2019020001'),
('MENU10016', 2, 'Optik', '489b6471fed740d5057001bfea73e4038ea9b27278f440b19774ff9c6352ef9a.jpg', 'beranda/faskes/2899dcb1416a59041a5a337b84901a2a613e08cd71f5cb47939dd773e80a2c90', '0', '2019-07-01 09:53:04', 'AD2019020001'),
('MENU10017', 2, 'Laboratorium', '786b8c352d1a0fc2c9cbd301c13d707ef5f4cdb690cd90827e1355d63d65afd3.jpg', 'beranda/faskes/c24579fc05ffe0e7318adce9b86bb986d89a398f36772cb4f1916eb727ffec90', '0', '2019-07-01 10:43:39', 'AD2019020001'),
('MENU10018', 4, 'Kuliner', '81cfb6cac6696c35395fde9558295f0f833582904e467d840c100cd5f5cb8e65.jpg', 'local', '0', '2019-06-13 05:58:01', 'AD2019020001'),
('MENU10019', 4, 'Hotel', '2df2d8f3ece3257b93e8bda1207757a2075c40cc2cb82e50d518dd9a3aa6c2dd.jpg', 'local', '0', '2019-06-13 05:59:52', 'AD2019020001'),
('MENU10020', 4, 'Guest House', 'db3196e5beb756ddd77cd398a582b751b09858dcd340b64baca18f31b9137c70.jpg', 'local', '0', '2019-06-13 06:04:46', 'AD2019020001'),
('MENU10021', 4, 'Tempat Wisata', '84cd468020feab5c2bdf2f81cfd704951639e8ac886eabaf5f91168ea3d00622.jpg', 'local', '0', '2019-06-13 06:05:08', 'AD2019020001'),
('MENU10022', 4, 'Museum', '3c0feaf51c0a767fe8a137d57efa75e78af01d021de7dca9be7543debf3c5d34.jpg', 'local', '0', '2019-06-13 06:05:26', 'AD2019020001'),
('MENU10023', 4, 'Taman Kota', '42446cd97a65ac709551e623281b915691794bcbbd9b8650de0bddb293329dd8.jpg', 'local', '0', '2019-06-13 06:07:13', 'AD2019020001'),
('MENU10024', 4, 'UKM', '249eaf0116ddf3de0c31c09545c3b62c02ab34036864b638de8fe822a51626f5.jpg', 'local', '0', '2019-06-13 06:11:21', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_antrian`
--

CREATE TABLE `ijin_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(9) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `id_sub` varchar(10) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_antrian`
--

INSERT INTO `ijin_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis`, `id_kategori`, `id_sub`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('IJ20190802100000001', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110002', 'IJS0110002', '2019-08-02 04:18:41', '2019-08-03 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 04:18:41', '0'),
('IJ20190802100000002', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110002', '2019-08-02 05:47:52', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 05:47:52', '0'),
('IJ20190802100000003', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110002', '2019-08-02 05:48:08', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 05:48:08', '0'),
('IJ20190802100000004', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110001', 'IJS0110001', '2019-08-02 06:01:33', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 06:01:33', '0'),
('IJ20190802100000005', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110002', 'IJS0210002', '2019-08-02 06:02:26', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 06:02:26', '0'),
('IJ20190802100000006', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110002', 'IJS0110002', '2019-08-02 09:24:48', '2019-08-03 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 09:24:48', '0'),
('IJ20190802100000007', '3573011903940006', 'MENU10003', 'IJJ10001', 'IJK0110002', 'IJS0210002', '2019-08-02 09:54:22', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 09:54:22', '0');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_jenis`
--

CREATE TABLE `ijin_jenis` (
  `id_jenis` varchar(8) NOT NULL,
  `ket_jenis` text NOT NULL,
  `foto_jenis` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_jenis`
--

INSERT INTO `ijin_jenis` (`id_jenis`, `ket_jenis`, `foto_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('IJJ10001', 'usaha pariwisata', 'b189b2f570a3504e71d1b204a1f88f93d2f823f9c4e24373dec77c0bbfd36e6c.jpg', '0', '2019-04-30 07:47:10', 'AD2019020001'),
('IJJ10002', 'usaha makanan', '4371f391ef11c3f8ade3b53b6720a5ca7b2e8ff81985c2cb83fc51f1e8d0c79a.jpg', '0', '2019-04-30 07:48:42', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_kategori`
--

CREATE TABLE `ijin_kategori` (
  `id_kategori` varchar(10) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `ket_kategori` text NOT NULL,
  `foto_kategori` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_kategori`
--

INSERT INTO `ijin_kategori` (`id_kategori`, `id_jenis`, `ket_kategori`, `foto_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
('IJK0110001', 'IJJ10001', 'Ijin Keramaian Umum', 'cc8069250df5515236f666f0070a5de9cfb5b6ad7f5223a5b631cb52b22585ea.jpg', '0', '2019-06-14 06:31:36', 'AD2019020001'),
('IJK0110002', 'IJJ10001', 'Ijin Penggunaan tanah atau makam', '02fa195b7e83d3edd5ee8adfdb24327c29b70d4c1b77246c6abc5a89e0eb4d36.jpg', '0', '2019-06-14 06:31:53', 'AD2019020001'),
('IJK0110003', 'IJJ10001', 'Ijin Sewa', 'c10b1999e0e3d079789db43b05e027b5742eaaa0e2822584d1612e6e93b3adbd.jpg', '0', '2019-06-14 06:32:09', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `ijin_sub_kategori`
--

CREATE TABLE `ijin_sub_kategori` (
  `id_sub` varchar(10) NOT NULL,
  `id_kategori` varchar(10) NOT NULL,
  `ket_sub` text NOT NULL,
  `foto_sub` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ijin_sub_kategori`
--

INSERT INTO `ijin_sub_kategori` (`id_sub`, `id_kategori`, `ket_sub`, `foto_sub`, `is_delete`, `time_update`, `id_admin`) VALUES
('IJS0110001', 'IJK0110001', 'Memperpanjang', '39430fb6f0f37571c6674ed5947604f20e197c88f0a424a17a76ee8bbdd8b8d6.jpg', '0', '2019-05-02 10:33:21', 'AD2019020001'),
('IJS0110002', 'IJK0110001', 'ok', 'ec166c7bd6f59683052ac9ee83d9fac34d464dada02350accb679ef7a14975fd.jpg', '0', '2019-05-02 10:36:58', 'AD2019020001'),
('IJS0210001', 'IJK0110002', 'Membuat Baru', '49ca361f5fa014e9a6888f93bb629b7b75e47ee8e8817362c493af03ece1c95f.jpg', '0', '2019-05-02 10:34:55', 'AD2019020001'),
('IJS0210002', 'IJK0110002', 'Memperpanjang', 'd1368b1f694c97c2e2fbe560c8423a29969a1be45fb02482db28452cb646cb40.jpg', '0', '2019-05-02 10:35:15', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_antrian`
--

CREATE TABLE `kependudukan_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(1) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `id_kategori` varchar(8) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_antrian`
--

INSERT INTO `kependudukan_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis`, `id_kategori`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('PD20190503100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:02:03', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:02:03', '0'),
('PD20190503100000002', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:02:53', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:02:53', '0'),
('PD20190503100000003', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:04:09', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:04:09', '0'),
('PD20190503100000004', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:04:49', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:04:49', '0'),
('PD20190503100000005', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:05:02', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:05:02', '0'),
('PD20190503100000006', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:05:35', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:05:35', '0'),
('PD20190503100000007', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:06:24', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:06:24', '0'),
('PD20190503100000008', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:07:13', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:07:13', '0'),
('PD20190503100000009', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:09:14', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:09:14', '0'),
('PD20190503100000010', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:10:46', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:10:46', '0'),
('PD20190503100000011', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:11:35', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:11:35', '0'),
('PD20190503100000012', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:12:05', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:12:05', '0'),
('PD20190503100000013', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:12:54', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:12:54', '0'),
('PD20190503100000014', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:13:21', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:13:21', '0'),
('PD20190503100000015', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:22:26', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:22:26', '0'),
('PD20190503100000016', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:22:55', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:22:55', '0'),
('PD20190503100000017', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:23:18', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:23:18', '0'),
('PD20190503100000018', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:25:29', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:25:29', '0'),
('PD20190503100000019', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:25:54', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:25:54', '0'),
('PD20190503100000020', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-03 08:31:44', '2019-05-03 00:00:00', 'A-002', '0', '', '', '0', '2019-05-03 08:31:44', '0'),
('PD20190506100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-05-06 05:25:22', '2019-05-06 00:00:00', 'A-002', '0', '', '', '0', '2019-05-06 05:25:22', '0'),
('PD20190614100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-14 03:38:54', '2019-06-14 00:00:00', 'A-002', '0', '', '', '0', '2019-06-14 03:38:54', '0'),
('PD20190614100000002', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-14 05:29:55', '2019-06-14 00:00:00', 'A-002', '0', '', '', '0', '2019-06-14 05:29:55', '0'),
('PD20190617100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-17 03:24:00', '2019-06-17 00:00:00', 'A-002', '0', '', '', '0', '2019-06-17 03:24:00', '0'),
('PD20190617100000002', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-17 04:39:02', '2019-06-17 00:00:00', 'A-002', '0', '', '', '0', '2019-06-17 04:39:02', '0'),
('PD20190617100000003', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-17 10:57:50', '2019-06-17 00:00:00', 'A-002', '0', '', '', '0', '2019-06-17 10:57:50', '0'),
('PD20190617100000004', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-17 11:16:32', '2019-06-18 00:00:00', 'A-002', '0', '', '', '0', '2019-06-17 11:16:32', '0'),
('PD20190617100000005', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-17 11:37:37', '2019-06-18 00:00:00', 'A-002', '0', '', '', '0', '2019-06-17 11:37:37', '0'),
('PD20190620100000001', '3573011903940006', '0', 'KPJ10013', 'KPC10018', '2019-06-20 08:57:44', '2019-06-20 00:00:00', 'A-002', '0', '', '', '0', '2019-06-20 08:57:44', '0'),
('PD20190620100000002', '3573011903940006', '0', 'KPJ10013', 'KPC10018', '2019-06-20 08:58:03', '2019-06-20 00:00:00', 'A-002', '0', '', '', '0', '2019-06-20 08:58:03', '0'),
('PD20190620100000003', '3573011903940006', '0', 'KPJ10013', 'KPC10018', '2019-06-20 08:58:04', '2019-06-20 00:00:00', 'A-002', '0', '', '', '0', '2019-06-20 08:58:04', '0'),
('PD20190620100000004', '3573011903940006', '0', 'KPJ10007', 'KPC10017', '2019-06-20 08:59:39', '2019-06-20 00:00:00', 'A-002', '0', '', '', '0', '2019-06-20 08:59:39', '0'),
('PD20190625100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-25 05:31:13', '2019-06-25 00:00:00', 'A-002', '0', '', '', '0', '2019-06-25 05:31:13', '0'),
('PD20190625100000002', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-25 05:31:23', '2019-06-25 00:00:00', 'A-002', '0', '', '', '0', '2019-06-25 05:31:23', '0'),
('PD20190625100000003', '3573011903940006', '0', 'KPJ10007', 'KPC10017', '2019-06-25 05:56:58', '2019-06-25 00:00:00', 'A-002', '0', '', '', '0', '2019-06-25 05:56:58', '0'),
('PD20190625100000004', '3573011903940006', '0', 'KPJ10007', 'KPC10017', '2019-06-25 09:34:56', '2019-06-25 00:00:00', 'A-002', '0', '', '', '0', '2019-06-25 09:34:56', '0'),
('PD20190625100000005', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-06-25 10:15:47', '2019-06-25 00:00:00', 'A-002', '0', '', '', '0', '2019-06-25 10:15:47', '0'),
('PD20190731100000001', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-07-31 10:51:59', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 10:51:59', '0'),
('PD20190731100000002', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-07-31 10:52:16', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 10:52:16', '0'),
('PD20190731100000003', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-07-31 10:54:21', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 10:54:21', '0'),
('PD20190731100000004', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-07-31 10:57:31', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 10:57:31', '0'),
('PD20190731100000005', '3573011903940006', '0', 'KPJ10007', 'KPC10017', '2019-07-31 10:59:10', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 10:59:10', '0'),
('PD20190802100000001', '3573011903940006', '0', 'KPJ10007', 'KPC10017', '2019-08-02 05:55:09', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 05:55:09', '0'),
('PD20190802100000002', '3573011903940006', '0', 'KPJ10007', 'KPC10017', '2019-08-02 05:57:34', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 05:57:34', '0'),
('PD20190802100000003', '3573011903940006', '0', 'KPJ10001', 'KPC10001', '2019-08-02 09:52:59', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 09:52:59', '0');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_jenis`
--

CREATE TABLE `kependudukan_jenis` (
  `id_jenis` varchar(8) NOT NULL,
  `ket_jenis` text NOT NULL,
  `foto_jenis` varchar(70) NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_jenis`
--

INSERT INTO `kependudukan_jenis` (`id_jenis`, `ket_jenis`, `foto_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('KPJ10001', 'Kartu Tanda Penduduk (KTP-el)', 'f5a947d14485004b9af99c6ae699cd36aa01d9429cd97ca9235f49c16a2e9bb5.jpg', '0', '2019-06-17 09:51:00', 'AD2019040001'),
('KPJ10007', 'Kartu Identitas Anak', 'bd0d411b5ed5d23e326df94f71723c27d1a2785a248aa30f54725f2115986454.jpg', '0', '2019-06-17 09:18:10', 'AD2019040001'),
('KPJ10008', 'Kartu Keluarga (KK)', '71893e9bf66285716b96565d3f935f465446ca9fdacd474d84d7bf0203ca4dba.jpg', '0', '2019-06-17 09:55:31', 'AD2019020001'),
('KPJ10009', 'Surat Keterangan Pindah Warga Negara Indonesia (SKPWNI)', '76ff4799a2f390f6b6e22be1a9efcd769b564f3d87a9aa877bb4619fbdd241e2.jpg', '0', '2019-06-17 09:56:24', 'AD2019020001'),
('KPJ10010', 'Surat Keterangan Tempat Tinggal (SKTT)', 'c2a4e95b5ee97c8d423f819b875df876029ec3a892bd58ce5745307c84f897ff.jpg', '0', '2019-06-17 09:56:57', 'AD2019020001'),
('KPJ10011', 'Surat Keterangan Orang Terlantar (SKOT)', '519fe8acff8fca438bff5a7bcaacba38a05cf221cfc8a307131ad1349f864146.jpg', '0', '2019-06-17 09:57:26', 'AD2019020001'),
('KPJ10013', 'Akta Kelahiran', '53159e9331795d111cfac581d0e46443241203030260ba433518bcace2b3339a.jpg', '0', '2019-06-20 06:19:11', 'AD2019020001'),
('KPJ10014', 'Akta Kematian', 'efdb0b3306d62a32f0b4f5699a3be06ed8ef5338c4b5ecf8543044a3acdf756e.jpg', '0', '2019-06-20 06:20:46', 'AD2019020001'),
('KPJ10015', 'Akta Perkawinan', '22a7280ea1f29678a229ca7f64012556d901e100ef0aed54cf8c32a638d4df2a.jpg', '0', '2019-06-20 06:20:58', 'AD2019020001'),
('KPJ10016', 'Penerbitan Kutipan Kedua Akta Pencatatan Sipil', '76742a4ec474c1db0705f744a4a227af74ba136df24c28680377b1801fcc303a.jpg', '0', '2019-06-20 06:21:35', 'AD2019020001'),
('KPJ10017', 'Pencatatan Perubahan Nama', '28a05a91a649eefc0c09492f8cd17822e3fd63329b5596003dae72c5009e792f.jpg', '0', '2019-06-20 06:21:59', 'AD2019020001'),
('KPJ10018', 'Akta Pengesahan Anak', '107bbc6410f488628df9fcb8a1fc7fa09fa828656e7b06061362a80522ff74e2.jpg', '0', '2019-06-20 06:22:30', 'AD2019020001'),
('KPJ10019', 'Pencatatan Pengangkatan Anak (Adopsi)', '9dc463de3a93ceca1afa2e6467e2af50b4ce2e28570beea5085e2646c6776616.jpg', '0', '2019-06-20 06:23:00', 'AD2019020001'),
('KPJ10020', 'Pencatatan Pengakuan Anak', '6a44575b59a2051e2e86716d8776bfa6056d253b72aae6d3ba8130c12d76cf49.jpg', '0', '2019-06-20 06:23:17', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kependudukan_kategori`
--

CREATE TABLE `kependudukan_kategori` (
  `id_kategori` varchar(8) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `ket_kategori` text NOT NULL,
  `foto_kategori` varchar(70) NOT NULL,
  `txt_syarat_kategori` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kependudukan_kategori`
--

INSERT INTO `kependudukan_kategori` (`id_kategori`, `id_jenis`, `ket_kategori`, `foto_kategori`, `txt_syarat_kategori`, `is_delete`, `waktu`, `id_admin`) VALUES
('KPC10001', 'KPJ10001', 'KTP BARU', '3079daa67b41e5fe307aece27914a28a01e1af6106679dd660f95cc32a59e2c3.jpg', '<p>\n								                                      	<b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n<br>\n								                                      	<ol>\n																			<li>Fotokopi KK terbitan Dinas;\n																			<li>Bagi penduduk yang belum berusia 17(tujuh belas) tahun namun sudah mennikah atau sudah kawin atau pernah kawin melampirkan  Surat Nikah / Akta Perkawinan;\n																			<li>Pas Photo berwarna ukuran 4x6 dengan ketentuan background merah untuk tahun kelahiran ganjil dan background biru untuk genap;\n																			<li>Surat Pindah Dalam (dalam wilayah NKRI)\n																		</ol>\n																		</p>', '0', '2019-06-20 05:24:53', 'AD2019020001'),
('KPC10002', 'KPJ10001', 'KTP HILANG / RUSAK', '15fb79b3b82d6dc7783b356b91a6374b9e6a3dbed4e268e745c76b1c370916d2.jpg', '<p>\n								                                      	<b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n<br>\n								                                      	<ol>\n																			<li>Surat Kehilangan dari Kepolisian;\n																			<li>KTP yang hilang / fotokopi KTP yang hilang\n																			<li>Fotokopi KK terbitan Dinas\n																		</ol>\n																		</p>', '0', '2019-06-20 05:25:08', 'AD2019020001'),
('KPC10003', 'KPJ10001', 'PERUBAHAN KTP YANG SALAH / DIRUBAH', '37453faee4b7ccdbe84bb49b4faf19a2536f978ee4834583953b9360f529de6a.jpg', '<p>\n								     <b><h5>PERSYARATAN PENGURUSAN :</h5></b>                               	<ol>\n																			<li>Fotokopi KK terbitan Dinas;\n																			<li>Fotokopi Akta Kelahiran / Surat Kelahiran dari Rumah sakit/ Kelurahan;\n																			<li>Fotokopi Ijasah;\n																			<li>Fotokopi Surat Nikah atau Surat Cerai bagi Perusahaan status;\n																			<li>Kartu Tanda Penduduk (KTP) asli\n																		</ol>\n																		</p>', '0', '2019-06-20 05:26:03', 'AD2019020001'),
('KPC10004', 'KPJ10008', 'KK BARU', '416eb5cffdcc5b1d134be61c8928621ffbc28cc32d468b57f7beb390f7787bdf.jpg', '<p>\n	<b><h5>PERSYARATAN PENGURUSAN :</h5></b>							                                      	<ol>\n																			<li>Mengisi formulir \n																			<li>Fotokopi Surat Nikah / Kutipan Akta Perkawinan / Akta Kelahiran  / Surat Kelahiran yang telah dilegalisir oleh Instansi penerbit atau dengan menunjukkan aslinya;\n																			<li>Surat keterangan pindah / Surat Keterangan Pindah Datang bagi penduduk yang pindah dalam wilayah NKRI. atau\n																			<li>Surat pindah datang dari tempat asal (dalam wilayah NKRI)\n																		</ol>\n																		</p>', '0', '2019-06-20 05:26:21', 'AD2019020001'),
('KPC10005', 'KPJ10008', 'PERUBAHAN KK [PENAMBAHAN ANGGOTA BARU]', '0816668a62220bfbe89ed051626f91eeb2e649bb2b0e8f68eebafb0d7ca27d31.jpg', '<p>\n	<b><h5>PERSYARATAN PENGURUSAN :</h5></b>							                                      	<ol>\n																			<li>Mengisi formulir F1-01 yang ditandatangani Kepala Keluarga dan diketahui RT, RW serta lurah;\n																			<li>KK asli;\n																			<li>Kutipan akta kelahiran/Surat Kelahiran bagi keluarga yang mempunyai anak;\n																			<li>KK yang ditumpangi (asli) bila pecah KK;\n																			<li>Surat pindah datang dari tempat asal (dalam wilayah NKRI)\n\n																		</ol>\n																		</p>', '0', '2019-06-20 05:27:19', 'AD2019020001'),
('KPC10006', 'KPJ10008', 'PERUBAHAN KK [PENGURANGAN ANGGOTA]', '49f10d1b9a314ccc5a59f3ae4c454784538506b500507d1dfcb9157a2a7840e7.jpg', '<p>\n		<b><h5>PERSYARATAN PENGURUSAN :</h5></b>						                                      	<ol>\n																			<li>Kartu Keluarga(KK)\n																			<li>Surat Keterangan Kematian;\n																			<li>Surat Cerai/Akta Cerai;\n																			<li>Surat pindah keluar (dalam wilayah NKRI) \n																		</ol>\n																		</p>', '0', '2019-06-20 05:27:38', 'AD2019020001'),
('KPC10007', 'KPJ10008', 'PENERBITAN KK HILANG /  RUSAK', '659b10f6e11213cc2736c2c919971ccc92f523fe9b3e4a844c2c7b51ea89a95c.jpg', '<p>\n		<b><h5>PERSYARATAN PENGURUSAN :</h5></b>						                                      	<ol>\n																			<li>Mengisi formulir F1-01 yang ditandatangani Kepala Keluarga dan diketahui RT, RW serta Lurah;\n																			<li>Surat Peryataan Kehilangan KK yang bermaterai Rp. 6000 dan diketahui RT, RW, dan Lurah;\n																			<li>KK yang rusak / fotokopi KK yang hilang;\n																			<li>Fotokopi salah satu arsip KK yang dari RT atau Kelurahan atau menunjukkan dokumen kependudukan dari salah satu anggota keluarga yang sudah memiliki NIK( Nomor Induk Kependudukan);\n																			<li>Dokumen keimigrasian bagi Orang Asing.\n																		</ol>\n																		</p>', '0', '2019-06-20 05:28:16', 'AD2019020001'),
('KPC10008', 'KPJ10008', 'PERUBAHAN ANGGOTA KK YANG SALAH /DIRUBAH', 'bed7ced5f2e2c6e6c0e6bc7641e677a7269e399ce1240a3f7b0391e993416010.jpg', '<p>\n	<b><h5>PERSYARATAN PENGURUSAN :</h5></b>							                                      	<ol>\n																			<li>Mengisi formulir F1-01 yang ditandatangani Kepala Keluarga dan diketahui RT, RW serta Lurah;\n																			<li>Fotokopi Akta Kelahiran/Surat Kelahiran dari Dokter / Kelurahan;\n																			<li>Fotokopi ijasah yang dimiliki;\n																			<li>Fotokopi Surat Nikah / Akta Perkawinan;\n																			<li>Kartu Keluarga (KK) asli.\n																		</ol>\n																		</p>', '0', '2019-06-20 05:28:50', 'AD2019020001'),
('KPC10009', 'KPJ10012', 'AKTA KELAHIRAN', '59e22fe07f54354fdc51fdedc5e370fc9bf32405665103e388cae9737b0a0bec.jpg', ' <p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n                                                            <li>Form F-20.1 yang ditandatangani pelapor  dan 2 orang saksi kelahiran dan diketahui  kelurahan\n                                                            <li>Surat kelahiran  Dokter / Bidan ASLI (ttd + stempel) ATAU  Surat Pernyataan Tanggung jawab mutlak  (SPTJM) Kebenaran Data Kelahiran ditandatangani oleh pelapor  dan 2 orang saksi yang melihat atau mengetahui penandatanganan SPTJM\n                                                            <li>Surat Pernyataan belum pernah membuat akta (untuk umur 11 th ke atas)\n                                                            <li>FC KTP-el dan KK Pelapor yang masih berlaku (bagi pelapor yang berdomisili diluar kota Malang, Pelapor adalah kepala keluarga/orang  tua kandung/kakek nenek/ keluarga  dalam 1 KK yang sudah memiliki KTP\n                                                            <li>FC KTP-EL 2  orang dan KK Ayah + Ibu (bagi Ayah + ibu yang berdomisili diluar Kota Malang)\n                                                            <li>FC KTP-el 2 orang saksi (bagi saksi yang berdomisili diluar Kota Malang, saksi dalam F-20. 1 diperbolehkan merujuk pada saksi dalam  SPTJM)\n                                                            <li>FC Surat Nikah  / Akta Perkawinan / Akta Cerai Orang Tua (Harus DILEGALISIR) ATAU Surat pernyataan anak seorang ibu *materai 6000 \n                                                            <li>Konsistensi data dan status pada KK, KTP-EL dan buku nikah/ Akta Perkawinan orang tua\n                                                            Berkas persyaratan dimasukkan map:\n                                                            KUNING  :  kelahiran 0-60 hari\n                                                            BIRU    :  kelahiran diatas 60 hari\n\n\n                                                            </p>', '1', '2019-06-20 08:27:54', 'AD2019020001'),
('KPC10010', 'KPJ10012', 'AKTA KEMATIAN', 'ba6a93c9ac6a1cd2d085503f75f629133c1b0094750ad7694c0a924d3ee247c3.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n                                                            <ol>\n                                                                <li>Mengisi Form F2.28 yang ditandatangani pelapor.\n                                                                <li>Mengisi Form F2.29 yang telah ditandatangani pelapor, 2 orang saksi kematian dan diketahui kelurahan\n                                                                <li>Surat Keterangan Kematian Asli dari paramedis atau surat pernyataan meninggal dirumah yang ditandatangani oleh pelapor\n                                                                <li>KTP-el Asli dan KK Asli almarhum dan KTP-el Asli Istri/suami Almarhum\n                                                                <li>FC KTP-el 2 orang saksi\n                                                                <li>FC KTP-el dan KK Pelapor\n                                                                <li>Akta Kelahiran Almarhum / Surat Nikah Orang Tua Almarhum\n                                                                <li>FC Surat Nikah/ Akta Perkawinan Almarhum\n                                                                <li>Apabila tidak dapat  memenuhi persyaratan poin (8) dilengkapi dengan Surat Peryataan Anak Seorang ibu yang ditandatangani oleh pelapor\n                                                                <li>Pelapor adalah Ahli waris yang dibuktikan dengan Akta Kelahiran/ Buku Nikah, apabila tidak ada, bisa dilaporkan oleh penduduk yang terdaftar dalam 1 (satu) KK atau dalam 1 (satu) alamat yang sama  dengan Almarhum\n                                                                <li>Apabila poin 10 tidak terpenuhi, pelaporan kematian oleh Ketua RT/RW dengan dilengkapi Surat Penyataan , TTD dan Stempel RT/RW. <br>\n                                                                    <br>\n\n                                                                Catatan:\n                                                                -   Apabila terjadi ketidaksesuaian data almarhum dalam dokumen kependudukan (KK & KTP-el) maka dilengkapi surat pernyataan data yang digunakan untuk pencatatan akta kematian\n                                                                -   Apabila almarhum meninggal sebelum th.2010/belum memiliki NIK Nasioanl, maka HARUS melengkapi surat pernyataan yang ditandatangani ketua RT dan Ketua RW, dan mengetahui Lurah\n                                                                Berkas persyaratan dimasukkan map warna HIJAU\n\n                                                            <ol>\n                                                            </p>', '1', '2019-06-20 08:33:32', 'AD2019020001'),
('KPC10011', 'KPJ10012', 'AKTA PERKAWINAN', 'f7c208e63214a4b07458c5ec8dda1c60c10c7964b6e225c384721106859b2d42.jpg', '<p>\n <b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n					                                      	<br>\n					                                      	 Pencatatan perkawinan yang sah berdasarkan ketentuan Peraturan perundang-undangan wajib dilaporkan oleh penduduk kepada Instansi Pelaksanaan paling lambat 60 (enam puluh) hari kerja sejak tanggal perkawinan. Pencatatan perkawinan di Daerah bagi WNI :<br>\n																<ol>\n																	<li>Surat keterangan N1, N2, N3 dan N4 dari kelurahan domisili masing-masing;\n																	<li>Surat keterangan Asli perkawinan / pemberkatan dari pemuka agama , dan Pemuka Penghayat Kepercayaan;\n																	<li>Fotokopi KTP dan KK calon mempelai;\n																	<li>Fotokopi KTP dan KK Orang Tua;\n																	<li>Fotokopi Kutipan Akta kelahiran calon mempelai; cukup dari orang yang bersangkuan diketahui oleh Lurah/Kepala Desa dan /atau dari Pejabat Instansi yang berwenang daerah asal;\n																	<li>Surat pernyataan belum pernah kawin bermaterai  cukup dari yang bersangkutan diketahui oleh Lurah/ Kepala Desa dan /atau dari Pejabat Instansi yang berwenang daerah asal;\n																	<li>Surat Baptis/Permandian, Sidi, Surat Keterangan anggota agama Kristen, Khatolik, Hindu, Budha, Khonghucu dan Penghayat Kepercayaan;\n																	<li>Surat Izin komandan  bagi mereka anggota TNI/POLRI.\n																	<li>Kutipan Akta perceraian bagi yang telah bercerai;\n																	<li>Fotokopi Kutipan akta kematian suami/isteri terdahulu bila telah meninggal dunia;\n																	<li>Kutipan akta Kelahiran anak yang akan disahkan (apabila ada);\n																	<li>Akta perjanjian Perkawinan (apabila ada);\n																	<li>Pas Foto berwarna ukuran 4x6 cm berdampingan sebanyak 5(lima lembar;\n																	<li>Fotokopi KTP dua orang saksi yang hadir pada waktu pencatatan.\n																	<li>Penetapan PN bagi yang berbeda agama;\n																	<li>Penetapan PN bagi yang berpoligami;\n																	<li>Dispensasi dari Pengadilan Negeri untuk pria < 19 tahun wanita < 16 tahun\n																	<li>Izin  dari orang tua bagi yang belum berumur 21 tahun;\n																	<li>Surat Rekomendasi Menikah dari Catatan Sipil bagi salah satu pasangan yang berdomisili di luar.\n																	<li>Semua dokumen persyaratan yang difotokopi haru dilegalisisr oleh instansi penerbit ATAU dengan menunjukkan aslinya.\n																	<li>Bagi penduduk Kota Malang melampirkan KK dan KTP-EL ASLI\n																</ol>\n																	<b>Berkas Persyaratan dimasukkan map warna MERAH</b>\n\n															</p>', '1', '2019-06-20 08:33:36', 'AD2019020001'),
('KPC10012', 'KPJ10012', 'PENCATATAN PERUBAHAN NAMA', '363bc44de5a9f36a60259ed35f2cb0de0a7deb8e1570d793913d84c58fac062f.jpg', '<p>\n<h5 align =\"center\">PERSYARATAN MENGURUS PENCATATAN PERUBAHAN NAMA</h5>\n					                                      	<br>\n																<ol>\n																	<li>Pencatatan perubahan nama dilaksanakan berdasarkan Penetapan Pengadilan Negeri tempat domisili  pemohon;\n																	<li>Pencatatan Pelaporan  perubahan nama dilaporkan kepada instansi pelaksana paling lambat 30 (tiga puluh)  hari sejak diterimanya Putusan dan / atau Penetapan Pengadilan Negeri;\n																	<li>Persyaratan Pencatatan  Perubahan nama sebagaimana di maksud adalah sebagai berikut:\n																	</li>\n																</ol>\n																<ol type =\"a\">\n																	<li>Kutipan Akta Kelahiran, Kutipan Akta Perkawinan dan Akta Perceraian (asli dan fotocopy)\n																	<li>Salinan penetapan Pengadilan Negeri yang mempunyai kekuatan hukum tetap tentang perubahan nama;\n																	<li>Fotocopy KK dan KTP-EL pemohon;\n																	<li>Fotocopy KTP-EL Pelapor;\n																	<li>Semua  dokumen  persyaratan yang di fotocopy harus dilegalisir oleh instansi penerbit atau  dengan menunjukkan aslinya<br>\n																</ol>\n																	<b>Berkas Persyaratan dimasukkan map warna HIJAU</b>\n																\n												</p>', '1', '2019-06-20 08:33:39', 'AD2019020001'),
('KPC10013', 'KPJ10012', 'PENERBITAN KUTIPAN KEDUA AKTA PENCATATAN SIPIL', 'a3ad74525f901772b8d4f3778eb2f6e615cb2bfbfeaf175b04118dfd78792c01.jpg', '<p>\n <b><h5>PERSYARATAN PENGURUSAN :</h5></b>					                                      	<br>\n																<ol>\n																	<li>Penerbitan Kutipan kedua akta catatan sipil dikarenakan kutipan akta catatan sipil yang pertama hilang / rusak kesalahan redaksional\n																	<li>Penerbitan Kutipan kedua akta catatan sipil sebagaimana dimaksud persyaratannya sebagai berikut:\n																	</ol>\n																	<ol type =\"a\">\n																	<li>Foto kopi akta catatan sipil;\n																	<li>Surat Laporan Kehilangan dari POLRI apabila akta hilang;\n																	<li>Kutipan Akta Asli apabila penerbitan kutipan kedua akta karena rusak;\n																	<li>Foto kopi KK dan KTP-el pelapor / orang tua ; \n																	<li>Formulir pendaftaran kutipan kedua akta pencatatan sipil.\n																</ol><br>\n																<b>Berkas persyaratan dimasukkan map warna HIJAU</b>\n												</p>', '1', '2019-06-20 08:33:43', 'AD2019020001'),
('KPC10014', 'KPJ10012', 'PENCATATAN DAN PENERBITAN AKTA PENGESAHAN ANAK', '727dcd70066eee67b3522576aa67bfa386a10963a5867edeb8e583cc2cc51fa8.jpg', '<p>\n <b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n					                                      	<br>\n																<ol>\n																	<li>Kutipan Akta Kelahiran anak ( Asli dan Fotokopi ) ;\n																	<li>Fotokopi Akta Perkawinan orang tua;\n																	<li>Fotokopi KK dan KTP-el pemohon ( Orang tua );\n																	<li>Fotokopi KTP-el 2 (dua) orang saksi;\n																	<li>Surat Pengantar dari Lurah  ( RT, RW, mengetahui Lurah ).\n																</ol><br>\n																<b>Berkas persyaratan dimasukkan map warna HIJAU.</b>\n												</p>', '1', '2019-06-20 08:33:46', 'AD2019020001'),
('KPC10015', 'KPJ10012', 'PENCATATAN PENGANGKATAN ANAK (ADOPSI)', '4de5b2e955738c524f83c0e0010843fe7cc69d3699b7bdc6f1ecfbdef757c251.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>					                                      	<br>\n																<ol>\n																	<li>Kutipan Akta Kelahiran anak ( Asli dan Fotokopi );\n																	<li>Salinan Penetapan Pengadilan Negeri yang mempunyai kekuatan hukum tetap tentang pengangkatan anak (Adopsi);\n																	<li>Fotokopi Akta Perkawinan orang tua Kandung;\n																	<li>Fotokopi KK dan KTP-el Orang tua Kandung;\n																	<li>Fotokopi KK dan KTP-el Orang tua Angkat;\n																	<li>Surat Pengantar dari Lurah ( RT, RW, mengetahui Lurah ).\n																</ol><br>\n																<b>Berkas persyaratan dimasukkan map warna HIJAU</b>\n												</p>', '1', '2019-06-20 08:33:49', 'AD2019020001'),
('KPC10016', 'KPJ10012', 'PENCATATAN PENGAKUAN ANAK', '2428a44aedb62bc9d8d61d02450bb41dde1ab63003b1639d64cf0e5cd4270c96.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>					                                      	<br>\n																<ol>\n																	<li>Penerbitan Kutipan kedua akta catatan sipil dikarenakan kutipan akta catatan sipil yang pertama hilang / rusak kesalahan redaksional\n																	<li>Penerbitan Kutipan kedua akta catatan sipil sebagaimana dimaksud persyaratannya sebagai berikut:\n																	</ol>\n																	<ol type =\"a\">\n																	<li>Foto kopi akta catatan sipil;\n																	<li>Surat Laporan Kehilangan dari POLRI apabila akta hilang;\n																	<li>Kutipan Akta Asli apabila penerbitan kutipan kedua akta karena rusak;\n																	<li>Foto kopi KK dan KTP-el pelapor / orang tua ; \n																	<li>Formulir pendaftaran kutipan kedua akta pencatatan sipil.\n																</ol>\n												</p>', '1', '2019-06-20 08:33:53', 'AD2019020001'),
('KPC10017', 'KPJ10007', 'KIA BARU', '6c3dda038dddde9b16acbcfc08babbfbfa232c87b7652226e172e2fd57a9d6a1.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN : </h5></b>\n                                                            <br>\n                                                            <ol>\n                                                                <li>Mengisi  Formulir KIA\n                                                                <li>Anak usia dibawah 5 tahun ( sudah punya akta kelahiran)<br>\n                                                                        a.  FC. Kartu Keluarga orang tua/ wali (dengan menunjukkan aslinya),<br>\n                                                                        b.  FC. KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                        c.  FC. Kutipan Akta kelahiran (dengan menunjukkan  aslinya).<br>\n                                                                <li>Anak Usia  5 tahun s/d 17 tahun kurang satu hari<br>\n                                                                        a.  FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                        b.  FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                        c.  FC Kutipan Akta kelahiran (dengan menunjukkan aslinya),<br>\n                                                                        d.  Pas Foto berwarna ukuran 3 x 4 cm = 2 lembar.<br>\n                                                                <li>Anak WNI baru datang dari Luar Negeri usia kurang 5 tahun<br>\n                                                                        a.  FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                        b.  FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                        c.  Surat keterangan datang dari Luar Negeri yang diterbitkan oleh Dinas,<br>\n                                                                        d.  Surat tanda bukti pelaporan kelahiran Luar Negeri.<br>\n                                                                <li>Anak WNI baru datang dari Luar Negeri usia 5 tahun s/d 17 th kurang     satu hari<br>\n                                                                        a.  FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                        b.  FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                        c.  Surat keterangan datang dari Luar Negeri yang diterbitkan oleh Dinas,<br>\n                                                                        d.  Surat tanda bukti pelaporan kelahiran Luar Negeri.<br>\n                                                                <li>Penerbitan Karena Hilang<br>\n                                                                        a.  FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                        b.  FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                        c.  Surat keterangan kehilangan dari kepolisian.<br>\n                                                                <li>Penerbitan karena rusak<br>\n                                                                        a.  FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                        b.  FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),\n                                                                        KIA yang rusak<br>\n                                                                <li>Karena Pindah Datang<br>\n                                                                        a.  FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                        b.  FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                    Surat keterangan pindah datang.<br>\n                                                                <li>KIA ORANG ASING<br>\n                                                                    a.  Usia Kurang 5 tahun<br>\n                                                                •   FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                •   FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                •   FC Paspor dan ijin tinggal tetap<br>\n\n\n                                                                    b.  Usia 5 tahun s/d 17 tahun kurang 1 hari<br>\n                                                                •   FC.Kartu Keluarga orangtua/wali (dengan menunjukkan aslinya),<br>\n                                                                •   FC.KTP-el orang tua/wali (dengan  menunjukkan aslinya),<br>\n                                                                •   FC Paspor dan Ijin tinggal tetap.<br>\n                                                                •   Pas Foto warna ukuran 3 x 4 cm = 2 lb<br>\n\n                                                            <ol>\n                                                            </p>', '0', '2019-06-20 05:23:13', 'AD2019020001'),
('KPC10018', 'KPJ10013', 'Akta Kelahiran Baru', '35e5fafc8abe590fc8d46fc967b662385a1753f7c7bda9d7076ce2fae5f13a7d.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>					                                      	<br>\n																<ol>\n                                                            <li>Form F-20.1 yang ditandatangani pelapor  dan 2 orang saksi kelahiran dan diketahui  kelurahan\n                                                            <li>Surat kelahiran  Dokter / Bidan ASLI (ttd + stempel) ATAU  Surat Pernyataan Tanggung jawab mutlak  (SPTJM) Kebenaran Data Kelahiran ditandatangani oleh pelapor  dan 2 orang saksi yang melihat atau mengetahui penandatanganan SPTJM\n                                                            <li>Surat Pernyataan belum pernah membuat akta (untuk umur 11 th ke atas)\n                                                            <li>FC KTP-el dan KK Pelapor yang masih berlaku (bagi pelapor yang berdomisili diluar kota Malang, Pelapor adalah kepala keluarga/orang  tua kandung/kakek nenek/ keluarga  dalam 1 KK yang sudah memiliki KTP\n                                                            <li>FC KTP-EL 2  orang dan KK Ayah + Ibu (bagi Ayah + ibu yang berdomisili diluar Kota Malang)\n                                                            <li>FC KTP-el 2 orang saksi (bagi saksi yang berdomisili diluar Kota Malang, saksi dalam F-20. 1 diperbolehkan merujuk pada saksi dalam  SPTJM)\n                                                            <li>FC Surat Nikah  / Akta Perkawinan / Akta Cerai Orang Tua (Harus DILEGALISIR) ATAU Surat pernyataan anak seorang ibu *materai 6000 \n                                                            <li>Konsistensi data dan status pada KK, KTP-EL dan buku nikah/ Akta Perkawinan orang tua\n                                                            Berkas persyaratan dimasukkan map:\n                                                            KUNING  :  kelahiran 0-60 hari\n                                                            BIRU    :  kelahiran diatas 60 hari\n																				</ol>\n												</p>', '0', '2019-06-20 08:38:29', 'AD2019020001'),
('KPC10020', 'KPJ10014', 'Akta Kematian Baru', 'b3e8bc7ff57d90edde24b83e2b60fbecfb3dcc71b8da3e32bfe51482fc842746.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n                                                            <ol>\n                                                                <li>Mengisi Form F2.28 yang ditandatangani pelapor.\n                                                                <li>Mengisi Form F2.29 yang telah ditandatangani pelapor, 2 orang saksi kematian dan diketahui kelurahan\n                                                                <li>Surat Keterangan Kematian Asli dari paramedis atau surat pernyataan meninggal dirumah yang ditandatangani oleh pelapor\n                                                                <li>KTP-el Asli dan KK Asli almarhum dan KTP-el Asli Istri/suami Almarhum\n                                                                <li>FC KTP-el 2 orang saksi\n                                                                <li>FC KTP-el dan KK Pelapor\n                                                                <li>Akta Kelahiran Almarhum / Surat Nikah Orang Tua Almarhum\n                                                                <li>FC Surat Nikah/ Akta Perkawinan Almarhum\n                                                                <li>Apabila tidak dapat  memenuhi persyaratan poin (8) dilengkapi dengan Surat Peryataan Anak Seorang ibu yang ditandatangani oleh pelapor\n                                                                <li>Pelapor adalah Ahli waris yang dibuktikan dengan Akta Kelahiran/ Buku Nikah, apabila tidak ada, bisa dilaporkan oleh penduduk yang terdaftar dalam 1 (satu) KK atau dalam 1 (satu) alamat yang sama  dengan Almarhum\n                                                                <li>Apabila poin 10 tidak terpenuhi, pelaporan kematian oleh Ketua RT/RW dengan dilengkapi Surat Penyataan , TTD dan Stempel RT/RW. <br>\n                                                                    <br>\n\n                                                                Catatan:\n                                                                -   Apabila terjadi ketidaksesuaian data almarhum dalam dokumen kependudukan (KK & KTP-el) maka dilengkapi surat pernyataan data yang digunakan untuk pencatatan akta kematian\n                                                                -   Apabila almarhum meninggal sebelum th.2010/belum memiliki NIK Nasioanl, maka HARUS melengkapi surat pernyataan yang ditandatangani ketua RT dan Ketua RW, dan mengetahui Lurah\n                                                                Berkas persyaratan dimasukkan map warna HIJAU\n\n                                                            <ol>\n                                                            </p>', '0', '2019-06-20 08:39:34', 'AD2019020001'),
('KPC10021', 'KPJ10015', 'Akta Perkawinan Baru', 'a189902a1b21be650202310db8bc049e5dc201c0286582bb6b07c488d26f2c62.jpg', '<p>\n <b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n					                                      	<br>\n					                                      	 Pencatatan perkawinan yang sah berdasarkan ketentuan Peraturan perundang-undangan wajib dilaporkan oleh penduduk kepada Instansi Pelaksanaan paling lambat 60 (enam puluh) hari kerja sejak tanggal perkawinan. Pencatatan perkawinan di Daerah bagi WNI :<br>\n																<ol>\n																	<li>Surat keterangan N1, N2, N3 dan N4 dari kelurahan domisili masing-masing;\n																	<li>Surat keterangan Asli perkawinan / pemberkatan dari pemuka agama , dan Pemuka Penghayat Kepercayaan;\n																	<li>Fotokopi KTP dan KK calon mempelai;\n																	<li>Fotokopi KTP dan KK Orang Tua;\n																	<li>Fotokopi Kutipan Akta kelahiran calon mempelai; cukup dari orang yang bersangkuan diketahui oleh Lurah/Kepala Desa dan /atau dari Pejabat Instansi yang berwenang daerah asal;\n																	<li>Surat pernyataan belum pernah kawin bermaterai  cukup dari yang bersangkutan diketahui oleh Lurah/ Kepala Desa dan /atau dari Pejabat Instansi yang berwenang daerah asal;\n																	<li>Surat Baptis/Permandian, Sidi, Surat Keterangan anggota agama Kristen, Khatolik, Hindu, Budha, Khonghucu dan Penghayat Kepercayaan;\n																	<li>Surat Izin komandan  bagi mereka anggota TNI/POLRI.\n																	<li>Kutipan Akta perceraian bagi yang telah bercerai;\n																	<li>Fotokopi Kutipan akta kematian suami/isteri terdahulu bila telah meninggal dunia;\n																	<li>Kutipan akta Kelahiran anak yang akan disahkan (apabila ada);\n																	<li>Akta perjanjian Perkawinan (apabila ada);\n																	<li>Pas Foto berwarna ukuran 4x6 cm berdampingan sebanyak 5(lima lembar;\n																	<li>Fotokopi KTP dua orang saksi yang hadir pada waktu pencatatan.\n																	<li>Penetapan PN bagi yang berbeda agama;\n																	<li>Penetapan PN bagi yang berpoligami;\n																	<li>Dispensasi dari Pengadilan Negeri untuk pria < 19 tahun wanita < 16 tahun\n																	<li>Izin  dari orang tua bagi yang belum berumur 21 tahun;\n																	<li>Surat Rekomendasi Menikah dari Catatan Sipil bagi salah satu pasangan yang berdomisili di luar.\n																	<li>Semua dokumen persyaratan yang difotokopi haru dilegalisisr oleh instansi penerbit ATAU dengan menunjukkan aslinya.\n																	<li>Bagi penduduk Kota Malang melampirkan KK dan KTP-EL ASLI\n																</ol>\n																	<b>Berkas Persyaratan dimasukkan map warna MERAH</b>\n\n															</p>', '0', '2019-06-20 08:39:57', 'AD2019020001'),
('KPC10022', 'KPJ10016', 'Kutipan Kedua Akta Pencatatan Sipil Baru', '52329f15a0f58d08327b2eec712263f8baa614939ea7cad5aba9198d4e4bbc8c.jpg', '<p>\n <b><h5>PERSYARATAN PENGURUSAN :</h5></b>					                                      	<br>\n																<ol>\n																	<li>Penerbitan Kutipan kedua akta catatan sipil dikarenakan kutipan akta catatan sipil yang pertama hilang / rusak kesalahan redaksional\n																	<li>Penerbitan Kutipan kedua akta catatan sipil sebagaimana dimaksud persyaratannya sebagai berikut:\n																	</ol>\n																	<ol type =\\\"a\\\">\n																	<li>Foto kopi akta catatan sipil;\n																	<li>Surat Laporan Kehilangan dari POLRI apabila akta hilang;\n																	<li>Kutipan Akta Asli apabila penerbitan kutipan kedua akta karena rusak;\n																	<li>Foto kopi KK dan KTP-el pelapor / orang tua ; \n																	<li>Formulir pendaftaran kutipan kedua akta pencatatan sipil.\n																</ol><br>\n																<b>Berkas persyaratan dimasukkan map warna HIJAU</b>\n												</p>', '0', '2019-06-20 08:44:59', 'AD2019020001'),
('KPC10023', 'KPJ10017', 'Pencatatan Perubahan Nama baru', '168c5ee44c70648a3dfa1b447049a3864f349903cc6c77c3d74cbaf2fa9723bc.jpg', '<p>\n <b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n					                                      	<br>\n																<ol>\n																	<li>Kutipan Akta Kelahiran anak ( Asli dan Fotokopi ) ;\n																	<li>Fotokopi Akta Perkawinan orang tua;\n																	<li>Fotokopi KK dan KTP-el pemohon ( Orang tua );\n																	<li>Fotokopi KTP-el 2 (dua) orang saksi;\n																	<li>Surat Pengantar dari Lurah  ( RT, RW, mengetahui Lurah ).\n																</ol><br>\n																<b>Berkas persyaratan dimasukkan map warna HIJAU.</b>\n												</p>', '0', '2019-06-20 08:45:41', 'AD2019020001'),
('KPC10024', 'KPJ10018', 'Akta pengesahan Anak Baru', '95bf2d6b990119dba4ffc49f2135a213de921a0b7c908eb49175779148aaa7eb.jpg', '<p>\n <b><h5>PERSYARATAN PENGURUSAN :</h5></b>\n					                                      	<br>\n																<ol>\n																	<li>Kutipan Akta Kelahiran anak ( Asli dan Fotokopi ) ;\n																	<li>Fotokopi Akta Perkawinan orang tua;\n																	<li>Fotokopi KK dan KTP-el pemohon ( Orang tua );\n																	<li>Fotokopi KTP-el 2 (dua) orang saksi;\n																	<li>Surat Pengantar dari Lurah  ( RT, RW, mengetahui Lurah ).\n																</ol><br>\n																<b>Berkas persyaratan dimasukkan map warna HIJAU.</b>\n												</p>', '0', '2019-06-20 08:46:26', 'AD2019020001'),
('KPC10025', 'KPJ10019', 'Pengesahan Pengangkatan Anak (Adopsi) Baru', '7c76b8689ed005e3b751af9172e8a0bbf899375399b5bda0857cfd98c5fad0af.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>					                                      	<br>\n																<ol>\n																	<li>Kutipan Akta Kelahiran anak ( Asli dan Fotokopi );\n																	<li>Salinan Penetapan Pengadilan Negeri yang mempunyai kekuatan hukum tetap tentang pengangkatan anak (Adopsi);\n																	<li>Fotokopi Akta Perkawinan orang tua Kandung;\n																	<li>Fotokopi KK dan KTP-el Orang tua Kandung;\n																	<li>Fotokopi KK dan KTP-el Orang tua Angkat;\n																	<li>Surat Pengantar dari Lurah ( RT, RW, mengetahui Lurah ).\n																</ol><br>\n																<b>Berkas persyaratan dimasukkan map warna HIJAU</b>\n												</p>', '0', '2019-06-20 08:47:09', 'AD2019020001'),
('KPC10026', 'KPJ10020', 'Pencatatan Pengangkatan Anak - Baru', '7db569b3f51715249b7e1cff85b9d2492737dce0d99db3faa973741a8638e06f.jpg', '<p>\n<b><h5>PERSYARATAN PENGURUSAN :</h5></b>					                                      	<br>\n																<ol>\n																	<li>Penerbitan Kutipan kedua akta catatan sipil dikarenakan kutipan akta catatan sipil yang pertama hilang / rusak kesalahan redaksional\n																	<li>Penerbitan Kutipan kedua akta catatan sipil sebagaimana dimaksud persyaratannya sebagai berikut:\n																	</ol>\n																	<ol type =\"a\">\n																	<li>Foto kopi akta catatan sipil;\n																	<li>Surat Laporan Kehilangan dari POLRI apabila akta hilang;\n																	<li>Kutipan Akta Asli apabila penerbitan kutipan kedua akta karena rusak;\n																	<li>Foto kopi KK dan KTP-el pelapor / orang tua ; \n																	<li>Formulir pendaftaran kutipan kedua akta pencatatan sipil.\n																</ol>\n												</p>', '0', '2019-06-20 08:48:29', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_antrian`
--

CREATE TABLE `kesehatan_antrian` (
  `id_antrian` varchar(30) NOT NULL,
  `nik` varchar(16) NOT NULL,
  `id_layanan_menu` varchar(1) NOT NULL,
  `id_jenis_kesehatan` varchar(8) NOT NULL,
  `id_rs` varchar(15) NOT NULL,
  `id_poli` varchar(11) NOT NULL,
  `wkt_pendaftaran` datetime NOT NULL,
  `wkt_booking` datetime NOT NULL,
  `no_booking` varchar(5) NOT NULL,
  `status_antrian` enum('0','1','2','3') NOT NULL,
  `admin_acc` varchar(12) NOT NULL,
  `text_antrian` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_antrian`
--

INSERT INTO `kesehatan_antrian` (`id_antrian`, `nik`, `id_layanan_menu`, `id_jenis_kesehatan`, `id_rs`, `id_poli`, `wkt_pendaftaran`, `wkt_booking`, `no_booking`, `status_antrian`, `admin_acc`, `text_antrian`, `is_delete`, `time_update`, `id_admin`) VALUES
('AC20190731100000001', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 08:47:18', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 08:47:18', ''),
('AC20190731100000002', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 08:48:16', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 08:48:16', ''),
('AC20190731100000003', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 08:50:31', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 08:50:31', ''),
('AC20190731100000004', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 08:51:32', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 08:51:32', ''),
('AC20190731100000005', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 09:40:03', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 09:40:03', ''),
('AC20190731100000006', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 09:40:52', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 09:40:52', ''),
('AC20190731100000007', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 09:43:24', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 09:43:24', ''),
('AC20190731100000008', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-07-31 09:44:53', '2019-07-31 00:00:00', 'A-002', '0', '', '', '0', '2019-07-31 09:44:53', ''),
('AC20190802100000001', '3573011903940006', '0', '1', 'RS2019071910001', 'P10001', '2019-08-02 09:52:17', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 09:52:17', ''),
('AC20190802100000002', '3573011903940006', '0', '1', 'RS2019071910008', 'P10001', '2019-08-02 09:53:47', '2019-08-02 00:00:00', 'A-002', '0', '', '', '0', '2019-08-02 09:53:47', '');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_jenis`
--

CREATE TABLE `kesehatan_jenis` (
  `id_layanan` varchar(8) NOT NULL,
  `nama_layanan` text NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_jenis`
--

INSERT INTO `kesehatan_jenis` (`id_layanan`, `nama_layanan`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('KSJ10001', 'Rumah Sakit Umum', '41e216aa43a641cac7d611b84323e37dfd67043b28a6d0d50fc16fbd46b189b6.jpg', '0', '2019-06-14 05:54:35', 'AD2019020001'),
('KSJ10002', 'Rumah Sakit Ibu dan Anak', '1483bcd0544c8299806b216c188f730ef9e187c076d69c75248f2dce221f8f28.jpg', '0', '2019-06-14 05:54:57', 'AD2019020001'),
('KSJ10003', 'Rumah Sakit Islam', 'b990492b26b993f77bab8fae398eb0909b2ac41e1c942c9e2d7786871ef3c4ce.jpg', '0', '2019-06-14 05:55:20', 'AD2019020001'),
('KSJ10006', 'Puskesmas', 'f52d513956d3b4bbbf1b198a74dd7b9fa8e1ef9e52dc0166e86455ecadad3bf1.jpg', '0', '2019-06-14 05:55:54', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_poli`
--

CREATE TABLE `kesehatan_poli` (
  `id_poli` varchar(11) NOT NULL,
  `nama_poli` varchar(30) NOT NULL,
  `foto` varchar(70) NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_poli`
--

INSERT INTO `kesehatan_poli` (`id_poli`, `nama_poli`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('P10001', 'mata', 'ee5100efd45180fb4269a3554a67eb034b5edf1a8a38d3897cb2e2f5297f08b4.jpg', '0', '2019-06-14 04:58:00', 'AD2019020001'),
('P10002', 'gigi', 'f31a026cd7eee1636517aba2bb3a9b4f470b8a9dda315f7becc3f35488e3637f.jpg', '0', '2019-06-14 04:59:30', 'AD2019020001'),
('P10003', 'penyakit dalam', '419b469169e2203d72960d4298014f189c94bfde5c0532f1ec9ebb6f52c023c7.jpg', '0', '2019-06-14 05:27:38', 'AD2019020001'),
('P10004', 'paru-paru', '767d8915eb55fddd11b4999be7e8e0df43b15bd5f91470f809fb0b1b315c3a78.jpg', '0', '2019-06-14 04:59:55', 'AD2019020001'),
('P10005', 'jantung', '4248ddf412efcddc34d5cf011a8d9354b158426e8fb439508c5f1231247e441f.jpg', '0', '2019-06-14 05:00:05', 'AD2019020001'),
('P10006', 'bedah khusus', '7520d15d0d33248cc6984b53f4aae01dd9decccf5b7f03ebc60ea3ead369888e.jpg', '0', '2019-06-14 05:05:07', 'AD2019020001'),
('P10007', 'bedah ringan', '74b8f8e90664162215e6d0b5ad0096fa71263017845f4367a75bbc5d740c5599.jpg', '0', '2019-06-14 05:07:01', 'AD2019020001'),
('P10008', 'Poli KIA', '8aacbcac4ac9106364e6f0330f9386533581a88aed9ae6fd73630dba146d704b.jpg', '0', '2019-07-19 03:54:58', 'AD2019020001'),
('P10009', 'Kandungan dan Kebidanan', '132b3314751e337b31d5712854c4acd0a850f6ae7c444da45f8b9b029722fe46.jpg', '0', '2019-07-19 03:55:25', 'AD2019020001'),
('P10010', 'Bedah Umum', '8c3e14281e1016837d26cb1218697fa3023ad101c3d21496aa2785605626e4d5.jpg', '0', '2019-07-19 03:56:52', 'AD2019020001'),
('P10011', 'Syaraf', '14e1fd303aaa862f9984d41814bb0b8498abb4923dac231f0123bf31bd220152.jpg', '0', '2019-07-19 03:58:28', 'AD2019020001'),
('P10012', 'Psikologi', '8de4b643ccb9c32261711ec0ac8810721ce7ccbe6f476fa22865c533184f5f1a.jpg', '0', '2019-07-19 04:01:55', 'AD2019020001'),
('P10013', 'GIZI', '4bd7538ca14288d8e50de1fde81c0d563f9477499e2d98cd0c5014afe6f7bdfe.jpg', '0', '2019-07-19 04:01:44', 'AD2019020001'),
('P10014', 'Fisioterapi', '185e835997d011d4a5c1e05cb70939592c9fb2ec359e1ad7af743a4dbaca3f4f.jpg', '0', '2019-07-19 04:46:17', 'AD2019020001'),
('P10015', 'Orthopedi', '6ecd0d37b5af5417d58f5dfc0be7f712502d55de15ce3e82b2d80a8b7a6f5282.jpg', '0', '2019-07-19 04:12:12', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `kesehatan_rs`
--

CREATE TABLE `kesehatan_rs` (
  `id_rs` varchar(15) NOT NULL,
  `nama_rumah_sakit` text NOT NULL,
  `alamat` text NOT NULL,
  `foto_rs` varchar(70) DEFAULT NULL,
  `telepon` varchar(13) NOT NULL,
  `id_layanan` varchar(8) NOT NULL,
  `id_poli` text NOT NULL,
  `is_delete` enum('0','1') DEFAULT NULL,
  `waktu` datetime DEFAULT NULL,
  `id_admin` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kesehatan_rs`
--

INSERT INTO `kesehatan_rs` (`id_rs`, `nama_rumah_sakit`, `alamat`, `foto_rs`, `telepon`, `id_layanan`, `id_poli`, `is_delete`, `waktu`, `id_admin`) VALUES
('RS2019071910001', 'RSUD. Dr. Saiful Anwar', 'Jl. Jaksa Agung Suprapto, Klojen, Kec. Klojen, Kota Malang, Jawa Timur 65112, Indonesia', '57179310114c5d02264042691b4df90e32aa01ee552e3d3451ca4dec963ef912.jpg', '62341366242', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:13:02', 'AD2019020001'),
('RS2019071910003', 'RS. Tk.II dr. Soepraoen', 'Jl. S. Supriadi No.22, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65112, Indonesia', 'f20a6f3ed8cca6a2b57c6e405972734ffe0465440eb95ae5053a67ace5a4a57b.jpg', '62341325112', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:13:13', 'AD2019020001'),
('RS2019071910004', 'RS. Panti Nirmala', 'Jl. Kebalen Wetan No.2-8, Kotalama, Kec. Kedungkandang, Kota Malang, Jawa Timur 65134, Indonesia', '25e88c584acbc081ea382eaefea3d8802d607543fb0afbd4b697e3508de2da76.jpg', '62341362459', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:13:26', 'AD2019020001'),
('RS2019071910005', 'RS. Panti Waluya Sawahan', 'Jl. Nusakambangan No.56, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117, Indonesia', '1e872a3e3057c15df0e046ce2a22acca70f87a4c3f93ad3265e21a09785b6ae4.jpg', '62341366033', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:13:37', 'AD2019020001'),
('RS2019071910006', 'RS. Lavalette', 'Jalan Wage Rudolf Supratman No.10, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65121, Indonesia', '4ce19396ebf0517fcb437e3fae1e395272481e208f4f0e81fcf530325ddda301.jpg', '62341470805', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:13:48', 'AD2019020001'),
('RS2019071910007', 'RSI. Aisyiyah', 'Jl. Sulawesi No.16, Kasin, Kec. Klojen, Kota Malang, Jawa Timur 65117, Indonesia', 'f32ce6bf19f468adda2d90fcbeac7695ca99d45f087c69dab5a7acc1b06b8573.jpg', '62341326773', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\']', '0', '2019-07-19 03:49:00', 'AD2019020001'),
('RS2019071910008', 'RSI. UNISMA MALANG', 'Jalan, Jl. Mayjen Haryono Gg. 10 No.139, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144, Indonesia', '051a69b8723fd4498b64fdac6caca39a8e0ad431d3dfd65fa7a23603abcccfc2.jpg', '62341551356', 'KSJ10003', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 05:07:20', 'AD2019020001'),
('RS2019071910009', 'RSIA. Permata Bunda', 'Jl. Soekarno Hatta No.75, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142, Indonesia', '0b14849a61abe73d5ca32200cf8a1a39b0edcaa52012de8831ac1f9843f0e57b.jpg', '62341487487', 'KSJ10002', '[\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10013\']', '0', '2019-07-19 04:14:13', 'AD2019020001'),
('RS2019071910010', 'RS. Hermina Tangkubanprahu', 'Jl. Tangkuban Perahu No.31-33, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia', '3030dd3ea3d9e665692cb91560ac19e90aaae0fe92f1597fbf2ccfd15658ea57.jpg', '62341325082', 'KSJ10002', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10006\',\'P10007\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:15:05', 'AD2019020001'),
('RS2019071910011', 'RS Persada Hospital', 'Jl. Panji Suroso Araya Bussiness Centre Kav. II-IV', 'd3cd5ba747c76fd2b4c8bda28e9ef2dd1555638ab6555d2eeefd9278c31e8e8f.jpg', '623412996333', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:16:01', 'AD2019020001'),
('RS2019071910012', 'RSUD Kota Malang', 'Jl. Rajasa No.27 Kel. Bumiayu Kec. Kedungkandang', '03dad72776a5aed35e04312981dc927db1461328e41ee231d836acaf8a3c583d.jpg', '62341754338', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:19:36', 'AD2019020001'),
('RS2019071910013', 'RS Universitas Brawijaya', 'Jl. Soekarno - Hatta, Lowokwaru, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141, Indonesia', '107072e084af9067de095bbc88c4b24fc823230b0a912b6f74950bc4d2df5332.jpg', '62341403000', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:24:02', 'AD2019020001'),
('RS2019071910015', 'RS. Bhakti Bunda', 'Jl. Mayjend. Panjaitan No.176, Penanggungan, Kec. Klojen, Kota Malang, Jawa Timur 65113, Indonesia', 'a9f9dea78bd3fb4d5a286101cd14d7140911c2f3ba4007f8c4ffc151177a72b3.jpg', '62341552955', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:25:24', 'AD2019020001'),
('RS2019071910020', 'RSIA Husada Bunda', 'Jl. Pahlawan Trip, Oro-oro Dowo, Klojen, Malang City, East Java 65112, Indonesia', '6f034a5a83cad02868df29154693f7eca3b7ed1c89f29123b91d4ff876c0bdb9.jpg', '62341566972', 'KSJ10002', '[\'P10008\',\'P10009\',\'P10010\',\'P10013\']', '0', '2019-07-19 04:55:21', 'AD2019020001'),
('RS2019071910021', 'RSIA Muhammadiyah', 'Jl. Kyai H. Wahid Hasyim No.30, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia', '052838e387fb3e7eaeef602a76b3c4649aa97af01f908dbea307af2b53665e38.jpg', '62341351549', 'KSJ10001', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10012\',\'P10013\',\'P10014\',\'P10015\']', '0', '2019-07-19 04:56:44', 'AD2019020001'),
('RS2019071910022', 'RSIA Mardi Waloeja Kauman', 'Jl. Kauman No.23, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia', 'b8d52a4ab4b33eb336ddb9a43a5a02e6feb6d1b283e6545a69635dd5d5a929df.jpg', '62341358508', 'KSJ10002', '[\'P10008\',\'P10009\',\'P10010\',\'P10012\',\'P10013\']', '0', '2019-07-19 04:57:25', 'AD2019020001'),
('RS2019071910023', 'RSIA Puri', 'Taman Slamet Street No.20, Gading Kasri, Klojen, Malang City, East Java 65115, Indonesia', '5a6aaab1276274ab191b0ba849ae2ca4343dd365b30101ab22e15c7eabfa6e40.jpg', '62341325329', 'KSJ10002', '[\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10013\']', '0', '2019-07-19 04:57:59', 'AD2019020001'),
('RS2019071910024', 'RSIA Permata Hati', 'Jl. Danau Toba Blok E 6 No.16 - 18, Lesanpuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65138, Indonesia', 'd5422859a368e745d0de6b7c127dc1de960e4dde70a17d7626f8d2b284086637.jpg', '62341718068', 'KSJ10002', '[\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10013\']', '0', '2019-07-19 05:00:00', 'AD2019020001'),
('RS2019071910025', 'RSIA Puri Bunda', 'Jl. Simpang Sulfat Utara No.60A, Pandanwangi, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia', '8ccd040eb414c288d8e69a0c912dc11c091aa594aec9212bb52ec0b4fc399967.jpg', '62341480047', 'KSJ10002', '[\'P10001\',\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10013\']', '0', '2019-07-19 05:07:59', 'AD2019020001'),
('RS2019071910026', 'RSIA Mardi Waloeja Rampal', 'Jl. W.R. Supratman No.1, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65111, Indonesia', '6a264c0bbf8e2485c9440d53599e7e31d3204ec49a3d57bb56694b3ce9e2e1f2.jpg', '62341364756', 'KSJ10002', '[\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10013\']', '0', '2019-07-19 05:01:19', 'AD2019020001'),
('RS2019071910027', 'RSIA Mutiara Bunda', 'Jl. Ciujung No.19, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia', 'c5e05da9b7bdeab377fa68e7e3342cdd63b5057493b85559c08df859aacde797.jpg', '62341400403', 'KSJ10002', '[\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10012\',\'P10013\']', '0', '2019-07-19 05:01:53', 'AD2019020001'),
('RS2019071910028', 'RSIA Melati Husada', 'Jalan Kawi No 32 - 34, Gading Kasri, Klojen, Gading Kasri, Kec. Klojen, Kota Malang, Jawa Timur 65115, Indonesia', '178bfd169009dedc7329e9ffdfe0a71835fa511bbc104ffca9fbaf186cc11101.jpg', '62341341357', 'KSJ10002', '[\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10013\']', '0', '2019-07-19 05:02:19', 'AD2019020001'),
('RS2019071910029', 'RSIA Galeri Candra', 'Jl. Andong No.3, Jatimulyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141, Indonesia', '22dbf6f1bf3e302463d7c52a895a34e9d8559aeec00b835fed782057f28f2400.jpg', '62341478571', 'KSJ10002', '[\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10013\']', '0', '2019-07-19 05:02:46', 'AD2019020001'),
('RS2019071910030', 'RSIA Rumkitban Malang', 'Jl. Panglima Sudirman No.E 20, Kesatrian, Kec. Blimbing, Kota Malang, Jawa Timur 65111, Indonesia', 'becf82f475d1f403d3bac46c8b6c445f83ab78c759444372dfeb995af8dc50e2.jpg', '62341354396', 'KSJ10002', '[\'P10006\',\'P10007\',\'P10008\',\'P10009\',\'P10010\',\'P10011\',\'P10013\']', '0', '2019-07-19 05:03:22', 'AD2019020001'),
('RS2019071910031', 'Puskesmas Gribig', 'Jl. Raya Ki Ageng Gribig No.97, Madyopuro, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139, Indonesia', '33788eed7f49dcf812db3f3431f5e9df0ae798124c85ffedafd7e79ee2b5cfcd.jpg', '62341718165', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:12:22', 'AD2019020001'),
('RS2019071910032', 'Puskesmas Arjowinangun', 'Jl. Raya Arjowinangun No.2, Arjowinangun, Kec. Kedungkandang, Kota Malang, Jawa Timur 65132, Indonesia', 'a3408ae1bb61456be07ae0877fffd33b96bff8f2995035f5c892a7ceb5a4d748.jpg', '62341754909', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:12:47', 'AD2019020001'),
('RS2019071910033', 'Puskesmas Janti', 'Jalan Janti, Sukun, Kec. Sukun, Kota Malang, Jawa Timur 65148, Indonesia', '183752147c9e86fee4b8cfcd0183aacd0ad23523fa920c166bf39f6efc02f66a.jpg', '62341352203', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10007\',\'P10008\',\'P10009\']', '0', '2019-07-19 05:13:15', 'AD2019020001'),
('RS2019071910034', 'Puskesmas Ciptomulyo', 'Jalan Kolonel Sugiono 8 No.54, Ciptomulyo, Kec. Sukun, Kota Malang, Jawa Timur 65148, Indonesia', '05e8a4bbb0e9c0c7ef60655be06e8a954ac2489de9b73e8e37d1d176916b00b5.jpg', '62341329918', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10003\',\'P10005\',\'P10007\',\'P10008\',\'P10009\']', '0', '2019-07-19 05:13:49', 'AD2019020001'),
('RS2019071910035', 'Puskesmas Mojolangu ', 'Jl. Sudimoro No.17 A, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142, Indonesia', '91e2a1cfc3d6bbf5063d6d54f22989c95948168adfb0c7de20a1ae92998c2497.jpg', '62341482905', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\',\'P10007\',\'P10008\',\'P10009\']', '0', '2019-07-19 05:14:15', 'AD2019020001'),
('RS2019071910036', 'Puskesmas Arjuno ', 'Jl. Simpang Arjuno No.17, Kauman, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia', '75e057589c3f380a38a83bdb0fd7071a0e0af46f30aed6ccafbdeb30997470b4.jpg', '62341356339', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:14:57', 'AD2019020001'),
('RS2019071910037', 'Puskesmas Bareng ', 'Jl. Bareng Tenes 4A No.639, Bareng, Kec. Klojen, Kota Malang, Jawa Timur 65116, Indonesia', 'e3a8eb5b60c4d75ede415c0248090302c0b12babf66227fa8c1a2f07bcf27e81.jpg', '62341322280', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:15:35', 'AD2019020001'),
('RS2019071910038', 'Puskesmas Rampal Celaket ', 'Jalan Simpang Kesembon No. 5, Rampal Celaket, Kec. Klojen, Kota Malang, Jawa Timur 65119, Indonesia', 'b6cfe9cd86d5bb8381ad870d4916a8cbaa385ec8462d2181470db5f8867090a5.jpg', '62341356380', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:17:22', 'AD2019020001'),
('RS2019071910039', 'Puskesmas Kendalkerep', 'Jalan Raya Sulfat No. 100 Purwantoro, Bunulrejo, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia', '96e2f6cc2db53c7bf08c7ec420c2785f011a5c5d98b9d2e84b04daa76638a600.jpg', '62341484477', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:17:48', 'AD2019020001'),
('RS2019071910040', 'Puskesmas Cisadea ', 'Jl. Cisadea No.19, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65122, Indonesia', '990bab3cbc23bc4a11446812fadaa09542a3a93087ce50f3e894ce2e4350481b.jpg', '62341489540', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:18:15', 'AD2019020001'),
('RS2019071910041', 'Puskesmas Pandanwangi', 'Jl. Laksda Adi Sucipto No.315, Pandanwangi, Kec. Blimbing, Kota Malang, Jawa Timur 65126, Indonesia', '77103acae63ea0459e0a381129b8bc6ff644306b5443b5cce9626a95df6129cc.jpg', '62341484472', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:18:43', 'AD2019020001'),
('RS2019071910042', 'Puskesmas Kedungkandang ', 'Jl. Raya Ki Ageng Gribig No.142, Kedungkandang, Kec. Kedungkandang, Kota Malang, Jawa Timur 65138, Indonesia', 'e297da20980e809e0044b19431e072b6de535af7602fa10ed63a7428e4b2d7dc.jpg', '62341710112', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:19:11', 'AD2019020001'),
('RS2019071910043', 'Puskesmas Dinoyo ', 'Jalan Mayjend M.T. Haryono, Dinoyo, Kecamatan Lowokwaru, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145, Indonesia', 'abe634cfc47525063daf5ed5adccae3019b014683de30ce664b5698600ae5d2c.jpg', '62341572640', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:19:33', 'AD2019020001'),
('RS2019071910044', 'Puskesmas Kendalsari ', 'Jl. Cengger Ayam I No.8, RW.02, Tulusrejo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141, Indonesia', '6afe379915797d353d3d3fb3714130d568ef50db3fbfcc7f6a1a0e29ca11b5e1.jpg', '62341478215', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:20:01', 'AD2019020001'),
('RS2019071910045', 'Puskesmas Mulyorejo ', 'Jl. Raya Mulyorejo No.11A, Mulyorejo, Kec. Sukun, Kota Malang, Jawa Timur 65147, Indonesia', '4efed117321622a13721d49b2e3379e561cb3b95dcfafd9f9fde81344306d4d4.jpg', '62341580955', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10003\',\'P10004\',\'P10005\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:20:29', 'AD2019020001'),
('RS2019071910046', 'Puskesmas Polowijen', 'Polowijen, Blimbing, Malang City, East Java 65126, Indonesia', 'eb76e5b259e989af1c1b6296e89618e3b327e4c99f84618288087db5f20cea52.jpg', '62341491320', 'KSJ10006', '[\'P10001\',\'P10002\',\'P10004\',\'P10005\',\'P10007\',\'P10008\',\'P10009\',\'P10013\']', '0', '2019-07-19 05:20:51', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `master_kecamatan`
--

CREATE TABLE `master_kecamatan` (
  `id_kecamatan` varchar(7) NOT NULL,
  `nama_kecamatan` varchar(64) NOT NULL,
  `area` text NOT NULL,
  `luas` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kecamatan`
--

INSERT INTO `master_kecamatan` (`id_kecamatan`, `nama_kecamatan`, `area`, `luas`, `is_delete`, `waktu`, `id_admin`) VALUES
('KEC1001', 'Kedungkandang', 'null', 39.87, '0', '0000-00-00 00:00:00', ''),
('KEC1002', 'Sukun', 'null', 20.97, '0', '0000-00-00 00:00:00', ''),
('KEC1003', 'Klojen', 'null', 8.83, '0', '0000-00-00 00:00:00', ''),
('KEC1004', 'Blimbing', 'null', 17.77, '0', '0000-00-00 00:00:00', ''),
('KEC1005', 'Lowokwaru', 'null', 22.6, '0', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `master_kelurahan`
--

CREATE TABLE `master_kelurahan` (
  `id_kelurahan` varchar(7) NOT NULL,
  `id_kecamatan` varchar(7) NOT NULL,
  `nama_kelurahan` varchar(64) NOT NULL,
  `area` text NOT NULL,
  `luas_kel` double NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_kelurahan`
--

INSERT INTO `master_kelurahan` (`id_kelurahan`, `id_kecamatan`, `nama_kelurahan`, `area`, `luas_kel`, `is_delete`, `waktu`, `id_admin`) VALUES
('KEL1001', 'KEC1004', 'Arjosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1002', 'KEC1004', 'Balearjosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1003', 'KEC1004', 'Blimbing', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1004', 'KEC1001', 'Bunulrejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1005', 'KEC1004', 'Jodipan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1006', 'KEC1004', 'Kesatrian', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1007', 'KEC1004', 'Pandanwangi', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1008', 'KEC1004', 'Polehan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1009', 'KEC1004', 'Polowijen', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1010', 'KEC1004', 'Purwantoro', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1011', 'KEC1004', 'Purwodadi', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1012', 'KEC1001', 'Arjowinangung', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1013', 'KEC1001', 'Bumiayu', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1014', 'KEC1001', 'Buring', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1015', 'KEC1001', 'Cemorokandang', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1016', 'KEC1001', 'Kedungkandang', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1017', 'KEC1001', 'Kotalama', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1018', 'KEC1001', 'Lesanpuro', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1019', 'KEC1001', 'Madyopuro', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1020', 'KEC1001', 'Mergosono', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1021', 'KEC1001', 'Sawojajar', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1022', 'KEC1001', 'Tlogowaru', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1023', 'KEC1001', 'Wonokoyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1024', 'KEC1003', 'Bareng', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1025', 'KEC1003', 'Gadingsari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1026', 'KEC1003', 'Kasin', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1027', 'KEC1003', 'Kauman', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1028', 'KEC1003', 'Kiduldalem', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1029', 'KEC1003', 'Klojen', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1030', 'KEC1003', 'Penanggungan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1031', 'KEC1003', 'Rampal Celaket', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1032', 'KEC1003', 'Samaan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1033', 'KEC1003', 'Sukoharjo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1034', 'KEC1005', 'Dinoyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1035', 'KEC1005', 'Jatimulyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1036', 'KEC1005', 'Ketawanggede', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1037', 'KEC1005', 'Lowokwaru', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1038', 'KEC1005', 'Merjosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1039', 'KEC1005', 'Mojolangu', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1040', 'KEC1005', 'Sumbersari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1041', 'KEC1005', 'Tasikmadu', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1042', 'KEC1005', 'Tlogomas', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1043', 'KEC1005', 'Tulusrejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1044', 'KEC1005', 'Tunggulwulung', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1045', 'KEC1005', 'Tunjungsekar', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1046', 'KEC1002', 'Bukalankrajan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1047', 'KEC1002', 'Bandulan', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1048', 'KEC1002', 'Bandungrejosari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1049', 'KEC1002', 'Ciptomulyo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1050', 'KEC1002', 'Gadang', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1051', 'KEC1002', 'Karangbesuki', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1052', 'KEC1002', 'Kebonsari', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1053', 'KEC1002', 'Mulyorejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1054', 'KEC1002', 'Pisangcandi', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1055', 'KEC1002', 'Sukun', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1056', 'KEC1002', 'Tanjungrejo', '0', 0, '0', '0000-00-00 00:00:00', ''),
('KEL1057', 'KEC1003', 'Oro Oro Dowo', '0', 0, '0', '0000-00-00 00:00:00', '0');

-- --------------------------------------------------------

--
-- Table structure for table `pariwisata_jenis`
--

CREATE TABLE `pariwisata_jenis` (
  `id_jenis` varchar(8) NOT NULL,
  `ket_jenis` text NOT NULL,
  `img` text NOT NULL,
  `img_marker` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pariwisata_main`
--

CREATE TABLE `pariwisata_main` (
  `id_pariwisata` varchar(16) NOT NULL,
  `detail` text NOT NULL,
  `foto` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pariwisata_main`
--

INSERT INTO `pariwisata_main` (`id_pariwisata`, `detail`, `foto`, `is_delete`, `waktu`, `id_admin`) VALUES
('PRW2019030100001', '{\"alamat\": {\"loc1\":{\"alamat\":\"jl simpang balapan\", \"loc\":[\"0.989897\", \"0.989897\"]}, \"loc2\":{\"alamat\":\"jl simpang balapan\", \"loc\":[\"0.989897\", \"0.989897\"]}},\"ket\":\"deskripsi\", \"tlp\":\"08123069xxxx\", \"website\":\"surya.com\", \"email\":\"surya@gmail.com\"}', '[\"jpi_1.jpg\",\"jpi_2.jpg\",\"jpi_3.jpg\"]', '1', '2019-07-16 00:00:00', 'AD2019020001');

-- --------------------------------------------------------

--
-- Table structure for table `pariwisata_sub_jenis`
--

CREATE TABLE `pariwisata_sub_jenis` (
  `id_sub` varchar(8) NOT NULL,
  `id_jenis` varchar(8) NOT NULL,
  `ket_sub_jenis` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `time_update` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_jenis`
--

CREATE TABLE `pendidikan_jenis` (
  `id_jenis` varchar(10) NOT NULL,
  `id_strata` varchar(8) NOT NULL,
  `nama_jenis` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_jenis`
--

INSERT INTO `pendidikan_jenis` (`id_jenis`, `id_strata`, `nama_jenis`, `is_delete`, `waktu`, `id_admin`) VALUES
('PDJ0110001', 'PDST1002', 'SD Negeri', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDJ0110002', 'PDST1002', 'SD Swasta', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDJ0310001', 'PDST1003', 'SMP Negeri', '0', '2019-05-09 08:27:00', 'AD2019020001'),
('PDJ0810001', 'PDST1008', 'SMA Negeri', '0', '2019-05-10 09:34:46', ''),
('PDJ0910001', 'PDST1009', 'Pondok Pesantren', '0', '2019-06-18 04:00:21', 'AD2019020001'),
('PDJ1110001', 'PDST1011', 'Universitas Negeri', '0', '2019-06-19 05:57:29', 'AD2019040001'),
('PDJ1110002', 'PDST1011', 'Universitas Swasta', '0', '2019-06-19 06:14:34', 'AD2019040001'),
('PDJ1210001', 'PDST1012', 'Perpustakaan', '0', '2019-06-19 08:06:58', 'AD2019040001');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_sekolah`
--

CREATE TABLE `pendidikan_sekolah` (
  `id_sekolah` varchar(17) NOT NULL,
  `id_jenis` varchar(10) NOT NULL,
  `id_kecamatan` varchar(7) NOT NULL,
  `id_kelurahan` varchar(7) NOT NULL,
  `nama_sekolah` text NOT NULL,
  `foto_sklh` text NOT NULL,
  `lokasi` varchar(32) NOT NULL,
  `detail_sekolah` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_sekolah`
--

INSERT INTO `pendidikan_sekolah` (`id_sekolah`, `id_jenis`, `id_kecamatan`, `id_kelurahan`, `nama_sekolah`, `foto_sklh`, `lokasi`, `detail_sekolah`, `is_delete`, `waktu`, `id_admin`) VALUES
('SKL20190429100001', 'PDJ0110001', 'KEC1003', 'KEL1029', 'SDN KLOJEN', '717a788ff7d293fca75109012e772e77dadf6cda5da9da18a7db13a33a1f78bb.jpg', '[\'-7.970732\', \'112.632759\']', '{\'alamat\': \'Jl. Patimura No.1, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'http://sdnklojen.blogspot.com/\',\'tlp\': \'0341350806\'}', '0', '2019-05-13 06:27:04', 'AD2019020001'),
('SKL20190429100002', 'PDJ0110001', 'KEC1003', 'KEL1028', 'SDN Kiduldalem 1', '1fb0246f490a904ff647619ba9fa8daf05379189fc672926bb36c17e116bb90c.jpg', '[\'-7.978529\', \'112.633037\']', '{\'alamat\': \'malang\',\'url\': \'https://www.php.net/manual/en/language.exceptions.php\',\'tlp\': \'0341123123\'}', '0', '2019-05-09 07:15:10', 'AD2019020001'),
('SKL20190429100003', 'PDJ0110001', 'KEC1003', '0', 'SDN Kiduldalem 2', '0', '[\'-7.979886\', \'112.635053\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100004', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 1', '0', '[\'-7.984757\', \'112.630161\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100005', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 2', '0', '[\'-7.978359\', \'112.624812\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100006', 'PDJ0110001', 'KEC1003', '0', 'SDN Kauman 3', '0', '[\'-7.983679\', \'112.628547\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100007', 'PDJ0110001', 'KEC1003', '0', 'SDN Kasin', '0', '[\'-7.985550\', \'112.625451\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100008', 'PDJ0110001', 'KEC1003', '0', 'SDN Sukoharjo 1', '0', '[\'-7.990147\', \'112.633196\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100009', 'PDJ0110001', 'KEC1003', '0', 'SDN Sukoharjo 2', '0', '[\'-7.989915\', \'112.632845\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100010', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 1', '0', '[\'-7.980727\', \'112.625345\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100011', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 2', '0', '[\'-7.979924\', \'112.621778\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190429100012', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 3', '0', '[\'-7.975719\', \'112.618941\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100001', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 4', '0', '[\'-7.976788\', \'112.617087\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100002', 'PDJ0110001', 'KEC1003', '0', 'SDN Bareng 5', '0', '[\'-7.977995\', \'112.622715\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100003', 'PDJ0110001', 'KEC1004', '0', 'SDN Balearjosari 1', '0', '[\'-7.923529\', \'112.651103\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100004', 'PDJ0110001', 'KEC1004', '0', 'SDN Balearjosari 2', '0', '[\'-7.926450\', \'112.657830\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100005', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 1', '0', '[\'-7.929054\', \'112.648380\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100006', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 2', '0', '[\'-7.929044\', \'112.642837\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100007', 'PDJ0110001', 'KEC1004', '0', 'SDN Polowijen 3', '0', '[\'-7.928972\', \'112.645164\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100008', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 1', '0', '[\'-7.922440\', \'112.651512\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100009', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 2', '0', '[\'-7.930654\', \'112.658624\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100010', 'PDJ0110001', 'KEC1004', '0', 'SDN Arjosari 3', '0', '[\'-7.927625\', \'112.654032\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100011', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 1', '0', '[\'-7.932690\', \'112.646803\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100012', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 2', '0', '[\'-7.9382737\', \' 112.6464189\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100013', 'PDJ0110001', 'KEC1004', '0', 'SDN Purwodadi 3', '0', '[\'-7.9384437 \', \'112.6469653\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100014', 'PDJ0110001', 'KEC1001', '0', 'SDN Kedungkandang 1', '0', '[\'-7.989953 \', \'112.650530\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100015', 'PDJ0110001', 'KEC1001', '0', 'SDN Kedungkandang 2', '0', '[\'-7.99115 \', \'112.647413\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100016', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 1', '0', '[\'-7.984983 \', \'112.657145\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100017', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 2', '0', '[\'-7.991327 \', \'112.664996\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100018', 'PDJ0110001', 'KEC1001', '0', 'SDN Lesanpuro 3', '0', '[\'-7.981281 \', \'112.660270\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100019', 'PDJ0110001', 'KEC1001', '0', 'SDN Buring', '0', '[\'-8.005362 \', \'112.644048\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100020', 'PDJ0110001', 'KEC1001', '0', 'SDN Wonokoyo 1', '0', '[\'-8.021992 \', \'112.648034\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100021', 'PDJ0110001', 'KEC1001', '0', 'SDN Wonokoyo 2', '0', '[\'-8.023499 \', \'112.669706\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100022', 'PDJ0110001', 'KEC1001', '0', 'SDN Tlogowaru 1', '0', '[\'-8.039433 \', \'112.650648\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100023', 'PDJ0110001', 'KEC1001', '0', 'SDN Tlogowaru 2', '0', '[\'-8.037279 \', \'112.666295\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100024', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 1', '0', '[\'-7.956957 \', \'112.635960\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100025', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 2', '0', '[\'-7.962162 \', \'112.632506\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100026', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 3', '0', '[\'-7.961433 \', \'112.634560\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100027', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 4', '0', '[\'-7.952290 \', \'112.632080\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100028', 'PDJ0110001', 'KEC1005', '0', 'SDN Lowokwaru 5', '0', '[\'-7.956750 \', \'112.625608\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100029', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 1', '0', '[\'-7.951383 \', \'112.633635\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100030', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 2', '0', '[\'-7.944885 \', \' 112.626397\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100031', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 3', '0', '[\'-7.946951 \', \' 112.635431\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100032', 'PDJ0110001', 'KEC1005', '0', 'SDN Tulusrejo 4', '0', '[\'-7.951106 \', \' 112.634715\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100033', 'PDJ0110001', 'KEC1005', '0', 'SDN Jatimulyo', '0', '[\'-7.942121 \', \' 112.616067\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100034', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 1', '0', '[\'-7.994275 \', \' 112.620484\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100035', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 2', '0', '[\'-7.991602 \', \' 112.616819\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100036', 'PDJ0110001', 'KEC1002', '0', 'SDN Sukun 3', '0', '[\'-7.991235 \', \' 112.620182\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100037', 'PDJ0110001', 'KEC1002', '0', 'SDN Bandungrejosari 1', '0', '[\'-8.008017 \', \' 112.618806\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190430100038', 'PDJ0110001', 'KEC1002', '0', 'SDN Bandungrejosari 2', '0', '[\'-8.001061 \', \' 112.614058\']', 'detail', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('SKL20190509100009', 'PDJ0310001', 'KEC1003', 'KEL1057', 'SMP Negeri 1 Malang', '1aed74b869912c26d20d212568544efb36c2f539b8cb24d03b11bb4c415978fe.jpg', '[\'-7.971186 \', \'112.623810\']', '{\'alamat\': \'Jl. Lawu No.12, Oro-oro Dowo, Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'http://smpn1-mlg.sch.id\',\'tlp\': \'0341325206\'}', '0', '2019-05-10 04:32:58', 'AD2019020001'),
('SKL20190509100010', 'PDJ0310001', 'KEC1003', 'KEL1033', 'SMP Negeri 2 Malang', 'cbe7b86d2a50b37ab2703512f783931bf706c551ee401e3453c503bfdb3eb277.jpg', '[\'-7.990190 \', \'112.630876\']', '{\'alamat\': \'Jl. Prof. Moch Yamin No.60, Sukoharjo, Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'smpn2mlg.sch.id\',\'tlp\': \'0341325508\'}', '0', '2019-05-10 04:32:46', 'AD2019020001'),
('SKL20190509100013', 'PDJ0110001', 'KEC1003', 'KEL1029', 'SMP Negeri 3 Malang', 'a613e2a2cfd9044665bdafb114b66eb594f3b92c239af14c848f02a5c5063e91.jpg', '[\'-7.969050 \', \'112.635547\']', '{\'alamat\': \'Jl. Dr. Cipto No.20, 3, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'smpn3-mlg.sch.id\',\'tlp\': \'0341362612\'}', '0', '2019-05-10 04:32:30', 'AD2019020001'),
('SKL20190509100014', 'PDJ0310001', 'KEC1005', 'KEL1040', 'SMP Negeri 4 Malang', '1f34e7106a0cb27c4fb661f02ca43180ba6c31c1e90eb88fb016bf26e395e43b.jpg', '[\'-7.957819 \', \'112.616046\']', '{\'alamat\': \'Jl. Veteran No.37, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'smpn4malang.wordpress.com\',\'tlp\': \'0341551289\'}', '0', '2019-05-10 04:32:15', 'AD2019020001'),
('SKL20190509100015', 'PDJ0310001', 'KEC1003', 'KEL1031', 'SMP Negeri 5 Malang', '7fba9bfc984026233c4d728aac7923e08306c5ebb5cef0a70eba0cac000bceec.jpg', '[\'-7.966499 \', \'112.638411\']', '{\'alamat\': \'Jalan Wage Rudolf Supratman No.12, RT./ RW:/RW.3, Rampal Celaket, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'smpn5-mlg.sch.id\',\'tlp\': \'0341482713\'}', '0', '2019-05-10 04:31:26', 'AD2019020001'),
('SKL20190510100001', 'PDJ0310001', 'KEC1003', 'KEL1024', 'SMP Negeri 6 Malang', '8ce2ba3bf92bb11baf9f355d2a0252a67b92e36d2c241682100ce2d61b3a4fe3.jpg', '[\'-7.979525 \', \'112.624688\']', '{\'alamat\': \'Jl. Kawi No.15A, Bareng, Klojen, Kota Malang, Jawa Timur 65116\',\'url\': \'smpn6-mlg.sch.id\',\'tlp\': \'0341364710\'}', '0', '2019-05-10 04:32:00', 'AD2019020001'),
('SKL20190510100002', 'PDJ0310001', 'KEC1001', 'KEL1013', 'SMP Negeri 7 Malang', '0251a2d1ac1d1573b6e2dae1895b77d74ff0ae884f95f1c43af2c500863557e6.jpg', '[\'-8.003875 \', \'112.636590\']', '{\'alamat\': \'Jl. Lembayung, Bumiayu, Kedungkandang, Kota Malang, Jawa Timur 65143\',\'url\': \'www.google.com\',\'tlp\': \'0341752032\'}', '0', '2019-05-10 04:31:49', 'AD2019020001'),
('SKL20190510100003', 'PDJ0310001', 'KEC1003', 'KEL1027', 'SMP Negeri 8 Malang', '260f2e9b61f2fed4cca693e17fbe756d25416dd334c69df7d4010603d6197c91.jpg', '[\'-7.977110 \', \'112.627288\']', '{\'alamat\': \'Jl. Arjuno No.19, Kauman, Klojen, Kota Malang, Jawa Timur 65119\',\'url\': \'www.google.com\',\'tlp\': \'0341325506\'}', '0', '2019-05-10 04:31:37', 'AD2019020001'),
('SKL20190510100004', 'PDJ0110001', 'KEC1003', 'KEL1033', 'SMP Negeri 9 Malang', '7750bd66c77c9be9b89891e0b448db9cc54a1b38e9269509af59077580c0e8f5.jpg', '[\'-7.989593 \', \'112.630854\']', '{\'alamat\': \'Jl. Prof. Moch Yamin Gg. 6 No.26, Sukoharjo, Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'smpn9mlg.sch.id\',\'tlp\': \'0341364842\'}', '0', '2019-05-10 04:31:09', 'AD2019020001'),
('SKL20190510100005', 'PDJ0310001', 'KEC1001', 'KEL1014', 'SMP Negeri 10 Malang', '2912531587133db9ea11d3437bf92d9f133c9aa29227d2b27e91261905883ead.jpg', '[\'-7.9384437 \', \'112.6469653\']', '{\'alamat\': \'Jl. Mayjen Sungkono No.57, Buring, Kedungkandang, Kota Malang, Jawa Timur 65136\',\'url\': \'www.google.com\',\'tlp\': \'0341752035\'}', '0', '2019-05-10 04:35:11', 'AD2019020001'),
('SKL20190510100006', 'PDJ0310001', 'KEC1005', 'KEL1045', 'SMP Negeri 11 Malang', 'df4df32633bb692dfb0d4825a4b576d24da6c65c5a51a6efc91351d9c645f746.jpg', '[\'-7.932676\', \'112.637659\']', '{\'alamat\': \'Jl. Ikan Piranha Atas No.185, Tunjungsekar, Kec. Lowokwaru, Kota Malang, Jawa Timur 65122\',\'url\': \'smpn11-mlg.sch.id\',\'tlp\': \'0341494086\'}', '0', '2019-05-10 04:50:04', 'AD2019020001'),
('SKL20190510100007', 'PDJ0310001', 'KEC1002', 'KEL1048', 'SMP Negeri 12 Malang', '8f1526b78f32f50af83971a5fb3dcf0cb40f03c3636402514ecec013ed747c17.jpg', '[\'-8.006130 \', \'112.620903\']', '{\'alamat\': \'Jl. Slamet Supriadi No.49, Bandungrejosari, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341801186\'}', '0', '2019-05-10 04:49:32', 'AD2019020001'),
('SKL20190510100008', 'PDJ0310001', 'KEC1005', 'KEL1034', 'SMP Negeri 13 Malang', 'ff84b5e660abde24c5c8615be08b6ff53deba6cf252a3373b0ce925502fb5cdc.jpg', '[\'-7.948816 \', \'1112.607352\']', '{\'alamat\': \'Jl. Sunan Ampel 2, RT.9/RW.2, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65149\',\'url\': \'smpngalasmalang.sch.id\',\'tlp\': \'0341552864\'}', '0', '2019-05-10 04:51:23', 'AD2019020001'),
('SKL20190510100009', 'PDJ0310001', 'KEC1004', 'KEL1007', 'SMP Negeri 14 Malang', '52c7755e78735f0392b0d9839d426854c6fc2e0568f8b8de45f85163d2fcbecc.jpg', '[\'-7.943489 \', \'112.662495\']', '{\'alamat\': \'Jl. Tlk. Bayur No.2, Pandanwangi, Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'smpn14-mlg.sch.id\',\'tlp\': \'0341474458\'}', '0', '2019-05-10 04:52:54', 'AD2019020001'),
('SKL20190510100010', 'PDJ0310001', 'KEC1002', 'KEL1054', 'SMP Negeri 15 Malang', '121ebdc6db0a98182dfa451f8cdc32bc807ad9f7e404436d34d00acb2a7dfb46.jpg', '[\'-7.976119 \', \'112.604433\']', '{\'alamat\': \'Jl. Bukit Dieng Permai No.8, Pisang Candi, Sukun, Kota Malang, Jawa Timur 65146\',\'url\': \'smp15mlg.blogspot.co.id\',\'tlp\': \'0341571715\'}', '0', '2019-05-10 05:07:27', 'AD2019020001'),
('SKL20190510100011', 'PDJ0310001', 'KEC1004', 'KEL1001', 'SMP Negeri 16 Malang', '277b952bd47721f411cdb094e1aeb5401829f44143c74af3253e9aa52df1ff7e.jpg', '[\'-7.934457 \', \'112.662148\']', '{\'alamat\': \'Jl. Teluk Pacitan No.46, Arjosari, Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'0341490441\'}', '0', '2019-05-10 05:32:24', 'AD2019020001'),
('SKL20190510100012', 'PDJ0110001', 'KEC1002', 'KEL1046', 'SMP Negeri 17 Malang', 'f1382755d59f33d6a6ffb2c0d6b077dfe7a4b7db5195db13abfade2d964dcbae.jpg', '[\'-8.006499 \', \'112.603642\']', '{\'alamat\': \'Jl. Pelabuhan Tanjung Priok No.170, Bakalankrajan, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-05-10 05:33:54', 'AD2019020001'),
('SKL20190510100013', 'PDJ0310001', 'KEC1005', 'KEL1039', 'SMP Negeri 18 Malang', '8609fdcb5a14379e035b0838189dfda1d594563c8b4de1718d9e413e192ed9cf.jpg', '[\'-7.939397 \', \'112.619543\']', '{\'alamat\': \'Jalan Soekarno Hatta No. A-394, Lowokwaru, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141\',\'url\': \'smpn18malang.sch.id\',\'tlp\': \'0341472418\'}', '0', '2019-05-10 05:39:53', 'AD2019020001'),
('SKL20190510100014', 'PDJ0310001', 'KEC1003', 'KEL1026', 'SMP Negeri 19 Malang', '', '[\'-7.993887\', \'112.625095\']', '{\'alamat\': \'Jl. Belitung No.1, Kasin, Klojen, Kota Malang, Jawa Timur 65117\',\'url\': \'beta.smpn20-malang.sch.id\',\'tlp\': \'0341324960\'}', '0', '2019-05-10 05:41:26', 'AD2019020001'),
('SKL20190510100015', 'PDJ0310001', 'KEC1004', 'KEL1003', 'SMP Negeri 20 Malang', '7a04681095008a825f81a8cd569b8cefd6bbe52b787a6407703b2d5460043f69.jpg', '[\'-7.963623\', \'112.640839\']', '{\'alamat\': \'Jl. R. Tumenggung Suryo No.38, Bunulrejo, Blimbing, Kota Malang, Jawa Timur 65123\',\'url\': \'beta.smpn20-malang.sch.id\',\'tlp\': \'0341491806\'}', '0', '2019-06-18 06:00:38', 'AD2019020001'),
('SKL20190510100016', 'PDJ0110001', 'KEC1001', 'KEL1018', 'SMP Negeri 21 Malang', 'c251515fd2e99e83af9c022beb3bb382b59a6765592f306e8e4959332b9e4b06.jpg', '[\'-7.974113 \', \' 112.664685\']', '{\'alamat\': \'Jl. Danau Tigi, Lesanpuro, Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'smpn21-mlg.sch.id\',\'tlp\': \'0341718066\'}', '0', '2019-05-10 05:50:52', 'AD2019020001'),
('SKL20190510100017', 'PDJ0310001', 'KEC1001', 'KEL1014', 'SMP Negeri 22 Malang', '6c7566f571d3bc09076222f925690a5200c46a929db9851b0fc02a879ad50985.jpg', '[\'-7.999339 \', \'112.680257\']', '{\'alamat\': \'Jl. Raya Tlogowaru No.23, Tlogowaru, Kedungkandang, Kota Malang, Jawa Timur 65133\',\'url\': \'smpn23-malang.sch.id\',\'tlp\': \'0341754085\'}', '0', '2019-05-10 05:53:52', 'AD2019020001'),
('SKL20190510100018', 'PDJ0310001', 'KEC1001', 'KEL1022', 'SMP Negeri 23 Malang', '2ec03fd83aa042a516f058f17e67f14c5e0e0634a15bdbab49c32b8da2b624ac.jpg', '[\'-8.034877\', \'112.647305\']', '{\'alamat\': \'Jl. Raya Tlogowaru No.23, Tlogowaru, Kedungkandang, Kota Malang, Jawa Timur 65133\',\'url\': \'smpn23-malang.sch.id\',\'tlp\': \'0341754085\'}', '0', '2019-05-10 05:59:53', 'AD2019020001'),
('SKL20190510100019', 'PDJ0110001', 'KEC1004', 'KEL1007', 'SMP Negeri 24 Malang', '2785fa8544a0f9b9685d99e07dbaf88a1f5cadaa8f6e5522a46b2cb3fba55af2.jpg', '[\'-7.953842 \', \'112.661222\']', '{\'alamat\': \'Gg. Makam, Pandanwangi, Blimbing, Kota Malang, Jawa Timur 65124\',\'url\': \'smpn24mlg.wordpress.com\',\'tlp\': \'0341415415\'}', '0', '2019-05-10 06:01:00', 'AD2019020001'),
('SKL20190510100020', 'PDJ0810001', 'KEC1003', 'KEL1028', 'SMA Negeri 1 Malang', '182b84337b93ff48a0c9aea1e99c6d87dfa58651484d06e29fb3be619524b238.jpg', '[\'-7.976751 \', \'112.634870\']', '{\'alamat\': \'Jl. Tugu Utara No.1, Kiduldalem, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'sman1-mlg.sch.id\',\'tlp\': \'0341366454\'}', '0', '2019-05-10 09:38:18', ''),
('SKL20190510100021', 'PDJ0810001', 'KEC1003', 'KEL1033', 'SMA Negeri 2 Malang', '891c13b50f1c664913b0627b36f7a4cb846b2533606707b881c470a0962c5c9d.jpg', '[\'-7.991443 \', \'112.633712\']', '{\'alamat\': \'Jl. Laksamana Martadinata No.84, Sukoharjo, Klojen, Kota Malang, Jawa Timur 65118\',\'url\': \'sman2-malang.sch.id\',\'tlp\': \'0341366311\'}', '0', '2019-05-10 09:46:26', ''),
('SKL20190510100022', 'PDJ0110001', 'KEC1003', 'KEL1029', 'SMA Negeri 3 Malang', 'd3066568b6db13d83c3ed2bceb741e58f6668f2df8c331b9efa8babd41d0140b.jpg', '[\'-7.975866 \', \'112.635134\']', '{\'alamat\': \'Jl. Sultan Agung No.7, Klojen, Kota Malang, Jawa Timur 65144\',\'url\': \'sman3-malang.sch.id\',\'tlp\': \'0341324768\'}', '0', '2019-05-10 09:57:58', ''),
('SKL20190510100023', 'PDJ0810001', 'KEC1003', 'KEL1029', 'SMA Negeri 4 Malang', 'cf0e648fee985e72f7bed86b4f5906c0f67b17468bda37dfa1ff3d8f2ebaef8d.jpg', '[\'-7.976350 \', \'112.634529\']', '{\'alamat\': \'Jl. Tugu No.1, Klojen, Kota Malang, Jawa Timur 65111\',\'url\': \'sman4malang.sch.id\',\'tlp\': \'0341325267\'}', '0', '2019-05-10 09:59:27', ''),
('SKL20190510100024', 'PDJ0810001', 'KEC1001', 'KEL1016', 'SMA Negeri 6 Malang', 'd524231b6e12141cbd7384113769e20bffae6649c4982acb2778913a79ae2867.jpg', '[\'-8.010166 \', \'112.643391\']', '{\'alamat\': \'Jl. Mayjen Sungkono No.58, Buring, Kedungkandang, Kota Malang, Jawa Timur 65136\',\'url\': \'Sman6malang.sch.id\',\'tlp\': \'0341752036\'}', '0', '2019-05-13 06:14:25', 'AD2019020001'),
('SKL20190513100001', 'PDJ0810001', 'KEC1003', 'KEL1026', 'SMA Negeri 5 Malang', '7e046a5de048a761e9c9742b199b3559407082e3511c97dab17594e2b87aaea6.jpg', '[\'-7.989170 \', \'112.626277\']', '{\'alamat\': \'Jl. Tanimbar No.24, Kasin, Klojen, Kota Malang, Jawa Timur 65117\',\'url\': \'sman5malang.sch.id\',\'tlp\': \'0341364580\'}', '0', '2019-05-13 05:57:01', 'AD2019020001'),
('SKL20190513100002', 'PDJ0810001', 'KEC1005', 'KEL1043', 'SMA Negeri 7 Malang', 'b95b6a527e25fe4bb4fcdd137cb20e342156816e6da9b5a863b40a0a3a06a05c.jpg', '[\'-7.945189 \', \'112.628170\']', '{\'alamat\': \'Jl. Cengger Ayam I No.14, Tulusrejo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65141\',\'url\': \'sman7malang.sch.id\',\'tlp\': \'0341495256\'}', '0', '2019-05-13 06:02:26', 'AD2019020001'),
('SKL20190513100003', 'PDJ0810001', 'KEC1005', 'KEL1040', 'SMA Negeri 8 Malang', 'a99b5bb449a62cce9881df07a80bb5aaa7fd1dba3d1b9b88a351ea662584336f.jpg', '[\'-7.956855 \', \'112.616811\']', '{\'alamat\': \'Jl. Veteran No.37, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'sman8malang.sch.id\',\'tlp\': \'0341551096\'}', '0', '2019-05-13 06:15:52', 'AD2019020001'),
('SKL20190513100004', 'PDJ0810001', 'KEC1005', 'KEL1039', 'SMA Negeri 9 Malang', '2f1d6103bbd5805cb67a09adb14694b461192d65ba92bfa70c9ced036287f81b.jpg', '[\'-7.936086 \', \'112.625215\']', '{\'alamat\': \'Jl. Puncak Borobudur No.1, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'sman9-mlg.sch.id\',\'tlp\': \'0341471855\'}', '0', '2019-05-13 06:17:32', 'AD2019020001'),
('SKL20190513100005', 'PDJ0810001', 'KEC1001', 'KEL1021', 'SMA Negeri 10 Malang', '3ccaa7e6a2f9db5c213152466ebbc65da824889a5b5f741d6e0a800a3834f456.jpg', '[\'-7.974468 \', \'112.660143\']', '{\'alamat\': \'Jl. Danau Grati No.1, Sawojajar, Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'sman10malang.sch.id\',\'tlp\': \'0341719300\'}', '0', '2019-05-13 06:18:56', 'AD2019020001'),
('SKL20190513100006', 'PDJ0810001', 'KEC1002', 'KEL1046', 'SMA Negeri 11 Malang', '4b9903a1f9b67453a9dc1981f7c2e685774217d2285e801ca44a10affddff53c.jpg', '[\'-8.006901 \', \'112.610152\']', '{\'alamat\': \'Jl. Pelabuhan Bakahuni No.1, Bakalankrajan, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341\'}', '0', '2019-05-13 08:35:53', '0'),
('SKL20190513100007', 'PDJ0810001', 'KEC1002', 'KEL1048', 'SMA Negeri 12 Malang', 'f5ac506296913f5f26d6a536fc8b46e2a071c927c3b512225218b2549c38b09c.jpg', '[\'-8.006147 \', \'1112.620906\']', '{\'alamat\': \'Jl. Slamet Supriadi No.49, Bandungrejosari, Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'www.google.com\',\'tlp\': \'0341801186\'}', '0', '2019-05-13 08:36:34', '0'),
('SKL20190618100001', 'PDJ0910001', 'KEC1001', 'KEL1015', 'Pondok Pesantren Cilik Al Ikhlash', '829cb9df5a7f80bd58e9b8059ad2fc2c676f06628df909064b654b4548523411.jpg', '[\'-7.977797\',\'112.680106\']', '{\'alamat\': \'Jl. Raya Cemorokandang No.41, Cemorokandang, Kec. Kedungkandang, Kota Malang, Jawa Timur 65138\',\'url\': \'www.google.com\',\'tlp\': \'0341722313\'}', '0', '2019-06-18 08:08:35', ''),
('SKL20190618100002', 'PDJ0910001', 'KEC1001', 'KEL1021', 'Pondok Pesantren Baitul Makmur', 'aa737a5dc54707bd7000a92156a9434ffb1f210acd60b986d1e8a56113cb53b0.jpg', '[\'-7.969062\',\'112.656741\']', '{\'alamat\': \'Jl. Sawojajar Gg. 17B No.58, Kel, Sawojajar, Kec. Kedungkandang, Kota Malang, Jawa Timur 65139\',\'url\': \'www.google.com\',\'tlp\': \'081234687909\'}', '0', '2019-06-18 08:10:32', ''),
('SKL20190618100003', 'PDJ0910001', 'KEC1001', 'KEL1023', 'Pondok Pesantren Bustanul Ulum Sekar putih ', '28b97164cf9038cc3d4f6d5338a8ac2b5a936054574489e1ae18b92a069eb575.jpg', '[\'-8.032559\',\'112.652245\']', '{\'alamat\': \'JL. Sekar Putih, RT 6/RW 3, Wonokoyo, Kedungkandang, Wonokoyo, Kec. Kedungkandang, Kota Malang, Jawa Timur 65135\',\'url\': \'www.google.com\',\'tlp\': \'0341751029\'}', '0', '2019-06-18 09:06:34', ''),
('SKL20190619100001', 'PDJ1110001', 'KEC1005', 'KEL1036', 'UNIVERSITAS BRAWIJAYA (UB)', '2282e099294e4a3da1798823b06b46731e99af07cbaa5d09eafeaaf4b07ea876.jpg', '[\'-7.952475\',\'112.613866\']', '{\'alamat\': \'Jl. Veteran Malang, Ketawanggede, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'ub.ac.id\',\'tlp\': \'0341551611\'}', '0', '2019-06-19 06:07:11', 'AD2019040001'),
('SKL20190619100002', 'PDJ1110001', 'KEC1005', 'KEL1034', 'UNIVERSITAS ISLAM NEGERI (UIN) MAULANA MALIK IBRAHIM MALANG', 'df4e85d9994b8c3cb4ba0a21958354eb3d189bb4bd6615eb2c4905c2e00e25ef.jpg', '[\'-7.951763\',\'112.607490\']', '{\'alamat\': \'Jl. Gajayana No.50, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'uin-malang.ac.id\',\'tlp\': \'0341551354\'}', '0', '2019-06-19 06:08:53', 'AD2019040001'),
('SKL20190619100003', 'PDJ1110001', 'KEC1005', 'KEL1040', 'UNIVERSITAS NEGERI MALANG (UM)', 'd23e566e5baf51e98dc53a8af16a22327fe1acb7c517249e2568fa5a9e89e40f.jpg', '[\'-7.961254\',\'112.617436\']', '{\'alamat\': \'Jl. Semarang No.5, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'malang.ut.ac.id\',\'tlp\': \'0341551312\'}', '0', '2019-06-19 06:13:50', 'AD2019040001'),
('SKL20190619100004', 'PDJ1110002', 'KEC1004', 'KEL1010', 'UNIVERSITAS KRISTEN CIPTA WACANA MALANG (UKCW)', '57db270d9386d80c0af0ad21000d22dee2d3a40b5aadf636c1e9f59d7db44410.jpg', '[\'-7.957056\',\'112.649005\']', '{\'alamat\': \'Jl. Emas No.29, Purwantoro, Kec. Blimbing, Kota Malang, Jawa Timur 65126\',\'url\': \'www.google.com\',\'tlp\': \'08123303675\'}', '0', '2019-06-19 06:17:33', 'AD2019040001'),
('SKL20190619100005', 'PDJ0110001', 'KEC1002', 'KEL1054', 'UNIVERSITAS MERDEKA MALANG (UNMER)', '953133ac3c47c86e0d1a60a0b4b2f5cdf74b7d8b7bff4fd4d5851844136fc009.jpg', '[\'-7.972414\',\'112.609572\']', '{\'alamat\': \'Jalan Terusan Dieng No. 62-64 Klojen, Pisang Candi, Kec. Sukun, Kota Malang, Jawa Timur 65146\',\'url\': \'unmer.ac.id\',\'tlp\': \'0341568395\'}', '0', '2019-06-19 06:18:32', 'AD2019040001'),
('SKL20190619100006', 'PDJ1110002', 'KEC1005', 'KEL1040', 'UNIVERSITAS MUHAMMADIYAH MALANG', 'cd426c490774c0576513ab3437aaaf8cfc0c8fcb5b7e6bd00965e2a1dc875907.jpg', '[\'-7.962298\',\'112.624534\']', '{\'alamat\': \'Jl. Bendungan Sutami No.188, Sumbersari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65145\',\'url\': \'umm.ac.id\',\'tlp\': \'0341551149\'}', '0', '2019-06-19 06:19:28', 'AD2019040001'),
('SKL20190619100007', 'PDJ1110002', 'KEC1005', 'KEL1038', 'UNIVERSITAS GAJAYANA (UNIGA)', 'fec5ddd1462225f66661949f7179b0e67b3b3d6e12c2eef7d60c3ed527c6b893.jpg', '[\'-7.939957\',\'112.602402\']', '{\'alamat\': \'Jl. Mertojoyo Blk. L, Merjosari, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'unigamalang.ac.id\',\'tlp\': \'0341562411\'}', '0', '2019-06-19 06:21:30', 'AD2019040001'),
('SKL20190619100008', 'PDJ1110002', 'KEC1002', 'KEL1048', 'UNIVERSITAS KANJURUHAN MALANG (UNIKAMA)', 'd96dcd0f80940f248295b5094e538a216dc20482a5ab8435fe39b8eb4648b352.jpg', '[\'-8.006764\',\'112.620127\']', '{\'alamat\': \'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'unikama.ac.id\',\'tlp\': \'0341801488\'}', '0', '2019-06-19 06:22:47', 'AD2019040001'),
('SKL20190619100009', 'PDJ1110002', 'KEC1005', 'KEL1034', 'UNIVERSITAS ISLAM MALANG', '003d56685010e46da069c5933b99daed36a5a752543b0129d440bdc33fb88439.jpg', '[\'-7.936574\',\'112.607298\']', '{\'alamat\': \'Jl. Mayjen Haryono Gg. 10 Kelurahan No.193, Dinoyo, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'unisma.ac.id\',\'tlp\': \'0341551932\'}', '0', '2019-06-19 08:02:33', 'AD2019040001'),
('SKL20190619100010', 'PDJ1110002', 'KEC1002', 'KEL1048', 'UNIVERSITAS KANJURUHAN MALANG (UNIKAMA)', 'c292b6261e84aa7498df343417dd7ce2d47de66c4d977d4c225c04eeacbc2e84.jpg', '[\'-8.006764\',\'112.620127\']', '{\'alamat\': \'Jl. S. Supriadi No.48, Bandungrejosari, Kec. Sukun, Kota Malang, Jawa Timur 65148\',\'url\': \'unikama.ac.id\',\'tlp\': \'0341801488\'}', '0', '2019-06-19 08:03:23', 'AD2019040001'),
('SKL20190619100011', 'PDJ1110002', 'KEC1003', 'KEL1025', 'UNIVERSITAS KATOLIK WIDYA KARYA MALANG', 'b5e131af2cc8cb2b59f4a625ce6fe7b18bf2d2d11bc1ddcf9b1ad6e1c8e63868.jpg', '[\'-7.968370\',\'112.617774\']', '{\'alamat\': \'Jl. Bondowoso 2-Malang No.2, Gading Kasri, Kec. Klojen, Kota Malang, Jawa Timur 65115\',\'url\': \'widyakarya.ac.id\',\'tlp\': \'0341553171\'}', '0', '2019-06-19 08:04:22', 'AD2019040001'),
('SKL20190619100012', 'PDJ1110002', 'KEC1005', 'KEL1042', 'UNIVERSITAS TRIBHUWANA TUNGGADEWI MALANG (UNITRI)', 'b4aa27d5c24fbab726bd46ebe293bfe2233c93f64fadd1c33488766dde5c5951.jpg', '[\'-7.932704\',\'112.600209\']', '{\'alamat\': \'Jl. Telaga Warna, Tlogomas, Kec. Lowokwaru, Kota Malang, Jawa Timur 65144\',\'url\': \'unitri.ac.id\',\'tlp\': \'0341565500\'}', '0', '2019-06-19 08:05:18', 'AD2019040001'),
('SKL20190619100013', 'PDJ1110002', 'KEC1005', 'KEL1039', 'UNIVERSITAS WIDYAGAMA MALANG  (UWG)', '9c47b5d47d141b3bc581eed32df7decae72afad034afdd31e9d53f98e64e342d.jpg', '[\'-7.939404\',\'112.635999\']', '{\'alamat\': \'Kampus II Universitas Widyagama, JL Borobudur, No. 35, Mojolangu, Kec. Lowokwaru, Kota Malang, Jawa Timur 65142\',\'url\': \'widyagama.ac.id\',\'tlp\': \'0341492282\'}', '0', '2019-06-19 08:06:20', 'AD2019040001'),
('SKL20190619100014', 'PDJ1210001', 'KEC1003', 'KEL1057', 'Dinas Perpustakaan Umum dan Arsip Daerah Kota Malang', 'b344328b9fc21cca5af4b1b2f04e4aa5129055130765745c84cd2f59b3bf2e87.jpg', '[\'-7.972199\',\'112.622084\']', '{\'alamat\': \'Jalan Semeru, Oro-oro Dowo, Klojen,Malang, Jl. Besar Ijen No.30a, Gading Kasri, Oro-oro Dowo, Kec. Klojen, Kota Malang, Jawa Timur 65115\',\'url\': \'perpustakaan.malangkota.go.id\',\'tlp\': \'0341362006\'}', '0', '2019-06-19 08:09:11', 'AD2019040001');

-- --------------------------------------------------------

--
-- Table structure for table `pendidikan_strata`
--

CREATE TABLE `pendidikan_strata` (
  `id_strata` varchar(8) NOT NULL,
  `nama_strata` text NOT NULL,
  `icon_32` text NOT NULL,
  `icon_64` text NOT NULL,
  `is_delete` enum('0','1') NOT NULL,
  `waktu` datetime NOT NULL,
  `id_admin` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendidikan_strata`
--

INSERT INTO `pendidikan_strata` (`id_strata`, `nama_strata`, `icon_32`, `icon_64`, `is_delete`, `waktu`, `id_admin`) VALUES
('PDST1001', 'Taman Kanak-Kanak (TK)', 'tk_32.png', 'tk_64.png', '0', '2019-07-01 03:11:38', 'AD2019020001'),
('PDST1002', 'Sekolah Dasar (SD)', 'sd_32.png', 'sd_64.png', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1003', 'Sekolah Menengah Pertama (SMP)', 'smp_32.png', 'smp_64.png', '0', '0000-00-00 00:00:00', 'AD2019020001'),
('PDST1008', 'Sekolah Menengah Atas (SMA)', 'sma_32.png', 'sma_64.png', '0', '2019-05-10 09:34:15', ''),
('PDST1009', 'Pondok Pesantren', 'pondok_32.png', 'pondok_64.png', '0', '2019-06-18 03:59:50', 'AD2019020001'),
('PDST1011', 'Perguruan Tinggi', 'kampus_32.png', 'kampus_64.png', '0', '2019-06-19 05:56:47', 'AD2019040001'),
('PDST1012', 'Perpustakaan', 'perpus_32.png', 'perpus_64.png', '0', '2019-06-19 08:06:45', 'AD2019040001');

-- --------------------------------------------------------

--
-- Table structure for table `table1`
--

CREATE TABLE `table1` (
  `id` varchar(7) NOT NULL DEFAULT '0',
  `name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table1`
--

INSERT INTO `table1` (`id`, `name`) VALUES
('LHPL001', 'joni'),
('LHPL002', 'joni'),
('LHPL003', 'joni'),
('LHPL004', 'joni'),
('LHPL005', 'joni'),
('LHPL006', 'joni'),
('LHPL007', 'joni'),
('LHPL008', 'joni'),
('LHPL009', 'joni'),
('LHPL010', 'joni'),
('LHPL011', 'joni'),
('LHPL012', 'joni'),
('LHPL013', 'joni'),
('LHPL014', 'joni'),
('LHPL015', 'joni'),
('LHPL016', 'joni'),
('LHPL017', 'joni'),
('LHPL018', 'joni'),
('LHPL019', 'joni'),
('LHPL020', 'joni'),
('LHPL021', 'joni'),
('LHPL022', 'joni'),
('LHPL023', 'joni'),
('LHPL024', 'joni'),
('LHPL025', 'joni'),
('LHPL026', 'joni'),
('LHPL027', 'joni'),
('LHPL028', 'joni'),
('LHPL029', 'joni'),
('LHPL030', 'joni'),
('LHPL031', 'joni'),
('LHPL032', 'joni'),
('LHPL033', 'joni'),
('LHPL034', 'joni'),
('LHPL035', 'joni'),
('LHPL036', 'joni'),
('LHPL037', 'joni'),
('LHPL038', 'joni'),
('LHPL039', 'joni'),
('LHPL040', 'joni'),
('LHPL041', 'joni'),
('LHPL042', 'joni'),
('LHPL043', 'joni'),
('LHPL044', 'joni'),
('LHPL045', 'joni'),
('LHPL046', 'joni'),
('LHPL047', 'joni'),
('LHPL048', 'joni'),
('LHPL049', 'joni'),
('LHPL050', 'joni'),
('LHPL051', 'joni'),
('LHPL052', 'joni'),
('LHPL053', 'joni'),
('LHPL054', 'joni'),
('LHPL055', 'joni'),
('LHPL056', 'joni'),
('LHPL057', 'joni'),
('LHPL058', 'joni'),
('LHPL059', 'joni'),
('LHPL060', 'joni'),
('LHPL061', 'joni'),
('LHPL062', 'joni'),
('LHPL063', 'joni'),
('LHPL064', 'joni'),
('LHPL065', 'joni'),
('LHPL066', 'joni'),
('LHPL067', 'joni'),
('LHPL068', 'joni'),
('LHPL069', 'joni'),
('LHPL070', 'joni'),
('LHPL071', 'joni'),
('LHPL072', 'joni'),
('LHPL073', 'joni'),
('LHPL074', 'joni'),
('LHPL075', 'joni'),
('LHPL076', 'joni'),
('LHPL077', 'joni'),
('LHPL078', 'joni'),
('LHPL079', 'joni'),
('LHPL080', 'joni'),
('LHPL081', 'joni'),
('LHPL082', 'joni'),
('LHPL083', 'joni'),
('LHPL084', 'joni'),
('LHPL085', 'joni'),
('LHPL086', 'joni'),
('LHPL087', 'joni'),
('LHPL088', 'joni'),
('LHPL089', 'joni'),
('LHPL090', 'joni'),
('LHPL091', 'joni'),
('LHPL092', 'joni'),
('LHPL093', 'joni'),
('LHPL094', 'joni'),
('LHPL095', 'joni'),
('LHPL096', 'joni'),
('LHPL097', 'joni'),
('LHPL098', 'joni'),
('LHPL099', 'joni'),
('LHPL100', 'joni'),
('LHPL101', 'joni'),
('LHPL102', 'joni'),
('LHPL103', 'joni'),
('LHPL104', 'joni'),
('LHPL105', 'joni'),
('LHPL106', 'joni'),
('LHPL107', 'joni'),
('LHPL108', 'joni'),
('LHPL109', 'joni'),
('LHPL110', 'joni'),
('LHPL111', 'joni'),
('LHPL112', 'joni'),
('LHPL113', 'joni'),
('LHPL114', 'joni'),
('LHPL115', 'joni'),
('LHPL116', 'joni'),
('LHPL117', 'joni'),
('LHPL118', 'joni'),
('LHPL119', 'joni'),
('LHPL120', 'joni'),
('LHPL121', 'joni'),
('LHPL122', 'joni'),
('LHPL123', 'joni'),
('LHPL124', 'joni'),
('LHPL125', 'joni'),
('LHPL126', 'joni'),
('LHPL127', 'joni'),
('LHPL128', 'joni'),
('LHPL129', 'joni'),
('LHPL130', 'joni'),
('LHPL131', 'joni'),
('LHPL132', 'joni'),
('LHPL133', 'joni'),
('LHPL134', 'joni'),
('LHPL135', 'joni'),
('LHPL136', 'joni'),
('LHPL137', 'joni'),
('LHPL138', 'joni'),
('LHPL139', 'joni'),
('LHPL140', 'joni'),
('LHPL141', 'joni'),
('LHPL142', 'joni'),
('LHPL143', 'joni'),
('LHPL144', 'joni'),
('LHPL145', 'joni'),
('LHPL146', 'joni'),
('LHPL147', 'joni'),
('LHPL148', 'joni'),
('LHPL149', 'joni'),
('LHPL150', 'joni'),
('LHPL151', 'joni'),
('LHPL152', 'joni'),
('LHPL153', 'joni'),
('LHPL154', 'joni'),
('LHPL155', 'joni'),
('LHPL156', 'joni'),
('LHPL157', 'joni'),
('LHPL158', 'joni'),
('LHPL159', 'joni'),
('LHPL160', 'joni'),
('LHPL161', 'joni'),
('LHPL162', 'joni'),
('LHPL163', 'joni'),
('LHPL164', 'joni'),
('LHPL165', 'joni'),
('LHPL166', 'joni'),
('LHPL167', 'joni'),
('LHPL168', 'joni'),
('LHPL169', 'joni'),
('LHPL170', 'joni'),
('LHPL171', 'joni'),
('LHPL172', 'joni'),
('LHPL173', 'joni'),
('LHPL174', 'joni'),
('LHPL175', 'joni'),
('LHPL176', 'joni'),
('LHPL177', 'joni'),
('LHPL178', 'joni'),
('LHPL179', 'joni'),
('LHPL180', 'joni'),
('LHPL181', 'joni'),
('LHPL182', 'joni'),
('LHPL183', 'joni'),
('LHPL184', 'joni'),
('LHPL185', 'joni'),
('LHPL186', 'joni'),
('LHPL187', 'joni'),
('LHPL188', 'joni'),
('LHPL189', 'joni'),
('LHPL190', 'joni'),
('LHPL191', 'joni'),
('LHPL192', 'joni'),
('LHPL193', 'joni'),
('LHPL194', 'joni'),
('LHPL195', 'joni'),
('LHPL196', 'joni'),
('LHPL197', 'joni'),
('LHPL198', 'joni'),
('LHPL199', 'joni'),
('LHPL200', 'joni'),
('LHPL201', 'joni'),
('LHPL202', 'joni'),
('LHPL203', 'joni'),
('LHPL204', 'joni'),
('LHPL205', 'joni'),
('LHPL206', 'joni'),
('LHPL207', 'joni'),
('LHPL208', 'joni'),
('LHPL209', 'joni'),
('LHPL210', 'joni'),
('LHPL211', 'joni'),
('LHPL212', 'joni'),
('LHPL213', 'joni'),
('LHPL214', 'joni'),
('LHPL215', 'joni'),
('LHPL216', 'joni'),
('LHPL217', 'joni'),
('LHPL218', 'joni'),
('LHPL219', 'joni'),
('LHPL220', 'joni'),
('LHPL221', 'joni'),
('LHPL222', 'joni'),
('LHPL223', 'joni'),
('LHPL224', 'joni'),
('LHPL225', 'joni'),
('LHPL226', 'joni'),
('LHPL227', 'joni'),
('LHPL228', 'joni'),
('LHPL229', 'joni'),
('LHPL230', 'joni'),
('LHPL231', 'joni'),
('LHPL232', 'joni'),
('LHPL233', 'joni'),
('LHPL234', 'joni'),
('LHPL235', 'joni'),
('LHPL236', 'joni'),
('LHPL237', 'joni'),
('LHPL238', 'joni'),
('LHPL239', 'joni'),
('LHPL240', 'joni'),
('LHPL241', 'joni'),
('LHPL242', 'joni'),
('LHPL243', 'joni'),
('LHPL244', 'joni'),
('LHPL245', 'joni'),
('LHPL246', 'joni'),
('LHPL247', 'joni'),
('LHPL248', 'joni'),
('LHPL249', 'joni'),
('LHPL250', 'joni'),
('LHPL251', 'joni'),
('LHPL252', 'joni'),
('LHPL253', 'joni'),
('LHPL254', 'joni'),
('LHPL255', 'joni'),
('LHPL256', 'joni'),
('LHPL257', 'joni'),
('LHPL258', 'joni'),
('LHPL259', 'joni'),
('LHPL260', 'joni'),
('LHPL261', 'joni'),
('LHPL262', 'joni'),
('LHPL263', 'joni'),
('LHPL264', 'joni'),
('LHPL265', 'joni'),
('LHPL266', 'joni'),
('LHPL267', 'joni'),
('LHPL268', 'joni'),
('LHPL269', 'joni'),
('LHPL270', 'joni'),
('LHPL271', 'joni'),
('LHPL272', 'joni'),
('LHPL273', 'joni'),
('LHPL274', 'joni'),
('LHPL275', 'joni'),
('LHPL276', 'joni'),
('LHPL277', 'joni'),
('LHPL278', 'joni'),
('LHPL279', 'joni'),
('LHPL280', 'joni'),
('LHPL281', 'joni'),
('LHPL282', 'joni'),
('LHPL283', 'joni'),
('LHPL284', 'joni'),
('LHPL285', 'joni'),
('LHPL286', 'joni'),
('LHPL287', 'joni'),
('LHPL288', 'joni'),
('LHPL289', 'joni'),
('LHPL290', 'joni'),
('LHPL291', 'joni'),
('LHPL292', 'joni'),
('LHPL293', 'joni'),
('LHPL294', 'joni'),
('LHPL295', 'joni'),
('LHPL296', 'joni'),
('LHPL297', 'joni'),
('LHPL298', 'joni'),
('LHPL299', 'joni'),
('LHPL300', 'joni'),
('LHPL301', 'joni'),
('LHPL302', 'joni'),
('LHPL303', 'joni'),
('LHPL304', 'joni'),
('LHPL305', 'joni'),
('LHPL306', 'joni'),
('LHPL307', 'joni'),
('LHPL308', 'joni'),
('LHPL309', 'joni'),
('LHPL310', 'joni'),
('LHPL311', 'joni'),
('LHPL312', 'joni'),
('LHPL313', 'joni'),
('LHPL314', 'joni'),
('LHPL315', 'joni'),
('LHPL316', 'joni'),
('LHPL317', 'joni'),
('LHPL318', 'joni'),
('LHPL319', 'joni'),
('LHPL320', 'joni'),
('LHPL321', 'joni'),
('LHPL322', 'joni'),
('LHPL323', 'joni'),
('LHPL324', 'joni'),
('LHPL325', 'joni'),
('LHPL326', 'joni'),
('LHPL327', 'joni'),
('LHPL328', 'joni'),
('LHPL329', 'joni'),
('LHPL330', 'joni'),
('LHPL331', 'joni'),
('LHPL332', 'joni'),
('LHPL333', 'joni'),
('LHPL334', 'joni'),
('LHPL335', 'joni'),
('LHPL336', 'joni'),
('LHPL337', 'joni'),
('LHPL338', 'joni'),
('LHPL339', 'joni'),
('LHPL340', 'joni'),
('LHPL341', 'joni'),
('LHPL342', 'joni'),
('LHPL343', 'joni'),
('LHPL344', 'joni'),
('LHPL345', 'joni'),
('LHPL346', 'joni'),
('LHPL347', 'joni'),
('LHPL348', 'joni'),
('LHPL349', 'joni'),
('LHPL350', 'joni'),
('LHPL351', 'joni'),
('LHPL352', 'joni'),
('LHPL353', 'joni'),
('LHPL354', 'joni'),
('LHPL355', 'joni'),
('LHPL356', 'joni'),
('LHPL357', 'joni'),
('LHPL358', 'joni'),
('LHPL359', 'joni'),
('LHPL360', 'joni'),
('LHPL361', 'joni'),
('LHPL362', 'joni'),
('LHPL363', 'joni'),
('LHPL364', 'joni'),
('LHPL365', 'joni'),
('LHPL366', 'joni'),
('LHPL367', 'joni'),
('LHPL368', 'joni'),
('LHPL369', 'joni'),
('LHPL370', 'joni'),
('LHPL371', 'joni'),
('LHPL372', 'joni'),
('LHPL373', 'joni'),
('LHPL374', 'joni'),
('LHPL375', 'joni'),
('LHPL376', 'joni'),
('LHPL377', 'joni'),
('LHPL378', 'joni'),
('LHPL379', 'joni'),
('LHPL380', 'joni'),
('LHPL381', 'joni'),
('LHPL382', 'joni'),
('LHPL383', 'joni'),
('LHPL384', 'joni'),
('LHPL385', 'joni'),
('LHPL386', 'joni'),
('LHPL387', 'joni'),
('LHPL388', 'joni'),
('LHPL389', 'joni'),
('LHPL390', 'joni'),
('LHPL391', 'joni'),
('LHPL392', 'joni'),
('LHPL393', 'joni'),
('LHPL394', 'joni'),
('LHPL395', 'joni'),
('LHPL396', 'joni'),
('LHPL397', 'joni'),
('LHPL398', 'joni'),
('LHPL399', 'joni'),
('LHPL400', 'joni'),
('LHPL401', 'joni'),
('LHPL402', 'joni'),
('LHPL403', 'joni'),
('LHPL404', 'joni'),
('LHPL405', 'joni'),
('LHPL406', 'joni');

--
-- Triggers `table1`
--
DELIMITER $$
CREATE TRIGGER `tg_table1_insert` BEFORE INSERT ON `table1` FOR EACH ROW BEGIN
  declare last_key_item varchar(20);
  declare count_row_item int;
  declare fix_key_item varchar(12);
  
  select count(*) into count_row_item from table1;
  
  select id into last_key_item from table1 order by id desc limit 1;
  
  
  if(count_row_item <1) then
  	set fix_key_item = concat("LHPL","001");
  else
    set fix_key_item = concat('LHPL', LPAD(RIGHT(last_key_item, 3)+1, 3, '0'));
      
  END IF;
  
  SET NEW.id = fix_key_item;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `table1_seq`
--

CREATE TABLE `table1_seq` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table1_seq`
--

INSERT INTO `table1_seq` (`id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8),
(9),
(10),
(11),
(12),
(13),
(14),
(15),
(16),
(17),
(18),
(19),
(20),
(21),
(22),
(23),
(24),
(25),
(26),
(27),
(28),
(29),
(30),
(31),
(32),
(33),
(34),
(35),
(36),
(37),
(38),
(39),
(40),
(41),
(42),
(43),
(44),
(45),
(46),
(47),
(48),
(49),
(50),
(51),
(52),
(53),
(54),
(55),
(56),
(57),
(58),
(59),
(60),
(61),
(62),
(63),
(64),
(65),
(66),
(67),
(68),
(69),
(70),
(71),
(72),
(73),
(74),
(75),
(76),
(77),
(78),
(79),
(80),
(81),
(82),
(83),
(84),
(85),
(86),
(87),
(88),
(89),
(90),
(91),
(92),
(93),
(94),
(95),
(96),
(97),
(98),
(99),
(100),
(101),
(102),
(103),
(104),
(105),
(106),
(107),
(108),
(109),
(110),
(111),
(112),
(113),
(114),
(115),
(116),
(117),
(118),
(119),
(120),
(121),
(122),
(123),
(124),
(125),
(126),
(127),
(128),
(129),
(130),
(131),
(132),
(133),
(134),
(135),
(136),
(137),
(138),
(139),
(140),
(141),
(142),
(143),
(144),
(145),
(146),
(147),
(148),
(149),
(150),
(151),
(152);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `admin_lv`
--
ALTER TABLE `admin_lv`
  ADD PRIMARY KEY (`id_lv`);

--
-- Indexes for table `device`
--
ALTER TABLE `device`
  ADD PRIMARY KEY (`id_device_kios`);

--
-- Indexes for table `dinas`
--
ALTER TABLE `dinas`
  ADD PRIMARY KEY (`id_dinas`);

--
-- Indexes for table `faskes_jenis`
--
ALTER TABLE `faskes_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `faskes_main`
--
ALTER TABLE `faskes_main`
  ADD PRIMARY KEY (`id_faskes`);

--
-- Indexes for table `home_page_kategori`
--
ALTER TABLE `home_page_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `home_page_main`
--
ALTER TABLE `home_page_main`
  ADD PRIMARY KEY (`id_page`);

--
-- Indexes for table `ijin_antrian`
--
ALTER TABLE `ijin_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `ijin_jenis`
--
ALTER TABLE `ijin_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `ijin_kategori`
--
ALTER TABLE `ijin_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `ijin_sub_kategori`
--
ALTER TABLE `ijin_sub_kategori`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `kependudukan_antrian`
--
ALTER TABLE `kependudukan_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `kependudukan_jenis`
--
ALTER TABLE `kependudukan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `kependudukan_kategori`
--
ALTER TABLE `kependudukan_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `kesehatan_antrian`
--
ALTER TABLE `kesehatan_antrian`
  ADD PRIMARY KEY (`id_antrian`);

--
-- Indexes for table `kesehatan_jenis`
--
ALTER TABLE `kesehatan_jenis`
  ADD PRIMARY KEY (`id_layanan`);

--
-- Indexes for table `kesehatan_poli`
--
ALTER TABLE `kesehatan_poli`
  ADD PRIMARY KEY (`id_poli`);

--
-- Indexes for table `kesehatan_rs`
--
ALTER TABLE `kesehatan_rs`
  ADD PRIMARY KEY (`id_rs`);

--
-- Indexes for table `master_kecamatan`
--
ALTER TABLE `master_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `master_kelurahan`
--
ALTER TABLE `master_kelurahan`
  ADD PRIMARY KEY (`id_kelurahan`);

--
-- Indexes for table `pariwisata_jenis`
--
ALTER TABLE `pariwisata_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pariwisata_main`
--
ALTER TABLE `pariwisata_main`
  ADD PRIMARY KEY (`id_pariwisata`);

--
-- Indexes for table `pariwisata_sub_jenis`
--
ALTER TABLE `pariwisata_sub_jenis`
  ADD PRIMARY KEY (`id_sub`);

--
-- Indexes for table `pendidikan_jenis`
--
ALTER TABLE `pendidikan_jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `pendidikan_sekolah`
--
ALTER TABLE `pendidikan_sekolah`
  ADD PRIMARY KEY (`id_sekolah`);

--
-- Indexes for table `pendidikan_strata`
--
ALTER TABLE `pendidikan_strata`
  ADD PRIMARY KEY (`id_strata`);

--
-- Indexes for table `table1`
--
ALTER TABLE `table1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table1_seq`
--
ALTER TABLE `table1_seq`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_lv`
--
ALTER TABLE `admin_lv`
  MODIFY `id_lv` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `home_page_kategori`
--
ALTER TABLE `home_page_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table1_seq`
--
ALTER TABLE `table1_seq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
